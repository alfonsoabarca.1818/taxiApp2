import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'src/pages/login/select_user.dart';
import 'src/provider/auditor_providers/auditor_provider.dart';
import 'src/provider/duenio_vehiculo_providers/home_provider.dart';
import 'src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'src/provider/provi.dart';
import 'src/provider/provider_preference.dart';

void main() async {

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => HomeProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => BorrarProvider(),
        ),

        ChangeNotifierProvider(
          create: (context) => AuditorProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => LoginProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProviderPreference(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (BuildContext, Orientation, ScreenType) {
        return MaterialApp(
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalMaterialLocalizations.delegate
          ],
          supportedLocales: const [
            Locale('es'),
          ],
          title: 'Administrador TaxiSeguro',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.amber.shade700),
            useMaterial3: true,
          ),
          home: const SelectUser(),
        );
      },
    );
  }
}
