import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();

getAllConductoresPartner(partner) async {
  print("escritura");
  try {
    Response response =
        await dio.post("$url/getAllDataConductorPartner/$partner");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getAllDuenioPartner(partner) async {
  print("escritura");
  try {
    Response response = await dio.post("$url/getAllDataDuenioPartner/$partner");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getAllVehiclePartner(partner) async {
  print("escritura");
  try {
    Response response =
        await dio.post("$url/getAllDataVehiclePartner/$partner");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getOneAuditor(dni) async {
  print("escritura");
  try {
    Response response = await dio.post("$url/getOneDatasAuditor/$dni");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error: $e");
  }
}
