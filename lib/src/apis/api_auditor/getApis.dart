import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();
getAllAuditor() async {
  print("escritura");
  Response response = await dio.get("$url/getAllDatasAuditor");

  return response.data;
}

getAllDatasConductores() async {
  print("escritura");
  try {
    Response response = await dio.get("$url/getAllDatasConductores");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getAllDatasDuenio() async {
  print("escritura");
  try {
    Response response = await dio.get("$url/getAllDatasDuenio");


    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getAllDatasVehicle() async {
  print("escritura");
  try {
    Response response = await dio.get("$url/getAllDatasVehicle");

    return response.data;
  } catch (e) {
    print("Ha ocurrido un error al consumir la api de tipo $e");
  }
}

getDniDuenio(patente) async {

  Response response = await dio.post("$url/getDniDuenioVehicle/$patente");
  var data = response.data;
  return data;
}
