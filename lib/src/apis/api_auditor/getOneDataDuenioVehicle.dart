import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();
getOneDatasDuenioVehicle(dni, pass) async {
  print("escritura");
  Response resp = await dio.post("$url/loginOneDuenioTaxi/$dni/$pass");
  var datas = resp.data;

  return datas;
}

getOneDatasVehicle(patente) async {
  print("escritura");
  Response resp = await dio.post("$url/loginOneTaxi/$patente");
  var datas = resp.data;

  return datas;
}

getOneDatasConductor(dni) async {
  print("escritura");

  Response resp = await dio.post("$url/loginOneConductor/$dni");
  var datas = resp.data;

  return datas;
}
