import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();

postLoginAuditor(rut, password) async {

  try {
    Response resp = await dio.post("$url/loginAuditor/$rut/$password");
    var datas = resp.data;

    return datas["status"];
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

postLoginConductor(rut, password) async {
  try {
    Response resp = await dio.post("$url/loginConductor/$rut/$password");
    var datas = resp.data;

    return datas["estado_conductor"];
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}
