import 'package:dio/dio.dart';
import '../../../tools/url_base.dart';

Dio dio = Dio();

updateAprobacionDuenio(aprobacion, dni, motivo) async {



  try {
    Response resp =
        await dio.put("$url/aprobacionDuenioVehiculo/$aprobacion/$dni/$motivo");
    var datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error: $e");
  }
}
