import 'package:dio/dio.dart';
import '../../../tools/url_base.dart';

Dio dio = Dio();

updateAprobacionConductor(aprobacion, dni, motivo) async {
  print("el motivo antes de enviar es: $motivo");
  try {
    Response resp = await dio.put(
        "$url/updateAprobacionConductores/$dni/$aprobacion/$motivo");
    var datas = resp.data;

    print("el data es: $datas");

    return datas;
  } catch (e) {
    print("Ha ocurrido un error de tipo: $e");
  }
}

updateDniVehicle(dni, patente) async {
  print("el motivo antes de enviar es: $dni");
  try {
    Response resp = await dio.put(
        "$url/updateRutVehicle/$dni/$patente");
    var datas = resp.data;

    print("el data es: $datas");

    return datas;
  } catch (e) {
    print("Ha ocurrido un error de tipo: $e");
  }
}

updatePartnerConductor(String rut, String partner, String aux) async {
  print("el partner antes de enviar es: $partner");
  try {
    Response resp = await dio
        .put("$url/updatePartner/$rut/$partner/$aux");
    var datas = resp.data;

    print("el data es: $datas");

    return datas;
  } catch (e) {
    print("Ha ocurrido un error de tipo: $e");
  }
}

updateFechaInicioConductor(String rut, String fecha) async {
  print("el partner antes de enviar es: $fecha");
  try {
    Response resp = await dio.put(
        "$url/updateFechaInicioConductor/$rut/$fecha");
    var datas = resp.data;

    print("el data es: $datas");

    return datas;
  } catch (e) {
    print("Ha ocurrido un error de tipo: $e");
  }
}

updateStatusConductor(status, dni) async {
  Response resp = await dio.put("$url/updateStatusConductor/$dni/$status");
  var datas = resp.data;

  return datas;
}
