import 'package:dio/dio.dart';
import '../../../tools/url_base.dart';

Dio dio = Dio();

updateAprobacionVehiculo(aprobacion, patente, motivo) async {


  try {
    Response resp = await dio
        .put("$url/updateAprobacionVehicle/$patente/$aprobacion/$motivo");
    var datas = resp.data;

    return datas;
  } catch (e) {
    print("Ha ocurrido un erro de tipo: $e");
  }
}
