import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();

updateEstadoAuditor(estado, dni) async {
  try {

    Response resp = await dio.put("$url/updateEstadoAuditor/$dni/$estado");
    var datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo: $e");
  }
}

SelectEstadoAuditor(dni) async {

  Response resp = await dio.post("$url/selectEstadoAuditor/$dni");
  var datas = resp.data;

  return datas;
}

updateEstadoConductor(estado, dni) async {

  Response resp = await dio.put("$url/updateEstadoConductor/$dni/$estado");
  var datas = resp.data;

  return datas;
}

selectEstadoConductor(dni) async {

  Response resp = await dio.post("$url/selectEstadoConductor/$dni");
  var datas = resp.data;

  return datas;
}
