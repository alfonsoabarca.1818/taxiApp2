import 'package:dio/dio.dart';

import '../../../tools/url_base.dart';

Dio dio = Dio();

getDatasDuenioVehiculo() async {

  try {
    Response resp = await dio.get("$url/getAllAprobacionDuenioVehiculo");

    var datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}
