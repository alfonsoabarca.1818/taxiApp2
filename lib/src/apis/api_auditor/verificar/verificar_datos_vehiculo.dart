import 'package:dio/dio.dart';

import '../../../tools/url_base.dart';

Dio dio = Dio();

getDatasAprobacionVehiculo() async {
  try {
    Response resp = await dio.get("$url/getAllAprobacionVehiculo");

    var datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

getDatasAprobacionConductor() async {
  try {
    List excepto = [];
    Response resp = await dio.get("$url/getAllAprobacionConductor");

    var datas = resp.data;

    return datas == null ? excepto : datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

getDatasStatusConductor() async {
  try {
    List excepto = [];
    Response resp = await dio.get("$url/getAllStatusConductor");

    var datas = resp.data;

    return datas == null ? excepto : datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}
