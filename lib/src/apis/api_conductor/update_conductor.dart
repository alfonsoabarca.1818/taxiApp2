import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

updateConductor(dni, data) async {
  Dio dio = Dio();

  print("escritura");

  try {

    Response resp = await dio.put("$url/updateConductor/$dni", data: data);
    Map datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}
