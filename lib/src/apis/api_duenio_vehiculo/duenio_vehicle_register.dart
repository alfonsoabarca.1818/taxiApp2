import 'package:dio/dio.dart';

import '../../tools/url_base.dart';
import '../api_auditor/getApis.dart';

saveDuenioVehicle(datas) async {
  print("escritura");

  try {
    Response resp = await dio.post("$url/saveDatasDuenio", data: datas);
    Map datass = resp.data;

    return datass;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

saveConductor(datas) async {
  print("escritura");
  try {
    Response resp = await dio.post("$url/saveDatasConductores", data: datas);
    Map datass = resp.data;

    return datass;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}
