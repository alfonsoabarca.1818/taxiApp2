import 'dart:convert';

import 'package:dio/dio.dart';

import '../../tools/url_base.dart';
import '../api_auditor/getApis.dart';

getAllPatentes(rut) async {
  try {
    Response resp =
        await dio.post("$url/getAllPatentes/$rut");
    var datas = resp.data;

    return jsonDecode(datas.toString());
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

getAllPatentesAll()async{
  
   try {
    Response resp =
        await dio.get("$url/getAllPatentesAll/");
    var datas = resp.data;


    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

getAllRutConductores(patente) async {
  try {

    Response resp = await dio
        .post("$url/getAllRutConductores/$patente");
    var datas = resp.data;

    return jsonDecode(datas.toString());
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

deletePatentes(data, dni) async {
  try {

    Response resp = await dio.put("$url/updatePatentesVehiculo/$dni",
        data: data);
    var datas = resp.data;



    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}


addRutConductores(data, patente) async {
  try {
    Response resp = await dio.put("$url/updateConductoresVehiculo/$patente",
        data: data);
    var datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}