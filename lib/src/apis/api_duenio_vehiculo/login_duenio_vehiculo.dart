import 'package:dio/dio.dart';

import '../../tools/url_base.dart';

Dio dio = Dio();

postLoginDuenioVehiculo(rut, password)async{
  try{

    Response resp = await dio.post("$url/loginOneDuenioTaxi/$rut/$password");
    var datas = resp.data;

    return datas["status"];
  }catch(e){
    print("ha ocurrido un error de tipo : $e");
  }
}

patentesDuenio(rut, pass)async{
  try{
    Response resp = await dio.post("$url/loginOneDuenioTaxi/$rut/$pass");
    var datas = resp.data;

    return datas;
  }catch(e){
    print("ha ocurrido un error de tipo : $e");
  }
}

getPatentesDuenio(rut)async{
  try{
    Response resp = await dio.post("$url/getAllPatentes/$rut");
    var datas = resp.data;

    return datas;
  }catch(e){
    print("ha ocurrido un error de tipo : $e");
  }
}