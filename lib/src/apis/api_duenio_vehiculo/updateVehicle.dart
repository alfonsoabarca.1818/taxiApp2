import 'package:dio/dio.dart';

import '../../tools/url_base.dart';
import '../api_auditor/getApis.dart';

updateVehicle(patente, data) async {
  print("escritura");
  try {

    Response resp = await dio.put(
        "$url/updateVehicle/$patente",
        data: data);
    Map datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo 1 : $e");
  }
}

saveVehicle(data) async {
  try {
    print("escritura");
    Response resp = await dio.post("$url/saveDatasVehicle",
        data: data);
    Map datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo 2 : $e");
  }
}

updateOnePatente(dni, data) async {
  print("escritura");
  try {

    Response resp = await dio.put("$url/updatePatenteDuenio/$dni",
        data: data);
    Map datas = resp.data;

    return datas;
  } catch (e) {
    print("ha ocurrido un error de tipo : $e");
  }
}

updateDuenioVehicle(dni, data)async{
  print("escritura");
  try {

    Response resp = await dio.put("$url/updateDuenio/$dni",data: data);
    var datareturn = resp.data;

    return datareturn;    
  } catch (e) {

    print("Ha ocurrido un error al actualizar dueño de tipo $e");
    
  }
}

updateEstadoDuenio(dni, estado) async{

  try {
    Response resp = await dio.put("$url/updateEstadoDuenio/$dni/$estado");
    var estadostrue = resp.data;

    return estadostrue;
  } catch (e) {
    print("Ha ocurrido un error al actualizar el estado de tipo: $e");
  }
}

getEstadoDuenio(dni)async{
  print("escritura");
  try {
    Response resp = await dio.get("$url/getEstadoDuenio/$dni");
    var elestadoes = resp.data;

    return elestadoes;
  } catch (e) {
    print("Ha ocurrido un error al obtener el estado de tipo: $e");
  }
}
