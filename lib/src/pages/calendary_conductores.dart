import 'package:flutter/material.dart';

getCalendaryConductores(
    context,
    controller,

    fecha,

    homeProvider, iboolean,) async {
  DateTime? pickedDate = await showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate:
    DateTime(1930),
    lastDate: DateTime(2100),
    currentDate: DateTime.now(),
    helpText: 'Selecciona una fecha',
    cancelText: 'Cancelar',
    locale: const Locale('es', 'ES'),
    keyboardType: TextInputType.text,
    errorInvalidText: 'Texto invalido',
    errorFormatText: 'Formato invalido',
  );

  controller.text = pickedDate.toString().substring(0, 10);

  iboolean = true;

}
