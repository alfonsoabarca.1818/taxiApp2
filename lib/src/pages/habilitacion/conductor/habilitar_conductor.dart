import 'package:admin_taxi_seguro/src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/loading.dart';
import 'mostrar_datos_conductor.dart';

class HabilitarConductorVehiculo extends StatefulWidget {
  const HabilitarConductorVehiculo({super.key});

  @override
  State<HabilitarConductorVehiculo> createState() =>
      _HabilitarConductorVehiculoState();
}

class _HabilitarConductorVehiculoState
    extends State<HabilitarConductorVehiculo> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Dio dio = Dio();

  bool loadintrue = false;

  @override
  Widget build(BuildContext context) {
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var loginProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
      key: _scaffoldKey,
      body: loadintrue == true ? circulatorCustom() : Container(
        width: 100.w,
        height: 100.h,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 17.h,
              decoration: BoxDecoration(
                 color: Colors.amber.shade700,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  "Habilita o Desabilitar\n       Conductores",
                  style: TextStyle(
                      color: Colors.deepPurple,
                      fontSize: 19.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: ListView.builder(
                  itemCount: verificaDuenioProvider
                      .getDatasVerificarConductor()
                      .length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () async {
                        setState(() {
                          loadintrue = true;
                        });
                        verificaDuenioProvider.setindex(index);
                        verificaDuenioProvider.setDatasVerificarOneConductor(
                            await getOneDatasConductor(
                                "${verificaDuenioProvider.getDatasVerificarConductor()[index]["dni"]}"));


                        setState(() {
                          loadintrue = false;
                        });

                        Navigator.of(_scaffoldKey.currentContext!).push( MaterialPageRoute(
                          builder: (context) =>
                          const MostrarDatosConductor(),
                        ),);


                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration( 
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                           color: Colors.amber.shade700,),
                          child: ListTile(
                            title: Text(
                              "${verificaDuenioProvider.getDatasVerificarConductor()[index]["dni"]}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepPurple),
                            ),
                            leading: Text(
                              "${index + 1}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepPurple),
                            ),
                            trailing: Text(
                              "${verificaDuenioProvider.getDatasVerificarConductor()[index]["name"]}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepPurple),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
