import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../apis/api_auditor/update_aprobacion/update_aprobacion_conductor.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../tools/loading.dart';
import '../../../tools/separador.dart';

class MostrarDatosConductor extends StatefulWidget {
  const MostrarDatosConductor({super.key});

  @override
  State<MostrarDatosConductor> createState() => _MostrarDatosConductorState();
}

class _MostrarDatosConductorState extends State<MostrarDatosConductor> {
  String _singleValue = "Text alignment right";

  TextEditingController controllerStatus = TextEditingController();
  TextEditingController controllerMotivo = TextEditingController();
  String? selectedValue;
  final List<String> items = ["habilitado", "deshabilitado"];

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<AuditorProvider>(context);
    var loginProvider = Provider.of<LoginProvider>(context);
    var name = datas.getDatasVerificarOneConductor()["name"];
    var lastName = datas.getDatasVerificarOneConductor()["lastName"];

    var dni = datas.getDatasVerificarOneConductor()["dni"];

    var fotoCarnetA = datas.getDatasVerificarOneConductor()["fotoCarnetA"];

    var fotoCarnetB = datas.getDatasVerificarOneConductor()["fotoCarnetB"];

    var email = datas.getDatasVerificarOneConductor()["email"];
    var password = datas.getDatasVerificarOneConductor()["password"];
    var phone = datas.getDatasVerificarOneConductor()["phone"];
    var fotoPerfil = datas.getDatasVerificarOneConductor()["fotoPerfil"];
    var tipoLicencia = datas.getDatasVerificarOneConductor()["tipoLicencia"];
    var restriccion = datas.getDatasVerificarOneConductor()["restriccion"];
    var fechaVencimientoLicencia =
        datas.getDatasVerificarOneConductor()["fechaVencimientoLicencia"];
    var fotoLicenciaConducirA =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirA"];

    var fotoLicenciaConducirB =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirB"];

    var aprobacion = datas.getDatasVerificarOneConductor()["aprobacion"];
    var certificadoantecedentes =
        datas.getDatasVerificarOneConductor()["certificadoAntecedentes"];

    var status = datas.getDatasVerificarOneConductor()["status"];

    var _site = {"aprobacion", "desaprobado", "revision"};
    return Scaffold(
      body: SizedBox(
        width: 100.w,
        height: 100.h,
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 17.h,
              decoration: BoxDecoration(
                color: Colors.amber.shade700,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  "Habilitación conductores",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp,
                      color: Colors.deepPurple),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: ListView(
                  children: [
                    showNames(name),
                    separador(),
                    showLastNames(lastName),
                    separador(),
                    fotoCarnetA == null
                        ? showFotoCarnet("", "")
                        : Text(
                            "Foto carnet lado A",
                            style: TextStyle(fontSize: 18.sp),
                          ),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoCarnetA",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    Text(
                      "Foto carnet lado B",
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoCarnetB",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    showEmail(email),
                    separador(),
                    showPhone(phone),
                    separador(),
                    showPhotoPerfil(fotoPerfil),
                    separador(),
                    showTipoLicencia(tipoLicencia),
                    separador(),
                    showRestriccion(restriccion),
                    separador(),
                    showFechaVencimientoLicencia(fechaVencimientoLicencia),
                    separador(),
                    fotoCarnetB == null
                        ? showFotoLicenciaConducir("", "")
                        : Text(
                            "Foto licencia lado A",
                            style: TextStyle(fontSize: 18.sp),
                          ),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoLicenciaConducirA",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    Text(
                      "Foto licencia lado B",
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoLicenciaConducirB",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    const Text("Certificado antecedentes"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$certificadoantecedentes",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    showAprobacion(aprobacion),
                    separador(),
                    DropdownButtonHideUnderline(
                      child: DropdownButton2<String>(
                        isExpanded: true,
                        hint: const Row(
                          children: [
                            Icon(
                              Icons.list,
                              size: 16,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Expanded(
                              child: Text(
                                'Selecciona un estado',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                        items: items
                            .map((String item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(
                                    item,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ))
                            .toList(),
                        value: selectedValue,
                        onChanged: (String? value) {
                          setState(() {
                            selectedValue = value;
                          });
                        },
                        buttonStyleData: ButtonStyleData(
                          height: 50,
                          width: 160,
                          padding: const EdgeInsets.only(left: 14, right: 14),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            border: Border.all(
                              color: Colors.black26,
                            ),
                            color: Colors.deepPurple,
                          ),
                          elevation: 2,
                        ),
                        iconStyleData: const IconStyleData(
                          icon: Icon(
                            Icons.arrow_forward_ios_outlined,
                          ),
                          iconSize: 14,
                          iconEnabledColor: Colors.white,
                          iconDisabledColor: Colors.grey,
                        ),
                        dropdownStyleData: DropdownStyleData(
                          maxHeight: 200,
                          width: 200,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            color: Colors.deepPurple,
                          ),
                          offset: const Offset(-20, 0),
                          scrollbarTheme: ScrollbarThemeData(
                            radius: const Radius.circular(40),
                            thickness: MaterialStateProperty.all<double>(6),
                            thumbVisibility:
                                MaterialStateProperty.all<bool>(true),
                          ),
                        ),
                        menuItemStyleData: const MenuItemStyleData(
                          height: 40,
                          padding: EdgeInsets.only(left: 14, right: 14),
                        ),
                      ),
                    ),
                    separador(),
                    aprobarVerificacion(
                        selectedValue,
                        dni,
                        context,
                        fotoPerfil,
                        fotoCarnetA,
                        fotoCarnetB,
                        fotoLicenciaConducirA,
                        fotoLicenciaConducirB,
                        certificadoantecedentes),
                    separador(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget showNames(names) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Nombre: $names',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showLastNames(lastNames) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Apellido: $lastNames',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showEmail(email) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Email: $email',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showPhone(phone) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Telefono: $phone',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showPhotoPerfil(fotoPerfil) {
  return SizedBox(
    width: 20.w,
    child: CachedNetworkImage(
      imageUrl: "$fotoPerfil",
      placeholder: (context, url) => circulatorCustom(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    ),
  );
}

Widget showAprobacion(aprobacion) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'aprobacion del usuario: $aprobacion',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showStatus(status, controller) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      controller: controller,
      enabled: true,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Estado del usuario: $status',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showFotoCarnet(fotoCarnet, fotoCarnetB) {
  return Column(
    children: [
      const Text("Foto carnet lado A"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoCarnet, width: 20.w),
      ),
      separador(),
      const Text("Foto carnet lado B"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoCarnetB, width: 20.w),
      ),
    ],
  );
}

Widget showTipoLicencia(tipoLicenciaConducir) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Tipo de licencia: $tipoLicenciaConducir',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showRestriccion(restriccion) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Restricción: $restriccion',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showFechaVencimientoLicencia(vencimientoLicencia) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'fecha vencimiento licencia: $vencimientoLicencia',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showFotoLicenciaConducir(fotoLicencia, fotoLicenciaB) {
  return const Column(
    children: [],
  );
}

Widget aprobarVerificacion(
    controller,
    dni,
    context,
    fotoPerfil,
    fotoCarnetA,
    fotoCarnetB,
    fotoLicenciaConducirA,
    fotoLicenciaConducirB,
    certificadoAntecedentes) {
  return ElevatedButton(
    style: const ButtonStyle(
        backgroundColor: MaterialStatePropertyAll(Colors.deepPurple)),
    onPressed: () async {
      CachedNetworkImage.evictFromCache("$fotoPerfil");
      CachedNetworkImage.evictFromCache("$fotoCarnetA");
      CachedNetworkImage.evictFromCache("$fotoCarnetB");
      CachedNetworkImage.evictFromCache("$fotoLicenciaConducirA");
      CachedNetworkImage.evictFromCache("$fotoLicenciaConducirB");
      CachedNetworkImage.evictFromCache("$certificadoAntecedentes");
      var response = await updateStatusConductor(controller, dni);

      if (response["status_code"] == true) {
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.success(
            message: "Datos actualizados correctamente",
          ),
        );
        Navigator.pop(context);
        Navigator.pop(context);
      } else {
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.error(
            message: "Usted ha ingresado una aprobación no valida !!!!!",
          ),
        );
      }
    },
    child: const Text(
      "Verificar",
      style: TextStyle(color: Colors.white),
    ),
  );
}
