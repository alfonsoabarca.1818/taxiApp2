import 'package:admin_taxi_seguro/src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../apis/api_auditor/getAllConductoresPartner.dart';
import '../../../apis/api_auditor/update_estado.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/separador.dart';
import '../../habilitacion/conductor/habilitar_conductor.dart';
import '../../login/login_auditor/login_auditor.dart';
import '../../verificacion/verificar_datos_conductor.dart';
import '../../verificacion/verificar_datos_duenio_vehiculo.dart';
import '../../verificacion/verificar_datos_vehiculo.dart';

class HomeAuditor extends StatefulWidget {
  const HomeAuditor({super.key});

  @override
  State<HomeAuditor> createState() => _HomeAuditorState();
}

class _HomeAuditorState extends State<HomeAuditor> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var loginProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
      body: WillPopScope(
        onWillPop: () => alertShow(
            context, verificaDuenioProvider.getRutAuditor, loginProvider),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.center,
                end: Alignment.centerRight,
                colors: [
                  Color(0xFFEE973A),
                  Color(0xFFA14BCC),
                ]),
          ),
          width: 100.w,
          height: 100.h,
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(top: 3.h),
                  child: Container(
                    width: 32.w,
                    margin: EdgeInsets.only(top: 3.h, left: 3.w),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Menu Auditor",
                        style: TextStyle(
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ),
              ),
              separador(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                          onTap: () async {
                            Map lMapas = await getAllDuenioPartner(
                                verificaDuenioProvider.nameAuditor);

                            List listaMapas = lMapas["Conductor"];

                            verificaDuenioProvider
                                .setDatasVerificarDuenio(listaMapas);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const VerificarDatosDuenioVehiculo(),
                              ),
                            );
                          },
                          child: btnVerificarDuenioAuto()),
                      separador(),
                      InkWell(
                          onTap: () async {
                            Map lMapas = await getAllVehiclePartner(
                                verificaDuenioProvider.nameAuditor);

                            List listaMapas = lMapas["Conductor"];

                            List listarevision = [];

                            listaMapas.forEach((element) {
                              if (element["aprobacion"] == "revision") {
                                setState(() {
                                  listarevision.add(element);
                                });
                              }
                            });

                            verificaDuenioProvider
                                .setDatasVerificarVehiculo(listarevision);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const VerificarDatosVehiculo(),
                              ),
                            );
                          },
                          child: btnVerificarDatosVehiculo()),
                      separador(),
                      InkWell(
                          onTap: () async {
                            Map lMapas = await getAllConductoresPartner(
                                verificaDuenioProvider.nameAuditor);

                            List listaMapas = lMapas["Conductor"];

                            verificaDuenioProvider
                                .setDatasVerificarConductor(listaMapas);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const VerificarDatosConductor(),
                              ),
                            );
                          },
                          child: btnVerificarDatosConductor()),
                      separador(),
                      InkWell(
                          onTap: () async {
                            Map lMapas = await getAllConductoresPartner(
                                verificaDuenioProvider.nameAuditor);

                            List listaMapas = lMapas["Conductor"];

                            verificaDuenioProvider
                                .setDatasVerificarConductor(listaMapas);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const HabilitarConductorVehiculo(),
                              ),
                            );
                          },
                          child: btnVerificarHabilitarUsuarios()),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<bool> alertShow(context, controllerRut, loginProvider) async {
  bool action = await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.black,
      title: const Text(
        "Alerta !!!",
        style: TextStyle(color: Colors.white),
      ),
      content: const Text(
        "Esta seguro que desea salir ???",
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () => Navigator.pop(context, false),
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white),
          ),
        ),
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () async {
            final SharedPreferences prefs =
                await SharedPreferences.getInstance();

            String? rutprefs = await prefs.getString("user_rut");

            var modificado =
                await updateEstadoAuditor('desconectado', rutprefs);

            loginProvider.milistaFotoRtecnica = <int>[];

            loginProvider.milistaFotoRnstp = <int>[];

            loginProvider.milistaFotoCirculacion = <int>[];

            loginProvider.milistaFotoInscripcion = <int>[];

            loginProvider.milistaFotoTaximetro = <int>[];

            await prefs.remove('idsession');
            await prefs.remove('user_rut');
            await prefs.remove('user_password');
            await prefs.remove('tipo');
            await prefs.remove('id');
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => LoginAuditor()),
              (route) => false,
            );
          },
          child: const Text(
            "Si",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    ),
  );

  return action;
}

Widget btnVerificarDuenioAuto() {
  return Container(
    width: 70.w,
    height: 10.h,
    decoration: const BoxDecoration(
      color: Colors.deepPurple,
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Verificar datos \ndueño vehiculo",
          style: TextStyle(color: Colors.white),
        ),
        Image.asset("assets/auditor/verificacion.png")
      ],
    ),
  );
}

Widget btnVerificarDatosVehiculo() {
  return Container(
    width: 70.w,
    height: 10.h,
    decoration: const BoxDecoration(
      color: Colors.deepPurple,
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Verificar datos \nvehiculo",
          style: TextStyle(color: Colors.white),
        ),
        Image.asset("assets/auditor/verificacion.png")
      ],
    ),
  );
}

Widget btnVerificarDatosConductor() {
  return Container(
    width: 70.w,
    height: 10.h,
    decoration: const BoxDecoration(
      color: Colors.deepPurple,
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Verificar datos \nconductor",
          style: TextStyle(color: Colors.white),
        ),
        Image.asset("assets/auditor/verificacion.png")
      ],
    ),
  );
}

Widget btnVerificarHabilitarUsuarios() {
  return Container(
    width: 70.w,
    height: 10.h,
    decoration: const BoxDecoration(
      color: Colors.deepPurple,
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Habilitar o desabilitar\n usuarios",
          style: TextStyle(color: Colors.white),
        ),
        Image.asset("assets/auditor/verificacion.png")
      ],
    ),
  );
}
