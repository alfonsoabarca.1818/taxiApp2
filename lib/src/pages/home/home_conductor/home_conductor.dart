import 'dart:io';

import 'package:admin_taxi_seguro/src/pages/calendary_conductores.dart';
import 'package:admin_taxi_seguro/src/pages/login/login_conductor/login_conductor.dart';
import 'package:admin_taxi_seguro/src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../../apis/api_auditor/update_aprobacion/update_aprobacion_conductor.dart';
import '../../../apis/api_auditor/update_estado.dart';
import '../../../apis/api_conductor/update_conductor.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/home_provider.dart';
import '../../../tools/loading.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';
import 'modify_partner.dart';

class HomeConductor extends StatefulWidget {
  const HomeConductor({super.key});

  @override
  State<HomeConductor> createState() => _HomeConductorState();
}

class _HomeConductorState extends State<HomeConductor> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerRut = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerAprobacion = TextEditingController();
  TextEditingController controllerTipoLicencia = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerRestriccion = TextEditingController();
  TextEditingController controllerFechaVencimiento = TextEditingController();
  TextEditingController controllerFechaVencimientoLicenciaDia =
      TextEditingController();
  TextEditingController controllerFechaVencimientoLicenciaMes =
      TextEditingController();
  TextEditingController controllerFechaVencimientoLicenciaAnio =
      TextEditingController();

  bool validateRutChile = false;
  bool validate2 = false;
  bool ismodifi = false;
  bool iboolean = false;

  bool camerais1 = false;
  bool camerais2 = false;
  bool camerais3 = false;
  bool camerais4 = false;
  bool camerais5 = false;
  bool camerais6 = false;

  String nombre = '';
  String apellido = '';
  String correo = '';
  String telefono = '';
  String restriccionn = '';
  String rutt = '';
  String tipoLicencias = '';
  String fechaVencimientoss = '';
  String fechaVencimientoDia = '';
  String fechaVencimientoMes = '';
  String fechaVencimientoAnio = '';

  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  File? image_to_upload1,
      image_to_upload2,
      image_to_upload3,
      image_to_upload4,
      image_to_upload5,
      image_to_upload6;
  var imageUrl;

  File? image1;
  bool istrue1 = false;

  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais1) {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    } else {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    }

    setState(() {});
  }

  File? image2;
  bool istrue2 = false;

  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais2) {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  File? image3;
  bool istrue3 = false;

  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais3) {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  File? image4;
  bool istrue4 = false;

  final _picker4 = ImagePicker();
  Future<void> openImagePicker4() async {
    if (camerais4) {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    } else {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    }
  }

  File? image5;
  bool istrue5 = false;

  final _picker5 = ImagePicker();
  Future<void> openImagePicker5() async {
    if (camerais5) {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    } else {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    }
  }

  File? image6;
  bool istrue6 = false;

  final _picker6 = ImagePicker();
  Future<void> openImagePicker6() async {
    if (camerais6) {
      final XFile? pickedImage6 = await _picker6.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage6 != null) {
        setState(() {
          image6 = File(pickedImage6.path);
          istrue6 = true;
        });
      }
    } else {
      final XFile? pickedImage6 = await _picker6.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage6 != null) {
        setState(() {
          image6 = File(pickedImage6.path);
          istrue6 = true;
        });
      }
    }
  }

  Dio dio = Dio();

  List<dynamic> losPartner = [];

  getAllPartners() async {
    Response resp = await dio.get("$url/getAllPartner");
    var respuesta = resp.data;

    List listaMapas = respuesta["Partners"];

    listaMapas.forEach((element) {
      losPartner.add(element["nombre"]);
    });

    setState(() {});

    setState(() {});
  }

  uuploadImageFotoPerfil(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();
    final serverUrl = '$url/uploadImageConductorPerfil/$rut';
    var pathimages = imagepath.path;

    print("el path es: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('EL RESPONSE ES: $response');
        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImageFotoCarnetA(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorCarnetLadoA/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageFotoCarnetB(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorCarnetLadoB/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageLicenciaConducirA(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorConducirA/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageLicenciaConducirB(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorConducirB/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageAntecedentes(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorAntecedentes/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  darvariable(
    controllerName,
    controllerLastName,
    controllerRut,
    controllerEmail,
    controllerPhone,
    controllerRestriccion,
    controllerFechaVencimiento,
    controllerTipoLicencia,
    names,
    lastNames,
    phone,
    email,
    rut,
    tipoLicencia,
    restriccion,
    fechaVencimientoLicencia,
  ) {
    if (iboolean) {
      nombre = controllerName.text;
      apellido = controllerLastName.text;
      correo = controllerEmail.text;
      telefono = controllerPhone.text;
      restriccionn = controllerRestriccion.text;
      rutt = controllerRut.text;
      tipoLicencias = controllerTipoLicencia.text;
      fechaVencimientoss = controllerFechaVencimiento.text;
      fechaVencimientoDia = controllerFechaVencimientoLicenciaDia.text;
      fechaVencimientoMes = controllerFechaVencimientoLicenciaMes.text;
      fechaVencimientoAnio = controllerFechaVencimientoLicenciaAnio.text;

      controllerName.text = nombre;
      controllerLastName.text = apellido;
      controllerEmail.text = correo;
      controllerPhone.text = telefono;
      controllerRestriccion.text = restriccionn;
      controllerRut.text = rutt;
      controllerTipoLicencia.text = tipoLicencias;
      controllerFechaVencimiento.text = fechaVencimientoss;
      controllerFechaVencimientoLicenciaDia.text =
          fechaVencimientoLicencia.toString().substring(8, 10);
      controllerFechaVencimientoLicenciaMes.text =
          fechaVencimientoLicencia.toString().substring(5, 7);
      controllerFechaVencimientoLicenciaAnio.text =
          fechaVencimientoLicencia.toString().substring(0, 4);
    } else {
      controllerName.text = names;
      controllerLastName.text = lastNames;
      controllerPhone.text = phone;
      controllerEmail.text = email;
      controllerRestriccion.text = restriccion;
      controllerRut.text = rut;
      controllerTipoLicencia.text = tipoLicencia;
      controllerFechaVencimiento.text = fechaVencimientoLicencia;
      controllerFechaVencimientoLicenciaDia.text =
          fechaVencimientoLicencia.toString().substring(8, 10);
      controllerFechaVencimientoLicenciaMes.text =
          fechaVencimientoLicencia.toString().substring(5, 7);
      controllerFechaVencimientoLicenciaAnio.text =
          fechaVencimientoLicencia.toString().substring(0, 4);
    }
  }

  controllermo(
    names,
    lastNames,
    phone,
    email,
    rut,
    tipoLicencia,
    restriccion,
    fechaVencimientoLicencia,
    fechaVencimientoDia,
    fechaVencimientoMes,
    fechaVencimientoAnio,
  ) {
    if (iboolean) {
      nombre = controllerName.text;
      apellido = controllerLastName.text;
      correo = controllerEmail.text;
      telefono = controllerPhone.text;
      restriccionn = controllerRestriccion.text;
      rutt = controllerRut.text;
      tipoLicencias = controllerTipoLicencia.text;
      fechaVencimientoss = controllerFechaVencimiento.text;
      fechaVencimientoDia = controllerFechaVencimientoLicenciaDia.text;
      fechaVencimientoMes = controllerFechaVencimientoLicenciaMes.text;
      fechaVencimientoAnio = controllerFechaVencimientoLicenciaAnio.text;

      controllerName.text = nombre;
      controllerLastName.text = apellido;
      controllerEmail.text = correo;
      controllerPhone.text = telefono;
      controllerRestriccion.text = restriccionn;
      controllerRut.text = rutt;
      controllerTipoLicencia.text = tipoLicencias;
      controllerFechaVencimiento.text = fechaVencimientoss;
      controllerFechaVencimientoLicenciaDia.text = fechaVencimientoDia;
      controllerFechaVencimientoLicenciaMes.text = fechaVencimientoMes;
      controllerFechaVencimientoLicenciaAnio.text = fechaVencimientoAnio;
    } else {
      controllerName.text = names;
      controllerLastName.text = lastNames;
      controllerPhone.text = phone;
      controllerEmail.text = email;
      controllerRestriccion.text = restriccion;
      controllerRut.text = rut;
      controllerTipoLicencia.text = tipoLicencia;
      controllerFechaVencimiento.text = fechaVencimientoLicencia;
      controllerFechaVencimientoLicenciaDia.text =
          fechaVencimientoLicencia.toString().substring(8, 10);
      controllerFechaVencimientoLicenciaMes.text =
          fechaVencimientoLicencia.toString().substring(5, 7);
      controllerFechaVencimientoLicenciaAnio.text =
          fechaVencimientoLicencia.toString().substring(0, 4);
    }
  }

  solicitarRevision(
    dni,
  ) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        alignment: AlignmentDirectional.centerStart,
        contentPadding: const EdgeInsets.symmetric(horizontal: 20),
        backgroundColor: Colors.amberAccent.shade700,
        title: Center(
            child: Text(
          "ALERTA",
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 20.sp,
              fontWeight: FontWeight.bold),
        )),
        content: Text(
          '¿Estas seguro que quieres solicitar revision?',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: const ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.deepPurple),
                  ),
                  onPressed: () async {
                    var response =
                        await updateAprobacionConductor('revision', dni, 'fg');
                    showTopSnackBar(
                      Overlay.of(context),
                      const CustomSnackBar.success(
                        message: "Solicitud enviada",
                      ),
                    );

                    var modificado =
                        await updateEstadoConductor('desconectado', dni);

                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => LoginConductor()),
                      (route) => false,
                    );
                  },
                  child: const Text(
                    "Si",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.red.shade700),
                  ),
                  onPressed: () => Navigator.pop(context),
                  child: const Text(
                    "No",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  modificarDatos(
    homeProvider,
    verificarConductorProvider,
    datas,
    name,
    lastName,
    dni,
    email,
    phone,
    tipoLicencia,
    restriccion,
    fechaVencimientoLicencia,
    fotoCarnetA,
    fotoCarnetB,
    fotoPerfil,
    fotoLicenciaConducirA,
    fotoLicenciaConducirB,
    certificadoAntecedentes,
  ) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.amber.shade700,
        title: Center(
            child: Text(
          "ATENCIÓN",
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 20.sp,
              fontWeight: FontWeight.bold),
        )),
        content: Text(
          '¿Estas seguro que quieres guardar los datos?',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        actions: [
          ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
            ),
            onPressed: () async {
              String fechafinal = "";
              if (controllerFechaVencimientoLicenciaDia.text.length < 2) {
                if (controllerFechaVencimientoLicenciaMes.text.length < 2) {
                  fechafinal = controllerFechaVencimientoLicenciaAnio.text +
                      "-" +
                      "0" +
                      controllerFechaVencimientoLicenciaMes.text +
                      "-" +
                      "0" +
                      controllerFechaVencimientoLicenciaDia.text;
                } else {
                  fechafinal = controllerFechaVencimientoLicenciaAnio.text +
                      "-" +
                      controllerFechaVencimientoLicenciaMes.text +
                      "-" +
                      "0" +
                      controllerFechaVencimientoLicenciaDia.text;
                }
              } else {
                if (controllerFechaVencimientoLicenciaMes.text.length < 2) {
                  fechafinal = controllerFechaVencimientoLicenciaAnio.text +
                      "-" +
                      "0" +
                      controllerFechaVencimientoLicenciaMes.text +
                      "-" +
                      controllerFechaVencimientoLicenciaDia.text;
                } else {
                  fechafinal = controllerFechaVencimientoLicenciaAnio.text +
                      "-" +
                      controllerFechaVencimientoLicenciaMes.text +
                      "-" +
                      controllerFechaVencimientoLicenciaDia.text;
                }
              }

              homeProvider.isbutton = false;

              Navigator.pop(context);
              var r1, r2, r3, r4, r5, r6;
              setState(() {
                verificarConductorProvider.setLoading = true;
              });
              datas.setLoading = true;

              if (image_to_upload1 != null ||
                  image_to_upload2 != null ||
                  image_to_upload3 != null ||
                  image_to_upload4 != null ||
                  image_to_upload5 != null ||
                  image_to_upload6 != null) {
                if (image_to_upload1 != null &&
                    image_to_upload2 != null &&
                    image_to_upload3 != null &&
                    image_to_upload4 != null &&
                    image_to_upload5 != null &&
                    image_to_upload6 != null) {
                  CachedNetworkImage.evictFromCache("$fotoPerfil");
                  CachedNetworkImage.evictFromCache("$fotoCarnetA");
                  CachedNetworkImage.evictFromCache("$fotoCarnetB");
                  CachedNetworkImage.evictFromCache("$fotoLicenciaConducirA");
                  CachedNetworkImage.evictFromCache("$fotoLicenciaConducirB");
                  CachedNetworkImage.evictFromCache("$certificadoAntecedentes");

                  var resp1 =
                      await uuploadImageFotoPerfil(image_to_upload1, dni);

                  var lastIndex = resp1.toString().length;

                  print("el lastindex1 es: $resp1");

                  setState(() {
                    r1 = resp1.toString();
                  });

                  print("El r1 es: $r1");

                  var resp2 = await uuploadImageLicenciaConducirA(
                      image_to_upload2, dni);

                  var lastIndex2 = resp2.toString().length;

                  print("el lastindex1 es: $resp2");

                  setState(() {
                    r2 = resp2.toString();
                  });

                  print("El r2 es: $r2");

                  var resp3 = await uuploadImageLicenciaConducirB(
                      image_to_upload3, dni);

                  var lastIndex3 = resp3.toString().length;

                  print("el lastindex3 es: $resp3");

                  setState(() {
                    r3 = resp3.toString();
                  });

                  print("El r1 es: $r3");

                  var resp4 =
                      await uuploadImageFotoCarnetA(image_to_upload4, dni);

                  var lastIndex4 = resp4.toString().length;

                  print("el lastindex4 es: $resp4");

                  setState(() {
                    r4 = resp4.toString();
                  });

                  print("El r4 es: $r4");

                  var resp5 =
                      await uuploadImageFotoCarnetB(image_to_upload5, dni);

                  var lastInde5 = resp5.toString().length;

                  print("el lastindex5 es: $resp5");

                  setState(() {
                    r5 = resp5.toString();
                  });

                  print("El r5 es: $r5");

                  var resp6 =
                      await uuploadImageAntecedentes(image_to_upload6, dni);

                  var lastInde6 = resp6.toString().length;

                  print("el lastindex6 es: $resp6");

                  setState(() {
                    r6 = resp6.toString();
                  });

                  print("El r6 es: $r6");

                  var data = {
                    "name": controllerName.text == ''
                        ? "$name"
                        : "${controllerName.text}",
                    "lastName": controllerLastName.text == ''
                        ? "$lastName"
                        : "${controllerLastName.text}",
                    "dni": controllerRut.text == ''
                        ? "$dni"
                        : "${controllerRut.text}",
                    "fotoCarnetA":
                        "${resp4.toString().substring(29, lastIndex4)}",
                    "fotoCarnetB":
                        "${resp5.toString().substring(29, lastInde5)}",
                    "email": controllerEmail.text == ''
                        ? "$email"
                        : "${controllerEmail.text}",
                    "phone": controllerPhone.text == ''
                        ? "$phone"
                        : "${controllerPhone.text}",
                    "fotoPerfil":
                        "${resp1.toString().substring(29, lastIndex)}",
                    "tipoLicencia": controllerTipoLicencia.text == ''
                        ? "$tipoLicencia"
                        : "${controllerTipoLicencia.text}",
                    "restriccion": controllerRestriccion.text == ''
                        ? "$restriccion"
                        : "${controllerRestriccion.text}",
                    "fechaVencimientoLicencia":
                        controllerFechaVencimiento.text == ''
                            ? "$fechaVencimientoLicencia"
                            : fechafinal,
                    "fotoLicenciaConducirA":
                        "${resp2.toString().substring(29, lastIndex2)}",
                    "fotoLicenciaConducirB":
                        "${resp3.toString().substring(29, lastIndex3)}",
                    "aprobacion": "rechazado",
                    "status": "revision",
                    "certificadoAntecedentes": image_to_upload6 == null
                        ? "$certificadoAntecedentes"
                        : resp6.toString().substring(29, lastInde6)
                  };

                  setState(() {
                    verificarConductorProvider.setLoading = false;
                    iboolean = false;
                  });
                  datas.setLoading = false;
                } else {
                  setState(() {
                    verificarConductorProvider.setLoading = true;
                  });
                  if (image_to_upload1 != null &&
                      image_to_upload2 != null &&
                      image_to_upload3 != null &&
                      image_to_upload4 != null &&
                      image_to_upload5 != null &&
                      image_to_upload6 != null) {
                    CachedNetworkImage.evictFromCache("$fotoPerfil");
                    CachedNetworkImage.evictFromCache("$fotoCarnetA");
                    CachedNetworkImage.evictFromCache("$fotoCarnetB");
                    CachedNetworkImage.evictFromCache("$fotoLicenciaConducirA");
                    CachedNetworkImage.evictFromCache("$fotoLicenciaConducirB");
                    CachedNetworkImage.evictFromCache(
                        "$certificadoAntecedentes");

                    var resp1 =
                        await uuploadImageFotoPerfil(image_to_upload1, dni);

                    var lastIndex = resp1.toString().length;

                    print("el lastindex1 es: $resp1");

                    setState(() {
                      r1 = resp1.toString();
                    });

                    print("El r1 es: $r1");

                    var resp2 = await uuploadImageLicenciaConducirA(
                        image_to_upload2, dni);

                    var lastIndex2 = resp2.toString().length;

                    print("el lastindex1 es: $resp2");

                    setState(() {
                      r2 = resp2.toString();
                    });

                    print("El r2 es: $r2");

                    var resp3 = await uuploadImageLicenciaConducirB(
                        image_to_upload3, dni);

                    var lastIndex3 = resp3.toString().length;

                    print("el lastindex3 es: $resp3");

                    setState(() {
                      r3 = resp3.toString();
                    });

                    print("El r1 es: $r3");

                    var resp4 =
                        await uuploadImageFotoCarnetA(image_to_upload4, dni);

                    var lastIndex4 = resp4.toString().length;

                    print("el lastindex4 es: $resp4");

                    setState(() {
                      r4 = resp4.toString();
                    });

                    print("El r4 es: $r4");

                    var resp5 =
                        await uuploadImageFotoCarnetB(image_to_upload5, dni);

                    var lastInde5 = resp5.toString().length;

                    print("el lastindex5 es: $resp5");

                    setState(() {
                      r5 = resp5.toString();
                    });

                    print("El r5 es: $r5");

                    var resp6 =
                        await uuploadImageAntecedentes(image_to_upload6, dni);

                    var lastInde6 = resp6.toString().length;

                    print("el lastindex6 es: $resp6");

                    setState(() {
                      r6 = resp6.toString();
                    });

                    print("El r6 es: $r6");

                    var data = {
                      "name": controllerName.text == ''
                          ? "$name"
                          : "${controllerName.text}",
                      "lastName": controllerLastName.text == ''
                          ? "$lastName"
                          : "${controllerLastName.text}",
                      "dni": controllerRut.text == ''
                          ? "$dni"
                          : "${controllerRut.text}",
                      "fotoCarnetA":
                          "${image_to_upload1 == null ? fotoCarnetA : resp4.toString().substring(29, lastIndex4)}",
                      "fotoCarnetB":
                          "${image_to_upload2 == null ? fotoCarnetB : resp5.toString().substring(29, lastInde5)}",
                      "email": controllerEmail.text == ''
                          ? "$email"
                          : "${controllerEmail.text}",
                      "phone": controllerPhone.text == ''
                          ? "$phone"
                          : "${controllerPhone.text}",
                      "fotoPerfil":
                          "${image_to_upload3 == null ? fotoPerfil : resp1.toString().substring(29, lastIndex)}",
                      "tipoLicencia": controllerTipoLicencia.text == ''
                          ? "$tipoLicencia"
                          : "${controllerTipoLicencia.text}",
                      "restriccion": controllerRestriccion.text == ''
                          ? "$restriccion"
                          : "${controllerRestriccion.text}",
                      "fechaVencimientoLicencia":
                          controllerFechaVencimiento.text == ''
                              ? "$fechaVencimientoLicencia"
                              : fechafinal,
                      "fotoLicenciaConducirA":
                          "${image_to_upload4 == null ? fotoLicenciaConducirA : resp2.toString().substring(29, lastIndex2)}",
                      "fotoLicenciaConducirB":
                          "${image_to_upload5 == null ? fotoLicenciaConducirB : resp3.toString().substring(29, lastIndex3)}",
                      "aprobacion": "rechazado",
                      "status": "revision",
                      "certificadoAntecedentes": image_to_upload6 == null
                          ? "$certificadoAntecedentes"
                          : resp6.toString().substring(29, lastInde6)
                    };

                    showTopSnackBar(
                      Overlay.of(context),
                      const CustomSnackBar.success(
                        message:
                            "Su los datos del conductor se han actualizado con exito",
                      ),
                    );
                    setState(() {
                      verificarConductorProvider.setLoading = false;
                      iboolean = false;
                    });
                    datas.setLoading = false;

                    Navigator.pop(context);
                  } else {
                    showTopSnackBar(
                      Overlay.of(context),
                      const CustomSnackBar.info(
                        message: "Actualizando datos",
                      ),
                    );
                    setState(() {
                      verificarConductorProvider.setLoading = true;
                    });
                    if (image_to_upload1 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache("$fotoPerfil");

                      var resp1 =
                          await uuploadImageFotoPerfil(image_to_upload1, dni);

                      var lastIndex = resp1.toString().length;

                      print("el lastindex1 es: $resp1");

                      setState(() {
                        r1 = resp1;
                      });

                      print("El r1 es: $r1");
                    }
                    if (image_to_upload2 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache(
                          "$fotoLicenciaConducirA");

                      var resp2 = await uuploadImageLicenciaConducirA(
                          image_to_upload2, dni);

                      var lastIndex2 = resp2.toString().length;

                      print("el lastindex1 es: $resp2");

                      setState(() {
                        r2 = resp2;
                      });

                      print("El r2 es: $r2");
                    }
                    if (image_to_upload3 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache(
                          "$fotoLicenciaConducirB");

                      var resp3 = await uuploadImageLicenciaConducirB(
                          image_to_upload3, dni);

                      var lastIndex3 = resp3.toString().length;

                      print("el lastindex3 es: $resp3");

                      setState(() {
                        r3 = resp3;
                      });

                      print("El r1 es: $r3");
                    }
                    if (image_to_upload4 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache("$fotoCarnetA");

                      var resp4 =
                          await uuploadImageFotoCarnetA(image_to_upload4, dni);

                      var lastIndex4 = resp4.toString().length;

                      print("el lastindex4 es: $resp4");

                      setState(() {
                        r4 = resp4;
                      });

                      print("El r4 es: $r4");
                    }
                    if (image_to_upload5 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache("$fotoCarnetB");

                      var resp5 =
                          await uuploadImageFotoCarnetB(image_to_upload5, dni);

                      var lastInde5 = resp5.toString().length;

                      print("el lastindex5 es: $resp5");

                      setState(() {
                        r5 = resp5;
                      });

                      print("El r5 es: $r5");
                    }

                    if (image_to_upload6 == null) {
                    } else {
                      CachedNetworkImage.evictFromCache(
                          "$certificadoAntecedentes");
                      var resp6 =
                          await uuploadImageAntecedentes(image_to_upload6, dni);

                      var lastInde6 = resp6.toString().length;

                      print("el lastindex6 es: $resp6");

                      setState(() {
                        r6 = resp6;
                      });

                      print("El r6 es: $r6");
                    }
                    var data = {
                      "name": controllerName.text == ''
                          ? "$name"
                          : "${controllerName.text}",
                      "lastName": controllerLastName.text == ''
                          ? "$lastName"
                          : "${controllerLastName.text}",
                      "dni": controllerRut.text == ''
                          ? "$dni"
                          : "${controllerRut.text}",
                      "fotoCarnetA":
                          "${image_to_upload4 == null ? fotoCarnetA : '$url3/' + r4.toString().substring(29, r4.toString().length)}",
                      "fotoCarnetB":
                          "${image_to_upload5 == null ? fotoCarnetB : '$url3/' + r5.toString().substring(29, r5.toString().length)}",
                      "email": controllerEmail.text == ''
                          ? "$email"
                          : "${controllerEmail.text}",
                      "phone": controllerPhone.text == ''
                          ? "$phone"
                          : "${controllerPhone.text}",
                      "fotoPerfil":
                          "${image_to_upload1 == null ? fotoPerfil : '$url3/' + r1.toString().substring(29, r1.toString().length)}",
                      "tipoLicencia": controllerTipoLicencia.text == ''
                          ? "$tipoLicencia"
                          : "${controllerTipoLicencia.text}",
                      "restriccion": controllerRestriccion.text == ''
                          ? "$restriccion"
                          : "${controllerRestriccion.text}",
                      "fechaVencimientoLicencia":
                          controllerFechaVencimiento.text == ''
                              ? "$fechaVencimientoLicencia"
                              : fechafinal,
                      "fotoLicenciaConducirA":
                          "${image_to_upload2 == null ? fotoLicenciaConducirA : '$url3/' + r2.toString().substring(29, r2.toString().length)}",
                      "fotoLicenciaConducirB":
                          "${image_to_upload3 == null ? fotoLicenciaConducirB : '$url3/' + r3.toString().substring(29, r3.toString().length)}",
                      "aprobacion": "rechazado",
                      "status": "revision",
                      "certificadoAntecedentes": image_to_upload6 == null
                          ? "$certificadoAntecedentes"
                          : '$url3/' +
                              r6.toString().substring(29, r6.toString().length)
                    };

                    var response = await updateConductor(dni, data);

                    setState(() {
                      verificarConductorProvider.setLoading = false;
                      iboolean = false;
                    });
                    datas.setLoading = false;
                  }
                }
              } else {
                var data = {
                  "name": controllerName.text == ''
                      ? "$name"
                      : "${controllerName.text}",
                  "lastName": controllerLastName.text == ''
                      ? "$lastName"
                      : "${controllerLastName.text}",
                  "dni": controllerRut.text == ''
                      ? "$dni"
                      : "${controllerRut.text}",
                  "fotoCarnetA": "$fotoCarnetA",
                  "fotoCarnetB": "$fotoCarnetB",
                  "email": controllerEmail.text == ''
                      ? "$email"
                      : "${controllerEmail.text}",
                  "phone": controllerPhone.text == ''
                      ? "$phone"
                      : "${controllerPhone.text}",
                  "fotoPerfil": "$fotoPerfil",
                  "tipoLicencia": controllerTipoLicencia.text == ''
                      ? "$tipoLicencia"
                      : "${controllerTipoLicencia.text}",
                  "restriccion": controllerRestriccion.text == ''
                      ? "$restriccion"
                      : "${controllerRestriccion.text}",
                  "fechaVencimientoLicencia":
                      controllerFechaVencimiento.text == ''
                          ? "$fechaVencimientoLicencia"
                          : fechafinal,
                  "fotoLicenciaConducirA": "$fotoLicenciaConducirA",
                  "fotoLicenciaConducirB": "$fotoLicenciaConducirB",
                  "aprobacion": "rechazado",
                  "status": "revision",
                  "certificadoAntecedentes": "$certificadoAntecedentes"
                };

                var response = await updateConductor(dni, data);

                datas.setLoading = false;

                setState(() {
                  verificarConductorProvider.setLoading = false;
                  iboolean = false;
                });
              }
              setState(() {
                verificarConductorProvider.setLoading = false;
              });
              datas.setLoading = false;
            },
            child: const Text(
              "Si",
              style: TextStyle(color: Colors.white),
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.red.shade700),
            ),
            onPressed: () => Navigator.pop(context),
            child: const Text(
              "No",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  String? selectedValue;
  final List<String> items = ["aprobado", "desaprobado"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllPartners();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificarConductorProvider = Provider.of<AuditorProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var homeProvider = Provider.of<HomeProvider>(context);
    var loginProvider = Provider.of<LoginProvider>(context);

    var aprobado = verificarConductorProvider.getAprobadoProvider;

    var datas = Provider.of<AuditorProvider>(context);
    var name = datas.getDatasVerificarOneConductor()["name"];

    var lastName = datas.getDatasVerificarOneConductor()["lastName"];
    var dni = datas.getDatasVerificarOneConductor()["dni"];

    var fotoCarnetA = datas.getDatasVerificarOneConductor()["fotoCarnetA"];
    var fotoCarnetB = datas.getDatasVerificarOneConductor()["fotoCarnetB"];

    var email = datas.getDatasVerificarOneConductor()["email"];
    var phone = datas.getDatasVerificarOneConductor()["phone"];
    var fotoPerfil = datas.getDatasVerificarOneConductor()["fotoPerfil"];
    var tipoLicencia = datas.getDatasVerificarOneConductor()["tipoLicencia"];
    var restriccion = datas.getDatasVerificarOneConductor()["restriccion"];

    var fechaVencimientoLicencia =
        datas.getDatasVerificarOneConductor()["fechaVencimientoLicencia"];
    var fotoLicenciaConducirA =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirA"];
    var fotoLicenciaConducirB =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirB"];

    var motivo = datas.getDatasVerificarOneConductor()["motivo"];
    var partner = datas.getDatasVerificarOneConductor()["Partner"];
    var certificadoAntecedentes =
        datas.getDatasVerificarOneConductor()["certificadoAntecedentes"];
    var fechaInicio = datas.getDatasVerificarOneConductor()["fechainicio"];

    if (nombre == '' &&
        apellido == '' &&
        correo == '' &&
        telefono == '' &&
        restriccionn == '' &&
        rutt == '' &&
        tipoLicencias == '' &&
        fechaVencimientoss == '' &&
        fechaVencimientoDia == '' &&
        fechaVencimientoMes == '' &&
        fechaVencimientoAnio == '') {
      if (image_to_upload1 == null &&
          image_to_upload2 == null &&
          image_to_upload3 == null &&
          image_to_upload4 == null &&
          image_to_upload5 == null) {
        controllermo(
            name,
            lastName,
            phone,
            email,
            dni,
            tipoLicencia,
            restriccion,
            fechaVencimientoLicencia,
            fechaVencimientoDia,
            fechaVencimientoMes,
            fechaVencimientoAnio);
      } else {
        setState(() {
          darvariable(
              controllerName,
              controllerLastName,
              controllerRut,
              controllerEmail,
              controllerPhone,
              controllerRestriccion,
              controllerFechaVencimiento,
              controllerTipoLicencia,
              name,
              lastName,
              phone,
              email,
              dni,
              tipoLicencia,
              restriccion,
              fechaVencimientoLicencia);
        });
      }
      List<dynamic> listafinalquery2 = [];

      losPartner.forEach((element) {
        if (element.toString() == 'taxiseguro') {
          listafinalquery2.add('ningun partner');
        } else {
          listafinalquery2.add(element.toString());
        }
      });
    }

    return Scaffold(
      body: verificaDuenioProvider.getLoading == true
          ? circulatorCustom()
          : aprobado == 'aprobado'
              ? WillPopScope(
                  onWillPop: () => alertShow(context, controllerRut),
                  child: Container(
                    width: 100.w,
                    height: 100.h,
                    color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 100.w,
                          height: 15.h,
                          decoration:
                              BoxDecoration(color: Colors.amber.shade700),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      backgroundColor: Colors.black,
                                      title: const Text(
                                        "Alerta !!!",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      content: const Text(
                                        "Esta seguro que desea salir ???",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      actions: [
                                        ElevatedButton(
                                          style: const ButtonStyle(
                                            backgroundColor:
                                                MaterialStatePropertyAll(
                                                    Colors.black),
                                          ),
                                          onPressed: () =>
                                              Navigator.pop(context, false),
                                          child: const Text(
                                            "No",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        ElevatedButton(
                                          style: const ButtonStyle(
                                            backgroundColor:
                                                MaterialStatePropertyAll(
                                                    Colors.black),
                                          ),
                                          onPressed: () async {
                                            final SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();

                                            var modificado =
                                                await updateEstadoConductor(
                                                    'desconectado', dni);

                                            await prefs.remove('idsession');
                                            await prefs.remove('user_rut');
                                            await prefs.remove('user_password');
                                            await prefs.remove('tipo');
                                            await prefs.remove('id');

                                            Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoginConductor()),
                                              (route) => false,
                                            );
                                          },
                                          child: const Text(
                                            "Si",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                icon: Icon(
                                  Icons.arrow_back,
                                  size: 9.w,
                                  color: Colors.deepPurple,
                                ),
                              ),
                              Center(
                                child: Text(
                                  "Bienvenido",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.sp,
                                    color: Colors.deepPurple,
                                  ),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  String fechaCambio =
                                      DateTime.now().toString();
                                  DateTime f1 = DateTime.parse(fechaCambio);
                                  print("el fq es: $f1");
                                  DateTime f2 = DateTime.parse(fechaInicio);
                                  print("el f2 es: $f2");
                                  Duration f = f1.difference(f2);
                                  print(
                                      "la diferencia de tiempo es: ${f.inDays}");

                                  if (f.inDays < 30) {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: const Text("AVISO"),
                                        content: const Text(
                                            "Aun no han pasado 30 dias\nUna vez pasan 30 dias desde su registro de partner, puede cambiarse"),
                                        actions: [
                                          ElevatedButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: const Text("OK")),
                                        ],
                                      ),
                                    );
                                  } else {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => ModifyPartner(),
                                        ));
                                  }
                                },
                                icon: Icon(Icons.change_circle,
                                    color: Colors.deepPurple, size: 5.0.h),
                              )
                            ],
                          ),
                        ),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(14.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.amber.shade700,
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(10.0),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        "Su cuenta se encuentra activa.\n Ya puedes utilizar TaxiSeguro!!!",
                                        style: TextStyle(
                                            color: Colors.deepPurple,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.sp),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(),
                      ],
                    ),
                  ),
                )
              : verificarConductorProvider.getLoading == true
                  ? const Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Actualizando datos"),
                          SizedBox(
                            width: 20,
                          ),
                          CircularProgressIndicator(),
                        ],
                      ),
                    )
                  : WillPopScope(
                      onWillPop: () => alertShow(context, controllerRut),
                      child: SizedBox(
                        width: 100.w,
                        height: 100.h,
                        child: Column(
                          children: [
                            aprobado == "rechazado" || aprobado == "rechasado"
                                ? Container(
                                    width: 100.w,
                                    height: 30.h,
                                    decoration: BoxDecoration(
                                      color: Colors.amber.shade700,
                                      borderRadius: const BorderRadius.only(
                                        bottomLeft: Radius.circular(20),
                                        bottomRight: Radius.circular(20),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                          child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 12.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Text(
                                              "ATENCIÓN ",
                                              style: TextStyle(
                                                  color: Colors.deepPurple,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20.sp),
                                            ),
                                            Text(
                                              textAlign: TextAlign.start,
                                              "* Su cuenta aún no ha sido enviada a revisión\n* Verifique la información ingresada\n* Para continuar envie sus datos a revisión \n\n* Estado: $motivo",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16.sp,
                                                  color: Colors.deepPurple),
                                            ),
                                          ],
                                        ),
                                      )),
                                    ),
                                  )
                                : Container(
                                    width: 100.w,
                                    height: 15.h,
                                    decoration: BoxDecoration(
                                        color: Colors.amber.shade700),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) => AlertDialog(
                                                backgroundColor: Colors.black,
                                                title: const Text(
                                                  "Alerta !!!",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                content: const Text(
                                                  "Esta seguro que desea salir ???",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                actions: [
                                                  ElevatedButton(
                                                    style: const ButtonStyle(
                                                      backgroundColor:
                                                          MaterialStatePropertyAll(
                                                              Colors.black),
                                                    ),
                                                    onPressed: () =>
                                                        Navigator.pop(
                                                            context, false),
                                                    child: const Text(
                                                      "No",
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                  ElevatedButton(
                                                    style: const ButtonStyle(
                                                      backgroundColor:
                                                          MaterialStatePropertyAll(
                                                              Colors.black),
                                                    ),
                                                    onPressed: () async {
                                                      final SharedPreferences
                                                          prefs =
                                                          await SharedPreferences
                                                              .getInstance();

                                                      var modificado =
                                                          await updateEstadoConductor(
                                                              'desconectado',
                                                              dni);

                                                      await prefs
                                                          .remove('idsession');
                                                      await prefs
                                                          .remove('user_rut');
                                                      await prefs.remove(
                                                          'user_password');
                                                      await prefs
                                                          .remove('tipo');
                                                      await prefs.remove('id');
                                                      Navigator
                                                          .pushAndRemoveUntil(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                LoginConductor()),
                                                        (route) => false,
                                                      );
                                                    },
                                                    child: const Text(
                                                      "Si",
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                          icon: const Icon(
                                            Icons.arrow_back,
                                            color: Colors.deepPurple,
                                          ),
                                        ),
                                        Center(
                                          child: aprobado == "revision"
                                              ? Column(
                                                  children: [
                                                    SizedBox(
                                                      height: 4.5.h,
                                                    ),
                                                    Text(
                                                      "ATENCIÓN ",
                                                      style: TextStyle(
                                                          color:
                                                              Colors.deepPurple,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 20.sp),
                                                    ),
                                                    Text(
                                                      textAlign:
                                                          TextAlign.start,
                                                      "* Su cuenta se encuentra en revision \n",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 16.sp,
                                                          color: Colors
                                                              .deepPurple),
                                                    ),
                                                  ],
                                                )
                                              : Text(
                                                  "Bienvenido",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20.sp,
                                                  ),
                                                ),
                                        ),
                                        Container(),
                                      ],
                                    ),
                                  ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: ListView(
                                  children: [
                                    showNames(name, aprobado, controllerName,
                                        homeProvider),
                                    separador(),
                                    showLastNames(lastName, aprobado,
                                        controllerLastName, homeProvider),
                                    separador(),
                                    showDni(dni, aprobado, controllerRut,
                                        rutValidate, homeProvider),
                                    separador(),
                                    showEmail(email, controllerEmail, aprobado,
                                        homeProvider),
                                    separador(),
                                    showPhone(phone, controllerPhone, aprobado,
                                        homeProvider),
                                    separador(),
                                    const Text("Foto perfil"),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais1 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais1 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );

                                          await openImagePicker();

                                          if (image_to_upload1 == null) {
                                            setState(() {
                                              image_to_upload1 = image1;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload1 = image1;
                                            });
                                          }

                                          if (controllerName.text == '' &&
                                              controllerLastName.text == '' &&
                                              controllerEmail.text == '' &&
                                              controllerPhone.text == '' &&
                                              image_to_upload1 == null &&
                                              image_to_upload2 == null &&
                                              image_to_upload3 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 20.w,
                                        child: fotoPerfil == null
                                            ? const Text(
                                                "No se ha encontrado ninguna foto")
                                            : istrue1 == false
                                                ? CachedNetworkImage(
                                                    imageUrl: "$fotoPerfil",
                                                    placeholder:
                                                        (context, url) =>
                                                            circulatorCustom(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  )
                                                : Image.file(
                                                    image1!,
                                                    width: 35.w,
                                                  ),
                                      ),
                                    ),
                                    separador(),
                                    showTipoLicencia(tipoLicencia, aprobado,
                                        controllerTipoLicencia, homeProvider),
                                    separador(),
                                    showRestriccion(
                                        restriccion,
                                        controllerRestriccion,
                                        aprobado,
                                        homeProvider),
                                    separador(),
                                    Text("Fecha vencimiento licencia"),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Column(
                                          children: [
                                            Text("DD"),
                                            Container(
                                              width: 18.w,
                                              child: TextField(
                                                enabled: aprobado ==
                                                            "rechazado" ||
                                                        aprobado == "rechasado"
                                                    ? true
                                                    : false,
                                                controller:
                                                    controllerFechaVencimientoLicenciaDia,
                                                maxLength: 2,
                                                onChanged: (value) {
                                                  if (controllerFechaVencimientoLicenciaDia
                                                          .text ==
                                                      '') {
                                                    homeProvider.isbutton =
                                                        false;
                                                    iboolean = false;
                                                  } else {
                                                    homeProvider.isbutton =
                                                        true;
                                                    iboolean = true;
                                                  }
                                                },
                                                decoration:
                                                    const InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(10.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        const Text("-"),
                                        Column(
                                          children: [
                                            Text("MM"),
                                            Container(
                                              width: 18.w,
                                              child: TextField(
                                                enabled: aprobado ==
                                                            "rechazado" ||
                                                        aprobado == "rechasado"
                                                    ? true
                                                    : false,
                                                controller:
                                                    controllerFechaVencimientoLicenciaMes,
                                                onChanged: (value) {
                                                  if (controllerFechaVencimientoLicenciaMes
                                                          .text ==
                                                      '') {
                                                    homeProvider.isbutton =
                                                        false;
                                                    iboolean = false;
                                                  } else {
                                                    homeProvider.isbutton =
                                                        true;
                                                    iboolean = true;
                                                  }
                                                },
                                                maxLength: 2,
                                                decoration:
                                                    const InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(10.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        const Text("-"),
                                        Column(
                                          children: [
                                            Text("YYYY"),
                                            Container(
                                              width: 38.w,
                                              child: TextField(
                                                enabled: aprobado ==
                                                            "rechazado" ||
                                                        aprobado == "rechasado"
                                                    ? true
                                                    : false,
                                                onChanged: (value) {
                                                  if (controllerFechaVencimientoLicenciaAnio
                                                          .text ==
                                                      '') {
                                                    homeProvider.isbutton =
                                                        false;
                                                    iboolean = false;
                                                  } else {
                                                    homeProvider.isbutton =
                                                        true;
                                                    iboolean = true;
                                                  }
                                                },
                                                controller:
                                                    controllerFechaVencimientoLicenciaAnio,
                                                maxLength: 4,
                                                decoration:
                                                    const InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(10.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    separador(),
                                    const Text("Licencia conducir lado A"),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais2 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais2 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );
                                          await openImagePicker2();

                                          if (image_to_upload2 == null) {
                                            setState(() {
                                              image_to_upload2 = image2;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload2 = image2;
                                            });
                                          }

                                          if (controllerName.text == '' &&
                                              controllerLastName.text == '' &&
                                              controllerEmail.text == '' &&
                                              controllerPhone.text == '' &&
                                              image_to_upload1 == null &&
                                              image_to_upload2 == null &&
                                              image_to_upload3 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 20.w,
                                        child: fotoLicenciaConducirA == null
                                            ? const Text(
                                                "No se ha encontrado foto")
                                            : istrue2 == false
                                                ? CachedNetworkImage(
                                                    imageUrl:
                                                        "$fotoLicenciaConducirA",
                                                    placeholder:
                                                        (context, url) =>
                                                            circulatorCustom(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  )
                                                : Image.file(
                                                    image2!,
                                                    width: 35.w,
                                                  ),
                                      ),
                                    ),
                                    separador(),
                                    const Text("Licencia conducir lado B"),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais3 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais3 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );
                                          await openImagePicker3();

                                          if (image_to_upload3 == null) {
                                            setState(() {
                                              image_to_upload3 = image3;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload3 = image3;
                                            });
                                          }

                                          if (controllerName.text == '' &&
                                              controllerLastName.text == '' &&
                                              controllerEmail.text == '' &&
                                              controllerPhone.text == '' &&
                                              image_to_upload1 == null &&
                                              image_to_upload2 == null &&
                                              image_to_upload3 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 20.w,
                                        child: fotoLicenciaConducirB == null
                                            ? const Text(
                                                "No se ha encontrado foto")
                                            : istrue3 == false
                                                ? CachedNetworkImage(
                                                    imageUrl:
                                                        "$fotoLicenciaConducirB",
                                                    placeholder:
                                                        (context, url) =>
                                                            circulatorCustom(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  )
                                                : Image.file(
                                                    image3!,
                                                    width: 35.w,
                                                  ),
                                      ),
                                    ),
                                    separador(),
                                    Center(
                                      child: Text(
                                        "Foto carnet\n por ambos lados",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    const Text('lado 1'),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais4 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais4 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );

                                          await openImagePicker4();

                                          if (image_to_upload4 == null) {
                                            setState(() {
                                              image_to_upload4 = image4;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload4 = image4;
                                            });
                                          }

                                          if (controllerName.text == '' &&
                                              controllerLastName.text == '' &&
                                              controllerEmail.text == '' &&
                                              controllerPhone.text == '' &&
                                              image_to_upload1 == null &&
                                              image_to_upload2 == null &&
                                              image_to_upload3 == null &&
                                              image_to_upload4 == null &&
                                              image_to_upload5 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 35.w,
                                        child: istrue4 == false
                                            ? CachedNetworkImage(
                                                imageUrl: "$fotoCarnetA",
                                                placeholder: (context, url) =>
                                                    circulatorCustom(),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Icon(Icons.error),
                                              )
                                            : Image.file(
                                                image4!,
                                                width: 35.w,
                                              ),
                                      ),
                                    ),
                                    const Text('lado 2'),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais5 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais5 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );
                                          await openImagePicker5();

                                          if (image_to_upload5 == null) {
                                            setState(() {
                                              image_to_upload5 = image5;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload5 = image5;
                                            });
                                          }

                                          if (controllerName.text == '' &&
                                              controllerLastName.text == '' &&
                                              controllerEmail.text == '' &&
                                              controllerPhone.text == '' &&
                                              image_to_upload1 == null &&
                                              image_to_upload2 == null &&
                                              image_to_upload3 == null &&
                                              image_to_upload4 == null &&
                                              image_to_upload5 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 35.w,
                                        child: istrue5 == false
                                            ? CachedNetworkImage(
                                                imageUrl: "$fotoCarnetB",
                                                placeholder: (context, url) =>
                                                    circulatorCustom(),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Icon(Icons.error),
                                              )
                                            : Image.file(
                                                image5!,
                                                width: 35.w,
                                              ),
                                      ),
                                    ),
                                    separador(),
                                    const Text("Certificado antecedentes"),
                                    InkWell(
                                      onTap: () async {
                                        if (aprobado == "rechazado" ||
                                            aprobado == "rechasado") {
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais6 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais6 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );

                                          await openImagePicker6();

                                          if (image_to_upload6 == null) {
                                            setState(() {
                                              image_to_upload6 = image6;
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload6 = image6;
                                            });
                                          }

                                          if (image_to_upload6 == null) {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 20.w,
                                        child: certificadoAntecedentes == null
                                            ? const Text(
                                                "No se ha encontrado foto")
                                            : istrue6 == false
                                                ? CachedNetworkImage(
                                                    imageUrl:
                                                        "$certificadoAntecedentes",
                                                    placeholder:
                                                        (context, url) =>
                                                            circulatorCustom(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  )
                                                : Image.file(
                                                    image6!,
                                                    width: 35.w,
                                                  ),
                                      ),
                                    ),
                                    separador(),
                                    Row(
                                      children: [
                                        Text(
                                          "Partner: ",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 17.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          partner.toString() == 'taxiseguro'
                                              ? "ningun partner"
                                              : "$partner",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 17.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    separador(),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
      floatingActionButton: aprobado == "aprobado" || aprobado == "revision"
          ? Container()
          : SpeedDial(
              icon: Icons.check,
              backgroundColor: Colors.amber.shade700,
              foregroundColor: Colors.deepPurple,
              overlayOpacity: 0.5,
              elevation: 8.0,
              onPress: () {
                if (iboolean == true) {
                  modificarDatos(
                    homeProvider,
                    verificarConductorProvider,
                    datas,
                    name,
                    lastName,
                    dni,
                    email,
                    phone,
                    tipoLicencia,
                    restriccion,
                    fechaVencimientoLicencia,
                    fotoCarnetA,
                    fotoCarnetB,
                    fotoPerfil,
                    fotoLicenciaConducirA,
                    fotoLicenciaConducirB,
                    certificadoAntecedentes,
                  );
                } else {
                  solicitarRevision(dni);
                }
              },
              shape: const CircleBorder(),
              children: const [],
            ),
    );
  }

  Widget showNames(names, aprobado, controller, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            controller: controller,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Nombre',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showLastNames(lastNames, aprobado, controller, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            controller: controller,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Apellido',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showDni(dni, aprobado, controller, rutValidate, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          Focus(
            onFocusChange: (value) {
              if (value == false) {
                String dad = rutValidate(controller.text);

                controller.text = dad.toString();
              }
            },
            child: TextFormField(
              enabled: aprobado == "rechazado" || aprobado == "rechasado"
                  ? true
                  : false,
              controller: controller,
              onChanged: (value) {
                if (controllerName.text == '' &&
                    controllerLastName.text == '' &&
                    controllerEmail.text == '' &&
                    controllerPhone.text == '' &&
                    controllerTipoLicencia.text == '' &&
                    controllerRestriccion.text == '' &&
                    image_to_upload1 == null &&
                    image_to_upload2 == null &&
                    image_to_upload3 == null &&
                    image_to_upload4 == null &&
                    image_to_upload5 == null) {
                  homeProvider.isbutton = false;
                  iboolean = false;
                } else {
                  homeProvider.isbutton = true;
                  iboolean = true;
                }

                setState(() {});
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                labelText: 'Rut',
                hintStyle: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showEmail(email, controller, aprobado, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            controller: controller,
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Email',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showPhone(phone, controller, aprobado, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            controller: controller,
            maxLength: 12,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            keyboardType: TextInputType.phone,
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Telefono',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showPasswod(password) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          const Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("Contraseña"),
            ],
          ),
          TextFormField(
            enabled: false,
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: '$password',
              hintStyle: const TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showPhotoPerfil(perfil) {
    return Column(
      children: [
        const Text("Foto perfil"),
        SizedBox(
          width: 20.w,
          child: Image.network(perfil, width: 20.w),
        ),
      ],
    );
  }

  Widget showAprobacion(aprobacion, controllerAprobacion) {
    return SizedBox(
      width: 20.w,
      child: TextFormField(
        controller: controllerAprobacion,
        enabled: true,
        decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Estado del usuario: $aprobacion',
          hintStyle: const TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget showFotoCarnet(fotoCarnetA, fotoCarnetB) {
    return Column(
      children: [
        const Text("foto carnet lado A"),
        SizedBox(
          width: 20.w,
          child: Image.network(fotoCarnetA, width: 20.w),
        ),
        separador(),
        const Text("foto carnet lado B"),
        SizedBox(
          width: 20.w,
          child: Image.network(fotoCarnetB, width: 20.w),
        ),
      ],
    );
  }

  Widget showTipoLicencia(
      tipoLicenciaConducir, aprobado, controller, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            controller: controller,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Tipo licencia',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showRestriccion(restriccion, controller, aprobado, homeProvider) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          TextFormField(
            controller: controller,
            enabled: aprobado == "rechazado" || aprobado == "rechasado"
                ? true
                : false,
            onChanged: (value) {
              if (controllerName.text == '' &&
                  controllerLastName.text == '' &&
                  controllerEmail.text == '' &&
                  controllerPhone.text == '' &&
                  controllerTipoLicencia.text == '' &&
                  controllerRestriccion.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null &&
                  image_to_upload5 == null) {
                homeProvider.isbutton = false;
                iboolean = false;
              } else {
                homeProvider.isbutton = true;
                iboolean = true;
              }
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Restriccion',
              hintStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showFechaVencimientoLicencia(vencimientoLicencia, aprobado, controller,
      context, homeProvider, iboolean) {
    return SizedBox(
      width: 20.w,
      child: Column(
        children: [
          aprobado == "rechazado" || aprobado == "rechasado"
              ? InkWell(
                  onTap: () async {
                    await getCalendaryConductores(context, controller,
                        vencimientoLicencia, homeProvider, iboolean);

                    if (controllerFechaVencimiento.text ==
                        vencimientoLicencia) {
                      homeProvider.isbutton = false;

                      iboolean = false;
                    } else {
                      homeProvider.isbutton = true;

                      iboolean = true;
                    }
                  },
                  child: TextFormField(
                    enabled: false,
                    controller: controller,
                    onChanged: (value) {},
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      labelText: 'Fecha vencimiento licencia',
                      hintStyle: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                )
              : TextFormField(
                  enabled: false,
                  controller: controller,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    hintText: '$vencimientoLicencia',
                    hintStyle: const TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  Widget showFotoLicenciaConducir(fotoLicenciaA, fotoLicenciaB) {
    return Column(
      children: [
        const Text("Licencia conducir lado A"),
        SizedBox(
          width: 20.w,
          child: Image.network(fotoLicenciaA, width: 20.w),
        ),
        separador(),
        const Text("Licencia conducir lado B"),
        SizedBox(
          width: 20.w,
          child: Image.network(fotoLicenciaB, width: 20.w),
        ),
      ],
    );
  }
}

Widget aprobarVerificacion(
    controllerAprobacion,
    dni,
    context,
    controllerEmail,
    controllerPhone,
    controllerRestriccion,
    aprobado,
    controllerName,
    controllerLastName,
    controllerRut,
    controllerTipoLicencia,
    controllerFechaVencimiento,
    providerUrl,
    imageToUpload1,
    imageToUpload2,
    imageToUpload3,
    imageToUpload4,
    imageToUpload5,
    providerSet,
    namej,
    lastNamej,
    emailj,
    phonej,
    fotoPerfilj,
    tipoLicenciaj,
    restriccionj,
    fechaVencimientoj,
    fotoLicenciaAj,
    fotoLicenciaBj,
    fotoCarnetAj,
    fotoCarnetBj,
    verificarConductorProvider) {
  return ElevatedButton(
    style: const ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(Colors.black),
    ),
    onPressed: () async {},
    child: const Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        "Modificar datos",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}

Widget solicitarRevision(aprobacion, dni, context) {
  return ElevatedButton(
    style: const ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(Colors.black),
    ),
    onPressed: () async {
      showTopSnackBar(
        Overlay.of(context),
        const CustomSnackBar.success(
          message: "Solicitud enviada",
        ),
      );

      Navigator.pop(context);
    },
    child: const Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        "Solicitar revision",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}

Future<bool> alertShow(context, dni) async {
  bool action = await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.black,
      title: const Text(
        "Alerta !!!",
        style: TextStyle(color: Colors.white),
      ),
      content: const Text(
        "Esta seguro que desea salir ???",
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () => Navigator.pop(context, false),
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white),
          ),
        ),
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () async {
            final SharedPreferences prefs =
                await SharedPreferences.getInstance();

            var modificado =
                await updateEstadoConductor('desconectado', dni.text);

            await prefs.remove('idsession');
            await prefs.remove('user_rut');
            await prefs.remove('user_password');
            await prefs.remove('tipo');
            await prefs.remove('id');

            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => LoginConductor()),
              (route) => false,
            );
          },
          child: const Text(
            "Si",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    ),
  );

  return action;
}
