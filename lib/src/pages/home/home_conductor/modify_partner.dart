import 'package:admin_taxi_seguro/src/apis/api_auditor/update_aprobacion/update_aprobacion_conductor.dart';
import 'package:admin_taxi_seguro/src/pages/login/login_conductor/login_conductor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:searchfield/searchfield.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../apis/api_auditor/update_estado.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';

class ModifyPartner extends StatefulWidget {
  const ModifyPartner({super.key});

  @override
  State<ModifyPartner> createState() => _ModifyPartnerState();
}

class _ModifyPartnerState extends State<ModifyPartner> {
  TextEditingController controllerPartner = TextEditingController();

  Dio dio = Dio();

  List<dynamic> losPartner = [];

  getAllPartners() async {
    Response resp = await dio.get("$url/getAllPartner");
    var respuesta = resp.data;

    List listaMapas = respuesta["Partners"];

    listaMapas.forEach((element) {
      losPartner.add(element["nombre"]);
    });

    setState(() {});

    setState(() {});
  }

  @override
  void initState() {
    getAllPartners();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<AuditorProvider>(context);
    var dni = datas.getDatasVerificarOneConductor()["dni"];
    print("${datas.getDatasVerificarOneConductor()}");
    var partner = datas.getDatasVerificarOneConductor()["Partner"];
    List<dynamic> listafinalquery2 = [];

    losPartner.forEach((element) {
      if (element.toString() == 'taxiseguro') {
        listafinalquery2.add('ningun partner');
      } else {
        listafinalquery2.add(element.toString());
      }
    });
    return Scaffold(
      body: ListView(
        children: [
          Column(
            children: [
              SizedBox(
                height: 35.h,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Seleccione el partner al cual quiere cambiar\n",
                  style:
                      TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
                ),
              ),
              separador(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w),
                child: SearchField<dynamic>(
                    searchStyle: TextStyle(fontWeight: FontWeight.bold),
                    controller: controllerPartner,
                    onSearchTextChanged: (query) {
                      if (query == 'Ningun partner') {}

                      List<dynamic> listafinalquery = [];

                      losPartner.forEach((element) {
                        if (element.toString() == 'taxiseguro') {
                          listafinalquery.add('Ningun partner');
                        } else {
                          listafinalquery.add(element.toString());
                        }
                      });

                      final filter = listafinalquery
                          .where((element) => element
                              .toLowerCase()
                              .contains(query.toLowerCase()))
                          .toList();
                      return filter
                          .map((e) => SearchFieldListItem<String>(e,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 4.0),
                                child: Text(e,
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.deepPurple)),
                              )))
                          .toList();
                    },
                    textInputAction: TextInputAction.done,
                    suggestionsDecoration: SuggestionDecoration(
                      padding: const EdgeInsets.all(4),
                      border: Border.all(color: Colors.amber.shade700),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    itemHeight: 50,
                    suggestionState: Suggestion.expand,
                    hint: 'Seleccione partner',
                    //searchStyle: TextStyle(color: Colors.deepPurple),

                    suggestions: listafinalquery2
                        .map(
                          (e) => SearchFieldListItem<dynamic>(
                            e,
                            child: Center(child: Text(e)),
                          ),
                        )
                        .toList()),
              ),
              SizedBox(
                height: 20.h,
              ),
              InkWell(
                onTap: () async {
                  final SharedPreferences prefs =
                      await SharedPreferences.getInstance();

                  var modificado =
                      await updateEstadoConductor('desconectado', dni);

                  await prefs.remove('idsession');
                  await prefs.remove('user_rut');
                  await prefs.remove('user_password');
                  await prefs.remove('tipo');
                  await prefs.remove('id');
                  print("el partner antes: $partner");
                  var respPartner = await updatePartnerConductor(dni, controllerPartner.text == 'ningun partner' ? 'taxiseguro' : controllerPartner.text, partner);
                  print("el respPartner es: $respPartner");
                  var response = await updateAprobacionConductor('revision',
                      dni,'true');
                  print("el response es: $response");
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            const LoginConductor()),
                        (route) =>
                    false, // Esto elimina todas las pantallas anteriores
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.amber.shade700,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Solicitar Cambio"),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
