import 'dart:convert';
import 'package:admin_taxi_seguro/src/pages/validaterutall.dart';
import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../../apis/api_duenio_vehiculo/get_all_datas_vehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/home_provider.dart';
import '../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../tools/separador.dart';

class AniadirConductores extends StatefulWidget {
  const AniadirConductores({super.key});

  @override
  State<AniadirConductores> createState() => _AniadirConductoresState();
}

class _AniadirConductoresState extends State<AniadirConductores> {
  TextEditingController controllerRut = TextEditingController();
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();

  String? selectedValue;
  bool isloadingrut = false;
  List allrutfinal = [];
  bool validateRutChile = false;
  bool validate2 = false;
  bool aniadirconductores = false;

  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  @override
  Widget build(BuildContext context) {
    var homeProvider = Provider.of<HomeProvider>(context);
    var dniProvider = Provider.of<LoginProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);

    var lista = dniProvider.getAllDatasPatentes;

    List listadatas = [];

    lista.forEach((key, value) {
      listadatas.add(value);
    });

    homeProvider.setLista = listadatas;
    final List<dynamic> items = homeProvider.getLista;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        foregroundColor: Colors.deepPurple,
        title: Center(
          child: Text(
            "Añadir conductor\n  para el vehiculo",
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.bold,
              color: Colors.deepPurple,
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: aniadirconductores == true
            ? circulatorCustom()
            : ListView(
                children: [
                  separador(),
                  DropdownButtonHideUnderline(
                    child: DropdownButton2<dynamic>(
                      isExpanded: true,
                      hint: Row(
                        children: [
                          const Icon(
                            Icons.list,
                            size: 16,
                            color: Colors.deepPurple,
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: Text(
                              'Selecciona una patente',
                              style: TextStyle(
                                fontSize: 18.sp,
                                fontFamily: "Times New Roman",
                                fontWeight: FontWeight.bold,
                                color: Colors.deepPurple,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      items: items
                          .map((dynamic item) => DropdownMenuItem<dynamic>(
                                value: item,
                                child: Text(
                                  item,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.deepPurple,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ))
                          .toList(),
                      value: selectedValue,
                      onChanged: (dynamic value) async {
                        setState(() {
                          isloadingrut = true;
                          selectedValue = value;
                          allrutfinal.clear();
                        });
                        var datasvehicle =
                            await getAllOneVehicle(selectedValue);
                        if (datasvehicle == null) {
                          setState(() {
                            isloadingrut = false;
                          });
                        } else {
                          setState(() {
                            List allrut = jsonDecode(
                                datasvehicle["rutconsuctores"].toString());

                            allrut.forEach((element) {
                              allrutfinal.add(element["id_apd"]);
                            });

                            isloadingrut = false;
                          });
                        }
                      },
                      buttonStyleData: ButtonStyleData(
                        height: 50,
                        width: 160,
                        padding: const EdgeInsets.only(left: 14, right: 14),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          border: Border.all(
                            color: Colors.amber.shade700,
                          ),
                          color: Colors.amber.shade700,
                        ),
                        elevation: 2,
                      ),
                      iconStyleData: const IconStyleData(
                        icon: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.deepPurple,
                        ),
                        iconSize: 16,
                        iconEnabledColor: Colors.black,
                        iconDisabledColor: Colors.grey,
                      ),
                      dropdownStyleData: DropdownStyleData(
                        maxHeight: 200,
                        width: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          color: Colors.amber.shade700,
                        ),
                        offset: const Offset(-20, 0),
                        scrollbarTheme: ScrollbarThemeData(
                          radius: const Radius.circular(40),
                          thickness: MaterialStateProperty.all<double>(6),
                          thumbVisibility:
                              MaterialStateProperty.all<bool>(true),
                        ),
                      ),
                      menuItemStyleData: const MenuItemStyleData(
                        height: 40,
                        padding: EdgeInsets.only(left: 14, right: 14),
                      ),
                    ),
                  ),
                  separador(),
                  selectedValue == null
                      ? Container()
                      : Column(
                          children: [
                            Text(
                              "Conductores asociados a la patente $selectedValue",
                              style: TextStyle(
                                  fontFamily: 'Times New Roman',
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.bold),
                            ),
                            Container(
                              width: 100.w,
                              height: 20.h,
                              child: isloadingrut == true
                                  ? Center(child: CircularProgressIndicator())
                                  : ListView.builder(
                                      itemCount: allrutfinal.length,
                                      itemBuilder: (context, index) {
                                        return allrutfinal[index] == null
                                            ? Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Center(
                                                    child: Text(
                                                  "No se ha encontrado ningun rut ",
                                                  style: TextStyle(
                                                      fontSize: 16.sp,
                                                      fontFamily:
                                                          "Times New Roman",
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )),
                                              )
                                            : Card(
                                                child: ListTile(
                                                  leading: Text(
                                                    "${index + 1}",
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        fontFamily:
                                                            "Times New Roman",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  title: Center(
                                                      child: Text(
                                                    '${allrutfinal[index]}',
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        fontFamily:
                                                            "Times New Roman",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )),
                                                ),
                                              );
                                      },
                                    ),
                            ),
                          ],
                        ),
                  separador(),
                  Focus(
                    onFocusChange: (value) {
                      if (value == false) {
                        String dad = rutValidate(controllerRut.text);

                        controllerRut.text = dad.toString();
                      }
                    },
                    child: _textFielFillUpRut(
                        controllerRut,
                        'R.U.T',
                        'Sin puntos ni guion',
                        Icons.info_outline_rounded,
                        rutValidate,
                        verificaDuenioProvider),
                  ),
                  separador(),
                  InkWell(
                    onTap: () async {
                      setState(() {
                        aniadirconductores = true;
                      });

                      if (selectedValue == null) {
                        aniadirconductores = false;
                        showTopSnackBar(
                          Overlay.of(context),
                          CustomSnackBar.error(
                            message: "Debe seleccionar una patente valida",
                          ),
                        );
                      } else {
                        if (controllerRut.text == '') {
                          aniadirconductores = false;
                          showTopSnackBar(
                            Overlay.of(context),
                            CustomSnackBar.error(
                              message: "Debe ingresar un rut",
                            ),
                          );
                        } else {
                          bool isverifi = verifiRut(
                              controllerRut,
                              rutValidate,
                              validate2,
                              isRutValid,
                              formatRut,
                              verificaDuenioProvider);
                          if (isverifi) {
                            var result;
                            var name = controllerName.text;

                            var lastName = controllerLastName.text;

                            var rut = controllerRut.text;

                            var selectedPatente = selectedValue;

                            var datas = await getAllOneVehicle(selectedPatente);

                            if (datas == null) {
                              setState(() {
                                aniadirconductores = false;
                              });

                              showTopSnackBar(
                                Overlay.of(context),
                                CustomSnackBar.error(
                                  message:
                                      "No existe ningun vehiculo con la patente seleccionada porfavor registre uno",
                                ),
                              );
                            } else {
                              Map data = {};
                              Map datasName = {};
                              Map datasRut = {};
                              Map datasLastName = {};

                              var ad =
                                  await getAllRutConductores(selectedPatente);

                              datasRut = {};

                              List listaName = datasName.values.toList();

                              List listaLastName =
                                  datasLastName.values.toList();

                              List listaDni = datasRut.values.toList();

                              listaName.add(name);
                              listaLastName.add(lastName);

                              listaDni.add(rut);

                              if (ad == null) {
                              } else {
                                for (var i = 0; i < ad.length; i++) {
                                  listaDni.add(ad[i]["id_apd"]);
                                }
                              }

                              listaDni.remove(null);
                              int count = 0;

                              List listaMup = [];
                              for (var i = 0; i < listaName.length; i++) {
                                for (var i = 0; i < listaDni.length; i++) {
                                  result = listaDni.map((item) {
                                    return {"id_apd": item};
                                  }).toList();
                                }
                              }

                              var object = await getOneDatasConductor(
                                  controllerRut.text);

                              var datasvehicle =
                                  await getAllOneVehicle(selectedValue);

                              List losruts =
                                  jsonDecode(datasvehicle["rutconsuctores"]);

                              List lostruts2 = [];

                              if (datasvehicle["rutconsuctores"].toString() ==
                                  "{}") {
                                lostruts2.add(losruts[0]["id_apd"]);
                              } else {
                                for (var i = 0; i < losruts.length; i++) {
                                  lostruts2.add(losruts[i]["id_apd"]);
                                }

                                bool asignado = false;

                                lostruts2.forEach((element) {
                                  if (controllerRut.text == element) {
                                    setState(() {
                                      asignado = true;
                                    });
                                  } else {}
                                });

                                if (asignado == true) {
                                  aniadirconductores = false;
                                  showTopSnackBar(
                                    Overlay.of(context),
                                    CustomSnackBar.error(
                                      message:
                                          "el rut: ${controllerRut.text} ya se encuentra asignado al vehiculo con la patente\n $selectedValue",
                                    ),
                                  );
                                } else {
                                  if (object == null) {
                                    setState(() {
                                      aniadirconductores = false;
                                    });
                                    showTopSnackBar(
                                      Overlay.of(context),
                                      CustomSnackBar.error(
                                        message:
                                            "No existe ningun conductor con el rut: ${controllerRut.text} en la base de datos",
                                      ),
                                    );
                                  } else {
                                    dniProvider.saveconductoresvehicle = true;
                                    var adata = await addRutConductores(
                                        result, selectedPatente);
                                    aniadirconductores = false;
                                    showTopSnackBar(
                                      Overlay.of(context),
                                      const CustomSnackBar.success(
                                        message:
                                            "Conductor ingresado con exito",
                                      ),
                                    );
                                    dniProvider.saveconductoresvehicle = false;
                                    Navigator.pop(context);
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    },
                    child: Container(
                      width: 20.w,
                      decoration: BoxDecoration(
                        color: Colors.amber.shade700,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Center(
                          child: Text(
                            "Registrar",
                            style: TextStyle(
                              color: Colors.deepPurple,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _textFielFillUpRut(
      TextEditingController cont,
      String labelText,
      String hintText,
      IconData icons,
      Function rutValidate,
      verificaDuenioProvider) {
    return Container(
      child: TextFormField(
        maxLength: 12,
        autocorrect: false,
        textInputAction: TextInputAction.next,
        controller: cont,
        keyboardType: TextInputType.datetime,
        inputFormatters: [RutFormatter()],
        decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: hintText,
            labelText: labelText,
            suffixIcon: Icon(
              icons,
              color: Colors.black,
            )),
        onChanged: (value) {
          var formattedRut = formatRut(controllerRut.text);
          verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);
        },
        onTap: () {
          validate2 = false;
          cont.text = deFormatRut(cont.text);
        },
        autovalidateMode: AutovalidateMode.onUserInteraction,
        onEditingComplete: () {
          if (validateRutChile == true) {
            String dad = rutValidate(cont.text);

            validate2 = true;

            FocusScope.of(context!).nextFocus();
          }
        },
        validator: (value) {
          validate2 = false;
          if (value!.isEmpty) {
            return 'Debes Ingresar RUT Valido';
          } else if (value.length < 7) {
            return 'Debes Ingresar RUT Valido';
          } else if ((value.length >= 8)) {
            value = rutValidate(value);
            bool isveri = isRutValid(value!);

            if (isveri) {
              validate2 = true;
              return null;
            } else {
              return 'si termina en K reemplaza por un 0';
            }
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        },
      ),
    );
  }
}
