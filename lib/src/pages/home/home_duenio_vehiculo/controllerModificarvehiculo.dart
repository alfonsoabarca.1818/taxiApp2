import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../apis/api_duenio_vehiculo/get_all_datas_vehicle.dart';
import '../../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../../tools/url_base.dart';
import 'funcionesmodificarvehiculo.dart';

modificarDatos(
  homeProvider,
  verificarConductorProvider,
  marca,
  modelo,
  motivo,
  patente,
  color,
  fotoRtecnica,
  patenteRNSTP,
  fotoRNSTP,
  fechaVenciomientoRtecnicca,
  fotoCertificadoInscripcion,
  fotoPermisoCirculacion,
  fechaVencimientoPermiso,
  dniProvider,
  estadoVehiculoRNSTP,
  certificadoTaximetro,
  fotoTaximetro,
  context,
  controllerFechaVencimientoRtecnicaMes,
  controllerFechaVencimientoRtecnicaDia,
  controllerFechaVencimientoPcirculacionMes,
  controllerFechaVencimientoPcirculacionDia,
  controllerFechaVencimientoRtecnicaAnio,
  controllerFechaVencimientoPcirculacionAnio,
  iboolean,
  image_to_upload1,
  image_to_upload2,
  image_to_upload3,
  image_to_upload4,
  image_to_upload5,
  controllerMarca,
  controllerModelo,
  controllerFechaRtecnica,
  controllerColor,
  selectedValue2,
  controllerFechaVencimientoPcirculacion,
) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.amberAccent.shade700,
      title: Text(
        "¡¡¡ATENCIÓN!!!",
        style: TextStyle(
            color: Colors.deepPurple,
            fontSize: 20.sp,
            fontWeight: FontWeight.bold),
      ),
      content: Text(
        '¿Estas seguro que quieres modificar datos?',
        style: TextStyle(
            color: Colors.deepPurple,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold),
      ),
      actions: [
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
          ),
          onPressed: () async {
            int rtecnicames = int.parse(
                controllerFechaVencimientoRtecnicaMes.text.length.toString());
            int rtecnicadia = int.parse(
                controllerFechaVencimientoRtecnicaDia.text.length.toString());
            int postulacionmes = int.parse(
                controllerFechaVencimientoPcirculacionMes.text.length
                    .toString());
            int postulaciondia = int.parse(
                controllerFechaVencimientoPcirculacionDia.text.length
                    .toString());
            String fechaVrTecnicas = '';
            String fechaVpCirculacion = '';

            if (rtecnicames < 2) {
              if (rtecnicadia < 2) {
                fechaVrTecnicas = controllerFechaVencimientoRtecnicaAnio.text +
                    "-" +
                    "0" +
                    controllerFechaVencimientoRtecnicaMes.text +
                    "-" +
                    "0" +
                    controllerFechaVencimientoRtecnicaDia.text;
              } else {
                fechaVrTecnicas = controllerFechaVencimientoRtecnicaAnio.text +
                    "-" +
                    "0" +
                    controllerFechaVencimientoRtecnicaMes.text +
                    "-" +
                    controllerFechaVencimientoRtecnicaDia.text;
              }

            } else {
              if (rtecnicadia < 2) {
                fechaVrTecnicas = controllerFechaVencimientoRtecnicaAnio.text +
                    "-" +
                    controllerFechaVencimientoRtecnicaMes.text +
                    "-" +
                    "0" +
                    controllerFechaVencimientoRtecnicaDia.text;
              } else {
                fechaVrTecnicas = controllerFechaVencimientoRtecnicaAnio.text +
                    "-" +
                    controllerFechaVencimientoRtecnicaMes.text +
                    "-" +
                    controllerFechaVencimientoRtecnicaDia.text;
              }

            }

            if (postulacionmes < 2) {
              if (postulaciondia < 2) {
                fechaVpCirculacion =
                    controllerFechaVencimientoPcirculacionAnio.text +
                        "-" +
                        "0" +
                        controllerFechaVencimientoPcirculacionMes.text +
                        "-" +
                        "0" +
                        controllerFechaVencimientoPcirculacionDia.text;
              } else {
                fechaVpCirculacion =
                    controllerFechaVencimientoPcirculacionAnio.text +
                        "-" +
                        "0" +
                        controllerFechaVencimientoPcirculacionMes.text +
                        "-" +
                        controllerFechaVencimientoPcirculacionDia.text;
              }

            } else {
              if (postulaciondia < 2) {
                fechaVpCirculacion =
                    controllerFechaVencimientoPcirculacionAnio.text +
                        "-" +
                        controllerFechaVencimientoPcirculacionMes.text +
                        "-" +
                        "0" +
                        controllerFechaVencimientoPcirculacionDia.text;
              } else {
                fechaVpCirculacion =
                    controllerFechaVencimientoPcirculacionAnio.text +
                        "-" +
                        controllerFechaVencimientoPcirculacionMes.text +
                        "-" +
                        controllerFechaVencimientoPcirculacionDia.text;
              }

            }

            homeProvider.modificarvehiculo = true;

            homeProvider.isbutton = false;
            Navigator.pop(context);

            iboolean = false;
            homeProvider.iboolean2 = false;


            verificarConductorProvider.setLoading = true;


            if (image_to_upload1 != null ||
                image_to_upload2 != null ||
                image_to_upload3 != null ||
                image_to_upload4 != null ||
                image_to_upload5 != null) {
              if (image_to_upload1 != null &&
                  image_to_upload2 != null &&
                  image_to_upload3 != null &&
                  image_to_upload4 != null &&
                  image_to_upload5 != null) {
                var save1 =
                    await uuploadImagevehiculo1(image_to_upload1, patenteRNSTP);

                var save2 =
                    await uuploadImagevehiculo2(image_to_upload2, patenteRNSTP);
                var save3 =
                    await uuploadImagevehiculo3(image_to_upload3, patenteRNSTP);
                var save4 =
                    await uuploadImagevehiculo4(image_to_upload4, patenteRNSTP);
                var save5 =
                    await uuploadImagevehiculo5(image_to_upload5, patenteRNSTP);

                CachedNetworkImage.evictFromCache("$fotoRtecnica");
                CachedNetworkImage.evictFromCache("$fotoRNSTP");
                CachedNetworkImage.evictFromCache(
                    "$fotoCertificadoInscripcion");
                CachedNetworkImage.evictFromCache("$fotoPermisoCirculacion");
                CachedNetworkImage.evictFromCache("$fotoTaximetro");

                var lastIndex1 = save1.toString().length;

                var lastIndex2 = save2.toString().length;

                var lastIndex3 = save3.toString().length;

                print("el $save3 lastindex3 es: $lastIndex3");

                var lastIndex4 = save4.toString().length;

                print("el $save4 lastindex4 es: $lastIndex4");

                var lastIndex5 = save5.toString().length;

                print("el $save5 lastindex5 es: $lastIndex5");

                var data = {
                  "marca": controllerMarca.text == ''
                      ? '$marca'
                      : "${controllerMarca.text}",
                  "modelo": controllerModelo.text == ''
                      ? '$modelo'
                      : "${controllerModelo.text}",
                  "revicionTecnica":
                      "{\"foto\": \"${image_to_upload1 == null ? fotoRtecnica : '$url3/' + save1.toString().substring(29, lastIndex1)}\", \"fechavencimiento\": \"${controllerFechaRtecnica.text == '' ? fechaVenciomientoRtecnicca : fechaVrTecnicas}\"}",
                  "color":
                      "${controllerColor.text == '' ? color : controllerColor.text}",
                  "certificadoInscripcionRNSTP":
                      "{\"patente\": \"$patenteRNSTP\",\"estadovehicle\": \"${selectedValue2 == 'Selecciona un estado' || selectedValue2 == null ? estadoVehiculoRNSTP : selectedValue2}\",\"fotoCertificadoRNSTP\":\"${image_to_upload2 == null ? fotoRNSTP : '$url3/' + save2.toString().substring(29, lastIndex2)}\"}",
                  'certificadoInscripcion':
                      '${image_to_upload3 == null ? fotoCertificadoInscripcion : '$url3/' + save3.toString().substring(29, lastIndex3)} }',
                  "permisocirculacion":
                      "{\"fotoPermisoCirculacion\": \"${image_to_upload3 == null ? fotoPermisoCirculacion : '$url3/' + save4.toString().substring(29, lastIndex4)}\", \"fechaVencimientoPermiso\": \"${controllerFechaVencimientoPcirculacion.text == '' ? fechaVencimientoPermiso : fechaVpCirculacion}\"}",
                  "motivo": "$motivo",
                  "certificadoTaximetro":
                      "${image_to_upload5 == null ? certificadoTaximetro : '$url3/' + save5.toString().substring(29, lastIndex5)}", //save5.toString().substring(29, lastIndex5)
                };
                await updateVehicle(patenteRNSTP, data);
                var datas = await getAllOneVehicle("$patenteRNSTP");

                dniProvider.setDatasVehicle(data);


                verificarConductorProvider.setLoading = false;


                homeProvider.isbutton = false;

                homeProvider.modificarvehiculo = false;
              } else {
                if (image_to_upload1 != null &&
                    image_to_upload2 != null &&
                    image_to_upload3 != null &&
                    image_to_upload4 != null) {
                  var save1 = await uuploadImagevehiculo1(
                      image_to_upload1, patenteRNSTP);
                  var save2 = await uuploadImagevehiculo2(
                      image_to_upload2, patenteRNSTP);
                  var save3 = await uuploadImagevehiculo3(
                      image_to_upload3, patenteRNSTP);
                  var save4 = await uuploadImagevehiculo4(
                      image_to_upload4, patenteRNSTP);
                  var save5 = await uuploadImagevehiculo5(
                      image_to_upload5, patenteRNSTP);

                  CachedNetworkImage.evictFromCache("$fotoRtecnica");
                  CachedNetworkImage.evictFromCache("$fotoRNSTP");
                  CachedNetworkImage.evictFromCache(
                      "$fotoCertificadoInscripcion");
                  CachedNetworkImage.evictFromCache("$fotoPermisoCirculacion");
                  CachedNetworkImage.evictFromCache("$fotoTaximetro");

                  var lastIndex1 = save1.toString().length;

                  print("el $save1 lastindex1 es: $lastIndex1");

                  var lastIndex2 = save2.toString().length;

                  print("el $save2 lastindex2 es: $lastIndex2");

                  var lastIndex3 = save3.toString().length;

                  print("el $save3 lastindex3 es: $lastIndex3");

                  var lastIndex4 = save4.toString().length;

                  print("el $save4 lastindex4 es: $lastIndex4");

                  var lastIndex5 = save5.toString().length;

                  print("el $save5 lastindex5 es: $lastIndex5");

                  var data = {
                    "marca": controllerMarca.text == ''
                        ? '$marca'
                        : "${controllerMarca.text}",
                    "modelo": controllerModelo.text == ''
                        ? '$modelo'
                        : "${controllerModelo.text}",
                    "revicionTecnica":
                        "{\"foto\": \"${image_to_upload1 == null ? fotoRtecnica : '$url3/' + save1.toString().substring(29, lastIndex1)}\", \"fechavencimiento\": \"${controllerFechaRtecnica.text == '' ? fechaVenciomientoRtecnicca : fechaVrTecnicas}\"}",
                    "color":
                        "${controllerColor.text == '' ? color : controllerColor.text}",
                    "certificadoInscripcionRNSTP":
                        "{\"patente\": \"$patenteRNSTP\",\"estadovehicle\": \"${selectedValue2 == 'Selecciona un estado' || selectedValue2 == null ? estadoVehiculoRNSTP : selectedValue2}\",\"fotoCertificadoRNSTP\":\"${image_to_upload2 == null ? fotoRNSTP : '$url3/' + save2.toString().substring(29, lastIndex2)}\"}",
                    'certificadoInscripcion':
                        '${image_to_upload3 == null ? fotoCertificadoInscripcion : '$url3/' + save3.toString().substring(29, lastIndex3)}',
                    "permisocirculacion":
                        "{\"fotoPermisoCirculacion\": \"${image_to_upload4 == null ? fotoPermisoCirculacion : '$url3/' + save4.toString().substring(29, lastIndex4)}\", \"fechaVencimientoPermiso\": \"${controllerFechaVencimientoPcirculacion.text == '' ? fechaVencimientoPermiso : fechaVpCirculacion}\"}",
                    "motivo": "$motivo",
                    "certificadoTaximetro":
                        "${image_to_upload5 == null ? certificadoTaximetro : '$url3/' + save5.toString().substring(29, lastIndex5)}", //save5.toString().substring(29, lastIndex5)
                  };
                  await updateVehicle(patenteRNSTP, data);
                  var datas = await getAllOneVehicle("$patenteRNSTP");

                  dniProvider.setDatasVehicle(data);

                  verificarConductorProvider.setLoading = false;

                  homeProvider.isbutton = false;

                  homeProvider.modificarvehiculo = false;
                } else {
                  var save1, save2, save3, save4, save5;
                  if (image_to_upload1 == null) {
                    print("el image to upload 1 es null");
                  } else {
                    CachedNetworkImage.evictFromCache("$fotoRtecnica");
                    print(
                        "el image to upload no es null antes del save 1 $patenteRNSTP jaja");
                    save1 = await uuploadImagevehiculo1(
                        image_to_upload1, patenteRNSTP);

                    print("imprimiendo despues del save1 $save1");

                    var lastIndex1 = save1.toString().length;

                    print("el $save1 lastindex1 es: $lastIndex1");
                  }

                  if (image_to_upload2 == null) {
                  } else {
                    CachedNetworkImage.evictFromCache("$fotoRNSTP");
                    save2 = await uuploadImagevehiculo2(
                        image_to_upload2, patenteRNSTP);
                    var lastIndex2 = save2.toString().length;

                    print("el $save2 lastindex2 es: $lastIndex2");
                  }

                  if (image_to_upload3 == null) {
                  } else {
                    print("el controller marca 11 es: ${controllerMarca.text}");
                    CachedNetworkImage.evictFromCache(
                        "$fotoCertificadoInscripcion");
                    save3 = await uuploadImagevehiculo3(
                        image_to_upload3, patenteRNSTP);

                    var lastIndex3 = save3.toString().length;

                    print("el $save3 lastindex3 es: $lastIndex3");
                  }

                  if (image_to_upload4 == null) {
                  } else {
                    print("el controller marca 12 es: ${controllerMarca.text}");
                    CachedNetworkImage.evictFromCache(
                        "$fotoPermisoCirculacion");

                    save4 = await uuploadImagevehiculo4(
                        image_to_upload4, patenteRNSTP);

                    var lastIndex4 = save4.toString().length;

                    print("el $save4 lastindex4 es: $lastIndex4");
                  }

                  if (image_to_upload5 == null) {
                  } else {
                    print("el controller marca 13 es: ${controllerMarca.text}");
                    CachedNetworkImage.evictFromCache("$fotoTaximetro");

                    save5 = await uuploadImagevehiculo5(
                        image_to_upload5, patenteRNSTP);
                    var lastIndex5 = save5.toString().length;

                    print("el $save5 lastindex5 es: $lastIndex5");
                  }
                  print("el controller marca 14 es: ${controllerMarca.text}");

                  var data = {
                    "marca": controllerMarca.text == ''
                        ? '$marca'
                        : "${controllerMarca.text}",
                    "modelo": controllerModelo.text == ''
                        ? '$modelo'
                        : "${controllerModelo.text}",
                    "revicionTecnica":
                        "{\"foto\": \"${image_to_upload1 == null ? fotoRtecnica : '$url3/' + save1.toString().substring(29, save1.toString().length)}\", \"fechavencimiento\": \"${controllerFechaRtecnica.text == '' ? fechaVenciomientoRtecnicca : fechaVrTecnicas}\"}",
                    "color":
                        "${controllerColor.text == '' ? color : controllerColor.text}",
                    "certificadoInscripcionRNSTP":
                        "{\"patente\": \"$patenteRNSTP\",\"estadovehicle\": \"${selectedValue2 == 'Selecciona un estado' || selectedValue2 == null ? estadoVehiculoRNSTP : selectedValue2}\",\"fotoCertificadoRNSTP\":\"${image_to_upload2 == null ? fotoRNSTP : '$url3/' + save2.toString().substring(29, save2.toString().length)}\"}",
                    'certificadoInscripcion':
                        '${image_to_upload3 == null ? fotoCertificadoInscripcion : '$url3/' + save3.toString().substring(29, save3.toString().length)}',
                    "permisocirculacion":
                        "{\"fotoPermisoCirculacion\": \"${image_to_upload4 == null ? fotoPermisoCirculacion : '$url3/' + save4.toString().substring(29, save4.toString().length)}\", \"fechaVencimientoPermiso\": \"${controllerFechaVencimientoPcirculacion.text == '' ? fechaVencimientoPermiso : fechaVpCirculacion}\"}",
                    "motivo": "$motivo",
                    "certificadoTaximetro":
                        "${image_to_upload5 == null ? certificadoTaximetro : '$url3/' + save5.toString().substring(29, save5.toString().length)}", //save5.toString().substring(29, lastIndex5)
                  };

                  await updateVehicle(patenteRNSTP, data);
                  var datas = await getAllOneVehicle("$patenteRNSTP");

                  dniProvider.setDatasVehicle(data);


                  verificarConductorProvider.setLoading = false;

                  print("el controller marca 15 es: ${controllerMarca.text}");

                  homeProvider.isbutton = false;

                  homeProvider.modificarvehiculo = false;

                  print("el controller marca 16 es: ${controllerMarca.text}");
                }
              }
            } else {
              var data = {
                "marca": controllerMarca.text == ''
                    ? '$marca'
                    : "${controllerMarca.text}",
                "modelo": controllerModelo.text == ''
                    ? '$modelo'
                    : "${controllerModelo.text}",
                "revicionTecnica":
                    "{\"foto\": \"$fotoRtecnica\", \"fechavencimiento\": \"${controllerFechaRtecnica.text == '' ? fechaVenciomientoRtecnicca : fechaVrTecnicas}\"}",
                "color":
                    "${controllerColor.text == '' ? color : controllerColor.text}",
                "certificadoInscripcionRNSTP":
                    "{\"patente\": \"$patenteRNSTP\",\"estadovehicle\": \"${selectedValue2 == 'Selecciona un estado' || selectedValue2 == null ? estadoVehiculoRNSTP : selectedValue2}\",\"fotoCertificadoRNSTP\":\"$fotoRNSTP\"}",
                'certificadoInscripcion': fotoCertificadoInscripcion,
                "permisocirculacion":
                    "{\"fotoPermisoCirculacion\": \"$fotoPermisoCirculacion\", \"fechaVencimientoPermiso\": \"${controllerFechaVencimientoPcirculacion.text == '' ? fechaVencimientoPermiso : fechaVpCirculacion}\"}",
                "motivo": "$motivo",
                "certificadoTaximetro": "$certificadoTaximetro",
              };
              dniProvider.setDatasVehicle(data);

              await updateVehicle(patenteRNSTP, data);
              var datas = await getAllOneVehicle("$patenteRNSTP");

              verificarConductorProvider.setLoading = false;

              homeProvider.isbutton = false;

              homeProvider.modificarvehiculo = false;
            }

          },
          child: const Text(
            "Si",
            style: TextStyle(color: Colors.white),
          ),
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.red.shade700),
          ),
          onPressed: () => Navigator.pop(context),
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    ),
  );
}
