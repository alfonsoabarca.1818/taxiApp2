import 'package:dio/dio.dart';

import '../../../tools/url_base.dart';

uuploadImagevehiculo1(imagepath, rut) async {
  print("llamando la funcion con la patente $rut");
  Dio dio = Dio();

  final serverUrl = '$url/uploadImageVehiculoRtecnica/$rut';
  var pathimages = imagepath.path;

  print("el path es: $pathimages");

  final formData = FormData.fromMap({
    'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
  });

  try {
    print("en el try del u");
    final response = await dio.post(
      serverUrl,
      data: formData,
    );

    if (response.statusCode == 200) {
      print('EL RESPONSE ES: $response');
      return response;
    } else {
      print('Error al subir la imagen');
    }
  } catch (e) {
    print('Error: $e');
  }
}

uuploadImagevehiculo2(imagepath, rut) async {
  print("llamando la funcion");
  Dio dio = Dio();

  final serverUrl = '$url/uploadImageVehiculoRnstp/$rut';
  var pathimages = imagepath.path;

  print("el path es 2: $pathimages");

  final formData = FormData.fromMap({
    'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
  });

  try {
    print("en el try del u 2");
    final response = await dio.post(
      serverUrl,
      data: formData,
    );

    if (response.statusCode == 200) {
      print('Imagen subida con éxito 2 $response');
      return response;
    } else {
      print('Error al subir la imagen 2');
    }
  } catch (e) {
    print('Error 2: $e');
  }
}

uuploadImagevehiculo3(imagepath, rut) async {
  print("llamando la funcion");
  Dio dio = Dio();

  final serverUrl = '$url/uploadImageVehiculoInscripcion/$rut';
  var pathimages = imagepath.path;

  print("el path es 2: $pathimages");

  final formData = FormData.fromMap({
    'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
  });

  try {
    print("en el try del u 2");
    final response = await dio.post(
      serverUrl,
      data: formData,
    );

    if (response.statusCode == 200) {
      print('Imagen subida con éxito 2 $response');
      return response;
    } else {
      print('Error al subir la imagen 2');
    }
  } catch (e) {
    print('Error 2: $e');
  }
}

uuploadImagevehiculo4(imagepath, rut) async {
  print("llamando la funcion");
  Dio dio = Dio();

  final serverUrl = '$url/uploadImageVehiculoCirculacion/$rut';
  var pathimages = imagepath.path;

  print("el path es 2: $pathimages");

  final formData = FormData.fromMap({
    'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
  });

  try {
    print("en el try del u 2");
    final response = await dio.post(
      serverUrl,
      data: formData,
    );

    if (response.statusCode == 200) {
      print('Imagen subida con éxito 2 $response');
      return response;
    } else {
      print('Error al subir la imagen 2');
    }
  } catch (e) {
    print('Error 2: $e');
  }
}

uuploadImagevehiculo5(imagepath, rut) async {
  print("llamando la funcion");
  Dio dio = Dio();

  final serverUrl = '$url/uploadImageVehiculoCertificadoTaximetro/$rut';
  var pathimages = imagepath.path;

  print("el path es 2: $pathimages");

  final formData = FormData.fromMap({
    'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
  });

  try {
    print("en el try del u 2");
    final response = await dio.post(
      serverUrl,
      data: formData,
    );

    if (response.statusCode == 200) {
      print('Imagen subida con éxito 2 $response');
      return response;
    } else {
      print('Error al subir la imagen 2');
    }
  } catch (e) {
    print('Error 2: $e');
  }
}
