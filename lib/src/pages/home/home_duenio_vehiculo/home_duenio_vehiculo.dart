import 'dart:convert';
import 'dart:io';
import 'package:admin_taxi_seguro/src/pages/home/home_duenio_vehiculo/modificar_vehiculo.dart';
import 'package:admin_taxi_seguro/src/pages/login/login_duenio_vehiculo/login_duenio_vehiculo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../../apis/api_auditor/update_aprobacion/update_aprobacion.dart';
import '../../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../../apis/api_duenio_vehiculo/get_all_datas_vehicle.dart';
import '../../../apis/api_duenio_vehiculo/login_duenio_vehiculo.dart';
import '../../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/home_provider.dart';
import '../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../tools/limpiarCache.dart';
import '../../../tools/loading.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';
import '../../resgister_patente/register_patente.dart';
import 'aniadir_conductores.dart';
import 'widgets/save_vehicle.dart';

class HomeDuenioVehiculo extends StatefulWidget {
  const HomeDuenioVehiculo({super.key});

  @override
  State<HomeDuenioVehiculo> createState() => _HomeDuenioVehiculoState();
}

class _HomeDuenioVehiculoState extends State<HomeDuenioVehiculo> {
  TextEditingController controllerName = TextEditingController();

  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerDni = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerConfirmPassword = TextEditingController();
  TextEditingController controllerPhotoPerfil = TextEditingController();

  FocusNode focusApellido = FocusNode();
  FocusNode focusRut = FocusNode();
  FocusNode focusCorreo = FocusNode();
  FocusNode focusTelefono = FocusNode();
  FocusNode focusPassword = FocusNode();
  FocusNode focusConfirmPassword = FocusNode();

  String nombre = '';
  String apellido = '';
  String correo = '';
  String telefono = '';

  darvariable(controllerName, controllerLastName, controllerPhone,
      controllerEmail, names, lastNames, phone, email) {
    if (iboolean) {
      nombre = controllerName.text;
      apellido = controllerLastName.text;
      correo = controllerEmail.text;
      telefono = controllerPhone.text;

      controllerName.text = nombre;
      controllerLastName.text = apellido;
      controllerPhone.text = telefono;
      controllerEmail.text = correo;
    } else {
      controllerName.text = names;
      controllerLastName.text = lastNames;
      controllerPhone.text = phone;
      controllerEmail.text = email;
    }
  }

  Dio dio = Dio();

  controllermo(names, lastNames, phone, email) {
    if (iboolean) {
      nombre = controllerName.text;
      apellido = controllerLastName.text;
      correo = controllerEmail.text;
      telefono = controllerPhone.text;

      controllerName.text = nombre;
      controllerLastName.text = apellido;
      controllerPhone.text = telefono;
      controllerEmail.text = correo;
    } else {
      controllerName.text = names;
      controllerLastName.text = lastNames;
      controllerPhone.text = phone;
      controllerEmail.text = email;
    }
  }

  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  bool iboolean = false;
  bool isyea = false;

  List listaPatentes = [];
  List<dynamic> listanew = [];
  var p;

  Map allOneData = {};
  Map patente2 = {};

  @override
  void initState() {
    super.initState();
  }

  File? image;
  File? image_to_upload;
  var imageUrl;
  bool istrue = false;
  bool camerais = false;
  bool camerais2 = false;

  File? image2;
  File? image_to_upload2;
  var imageUrl2;
  bool istrue2 = false;

  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais) {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image = File(pickedImage.path);
          istrue = true;
        });
      }
    } else {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image = File(pickedImage.path);
          istrue = true;
        });
      }
    }
  }

  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais) {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  File? image3;
  File? image_to_upload3;
  var imageUrl3;
  bool istrue3 = false;

  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais2) {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  var myGlobalKey = GlobalKey();

  solicitarRevision(dniProvider, homeProvider) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        alignment: AlignmentDirectional.centerStart,
        contentPadding: EdgeInsets.symmetric(horizontal: 20),
        backgroundColor: Colors.amberAccent.shade700,
        title: Center(
            child: Text(
          "ATENCIÓN",
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 20.sp,
              fontWeight: FontWeight.bold),
        )),
        content: Text(
          '¿Estas seguro que quieres solicitar revision?',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.deepPurple),
                  ),
                  onPressed: () async {
                    var response = await updateAprobacionDuenio(
                        'revision', dniProvider.getRut, 'ds');
                    dniProvider.setAprobacion = "revision";

                    var conectado = await updateEstadoDuenio(
                        dniProvider.getRut, "desconectado");

                    homeProvider.setLista = [];
                    showTopSnackBar(
                      Overlay.of(context),
                      const CustomSnackBar.success(
                        message: "Solicitud enviada",
                      ),
                    );

                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginDuenioVehiculo()),
                      (route) => false,
                    );
                  },
                  child: const Text(
                    "Si",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.red.shade700),
                  ),
                  onPressed: () => Navigator.pop(context),
                  child: const Text(
                    "No",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  uuploadImageDuenio1(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageDuenioLateral/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'imagelateral':
          await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImageDuenio2(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageDuenioreves/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'imagereves':
          await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImageDuenio3(imagepath, imagepath2) async {
    Dio dio = Dio();
    final serverUrl = '$url/uploadImageDuenio';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito');
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  modificarDatos(verificaDuenioProvider, names, lastNames, email, phone,
      password, photoCarnetA, photoCarnetB, dni) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        alignment: AlignmentDirectional.centerStart,
        contentPadding: EdgeInsets.symmetric(horizontal: 20),
        backgroundColor: Colors.amberAccent.shade700,
        title: Center(
            child: Text(
          "ATENCIÓN",
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 20.sp,
              fontWeight: FontWeight.bold),
        )),
        content: Text(
          '¿Estas seguro que quieres guardar los datos?',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.deepPurple),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);

                    showTopSnackBar(
                      Overlay.of(context),
                      const CustomSnackBar.info(
                        message: "Guardando datos",
                      ),
                    );

                    var r1, r2;

                    if (image_to_upload2 == null || image_to_upload3 == null) {
                      setState(() {
                        isyea = true;
                      });

                      if (image_to_upload2 == null) {
                      } else {
                        CachedNetworkImage.evictFromCache("$photoCarnetA");
                        var resp1 =
                            await uuploadImageDuenio1(image_to_upload2, dni);

                        var lastIndex = resp1.toString().length;

                        setState(() {
                          r1 = resp1.toString().substring(29, lastIndex);
                        });
                      }

                      if (image_to_upload3 == null) {
                      } else {
                        CachedNetworkImage.evictFromCache("$photoCarnetB");
                        var resp2 =
                            await uuploadImageDuenio2(image_to_upload3, dni);
                        var lastIndex2 = resp2.toString().length;

                        setState(() {
                          r2 = resp2.toString().substring(29, lastIndex2);
                        });
                      }

                      verificaDuenioProvider.setLoading = true;

                      var datas = {
                        "names": controllerName.text == ''
                            ? names
                            : controllerName.text,
                        "lastNames": controllerLastName.text == ''
                            ? lastNames
                            : controllerLastName.text,
                        "email": controllerEmail.text == ''
                            ? email
                            : controllerEmail.text,
                        "phone":
                            "${controllerPhone.text == '' ? phone : controllerPhone.text}",
                        "password": password,
                        "fotoCarnetA": image_to_upload2 == null
                            ? "${photoCarnetA}"
                            : "$url3/$r1",
                        "fotoCarnetB": image_to_upload3 == null
                            ? "${photoCarnetB}"
                            : "$url3/$r2",
                      };
                      var stat = await updateDuenioVehicle(dni, datas);

                      if (stat["status"] == 200 || stat["status"] == "ok") {
                        verificaDuenioProvider.setLoading = false;

                        iboolean = false;
                        setState(() {
                          isyea = false;
                        });
                      } else {
                        setState(() {
                          isyea = false;
                        });
                        showTopSnackBar(
                          Overlay.of(context),
                          const CustomSnackBar.error(
                            message: "Hay un error al guardar los datos",
                          ),
                        );
                      }
                      setState(() {});
                    } else {
                      setState(() {
                        isyea = true;
                      });

                      CachedNetworkImage.evictFromCache("$photoCarnetA");
                      CachedNetworkImage.evictFromCache("$photoCarnetB");

                      var resp1 =
                          await uuploadImageDuenio1(image_to_upload2, dni);
                      var resp2 =
                          await uuploadImageDuenio2(image_to_upload3, dni);

                      var lastIndex = resp1.toString().length;

                      var lastIndex2 = resp2.toString().length;

                      verificaDuenioProvider.setLoading = true;

                      var datas = {
                        "names": controllerName.text == ''
                            ? names
                            : controllerName.text,
                        "lastNames": controllerLastName.text == ''
                            ? lastNames
                            : controllerLastName.text,
                        "email": controllerEmail.text == ''
                            ? email
                            : controllerEmail.text,
                        "phone":
                            "${controllerPhone.text == '' ? phone : controllerPhone.text}",
                        "password": password,
                        "fotoCarnetA":
                            "${image_to_upload2 == null ? photoCarnetA : '$url3/${resp1.toString().substring(29, lastIndex)}'}",
                        "fotoCarnetB":
                            "${image_to_upload3 == null ? photoCarnetB : '$url3/${resp2.toString().substring(29, lastIndex2)}'}",
                      };
                      var stat = await updateDuenioVehicle(dni, datas);
                      ("el stat es: $stat");
                      if (stat["status"] == 200 || stat["status"] == "ok") {
                        verificaDuenioProvider.setLoading = false;

                        iboolean = false;
                        setState(() {
                          isyea = false;
                        });

                        showTopSnackBar(
                          Overlay.of(context),
                          const CustomSnackBar.success(
                            message: "Datos guardados con exito",
                          ),
                        );
                      } else {
                        setState(() {
                          isyea = false;
                        });
                        showTopSnackBar(
                          Overlay.of(context),
                          const CustomSnackBar.error(
                            message: "Hay un error al guardar los datos",
                          ),
                        );
                      }
                      setState(() {});
                    }
                  },
                  child: const Text(
                    "Si",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                width: 30.w,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.red.shade700),
                  ),
                  onPressed: () => Navigator.pop(context),
                  child: const Text(
                    "No",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  bool circuloVehiculos = false;

  int nu = 0;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var dniProvider = Provider.of<LoginProvider>(context);
    var homeProvider = Provider.of<HomeProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var verificarConductorProvider = Provider.of<AuditorProvider>(context);

    var dni_duenio = dniProvider.getdniduenio;

    var datas = Provider.of<AuditorProvider>(context);
    var datas2 = dniProvider.getAllDatasProvider;

    var id = datas2["id"];

    var names = datas2["names"];
    var lastNames = datas2["lastNames"];
    var dni = datas2["dni"];
    var email = datas2["email"];
    var phone = datas2["phone"];
    var password = datas2["password"];
    var status = datas2["status"];
    var aprobacion = datas2["aprobacion"];
    var photoCarnetA = datas2["fotoCarnetA"];
    var photoCarnetB = datas2["fotoCarnetB"];
    var partner = datas2["partner"];

    if (homeProvider.getNumm == 1) {
      homeProvider.setNumm = 0;
    }

    if (nombre == '' && apellido == '' && correo == '' && telefono == '') {
      if (image_to_upload2 == null && image_to_upload3 == null) {
        controllermo(names, lastNames, phone, email);
      } else {
        setState(() {
          darvariable(controllerName, controllerLastName, controllerPhone,
              controllerEmail, names, lastNames, phone, email);
        });
      }
    }

    List misPatentes = [];

    Map objectPatentes = dniProvider.getAllDatasPatentes;

    String estadoVehiculo = dniProvider.estadoVehiculoLogin;
    String motivoVehiculo = dniProvider.motivoVehiculoLogin;

    objectPatentes.forEach((key, value) {
      misPatentes.add(value);
    });

    setState(() {
      verificaDuenioProvider.setLoading = false;
    });

    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          return alertShow(context, homeProvider, dniProvider.dni_duenio,
              dniProvider, photoCarnetA, photoCarnetB);
        },
        child: isyea == true
            ? circulatorCustom()
            : Container(
                width: 100.w,
                height: 100.h,
                child: dniProvider.aprobacion == "rechazado" ||
                        dniProvider.aprobacion == "revision"
                    ? Column(
                        children: [
                          Container(
                            width: 100.w,
                            height: dniProvider.aprobacion == "revision"
                                ? 20.h
                                : 30.h,
                            decoration: BoxDecoration(
                              color: Colors.amber.shade700,
                              borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                  child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 12.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 4.h,
                                    ),
                                    Text(
                                      "ATENCIÓN ",
                                      style: TextStyle(
                                          color: Colors.deepPurple,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.sp),
                                    ),
                                    Text(
                                      textAlign: TextAlign.start,
                                      dniProvider.aprobacion == "revision"
                                          ? "Su cuenta se encuentra en revision"
                                          : "* Su cuenta aún no ha sido enviada a revisión\n* Verifique la información ingresada\n* Para continuar envie sus datos a revisión \n\n* Estado: ${dniProvider.getMotivo}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.sp,
                                          color: Colors.deepPurple),
                                    ),
                                  ],
                                ),
                              )),
                            ),
                          ),
                          Expanded(
                            child: ListView(
                              children: [
                                Column(
                                  children: [
                                    separador(),
                                    inputName(
                                        controllerName,
                                        focusApellido,
                                        context,
                                        names,
                                        homeProvider,
                                        dniProvider),
                                    separador(),
                                    inputLastName(
                                        controllerLastName,
                                        focusApellido,
                                        focusRut,
                                        context,
                                        lastNames,
                                        homeProvider,
                                        dniProvider),
                                    separador(),
                                    inputEmail(
                                        controllerEmail,
                                        focusCorreo,
                                        focusTelefono,
                                        context,
                                        email,
                                        homeProvider,
                                        dniProvider),
                                    separador(),
                                    inputPhone(
                                        controllerPhone,
                                        focusTelefono,
                                        focusPassword,
                                        context,
                                        phone,
                                        homeProvider,
                                        dniProvider),
                                    separador(),
                                    Column(
                                      children: [
                                        Text(
                                          'Foto carnet',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Column(
                                          children: [
                                            const Text("Lado a"),
                                            InkWell(
                                              onTap: () async {
                                                if (dniProvider.aprobacion ==
                                                    "revision") {
                                                } else {
                                                  await showDialog(
                                                    context: context,
                                                    builder: (context) =>
                                                        AlertDialog(
                                                      title: Text(
                                                          "Elija una opcion"),
                                                      content: Text(
                                                          "¿De que manera desea subir la imagen?"),
                                                      actions: [
                                                        IconButton(
                                                          onPressed: () {
                                                            setState(() {
                                                              camerais = false;
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          icon: Row(
                                                            children: [
                                                              Text("Galeria"),
                                                              SizedBox(
                                                                width: 3.w,
                                                              ),
                                                              Icon(Icons.image),
                                                            ],
                                                          ),
                                                        ),
                                                        IconButton(
                                                          onPressed: () {
                                                            setState(() {
                                                              camerais = true;
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          icon: Row(
                                                            children: [
                                                              Text("Camara"),
                                                              SizedBox(
                                                                width: 3.w,
                                                              ),
                                                              Icon(
                                                                  Icons.camera),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );

                                                  await openImagePicker2();
                                                  if (image_to_upload2 ==
                                                      null) {
                                                    image_to_upload2 =
                                                        File(image2!.path);
                                                  } else {
                                                    image_to_upload2 =
                                                        File(image2!.path);
                                                  }
                                                  if (controllerName.text ==
                                                          '' &&
                                                      controllerLastName.text ==
                                                          '' &&
                                                      controllerEmail.text ==
                                                          '' &&
                                                      controllerPhone.text ==
                                                          '' &&
                                                      image_to_upload2 ==
                                                          null &&
                                                      image_to_upload3 ==
                                                          null) {
                                                    homeProvider.isbutton =
                                                        false;
                                                    iboolean = false;
                                                  } else {
                                                    homeProvider.isbutton =
                                                        true;
                                                    iboolean = true;
                                                  }
                                                }
                                              },
                                              child: SizedBox(
                                                width: 35.w,
                                                child: istrue2 == false
                                                    ? CachedNetworkImage(
                                                        imageUrl:
                                                            "$photoCarnetA",
                                                        placeholder: (context,
                                                                url) =>
                                                            circulatorCustom(),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(Icons.error),
                                                      )
                                                    : Image.file(
                                                        image2!,
                                                        width: 35.w,
                                                      ),
                                              ),
                                            ),
                                            const Text("Lado B"),
                                            InkWell(
                                              onTap: () async {
                                                if (dniProvider.aprobacion ==
                                                    "revision") {
                                                } else {
                                                  await showDialog(
                                                    context: context,
                                                    builder: (context) =>
                                                        AlertDialog(
                                                      title: Text(
                                                          "Elija una opción"),
                                                      content: Text(
                                                          "¿Como desea subir la imagen?"),
                                                      actions: [
                                                        ElevatedButton(
                                                          onPressed: () {
                                                            setState(() {
                                                              camerais2 = false;
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child:
                                                              Text("Galeria"),
                                                        ),
                                                        ElevatedButton(
                                                          onPressed: () {
                                                            setState(() {
                                                              camerais2 = true;
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Text("camara"),
                                                        ),
                                                      ],
                                                    ),
                                                  );

                                                  await openImagePicker3();
                                                  if (image_to_upload3 ==
                                                      null) {
                                                    image_to_upload3 =
                                                        File(image3!.path);
                                                  } else {
                                                    image_to_upload3 =
                                                        File(image3!.path);
                                                  }

                                                  if (controllerName.text ==
                                                          '' &&
                                                      controllerLastName.text ==
                                                          '' &&
                                                      controllerEmail.text ==
                                                          '' &&
                                                      controllerPhone.text ==
                                                          '' &&
                                                      image_to_upload2 ==
                                                          null &&
                                                      image_to_upload3 ==
                                                          null) {
                                                    homeProvider.isbutton =
                                                        false;
                                                    iboolean = false;
                                                  } else {
                                                    homeProvider.isbutton =
                                                        true;
                                                    iboolean = true;
                                                  }
                                                }
                                              },
                                              child: SizedBox(
                                                width: 35.w,
                                                child: istrue3 == false
                                                    ? CachedNetworkImage(
                                                        imageUrl:
                                                            "$photoCarnetB",
                                                        placeholder: (context,
                                                                url) =>
                                                            circulatorCustom(),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(Icons.error),
                                                      )
                                                    : Image.file(
                                                        image3!,
                                                        width: 35.w,
                                                      ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    separador(),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 4.w),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Partner: ",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 17.sp,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            partner.toString() == 'taxiseguro'
                                                ? "ningun partner"
                                                : "$partner",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 17.sp,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    separador(),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          Container(
                            width: 100.w,
                            height: 17.h,
                            decoration: BoxDecoration(
                              color: Colors.amber.shade700,
                              borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                "Lista de patentes",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.sp,
                                    color: Colors.deepPurple),
                              ),
                            ),
                          ),
                          verificaDuenioProvider.getLoading == true
                              ? circulatorCustom()
                              : dniProvider.eliminarPatente == true
                                  ? circulatorCustom()
                                  : Expanded(
                                      child: ListView.builder(
                                        itemCount: misPatentes.isEmpty
                                            ? 1
                                            : circuloVehiculos == true
                                                ? 1
                                                : misPatentes.length,
                                        itemBuilder: (context, index) {
                                          return misPatentes.isEmpty
                                              ? Center(
                                                  child: Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                        color: Colors.amber,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(10.0),
                                                        ),
                                                      ),
                                                      child: const Padding(
                                                        padding:
                                                            EdgeInsets.all(8.0),
                                                        child: Text(
                                                          "No se han encontrado patentes",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .deepPurple),
                                                        ),
                                                      )))
                                              : circuloVehiculos == true
                                                  ? circulatorCustom()
                                                  : Dismissible(
                                                      key: UniqueKey(),
                                                      direction:
                                                          DismissDirection
                                                              .endToStart,
                                                      onDismissed: (direction) {
                                                        showDialog(
                                                          context: context,
                                                          builder: (context) =>
                                                              AlertDialog(
                                                            backgroundColor:
                                                                Colors.black,
                                                            title: const Text(
                                                              "Eliminar",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            content: const Text(
                                                              "Estas seguro que deseas eliminar la patente",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            actions: [
                                                              ElevatedButton(
                                                                style:
                                                                    const ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStatePropertyAll(
                                                                          Colors
                                                                              .black),
                                                                ),
                                                                onPressed:
                                                                    () async {
                                                                  setState(() {
                                                                    dniProvider
                                                                            .eliminarPatente =
                                                                        true;
                                                                  });
                                                                  setState(() {
                                                                    verificaDuenioProvider
                                                                            .setLoading =
                                                                        true;
                                                                  });
                                                                  Navigator.pop(
                                                                      context);
                                                                  Map elmapapatentes =
                                                                      dniProvider
                                                                          .getAllDatasPatentes;

                                                                  var lista =
                                                                      dniProvider
                                                                          .getAllDatasPatentes;

                                                                  List
                                                                      listadatas =
                                                                      [];

                                                                  lista.forEach(
                                                                      (key,
                                                                          value) {
                                                                    listadatas.add(
                                                                        value);
                                                                  });

                                                                  homeProvider
                                                                          .setLista =
                                                                      listadatas;

                                                                  homeProvider
                                                                      .lista
                                                                      .removeAt(
                                                                          index);

                                                                  listanew =
                                                                      homeProvider
                                                                          .getLista;

                                                                  var numIndex =
                                                                      homeProvider
                                                                          .lista
                                                                          .length;

                                                                  Map data = {};

                                                                  for (var i =
                                                                          0;
                                                                      i < numIndex;
                                                                      i++) {
                                                                    data.addAll({
                                                                      "patente$i":
                                                                          "${listanew[i]}"
                                                                    });
                                                                  }

                                                                  homeProvider
                                                                      .setLista = [];

                                                                  await deletePatentes(
                                                                      data,
                                                                      dni_duenio);

                                                                  dniProvider.setDni(
                                                                      await getAllPatentes(
                                                                          dni_duenio));

                                                                  homeProvider
                                                                          .setLista =
                                                                      dniProvider
                                                                          .getDni()
                                                                          .values
                                                                          .toList();

                                                                  homeProvider
                                                                      .setLista = [];
                                                                  String rut =
                                                                      dniProvider
                                                                          .getRut;
                                                                  String pass =
                                                                      dniProvider
                                                                          .getPassword;

                                                                  var datasPatentes =
                                                                      await patentesDuenio(
                                                                          rut,
                                                                          pass);

                                                                  Map patentes =
                                                                      jsonDecode(
                                                                          datasPatentes[
                                                                              "patentes"]);

                                                                  dniProvider
                                                                          .setAllDatasPatentes =
                                                                      patentes;
                                                                  setState(
                                                                      () {});

                                                                  setState(() {
                                                                    dniProvider
                                                                            .eliminarPatente =
                                                                        false;
                                                                  });

                                                                  setState(() {
                                                                    verificaDuenioProvider
                                                                            .setLoading =
                                                                        false;
                                                                  });
                                                                },
                                                                child:
                                                                    const Text(
                                                                  "Si",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                              ElevatedButton(
                                                                style:
                                                                    const ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStatePropertyAll(
                                                                          Colors
                                                                              .black),
                                                                ),
                                                                onPressed: () {
                                                                  setState(
                                                                      () {});
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                child:
                                                                    const Text(
                                                                  "No",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                        /**/ ("en el dissio");
                                                        /**/ ("$direction");
                                                      },
                                                      child: Card(
                                                        color: Colors
                                                            .amber.shade700,
                                                        child: ListTile(
                                                          onTap: () async {
                                                            circuloVehiculos =
                                                                true;

                                                            List listaPatentes =
                                                                [];

                                                            Map patentesprovider =
                                                                dniProvider
                                                                    .getAllDatasPatentes;

                                                            patentesprovider
                                                                .forEach((key,
                                                                    value) {
                                                              listaPatentes
                                                                  .add(value);
                                                            });

                                                            var data =
                                                                await getAllOneVehicle(
                                                                    listaPatentes[
                                                                        index]);

                                                            if (data == null) {
                                                              data = await getAllOneVehicle(
                                                                  listaPatentes[
                                                                      index]);
                                                            }

                                                            var consuctores =
                                                                await getAllRutConductores(
                                                                    listaPatentes[
                                                                        index]);

                                                            if (data == null) {
                                                              setState(() {
                                                                circuloVehiculos =
                                                                    false;
                                                              });

                                                              showDialog(
                                                                context:
                                                                    context,
                                                                builder:
                                                                    (context) {
                                                                  return AlertDialog(
                                                                    backgroundColor:
                                                                        Colors
                                                                            .amber
                                                                            .shade700,
                                                                    title:
                                                                        Center(
                                                                      child:
                                                                          Text(
                                                                        'AVISO',
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.deepPurple,
                                                                            fontSize: 20.sp),
                                                                      ),
                                                                    ),
                                                                    content:
                                                                        Text(
                                                                      "*¡No existe ningun vehiculo con esta patente ${misPatentes[index]}!\n ¿Desea agregarlo?",
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .deepPurple,
                                                                          fontSize:
                                                                              18.sp),
                                                                    ),
                                                                    actions: [
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceAround,
                                                                        children: [
                                                                          ElevatedButton(
                                                                            style:
                                                                                const ButtonStyle(
                                                                              backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
                                                                            ),
                                                                            onPressed:
                                                                                () {
                                                                              homeProvider.setLaPatente = misPatentes[index];
                                                                              Navigator.pop(context);
                                                                              Navigator.push(
                                                                                context,
                                                                                MaterialPageRoute(
                                                                                  builder: (context) => const SaveVehicle(),
                                                                                ),
                                                                              );
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "Agregar",
                                                                              style: TextStyle(color: Colors.white),
                                                                            ),
                                                                          ),
                                                                          ElevatedButton(
                                                                            style:
                                                                                const ButtonStyle(
                                                                              backgroundColor: MaterialStatePropertyAll(Colors.red),
                                                                            ),
                                                                            onPressed: () =>
                                                                                Navigator.pop(context),
                                                                            child:
                                                                                const Text(
                                                                              "Cancelar",
                                                                              style: TextStyle(color: Colors.white),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      )
                                                                    ],
                                                                  );
                                                                },
                                                              );
                                                            } else {
                                                              if (consuctores
                                                                      .toString() ==
                                                                  "[{}]") {
                                                                setState(() {
                                                                  circuloVehiculos =
                                                                      false;
                                                                });

                                                                showTopSnackBar(
                                                                  Overlay.of(
                                                                      context),
                                                                  CustomSnackBar
                                                                      .error(
                                                                    maxLines: 3,
                                                                    message:
                                                                        "No existe ningun conductor para este vehiculo\n¡¡¡Agrege un conductor!!!",
                                                                  ),
                                                                );
                                                              } else {
                                                                setState(() {
                                                                  verificaDuenioProvider
                                                                          .setLoading =
                                                                      true;
                                                                });
                                                                dniProvider
                                                                    .setDatasVehicle(
                                                                        data);

                                                                var datas2 =
                                                                    await getAllRutConductores(
                                                                        listaPatentes[
                                                                            index]);

                                                                if (datas2
                                                                        .toString() ==
                                                                    "{}") {
                                                                  setState(() {
                                                                    verificaDuenioProvider
                                                                            .setLoading =
                                                                        false;
                                                                  });

                                                                  setState(() {
                                                                    circuloVehiculos =
                                                                        false;
                                                                  });
                                                                  showTopSnackBar(
                                                                    Overlay.of(
                                                                        context),
                                                                    const CustomSnackBar
                                                                        .error(
                                                                      maxLines:
                                                                          3,
                                                                      message:
                                                                          "No existe ningun conductor asociado al vehiculo\n¡¡¡Agrege un conductor!!!",
                                                                    ),
                                                                  );
                                                                } else {
                                                                  List<dynamic>
                                                                      datas =
                                                                      await getAllRutConductores(
                                                                          listaPatentes[
                                                                              index]);

                                                                  circuloVehiculos =
                                                                      false;

                                                                  dniProvider
                                                                          .setAllRut =
                                                                      datas;

                                                                  Navigator
                                                                      .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              const ModificarVehiculo(),
                                                                    ),
                                                                  );

                                                                  setState(() {
                                                                    verificaDuenioProvider
                                                                            .setLoading =
                                                                        false;
                                                                  });
                                                                }
                                                                setState(() {
                                                                  verificaDuenioProvider
                                                                          .setLoading =
                                                                      false;
                                                                });
                                                              }
                                                            }
                                                          },
                                                          title: Text(
                                                            "${misPatentes[index]}",
                                                            style:
                                                                const TextStyle(
                                                              color: Colors
                                                                  .deepPurple,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                          trailing: const Icon(
                                                            Icons.car_crash,
                                                            color: Colors
                                                                .deepPurple,
                                                          ),
                                                          leading: Text(
                                                            "$index",
                                                            style: TextStyle(
                                                              fontSize: 16.sp,
                                                              color: Colors
                                                                  .deepPurple,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                        },
                                      ),
                                    ),
                        ],
                      ),
              ),
      ),
      floatingActionButton: dniProvider.getAprobacion == "rechazado"
          ? SpeedDial(
              renderOverlay: true,
              isOpenOnStart: true,
              icon: Icons.check,
              openCloseDial: isDialOpen,
              activeIcon: iboolean ? Icons.save : Icons.edit,
              backgroundColor: Colors.deepPurple,
              foregroundColor: Colors.white,
              activeBackgroundColor: Colors.deepPurpleAccent,
              activeForegroundColor: Colors.white,
              visible: true,
              curve: Curves.bounceIn,
              overlayOpacity: 0.5,
              onPress: () {
                iboolean == true
                    ? modificarDatos(
                        verificarConductorProvider,
                        names,
                        lastNames,
                        email,
                        phone,
                        password,
                        photoCarnetA,
                        photoCarnetB,
                        dni)
                    : solicitarRevision(dniProvider, homeProvider);
              },
              onOpen: () {
                isDialOpen.value = true;
              },
              onClose: () {},
              elevation: 8.0,
              shape: const CircleBorder(),
              children: [],
            )
          : SpeedDial(
              icon: Icons.menu,
              activeIcon: Icons.close,
              backgroundColor: Colors.amber.shade700,
              foregroundColor: Colors.deepPurple,
              activeBackgroundColor: Colors.deepPurpleAccent,
              activeForegroundColor: Colors.white,
              visible: dniProvider.aprobacion == "revision" ? false : true,
              closeManually: false,
              curve: Curves.bounceIn,
              overlayColor: Colors.black,
              overlayOpacity: 0.5,
              onOpen: () => /**/ ('OPENING DIAL'),
              onClose: () => /**/ ('DIAL CLOSED'),
              elevation: 8.0,
              shape: const CircleBorder(),
              children: [
                SpeedDialChild(
                  child: IconButton(
                    onPressed: () {
                      showTopSnackBar(
                        Overlay.of(context),
                        const CustomSnackBar.info(
                            message:
                                "Puede eliminar patentes deslizandola hacia la izquierda"),
                      );
                    },
                    icon: const Icon(Icons.lightbulb),
                  ),
                  backgroundColor: Colors.red,
                  foregroundColor: Colors.white,
                  label: 'AYUDA',
                  labelStyle: const TextStyle(fontSize: 18.0),
                  onTap: () => showTopSnackBar(
                    Overlay.of(context),
                    const CustomSnackBar.info(
                        message:
                            "Puede eliminar patentes deslizandola hacia la izquierda"),
                  ),
                ),
                SpeedDialChild(
                  child: const Icon(Icons.people),
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.green,
                  label: 'Añadir conductores',
                  labelStyle: const TextStyle(fontSize: 18.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AniadirConductores(),
                    ),
                  ),
                ),
                SpeedDialChild(
                  child: const Icon(Icons.car_crash),
                  backgroundColor: Colors.blue,
                  foregroundColor: Colors.white,
                  label: 'Añadir patentes',
                  labelStyle: const TextStyle(fontSize: 18.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const RegisterPatente(),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Widget inputName(TextEditingController controller, focus, context, names,
      homeProvider, dniProvider) {
    return SizedBox(
      width: 70.w,
      child: Focus(
        onFocusChange: (value) {
          homeProvider.isbutton = false;
        },
        child: TextFormField(
          maxLength: 20,
          enabled: dniProvider.aprobacion == "revision" ? false : true,
          onChanged: (value) {
            if (controllerName.text == '' &&
                controllerLastName.text == '' &&
                controllerEmail.text == '' &&
                controllerPhone.text == '' &&
                image_to_upload2 == null &&
                image_to_upload3 == null) {
              homeProvider.isbutton = false;

              iboolean = false;
            } else {
              homeProvider.isbutton = true;

              iboolean = true;
            }
          },
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus);
          },
          controller: controller,
          decoration: InputDecoration(
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              labelText: 'Nombre:'),
        ),
      ),
    );
  }

  Widget inputLastName(TextEditingController controller, focus, focus2, context,
      lastName, homeProvider, dniProvider) {
    return SizedBox(
      width: 70.w,
      child: Focus(
        onFocusChange: (value) {},
        child: TextFormField(
          enabled: dniProvider.aprobacion == "revision" ? false : true,
          maxLength: 20,
          controller: controller,
          focusNode: focus,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          onChanged: (value) {

            if (controllerName.text == '' &&
                controllerLastName.text == '' &&
                controllerEmail.text == '' &&
                controllerPhone.text == '' &&
                image_to_upload2 == null &&
                image_to_upload3 == null) {


              homeProvider.isbutton = false;

              iboolean = false;
            } else {


              homeProvider.isbutton = true;

              iboolean = true;
            }

          },
          decoration: InputDecoration(
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: '$lastName',
              labelText: 'Apellido:'),
        ),
      ),
    );
  }

  Widget inputEmail(TextEditingController controller, focus, focus2, context,
      email, homeProvider, dniProvider) {
    return SizedBox(
      width: 70.w,
      child: Focus(
        onFocusChange: (value) {},
        child: TextFormField(
          enabled: dniProvider.aprobacion == "revision" ? false : true,
          maxLength: 255,
          controller: controller,
          focusNode: focus,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          onChanged: (value) {

            if (controllerName.text == '' &&
                controllerLastName.text == '' &&
                controllerEmail.text == '' &&
                controllerPhone.text == '' &&
                image_to_upload2 == null &&
                image_to_upload3 == null) {


              homeProvider.isbutton = false;

              iboolean = false;
            } else {


              homeProvider.isbutton = true;

              iboolean = true;
            }

          },
          decoration: InputDecoration(
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: '$email',
              labelText: 'Email:'),
        ),
      ),
    );
  }

  Widget inputPhone(TextEditingController controller, focus, focus2, context,
      phone, homeProvider, dniProvider) {
    return SizedBox(
      width: 70.w,
      child: Focus(
        onFocusChange: (value) {},
        child: TextFormField(
          enabled: dniProvider.aprobacion == "revision" ? false : true,
          maxLength: 14,
          controller: controller,
          focusNode: focus,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          onChanged: (value) {

            if (controllerName.text == '' &&
                controllerLastName.text == '' &&
                controllerEmail.text == '' &&
                controllerPhone.text == '' &&
                image_to_upload2 == null &&
                image_to_upload3 == null) {


              homeProvider.isbutton = false;

              iboolean = false;
            } else {


              homeProvider.isbutton = true;

              iboolean = true;
            }

          },
          decoration: InputDecoration(
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: '$phone',
              labelText: 'Telefono:'),
        ),
      ),
    );
  }

  Future<SharedPreferences> getPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs;
  }
}

Widget inputDni(TextEditingController controller, verificaDuenioProvider, focus,
    focus2, context, dni) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      focusNode: focus,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      autovalidateMode: AutovalidateMode.always,
      validator: validateRut,
      maxLength: 12,
      onChanged: (value) {
        var formattedRut = formatRut(controller.text);
        verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);

      },
      inputFormatters: [RutFormatter()],
      decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: '$dni',
          labelText: 'Rut:$dni'),
    ),
  );
}

Future<bool> alertShow(
    context, homeProvider, rut, dniProvider, fotoa, fotob) async {
  bool action = await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.black,
      title: const Text(
        "Alerta !!!",
        style: TextStyle(color: Colors.white),
      ),
      content: const Text(
        "Esta seguro que desea salir ???",
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () => Navigator.pop(context, false),
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white),
          ),
        ),
        ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.black),
            ),
            onPressed: () async {
              final SharedPreferences prefs =
                  await SharedPreferences.getInstance();

              var conectado = await updateEstadoDuenio(rut, "desconectado");


              clearCachedImages(fotoa, fotob);

              await prefs.remove('idsession');
              await prefs.remove('user_rut');
              await prefs.remove('user_password');
              await prefs.remove('tipo');
              await prefs.remove('id');

              dniProvider.setAllDatasPatentes = {};
              dniProvider.setPatentesObtenidasList = [];
              dniProvider.patenteguardada = "";
              homeProvider.setLista = [];
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => LoginDuenioVehiculo()),
                (route) => false,
              );
            },
            child: const Text(
              "Si",
              style: TextStyle(color: Colors.white),
            ))
      ],
    ),
  );

  return action;
}
