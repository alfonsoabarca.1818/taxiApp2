// ignore_for_file: prefer_const_constructors

import 'dart:convert';
import 'dart:io';
import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../../apis/api_auditor/update_aprobacion/update_aprobacion_vehiculo.dart';
import '../../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../../apis/api_duenio_vehiculo/get_all_datas_vehicle.dart';
import '../../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/home_provider.dart';
import '../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../tools/separador.dart';
import 'controllerModificarvehiculo.dart';

class ModificarVehiculo extends StatefulWidget {
  const ModificarVehiculo({super.key});

  @override
  State<ModificarVehiculo> createState() => _ModificarVehiculoState();
}

class _ModificarVehiculoState extends State<ModificarVehiculo> {
  TextEditingController controllerMarca = TextEditingController();
  TextEditingController controllerModelo = TextEditingController();
  TextEditingController controllerColor = TextEditingController();
  TextEditingController controllerFechaRtecnica = TextEditingController();
  TextEditingController controllerPatente = TextEditingController();
  TextEditingController controllerFechaVencimientoPcirculacion =
      TextEditingController();

  TextEditingController controllerFechaVencimientoRtecnicaDia =
      TextEditingController();
  TextEditingController controllerFechaVencimientoRtecnicaMes =
      TextEditingController();
  TextEditingController controllerFechaVencimientoRtecnicaAnio =
      TextEditingController();
  TextEditingController controllerFechaVencimientoPcirculacionDia =
      TextEditingController();
  TextEditingController controllerFechaVencimientoPcirculacionMes =
      TextEditingController();
  TextEditingController controllerFechaVencimientoPcirculacionAnio =
      TextEditingController();

  FocusNode focusMarca = FocusNode();
  FocusNode focusModelo = FocusNode();
  FocusNode focusColor = FocusNode();
  FocusNode focusConductores = FocusNode();
  FocusNode focusFechaVrevisionTecnica = FocusNode();
  FocusNode focusEstadoVehiculoRNSTP = FocusNode();
  FocusNode focusPatenteRNSTP = FocusNode();
  FocusNode focusFechaVpermisoCirculacion = FocusNode();

  bool iboolean = false;
  bool camerais = false;
  bool camerais2 = false;
  bool camerais3 = false;
  bool camerais4 = false;
  bool camerais5 = false;

  String marca1 = '';
  String modelo1 = '';
  String color1 = '';
  String FechaRtecnica1 = '';
  String fechaVencimientoPcirculacion1 = '';
  String estados1 = '';
  String fechaVencimientoRtecnicaDia = '';
  String fechaVencimientoRtecnicaMes = '';
  String fechaVencimientoRtecnicaAnio = '';
  String fechaVencimientoPcirculacionDia = '';
  String fechaVencimientoPcirculacionMes = '';
  String fechaVencimientoPcirculacionAnio = '';

  bool istrue = false;

  final picker = ImagePicker();
  File? image, image2, image3;
  File? image_to_upload1,
      image_to_upload2,
      image_to_upload3,
      image_to_upload4,
      image_to_upload5;
  var imageUrl;

  File? image1;
  bool istrue1 = false;

  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais) {
      final XFile? pickedImage =
          await _picker.pickImage(source: ImageSource.camera);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);

          istrue1 = true;
        });
      }
    } else {
      final XFile? pickedImage =
          await _picker.pickImage(source: ImageSource.gallery);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    }
    setState(() {});
  }

  bool istrue2 = false;

  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais2) {
      final XFile? pickedImage2 =
          await _picker2.pickImage(source: ImageSource.camera);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 =
          await _picker2.pickImage(source: ImageSource.gallery);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  bool istrue3 = false;

  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais3) {
      final XFile? pickedImage3 =
          await _picker3.pickImage(source: ImageSource.camera);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 =
          await _picker3.pickImage(source: ImageSource.gallery);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  File? image4;
  bool istrue4 = false;

  final _picker4 = ImagePicker();
  Future<void> openImagePicker4() async {
    if (camerais4) {
      final XFile? pickedImage4 =
          await _picker4.pickImage(source: ImageSource.camera);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    } else {
      final XFile? pickedImage4 =
          await _picker4.pickImage(source: ImageSource.gallery);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    }
  }

  File? image5;
  bool istrue5 = false;

  final _picker5 = ImagePicker();
  Future<void> openImagePicker5() async {
    if (camerais5) {
      final XFile? pickedImage5 =
          await _picker5.pickImage(source: ImageSource.camera);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    } else {
      final XFile? pickedImage5 =
          await _picker5.pickImage(source: ImageSource.gallery);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    }
  }

  solicitudRevision(patenteRNSTP) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.amberAccent.shade700,
        title: Text(
          "¡¡¡ATENCIÓN!!!",
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 20.sp,
              fontWeight: FontWeight.bold),
        ),
        content: Text(
          '¿Estas seguro que quieres solicitar revision?',
          style: TextStyle(
              color: Colors.deepPurple,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold),
        ),
        actions: [
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
            ),
            onPressed: () async {
              var response = await updateAprobacionVehiculo(
                  'revision', patenteRNSTP, 'motivo');
              showTopSnackBar(
                Overlay.of(context),
                const CustomSnackBar.success(
                  message: "Solicitud enviada",
                ),
              );

              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: const Text(
              "Si",
              style: TextStyle(color: Colors.white),
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.red.shade700),
            ),
            onPressed: () => Navigator.pop(context),
            child: const Text(
              "No",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  darvariable(
      controllerMarca,
      controllerModelo,
      controllerColor,
      controllerFechaRtecnica,
      controllerFechaVencimientoPcirculacion,
      marca1,
      modelo1,
      color1,
      FechaRtecnica1,
      fechaVencimientoPcirculacion1,
      fechaVenciomientoRtecnicca,
      fechaVencimientoPermiso) {
    if (iboolean) {
      marca1 = controllerMarca.text;
      modelo1 = controllerModelo.text;
      color1 = controllerColor.text;
      FechaRtecnica1 = controllerFechaRtecnica.text;
      fechaVencimientoPcirculacion1 =
          controllerFechaVencimientoPcirculacion.text;

      controllerMarca.text = marca1;
      controllerModelo.text = modelo1;
      controllerColor.text = color1;
      controllerFechaRtecnica.text = FechaRtecnica1;
      controllerFechaVencimientoPcirculacion.text =
          fechaVencimientoPcirculacion1;
      controllerFechaVencimientoRtecnicaDia.text =
          fechaVenciomientoRtecnicca.toString().substring(8, 10);
      controllerFechaVencimientoRtecnicaMes.text =
          fechaVenciomientoRtecnicca.toString().substring(5, 7);
      controllerFechaVencimientoRtecnicaAnio.text =
          fechaVenciomientoRtecnicca.toString().substring(0, 4);
      controllerFechaVencimientoPcirculacionDia.text =
          fechaVencimientoPermiso.toString().substring(8, 10);
      controllerFechaVencimientoPcirculacionMes.text =
          fechaVencimientoPermiso.toString().substring(5, 7);
      controllerFechaVencimientoPcirculacionAnio.text =
          fechaVencimientoPermiso.toString().substring(0, 4);
    } else {
      controllerMarca.text = marca1;
      controllerModelo.text = modelo1;
      controllerColor.text = color1;
      controllerFechaRtecnica.text = FechaRtecnica1;
      controllerFechaVencimientoPcirculacion.text =
          fechaVencimientoPcirculacion1;
      controllerFechaVencimientoRtecnicaDia.text =
          fechaVenciomientoRtecnicca.toString().substring(8, 10);
      controllerFechaVencimientoRtecnicaMes.text =
          fechaVenciomientoRtecnicca.toString().substring(5, 7);
      controllerFechaVencimientoRtecnicaAnio.text =
          fechaVenciomientoRtecnicca.toString().substring(0, 4);
      controllerFechaVencimientoPcirculacionDia.text =
          fechaVencimientoPermiso.toString().substring(8, 10);
      controllerFechaVencimientoPcirculacionMes.text =
          fechaVencimientoPermiso.toString().substring(5, 7);
      controllerFechaVencimientoPcirculacionAnio.text =
          fechaVencimientoPermiso.toString().substring(0, 4);
    }
  }

  controllermo(
    marca,
    modelo,
    color,
    fechaVenciomientoRtecnicca,
    fechaVencimientoPermiso,
  ) {
    if (iboolean) {
      marca1 = controllerMarca.text;
      modelo1 = controllerModelo.text;
      color1 = controllerColor.text;
      FechaRtecnica1 = controllerFechaRtecnica.text;
      fechaVencimientoRtecnicaDia = controllerFechaVencimientoRtecnicaDia.text;
      fechaVencimientoRtecnicaMes = controllerFechaVencimientoRtecnicaMes.text;
      fechaVencimientoRtecnicaAnio =
          controllerFechaVencimientoRtecnicaAnio.text;
      fechaVencimientoPcirculacionDia =
          controllerFechaVencimientoPcirculacionDia.text;
      fechaVencimientoPcirculacionMes =
          controllerFechaVencimientoPcirculacionMes.text;
      fechaVencimientoPcirculacionAnio =
          controllerFechaVencimientoPcirculacionAnio.text;
      fechaVencimientoPcirculacion1 =
          controllerFechaVencimientoPcirculacion.text;

      controllerMarca.text = marca1;
      controllerModelo.text = modelo1;
      controllerColor.text = color1;
      controllerFechaRtecnica.text = FechaRtecnica1;
      controllerFechaVencimientoPcirculacion.text =
          fechaVencimientoPcirculacion1;
      controllerFechaVencimientoRtecnicaDia.text = fechaVencimientoRtecnicaDia;
      controllerFechaVencimientoRtecnicaMes.text = fechaVencimientoRtecnicaMes;
      controllerFechaVencimientoRtecnicaAnio.text =
          fechaVencimientoRtecnicaAnio;

      controllerFechaVencimientoPcirculacionDia.text =
          fechaVencimientoPcirculacionDia;
      controllerFechaVencimientoPcirculacionMes.text =
          fechaVencimientoPcirculacionMes;
      controllerFechaVencimientoPcirculacionAnio.text =
          fechaVencimientoPcirculacionAnio;
    } else {
      controllerMarca.text = marca;
      controllerModelo.text = modelo;
      controllerColor.text = color;
      controllerFechaRtecnica.text = fechaVenciomientoRtecnicca;
      controllerFechaVencimientoPcirculacion.text = fechaVencimientoPermiso;
      controllerFechaVencimientoRtecnicaDia.text =
          fechaVenciomientoRtecnicca.toString().substring(8, 10);
      controllerFechaVencimientoRtecnicaMes.text =
          fechaVenciomientoRtecnicca.toString().substring(5, 7);
      controllerFechaVencimientoRtecnicaAnio.text =
          fechaVenciomientoRtecnicca.toString().substring(0, 4);

      controllerFechaVencimientoPcirculacionDia.text =
          fechaVencimientoPermiso.toString().substring(8, 10);
      controllerFechaVencimientoPcirculacionMes.text =
          fechaVencimientoPermiso.toString().substring(5, 7);
      controllerFechaVencimientoPcirculacionAnio.text =
          fechaVencimientoPermiso.toString().substring(0, 4);
    }
  }

  getAll(dniProvider, dni, homeProvider) async {
    if (homeProvider.getIsTRue) {
      dniProvider.setDni(await getAllPatentes(dni));
      setState(() {});
      homeProvider.setIsTRue = false;
    } else {
      dniProvider.setDni(await getAllPatentes(dni));
    }
  }

  String? selectedValue;
  String? selectedValue2;
  List<String> listasHabilitado = ["habilitado", "deshabilitado"];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var homeProvider = Provider.of<HomeProvider>(context);

    List<dynamic> datas2 = [];

    var dniProvider = Provider.of<LoginProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var verificarConductorProvider = Provider.of<AuditorProvider>(context);
    var patente = dniProvider.getDatasVehicle();
    var dni_duenio = dniProvider.getdniduenio;

    var marca = patente["marca"];
    var marcas = patente["patente"];

    var modelo = patente["modelo"];
    var color = patente["color"];

    var rev_tecnica =
        jsonDecode(dniProvider.getDatasVehicle()["revicionTecnica"]);

    var fechaVenciomientoRtecnicca = rev_tecnica["fechavencimiento"];

    var fotoRtecnica = rev_tecnica["foto"];

    var certificadoRNSTP = jsonDecode(patente["certificadoInscripcionRNSTP"]);

    var estadoVehiculoRNSTP = certificadoRNSTP["estadovehicle"];
    var patenteRNSTP = certificadoRNSTP["patente"];
    var fotoRNSTP = certificadoRNSTP["fotoCertificadoRNSTP"];

    var fotoCertificadoInscripcion =
        dniProvider.getDatasVehicle()["certificadoInscripcion"];

    var permisoCirculacion = jsonDecode(patente["permisocirculacion"]);
    var fotoPermisoCirculacion = permisoCirculacion["fotoPermisoCirculacion"];
    var fechaVencimientoPermiso = permisoCirculacion["fechaVencimientoPermiso"];
    var aprobacion = patente["aprobacion"];
    var taximetro = {};
    if (patente["taximetro"] == null) {
      print("Es null");
      taximetro = {};
    } else {
      setState(() {
        taximetro = jsonDecode(patente["taximetro"]);
      });
    }

    var fotoTaximetro = taximetro["fotoCertificadoTaximetro"];

    var motivo = patente["motivo"];

    if (marca1 == '' &&
        modelo1 == '' &&
        color1 == '' &&
        FechaRtecnica1 == '' &&
        fechaVencimientoPcirculacion1 == '' &&
        fechaVencimientoRtecnicaDia == '' &&
        fechaVencimientoRtecnicaMes == '' &&
        fechaVencimientoRtecnicaAnio == '' &&
        fechaVencimientoPcirculacionDia == '' &&
        fechaVencimientoPcirculacionMes == '' &&
        fechaVencimientoPcirculacionAnio == '') {
      if (image_to_upload1 == null &&
          image_to_upload2 == null &&
          image_to_upload3 == null &&
          image_to_upload4 == null &&
          image_to_upload5 == null) {
        controllermo(marca, modelo, color, fechaVenciomientoRtecnicca,
            fechaVencimientoPermiso);
      } else {
        setState(() {
          darvariable(
            controllerMarca,
            controllerModelo,
            controllerColor,
            controllerFechaRtecnica,
            controllerFechaVencimientoPcirculacion,
            marca1,
            modelo1,
            color1,
            FechaRtecnica1,
            fechaVencimientoPcirculacion1,
            fechaVenciomientoRtecnicca,
            fechaVencimientoPermiso,
          );
        });
      }
    }

    for (var i = 0; i < dniProvider.getAllRut.length; i++) {
      datas2.add(dniProvider.getAllRut[i]["id_apd"].toString());
    }

    verificaDuenioProvider.setLoading = false;

    return Scaffold(
      body: homeProvider.modificarvehiculo == true
          ? circulatorCustom()
          : Column(children: [
              Container(
                width: 100.w,
                height: aprobacion == 'rechazado' ? 27.h : 20.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                  color: Colors.amber.shade700,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: aprobacion == "aprobado"
                          ? Text(
                              "Patente: $patenteRNSTP",
                              style: TextStyle(
                                  fontSize: 20.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepPurple),
                            )
                          : aprobacion == "revision"
                              ? Text(
                                  "Miestras su estado sea revision,\nno puede modificar datos",
                                  style: TextStyle(
                                    color: Colors.deepPurple,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Times New Roman",
                                    fontSize: 18.sp,
                                  ),
                                )
                              : Text(
                                  "Patente: $patenteRNSTP\n* Su vehiculo aún no ha sido enviado a revisión\n* Verifique la información ingresada\n* Para continuar envie sus datos a revisión \n\n* Estado: $motivo",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.deepPurple),
                                ),
                    )
                  ],
                ),
              ),
              verificarConductorProvider.getLoading == true
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Actualizando datos"),
                              SizedBox(
                                width: 20,
                              ),
                              CircularProgressIndicator(),
                            ],
                          ),
                        ),
                      ],
                    )
                  : Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView(
                          children: [
                            inputTextMarca(
                                marca,
                                controllerMarca,
                                focusModelo,
                                context,
                                aprobacion,
                                homeProvider,
                                modelo,
                                color),
                            separador(),
                            inputTextModelo(
                                modelo,
                                controllerModelo,
                                focusModelo,
                                focusColor,
                                context,
                                aprobacion,
                                homeProvider),
                            separador(),
                            inputTextColor(
                                color,
                                controllerColor,
                                focusColor,
                                focusConductores,
                                context,
                                aprobacion,
                                homeProvider),
                            separador(),
                            DropdownButtonHideUnderline(
                              child: DropdownButton2<dynamic>(
                                focusNode: focusConductores,
                                isExpanded: true,
                                hint: Row(
                                  children: const [
                                    Icon(
                                      Icons.list,
                                      size: 16,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Conductores asignados',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                                items: datas2
                                    .map((dynamic item) =>
                                        DropdownMenuItem<dynamic>(
                                          value: item,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                item,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              aprobacion == 'revision'
                                                  ? Container()
                                                  : IconButton(
                                                      onPressed: () async {
                                                        var result;
                                                        var rut = item;

                                                        var selectedPatente =
                                                            patente["patente"];

                                                        var datas =
                                                            await getAllOneVehicle(
                                                                selectedPatente);

                                                        if (datas == null) {
                                                          showTopSnackBar(
                                                            Overlay.of(context),
                                                            CustomSnackBar
                                                                .error(
                                                              message:
                                                                  "No existe ningun vehiculo con la patente ${patente["patente"]} porfavor registre uno",
                                                            ),
                                                          );
                                                        } else {
                                                          Map data = {};
                                                          Map datasName = {};
                                                          Map datasRut = {};
                                                          Map datasLastName =
                                                              {};

                                                          List ad =
                                                              await getAllRutConductores(
                                                                  selectedPatente);

                                                          datasRut = {};

                                                          List listaDni =
                                                              datasRut.values
                                                                  .toList();

                                                          listaDni.add(rut);

                                                          List listasss = [];
                                                          List listaMup = [];

                                                          if (ad == null) {
                                                          } else {
                                                            for (var i = 0;
                                                                i < ad.length;
                                                                i++) {
                                                              listasss
                                                                  .add(ad[i]);
                                                            }

                                                            for (var i = 0;
                                                                i <
                                                                    listasss
                                                                        .length;
                                                                i++) {
                                                              listaMup.add(
                                                                  listasss[i][
                                                                      "id_apd"]);
                                                            }
                                                          }

                                                          if (listaDni
                                                              .isEmpty) {
                                                            result = listaDni
                                                                .map((item) {
                                                              return {
                                                                "id_apd": item
                                                              };
                                                            }).toList();
                                                          } else {
                                                            for (var i = 0;
                                                                i <
                                                                    listaDni
                                                                        .length;
                                                                i++) {
                                                              listaMup
                                                                  .remove(item);

                                                              for (var i = 0;
                                                                  i <
                                                                      listaDni
                                                                          .length;
                                                                  i++) {
                                                                result = listaMup
                                                                    .map(
                                                                        (item) {
                                                                  return {
                                                                    "id_apd":
                                                                        item
                                                                  };
                                                                }).toList();
                                                              }
                                                            }
                                                          }

                                                          var adata =
                                                              await addRutConductores(
                                                                  result,
                                                                  patente[
                                                                      "patente"]);

                                                          List<dynamic> datas =
                                                              await getAllRutConductores(
                                                                  "${patente["patente"]}");

                                                          dniProvider
                                                                  .setAllRut =
                                                              datas;

                                                          datas2.clear();

                                                          for (var i = 0;
                                                              i <
                                                                  dniProvider
                                                                      .getAllRut
                                                                      .length;
                                                              i++) {
                                                            datas2.add(dniProvider
                                                                .getAllRut[i]
                                                                    ["id_apd"]
                                                                .toString());

                                                            setState(() {});
                                                          }

                                                          setState(() {
                                                            iboolean = true;
                                                          });

                                                          homeProvider
                                                              .iboolean2 = true;

                                                          showTopSnackBar(
                                                            Overlay.of(context),
                                                            const CustomSnackBar
                                                                .success(
                                                              message:
                                                                  "Conductor eliminado con exito",
                                                            ),
                                                          );

                                                          Navigator.pop(
                                                              context);
                                                          setState(() {});
                                                        }
                                                      },
                                                      icon: Icon(
                                                        Icons.delete,
                                                        color: Colors.red,
                                                      ),
                                                    ),
                                            ],
                                          ),
                                        ))
                                    .toList(),
                                value: selectedValue,
                                onChanged: (dynamic value) {
                                  setState(() {
                                    selectedValue = value;
                                  });
                                },
                                buttonStyleData: ButtonStyleData(
                                  height: 50,
                                  width: 160,
                                  padding: const EdgeInsets.only(
                                      left: 14, right: 14),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14),
                                    border: Border.all(
                                      color: Colors.black26,
                                    ),
                                    color: Colors.black,
                                  ),
                                  elevation: 2,
                                ),
                                iconStyleData: const IconStyleData(
                                  icon: Icon(
                                    Icons.arrow_forward_ios_outlined,
                                  ),
                                  iconSize: 14,
                                  iconEnabledColor: Colors.white,
                                  iconDisabledColor: Colors.grey,
                                ),
                                dropdownStyleData: DropdownStyleData(
                                  maxHeight: 200,
                                  width: 200,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14),
                                    color: Colors.black,
                                  ),
                                  offset: const Offset(-20, 0),
                                  scrollbarTheme: ScrollbarThemeData(
                                    radius: const Radius.circular(40),
                                    thickness:
                                        MaterialStateProperty.all<double>(6),
                                    thumbVisibility:
                                        MaterialStateProperty.all<bool>(true),
                                  ),
                                ),
                                menuItemStyleData: const MenuItemStyleData(
                                  height: 40,
                                  padding: EdgeInsets.only(left: 14, right: 14),
                                ),
                              ),
                            ),
                            separador(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Revision tecnica",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 19.sp,
                                  ),
                                ),
                              ],
                            ),
                            separador(),
                            Text("Fecha vencimiento R.tecnica"),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  children: [
                                    Text("DD"),
                                    Container(
                                      width: 18.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        controller:
                                            controllerFechaVencimientoRtecnicaDia,
                                        maxLength: 2,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoRtecnicaDia
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const Text("-"),
                                Column(
                                  children: [
                                    Text("MM"),
                                    Container(
                                      width: 18.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        controller:
                                            controllerFechaVencimientoRtecnicaMes,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoRtecnicaMes
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        maxLength: 2,
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const Text("-"),
                                Column(
                                  children: [
                                    Text("YYYY"),
                                    Container(
                                      width: 38.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoRtecnicaAnio
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        controller:
                                            controllerFechaVencimientoRtecnicaAnio,
                                        maxLength: 4,
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            separador(),
                            const Text("Ingrese foto revision tecnica"),
                            InkWell(
                              onTap: () async {
                                if (image_to_upload1 == null) {
                                  setState(() {
                                    image_to_upload1 = image;
                                  });
                                } else {
                                  setState(() {
                                    image_to_upload1 = image;
                                  });
                                }

                                if (aprobacion == 'revision' ||
                                    aprobacion == 'aprobado') {
                                } else {
                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opcion"),
                                      content: Text(
                                          "¿De que manera desea subir la imagen?"),
                                      actions: [
                                        IconButton(
                                          onPressed: () {
                                            camerais = false;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Galeria"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.image),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            camerais = true;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Camara"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.camera),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );

                                  await openImagePicker();
                                  setState(() {
                                    image_to_upload1 = image1;
                                  });

                                  if (image1 == null) {
                                    homeProvider.isbutton = false;
                                    iboolean = false;
                                    homeProvider.iboolean2 = false;
                                  } else {
                                    homeProvider.isbutton = true;
                                    iboolean = true;
                                    homeProvider.iboolean2 = true;
                                  }
                                }
                              },
                              child: SizedBox(
                                  width: 35.w,
                                  child: image1 == null
                                      ? CachedNetworkImage(
                                          imageUrl: "$fotoRtecnica",
                                          placeholder: (context, url) =>
                                              circulatorCustom(),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        ) // Image.network("$fotoRtecnica")
                                      : Image.file(image1!)),
                            ),
                            separador(),
                            const Text("Estado del vehiculo RNSTP"),
                            DropdownButtonHideUnderline(
                              child: DropdownButton2<String>(
                                isExpanded: true,
                                hint: const Row(
                                  children: [
                                    Icon(
                                      Icons.list,
                                      size: 16,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Selecciona un estado',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                                items: listasHabilitado
                                    .map((String item) =>
                                        DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                value: selectedValue2,
                                onChanged: (String? value) {
                                  setState(() {
                                    selectedValue2 = value;
                                    iboolean = true;
                                    homeProvider.iboolean2 = true;
                                  });
                                },
                                buttonStyleData: ButtonStyleData(
                                  height: 50,
                                  width: 160,
                                  padding: const EdgeInsets.only(
                                      left: 14, right: 14),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14),
                                    border: Border.all(
                                      color: Colors.black26,
                                    ),
                                    color: Colors.black,
                                  ),
                                  elevation: 2,
                                ),
                                iconStyleData: const IconStyleData(
                                  icon: Icon(
                                    Icons.arrow_forward_ios_outlined,
                                  ),
                                  iconSize: 14,
                                  iconEnabledColor: Colors.white,
                                  iconDisabledColor: Colors.grey,
                                ),
                                dropdownStyleData: DropdownStyleData(
                                  maxHeight: 200,
                                  width: 200,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14),
                                    color: Colors.black,
                                  ),
                                  offset: const Offset(-20, 0),
                                  scrollbarTheme: ScrollbarThemeData(
                                    radius: const Radius.circular(40),
                                    thickness:
                                        MaterialStateProperty.all<double>(6),
                                    thumbVisibility:
                                        MaterialStateProperty.all<bool>(true),
                                  ),
                                ),
                                menuItemStyleData: const MenuItemStyleData(
                                  height: 40,
                                  padding: EdgeInsets.only(left: 14, right: 14),
                                ),
                              ),
                            ),
                            separador(),
                            const Text("Patente RNSTP"),
                            SizedBox(
                              width: 70.w,
                              child: TextFormField(
                                focusNode: focusPatenteRNSTP,
                                controller: controllerPatente,
                                enabled: false,
                                decoration: InputDecoration(
                                  labelText: '$patenteRNSTP',
                                  hintText: '',
                                  border: const OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            separador(),
                            const Text("Ingrese foto RNSTP"),
                            InkWell(
                              onTap: () async {
                                if (aprobacion == 'revision' ||
                                    aprobacion == 'aprobado') {
                                } else {
                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opcion"),
                                      content: Text(
                                          "¿De que manera desea subir la imagen?"),
                                      actions: [
                                        IconButton(
                                          onPressed: () {
                                            camerais2 = false;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Galeria"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.image),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            camerais2 = true;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Camara"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.camera),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                  if (image_to_upload2 == null) {
                                    setState(() {
                                      image_to_upload2 = image2;
                                    });
                                  } else {
                                    setState(() {
                                      image_to_upload2 = image2;
                                    });
                                  }

                                  await openImagePicker2();
                                  setState(() {
                                    image_to_upload2 = image2;
                                  });

                                  if (controllerMarca.text == '' &&
                                      controllerModelo.text == '' &&
                                      controllerColor.text == '' &&
                                      image_to_upload1 == null &&
                                      image_to_upload2 == null &&
                                      image_to_upload3 == null &&
                                      image_to_upload4 == null) {
                                    homeProvider.isbutton = false;
                                    iboolean = false;
                                    homeProvider.iboolean2 = false;
                                  } else {
                                    homeProvider.isbutton = true;
                                    iboolean = true;
                                    homeProvider.iboolean2 = true;
                                  }
                                }
                              },
                              child: SizedBox(
                                  width: 35.w,
                                  child: image2 == null
                                      ? CachedNetworkImage(
                                          imageUrl: "$fotoRNSTP",
                                          placeholder: (context, url) =>
                                              circulatorCustom(),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        )
                                      : Image.file(image2!)),
                            ),
                            separador(),
                            Center(
                              child: Text(
                                "Certificado inscripción",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            separador(),
                            const Text(
                              "Foto certificado inscripcion",
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                if (aprobacion == 'revision' ||
                                    aprobacion == 'aprobado') {
                                } else {
                                  if (image_to_upload3 == null) {
                                    setState(() {
                                      image_to_upload3 = image3;
                                    });
                                  } else {
                                    setState(() {
                                      image_to_upload3 = image3;
                                    });
                                  }

                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opcion"),
                                      content: Text(
                                          "¿De que manera desea subir la imagen?"),
                                      actions: [
                                        IconButton(
                                          onPressed: () {
                                            camerais3 = false;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Galeria"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.image),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            camerais3 = true;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Camara"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.camera),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );

                                  await openImagePicker3();
                                  setState(() {
                                    image_to_upload3 = image3;
                                  });

                                  if (controllerMarca.text == '' &&
                                      controllerModelo.text == '' &&
                                      controllerColor.text == '' &&
                                      image_to_upload1 == null &&
                                      image_to_upload2 == null &&
                                      image_to_upload3 == null &&
                                      image_to_upload4 == null) {
                                    homeProvider.isbutton = false;
                                    iboolean = false;
                                    homeProvider.iboolean2 = false;
                                  } else {
                                    homeProvider.isbutton = true;
                                    iboolean = true;
                                    homeProvider.iboolean2 = true;
                                  }
                                }
                              },
                              child: SizedBox(
                                width: 35.w,
                                child: image3 == null
                                    ? CachedNetworkImage(
                                        imageUrl: "$fotoCertificadoInscripcion",
                                        placeholder: (context, url) =>
                                            circulatorCustom(),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      )
                                    : Image.file(
                                        image3!,
                                        width: 35.w,
                                      ),
                              ),
                            ),
                            separador(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Permiso de circulación",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 19.sp,
                                  ),
                                ),
                              ],
                            ),
                            separador(),
                            Text("Fecha vencimiento P.circulación"),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  children: [
                                    Text("DD"),
                                    Container(
                                      width: 18.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        controller:
                                            controllerFechaVencimientoPcirculacionDia,
                                        maxLength: 2,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoPcirculacionDia
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const Text("-"),
                                Column(
                                  children: [
                                    Text("MM"),
                                    Container(
                                      width: 18.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        controller:
                                            controllerFechaVencimientoPcirculacionMes,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoPcirculacionMes
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        maxLength: 2,
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const Text("-"),
                                Column(
                                  children: [
                                    Text("YYYY"),
                                    Container(
                                      width: 38.w,
                                      child: TextField(
                                        enabled: aprobacion == 'revision' ||
                                                aprobacion == 'aprobado'
                                            ? false
                                            : true,
                                        onChanged: (value) {
                                          if (controllerFechaVencimientoPcirculacionAnio
                                                  .text ==
                                              '') {
                                            homeProvider.isbutton = false;
                                            iboolean = false;
                                            homeProvider.iboolean2 = false;
                                          } else {
                                            homeProvider.isbutton = true;
                                            iboolean = true;
                                            homeProvider.iboolean2 = true;
                                          }
                                        },
                                        controller:
                                            controllerFechaVencimientoPcirculacionAnio,
                                        maxLength: 4,
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            separador(),
                            const Text("Foto permiso circulación"),
                            InkWell(
                              onTap: () async {
                                if (aprobacion == 'revision' ||
                                    aprobacion == 'aprobado') {
                                } else {
                                  if (image_to_upload4 == null) {
                                    setState(() {
                                      image_to_upload4 = image4;
                                    });
                                  } else {
                                    setState(() {
                                      image_to_upload4 = image4;
                                    });
                                  }

                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opcion"),
                                      content: Text(
                                          "¿De que manera desea subir la imagen?"),
                                      actions: [
                                        IconButton(
                                          onPressed: () {
                                            //setState(() {
                                            camerais4 = false;
                                            //});
                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Galeria"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.image),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            camerais4 = true;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Camara"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.camera),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );

                                  await openImagePicker4();
                                  setState(() {
                                    image_to_upload4 = image4;
                                  });

                                  if (controllerMarca.text == '' &&
                                      controllerModelo.text == '' &&
                                      controllerColor.text == '' &&
                                      image_to_upload1 == null &&
                                      image_to_upload2 == null &&
                                      image_to_upload3 == null &&
                                      image_to_upload4 == null) {
                                    homeProvider.isbutton = false;
                                    iboolean = false;
                                    homeProvider.iboolean2 = false;
                                  } else {
                                    homeProvider.isbutton = true;
                                    iboolean = true;
                                    homeProvider.iboolean2 = true;
                                  }
                                }
                              },
                              child: SizedBox(
                                  width: 35.w,
                                  child: image4 == null
                                      ? CachedNetworkImage(
                                          imageUrl: "$fotoPermisoCirculacion",
                                          placeholder: (context, url) =>
                                              circulatorCustom(),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        )
                                      : Image.file(image4!)),
                            ),
                            separador(),
                            const Text("Foto certificado taximetro"),
                            InkWell(
                              onTap: () async {
                                if (aprobacion == 'revision' ||
                                    aprobacion == 'aprobado') {
                                } else {
                                  if (image_to_upload5 == null) {
                                    setState(() {
                                      image_to_upload5 = image5;
                                    });
                                  } else {
                                    setState(() {
                                      image_to_upload5 = image5;
                                    });
                                  }

                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opcion"),
                                      content: Text(
                                          "¿De que manera desea subir la imagen?"),
                                      actions: [
                                        IconButton(
                                          onPressed: () {
                                            camerais5 = false;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Galeria"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.image),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            camerais5 = true;

                                            Navigator.pop(context);
                                          },
                                          icon: Row(
                                            children: [
                                              Text("Camara"),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Icon(Icons.camera),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );

                                  await openImagePicker5();
                                  setState(() {
                                    image_to_upload5 = image5;
                                  });

                                  if (controllerMarca.text == '' &&
                                      controllerModelo.text == '' &&
                                      controllerColor.text == '' &&
                                      image_to_upload1 == null &&
                                      image_to_upload2 == null &&
                                      image_to_upload3 == null &&
                                      image_to_upload5 == null) {
                                    homeProvider.isbutton = false;
                                    iboolean = false;
                                    homeProvider.iboolean2 = false;
                                  } else {
                                    homeProvider.isbutton = true;
                                    homeProvider.iboolean2 = true;
                                    iboolean = true;
                                  }
                                }
                              },
                              child: SizedBox(
                                  width: 35.w,
                                  child: image5 == null
                                      ? CachedNetworkImage(
                                          imageUrl: "$fotoTaximetro",
                                          placeholder: (context, url) =>
                                              circulatorCustom(),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        )
                                      : Image.file(image5!)),
                            ),
                            separador(),
                          ],
                        ),
                      ),
                    ),
            ]),
      floatingActionButton: aprobacion == "revision" || aprobacion == "aprobado"
          ? Container()
          : SpeedDial(
              icon: Icons.check,
              activeIcon: Icons.close,
              backgroundColor: Colors.amber.shade700,
              foregroundColor: Colors.deepPurple,
              activeBackgroundColor: Colors.deepPurpleAccent,
              activeForegroundColor: Colors.white,
              visible: true,
              closeManually: false,
              curve: Curves.bounceIn,
              overlayOpacity: 0.5,
              onOpen: () {
                /**/ ('OPENING DIAL');
                /**/ ("impresion final final ${homeProvider.isbutton}");
              },
              onPress: () {
                print(
                    "object final el boolean es: $iboolean y el home provider boolean es: ${homeProvider.iboolean2}");
                if (homeProvider.iboolean2) {
                  modificarDatos(
                    homeProvider,
                    verificarConductorProvider,
                    marca,
                    modelo,
                    motivo,
                    patente,
                    color,
                    fotoRtecnica,
                    patenteRNSTP,
                    fotoRNSTP,
                    fechaVenciomientoRtecnicca,
                    fotoCertificadoInscripcion,
                    fotoPermisoCirculacion,
                    fechaVencimientoPermiso,
                    dniProvider,
                    estadoVehiculoRNSTP,
                    fotoTaximetro,
                    fotoTaximetro,
                    context,
                    controllerFechaVencimientoRtecnicaMes,
                    controllerFechaVencimientoRtecnicaDia,
                    controllerFechaVencimientoPcirculacionMes,
                    controllerFechaVencimientoPcirculacionDia,
                    controllerFechaVencimientoRtecnicaAnio,
                    controllerFechaVencimientoPcirculacionAnio,
                    iboolean,
                    image_to_upload1,
                    image_to_upload2,
                    image_to_upload3,
                    image_to_upload4,
                    image_to_upload5,
                    controllerMarca,
                    controllerModelo,
                    controllerFechaRtecnica,
                    controllerColor,
                    selectedValue2,
                    controllerFechaVencimientoPcirculacion,
                  );
                } else {
                  solicitudRevision(patenteRNSTP);
                }
              },
              onClose: () => /**/ ('DIAL CLOSED'),
              elevation: 8.0,
              shape: const CircleBorder(),
              children: [],
            ),
    );
  }

  Widget inputTextMarca(marca, controller, focus, context, aprobacion,
      homeProvider, modelo, color) {
    return SizedBox(
      width: 70.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobacion == 'revision' || aprobacion == 'aprobado'
                ? false
                : true,
            onChanged: (value) {
              /**/ ("object");
              /**/ (controllerMarca.text);
              /**/ (controllerModelo.text);
              /**/ (controllerColor.text);

              if (controllerMarca.text == '' &&
                  controllerModelo.text == '' &&
                  controllerColor.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null) {
                /**/ ("dentro del if");
                //myGlobalKey = GlobalKey();
                homeProvider.isbutton = false;
                iboolean = false;
                homeProvider.iboolean2 = false;
              } else {
                /**/ (controller.text);
                /**/ (marca);
                // myGlobalKey = GlobalKey();
                homeProvider.isbutton = true;
                iboolean = true;
                homeProvider.iboolean2 = true;
              }
              /**/ (homeProvider.isbutton);
              /**/ ('OPENING DIAL');
              //setState(() {});
            },
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(focus);
            },
            controller: controller,
            decoration: InputDecoration(
              labelText: 'Marca',
              //hintText: '$marca',
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget inputTextModelo(
      modelo, controller, focus, focus2, context, aprobacion, homeProvider) {
    return SizedBox(
      width: 70.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobacion == 'revision' || aprobacion == 'aprobado'
                ? false
                : true,
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(focus2);
            },
            onChanged: (value) {
              /**/ ("object");
              /**/ (controllerMarca.text);
              /**/ (controllerModelo.text);
              /**/ (controllerColor.text);

              if (controllerMarca.text == '' &&
                  controllerModelo.text == '' &&
                  controllerColor.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null) {
                /**/ ("dentro del if");
                //myGlobalKey = GlobalKey();
                homeProvider.isbutton = false;
                iboolean = false;
                homeProvider.iboolean2 = false;
              } else {
                /**/ (controller.text);
                /**/ (modelo);
                // myGlobalKey = GlobalKey();
                homeProvider.isbutton = true;
                iboolean = true;
                homeProvider.iboolean2 = true;
              }
              /**/ (homeProvider.isbutton);
              /**/ ('OPENING DIAL');
              //setState(() {});
            },
            focusNode: focus,
            controller: controller,
            decoration: InputDecoration(
              labelText: 'Modelo',
              //hintText: '$modelo',
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget inputTextColor(
      color, controller, focus, focus2, context, aprobacion, homeProvider) {
    return SizedBox(
      width: 70.w,
      child: Column(
        children: [
          TextFormField(
            enabled: aprobacion == 'revision' || aprobacion == 'aprobado'
                ? false
                : true,
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(focus2);
            },
            onChanged: (value) {
              /**/ ("object");
              /**/ (controllerMarca.text);
              /**/ (controllerModelo.text);
              /**/ (controllerColor.text);

              if (controllerMarca.text == '' &&
                  controllerModelo.text == '' &&
                  controllerColor.text == '' &&
                  image_to_upload1 == null &&
                  image_to_upload2 == null &&
                  image_to_upload3 == null &&
                  image_to_upload4 == null) {
                /**/ ("dentro del if");
                //myGlobalKey = GlobalKey();
                homeProvider.isbutton = false;
                iboolean = false;
                homeProvider.iboolean2 = false;
              } else {
                /**/ (controller.text);
                /**/ (color);
                // myGlobalKey = GlobalKey();
                homeProvider.isbutton = true;
                iboolean = true;
                homeProvider.iboolean2 = true;
              }
              /**/ (homeProvider.isbutton);
              /**/ ('OPENING DIAL');
              //setState(() {});
            },
            focusNode: focus,
            controller: controller,
            decoration: InputDecoration(
              labelText: 'Color',
              //hintText: '$color',
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget btnModificarDatos(
    providerUrl,
    image_to_upload2,
    image_to_upload1,
    image_to_upload3,
    patente,
    marca,
    modelo,
    fechaVencimientoTecnica,
    color,
    fechaVencimientoCirculacion,
    context,
    providerSet,
    fotoRtecnica,
    fotoRNSTP,
    fotoPermisoCirculacion,
    marcaj,
    modeloj,
    colorj,
    fechaRtecnicaj,
    estadoRNSTPj,
    fechaPermisoJ,
    image_to_upload4,
    fotoCertificadoInscripcion,
    dniProvider,
    verificarConductorProvider) {
  return InkWell(
    onTap: () async {
      verificarConductorProvider.setLoading = true;
      /**/ ("presionando el boton");
      /**/ ("$marca y $modelo");
      providerSet.setIsUpload = false;
      if (image_to_upload1 != null ||
          image_to_upload2 != null ||
          image_to_upload3 != null ||
          image_to_upload4 != null) {
        if (image_to_upload1 != null &&
            image_to_upload2 != null &&
            image_to_upload3 != null &&
            image_to_upload4 != null) {
          var data = {
            "marca": marca.text == null ? '$marcaj' : "${marca.text}",
            "modelo": modelo.text == null ? '$modeloj' : "${modelo.text}",
            "revicionTecnica":
                "{\"foto\": \"${image_to_upload1 == null ? fotoRtecnica : providerUrl.getUrlProvider1()}\", \"fechavencimiento\": \"${fechaVencimientoTecnica.text == '' ? fechaRtecnicaj : fechaVencimientoTecnica.text}\"}",
            "color": "${color.text == '' ? colorj : color.text}",
            "certificadoInscripcionRNSTP":
                "{\"patente\": \"$patente\",\"estado\": \"habilitado\",\"fotoCertificadoRNSTP\":\"${image_to_upload2 == null ? fotoRNSTP : providerUrl.getUrlProvider2()}\"}",
            'certificadoInscripcion':
                '${image_to_upload3 == null ? fotoCertificadoInscripcion : providerUrl.getUrlProvider4()}',
            "permisocirculacion":
                "{\"fotoPermisoCirculacion\": \"${image_to_upload3 == null ? fotoPermisoCirculacion : providerUrl.getUrlProvider3()}\", \"fechaVencimientoPermiso\": \"${fechaVencimientoCirculacion.text == '' ? fechaPermisoJ : fechaVencimientoCirculacion.text}\"}",
          };
          await updateVehicle(patente, data);
          var datas = await getAllOneVehicle("$patente");

          dniProvider.setDatasVehicle(data);
          /**/ ("ppppp 4 ${dniProvider.getDatasVehicle()}");

          /**/ ("guardando");
          providerSet.setIsUpload = true;
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.success(
              message: "Datos actualizados correctamente",
            ),
          );
          Future.delayed(
            const Duration(seconds: 1),
            () {
              Navigator.pop(context);
            },
          );
        } else {
          if (image_to_upload1 != null &&
              image_to_upload2 != null &&
              image_to_upload3 != null &&
              image_to_upload4 != null) {
            /**/ ("son null");
            /**/ ("desde el else 1");
            /**/ ("el modelo es: ${modelo.text} y ${marca.text}");
            /**/ ("el color else final es: ${color.text}, $colorj");
            var data = {
              "marca": marca.text == '' ? '$marcaj' : "${marca.text}",
              "modelo": modelo.text == '' ? '$modeloj' : "${modelo.text}",
              "revicionTecnica":
                  "{\"foto\": \"${image_to_upload1 == null ? fotoRtecnica : fotoRtecnica}\", \"fechavencimiento\": \"${fechaVencimientoTecnica.text == '' ? fechaRtecnicaj : fechaVencimientoTecnica.text}\"}",
              "color": "${color.text == '' ? colorj : color.text}",
              "certificadoInscripcionRNSTP":
                  "{\"patente\": \"$patente\",\"estado\": \"habilitado\",\"fotoCertificadoRNSTP\":\"${image_to_upload2 == null ? fotoRNSTP : fotoRNSTP}\"}",
              'certificadoInscripcion':
                  '${image_to_upload3 == null ? fotoCertificadoInscripcion : fotoCertificadoInscripcion}',
              "permisocirculacion":
                  "{\"fotoPermisoCirculacion\": \"${image_to_upload4 == null ? fotoPermisoCirculacion : fotoPermisoCirculacion}\", \"fechaVencimientoPermiso\": \"${fechaVencimientoCirculacion.text == '' ? fechaPermisoJ : fechaVencimientoCirculacion.text}\"}",
            };
            await updateVehicle(patente, data);
            var datas = await getAllOneVehicle("$patente");

            dniProvider.setDatasVehicle(data);
            /**/ ("ppppp 5 ${dniProvider.getDatasVehicle()}");
            /**/ ("guardando");
            providerSet.setIsUpload = true;
            showTopSnackBar(
              Overlay.of(context),
              const CustomSnackBar.success(
                message: "Datos actualizados correctamente",
              ),
            );
          } else {
            /**/ ("los images to upload son: $image_to_upload1, $image_to_upload2, $image_to_upload3, $image_to_upload4");
            showTopSnackBar(
              Overlay.of(context),
              const CustomSnackBar.error(
                message: "Debes actualizar todas las fotos",
              ),
            );
          }
        }
      } else {
        /**/ ("son null");
        /**/ ("desde el else 1");
        /**/ ("el modelo es: ${modelo.text} y ${marca.text}");
        /**/ ("el color else final es: ${color.text}, $colorj");
        var data = {
          "marca": marca.text == '' ? '$marcaj' : "${marca.text}",
          "modelo": modelo.text == '' ? '$modeloj' : "${modelo.text}",
          "revicionTecnica":
              "{\"foto\": \"$fotoRtecnica\", \"fechavencimiento\": \"${fechaVencimientoTecnica.text == '' ? fechaRtecnicaj : fechaVencimientoTecnica.text}\"}",
          "color": "${color.text == '' ? colorj : color.text}",
          "certificadoInscripcionRNSTP":
              "{\"patente\": \"$patente\",\"estado\": \"habilitado\",\"fotoCertificadoRNSTP\":\"$fotoRNSTP\"}",
          'certificadoInscripcion': '$fotoCertificadoInscripcion',
          "permisocirculacion":
              "{\"fotoPermisoCirculacion\": \"$fotoPermisoCirculacion\", \"fechaVencimientoPermiso\": \"${fechaVencimientoCirculacion.text == '' ? fechaPermisoJ : fechaVencimientoCirculacion.text}\"}",
        };
        await updateVehicle(patente, data);
        var datas = await getAllOneVehicle("$patente");

        dniProvider.setDatasVehicle(data);
        /**/ ("ppppp 6 ${dniProvider.getDatasVehicle()}");
        /**/ ("guardando");
        providerSet.setIsUpload = true;
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.success(
            message: "Datos actualizados correctamente",
          ),
        );
      }
      verificarConductorProvider.setLoading = false;
    },
    child: Container(
      width: 50.w,
      height: 8.h,
      decoration: const BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      child: const Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Text(
            "Modificar Datos",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    ),
  );
}
