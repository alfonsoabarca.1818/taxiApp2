import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../../../tools/upper_case.dart';

Future<Future> addPatentes(
    context, homeProvider, dni_duenio, dniProvider) async {
  TextEditingController controllerPatente = TextEditingController();


  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.black,
      title: const Text(
        "Añadir patente",
        style: TextStyle(color: Colors.white),
      ),
      content: Container(
        width: 50.w,
        height: 20.h,
        child: Column(
          children: [
            TextField(
              controller: controllerPatente,
              inputFormatters: [
                UpperCaseTextFormatter(),
              ],
              textCapitalization: TextCapitalization.characters,
              style: const TextStyle(
                color: Colors.white,
              ),
              decoration: const InputDecoration(
                labelStyle: TextStyle(
                  color: Colors.white,
                ),
                hintStyle: TextStyle(
                  color: Colors.white,
                ),
                hintText: 'Ingrese nueva patente',
                labelText: 'Ingrese nueva patente',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () async {

            homeProvider.datasNew.add("${controllerPatente.text}");

            Map data = {};

            for (var i = 0; i < homeProvider.datasNew.length; i++) {
              data.addAll({"patente$i": "${homeProvider.datasNew[i]}"});
            }

            dniProvider.setDni(await getAllPatentes(dni_duenio));


            for (var i = 0; i < dniProvider.getDni().length; i++) {

              homeProvider.setLista = dniProvider.getDni().values.toList();
            }

            var d = await deletePatentes(data, dni_duenio);

            List list = [];
            Map c = d["Datas"];

            list.add(d["Datas"]);

            homeProvider.setLista = list;

            showTopSnackBar(
              Overlay.of(context),
              const CustomSnackBar.success(
                message: "Patente añadida con exito",
              ),
            );

            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Añadir',
              style: TextStyle(color: Colors.black),
            ),
          ),
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
        ),
        ElevatedButton(
          onPressed: () => Navigator.pop(context),
          child: const Text(
            'Cancelar',
            style: TextStyle(color: Colors.black),
          ),
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.red),
          ),
        )
      ],
    ),
  );
}
