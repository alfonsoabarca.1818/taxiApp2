import 'dart:io';

import 'package:admin_taxi_seguro/src/apis/api_auditor/getOneDataDuenioVehicle.dart';
import 'package:dio/dio.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:searchfield/searchfield.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../../../provider/auditor_providers/auditor_provider.dart';
import '../../../../provider/duenio_vehiculo_providers/home_provider.dart';
import '../../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../../tools/calendary.dart';
import '../../../../tools/loading.dart';
import '../../../../tools/separador.dart';
import '../../../../tools/url_base.dart';

class SaveVehicle extends StatefulWidget {
  const SaveVehicle({super.key});

  @override
  State<SaveVehicle> createState() => _SaveVehicleState();
}

class _SaveVehicleState extends State<SaveVehicle> {
  String? selectedValue;
  final List<String> items = ["Habilitado", "Deshabilitado"];

  TextEditingController controllerPatente = TextEditingController();
  TextEditingController controllerMarca = TextEditingController();
  TextEditingController controllerModelo = TextEditingController();
  TextEditingController controllerColor = TextEditingController();
  TextEditingController controllerFechaVencimientoRT = TextEditingController();
  TextEditingController controllerPatenteRNSTP = TextEditingController();
  TextEditingController controllerFechaVencimientoPcirculacion =
      TextEditingController();
  TextEditingController controllerMarcaT = TextEditingController();
  TextEditingController controllerModeloT = TextEditingController();
  TextEditingController controllerNumeroSerieT = TextEditingController();
  TextEditingController controllerFechaControlTaximetroT =
      TextEditingController();
  TextEditingController controllerBajadaBanderaT = TextEditingController();
  TextEditingController controllercaidasParciales200mT =
      TextEditingController();
  TextEditingController controllercaidasParcialesMT = TextEditingController();

  TextEditingController controllerCiudad = TextEditingController();

  List<dynamic> lasCiudades = ["Punta Arenas"];

  FocusNode focusPatente = FocusNode();
  FocusNode focusMarca = FocusNode();
  FocusNode focusModelo = FocusNode();
  FocusNode focusColor = FocusNode();
  FocusNode focusEstadoVehiculo = FocusNode();
  FocusNode focusMarcaTaximetro = FocusNode();
  FocusNode focusModeloTaximetro = FocusNode();
  FocusNode focusNumeroSerieTaximetro = FocusNode();
  FocusNode focusBajadaBandera = FocusNode();
  FocusNode focusCaidasParcialesMetro = FocusNode();
  FocusNode focusCaidasParcialesMinuto = FocusNode();
  FocusNode focusSearch = FocusNode();

  File? image_to_upload1,
      image_to_upload2,
      image_to_upload3,
      image_to_upload4,
      image_to_upload5,
      image_to_upload6;
  var imageUrl;

  bool camerais = false;
  bool camerais2 = false;
  bool camerais3 = false;
  bool camerais4 = false;
  bool camerais5 = false;

  File? image1;
  bool istrue1 = false;

  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais) {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    } else {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    }
  }

  File? image2;
  bool istrue2 = false;

  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais2) {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  File? image3;
  bool istrue3 = false;

  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais3) {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  File? image4;
  bool istrue4 = false;

  final _picker4 = ImagePicker();
  Future<void> openImagePicker4() async {
    if (camerais4) {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    } else {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    }
  }

  File? image5;
  bool istrue5 = false;

  final _picker5 = ImagePicker();
  Future<void> openImagePicker5() async {
    if (camerais5) {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    } else {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    }
  }

  uuploadImagevehiculo1(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageVehiculoRtecnica/$rut';
    var pathimages = imagepath.path;

    print("el path es: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('EL RESPONSE ES: $response');
        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImagevehiculo2(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageVehiculoRnstp/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImagevehiculo3(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageVehiculoInscripcion/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImagevehiculo4(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageVehiculoCirculacion/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImagevehiculo5(imagepath, rut) async {
    print("llamando la funcion");
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageVehiculoCertificadoTaximetro/$rut';
    var pathimages = imagepath.path;

    print("el path es 2: $pathimages");

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      print("en el try del u 2");
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    var homeProvider = Provider.of<HomeProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var verificarConductorProvider = Provider.of<AuditorProvider>(context);
    var dniProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 100.w,
            height: 17.h,
            decoration: BoxDecoration(
              color: Colors.amber.shade700,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
            child: Center(
              child: Text(
                "Agregar vehiculo\nPatente ${homeProvider.getLaPatente}",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.sp,
                    color: Colors.deepPurple),
              ),
            ),
          ),
          verificaDuenioProvider.getLoading == true
              ? circulatorCustom()
              : Expanded(
                  child: ListView(
                  children: [
                    Column(
                      children: [
                        separador(),
                        inputPatente(
                            controllerPatente, homeProvider, focusPatente),
                        separador(),
                        inputMarca(
                            controllerMarca, focusMarca, focusModelo, context),
                        separador(),
                        inputModelo(
                            controllerModelo, focusModelo, focusColor, context),
                        separador(),
                        inputColor(controllerColor, focusColor, context),
                        separador(),
                        Center(
                          child: Text(
                            "Revision tecnica",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        separador(),
                        inputFvencimiento(
                            context, controllerFechaVencimientoRT),
                        separador(),
                        Text(
                          'Foto revision tecnica',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        InkWell(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text("Elija una opcion"),
                                content: Text(
                                    "¿De que manera desea subir la imagen?"),
                                actions: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais = false;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Galeria"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.image),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais = true;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Camara"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.camera),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                            await openImagePicker();

                            if (image_to_upload1 == null) {
                              setState(() {
                                image_to_upload1 = image1;
                              });
                            } else {
                              setState(() {
                                image_to_upload1 = image1;
                              });
                            }
                          },
                          child: SizedBox(
                            width: 35.w,
                            child: istrue1 == false
                                ? Image.asset(
                                    "assets/auditor/agregar.png",
                                    width: 35.w,
                                  )
                                : Image.file(
                                    image1!,
                                    width: 35.w,
                                  ),
                          ),
                        ),
                      ],
                    ),
                    separador(),
                    Center(
                      child: Text(
                        "Certificado inscripción \n RNSTP",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    separador(),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15.w),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2<String>(
                          isExpanded: true,
                          hint: const Row(
                            children: [
                              Icon(
                                Icons.list,
                                size: 16,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Expanded(
                                child: Text(
                                  'Selecciona un estado',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          items: items
                              .map((String item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(
                                      item,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ))
                              .toList(),
                          value: selectedValue,
                          onChanged: (String? value) {
                            setState(() {
                              selectedValue = value;
                            });
                          },
                          buttonStyleData: ButtonStyleData(
                            height: 50,
                            width: 160,
                            padding: const EdgeInsets.only(left: 14, right: 14),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                              color: Colors.deepPurple,
                            ),
                            elevation: 2,
                          ),
                          iconStyleData: const IconStyleData(
                            icon: Icon(
                              Icons.arrow_forward_ios_outlined,
                            ),
                            iconSize: 14,
                            iconEnabledColor: Colors.white,
                            iconDisabledColor: Colors.grey,
                          ),
                          dropdownStyleData: DropdownStyleData(
                            maxHeight: 200,
                            width: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              color: Colors.deepPurple,
                            ),
                            offset: const Offset(-20, 0),
                            scrollbarTheme: ScrollbarThemeData(
                              radius: const Radius.circular(40),
                              thickness: MaterialStateProperty.all<double>(6),
                              thumbVisibility:
                                  MaterialStateProperty.all<bool>(true),
                            ),
                          ),
                          menuItemStyleData: const MenuItemStyleData(
                            height: 40,
                            padding: EdgeInsets.only(left: 14, right: 14),
                          ),
                        ),
                      ),
                    ),
                    separador(),
                    inputPatenteRNSTP(controllerPatenteRNSTP, homeProvider),
                    separador(),
                    const Column(
                      children: [
                        Text(
                          "Foto RNSTP",
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        InkWell(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text("Elija una opcion"),
                                content: Text(
                                    "¿De que manera desea subir la imagen?"),
                                actions: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais2 = false;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Galeria"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.image),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais2 = true;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Camara"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.camera),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                            await openImagePicker2();

                            if (image_to_upload2 == null) {
                              setState(() {
                                image_to_upload2 = image2;
                              });
                            } else {
                              setState(() {
                                image_to_upload2 = image2;
                              });
                            }
                          },
                          child: SizedBox(
                            width: 35.w,
                            child: istrue2 == false
                                ? Image.asset(
                                    "assets/auditor/agregar.png",
                                    width: 35.w,
                                  )
                                : Image.file(
                                    image2!,
                                    width: 35.w,
                                  ),
                          ),
                        ),
                      ],
                    ),
                    separador(),
                    Center(
                      child: Text(
                        "Certificado inscripción",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    separador(),
                    const Column(
                      children: [
                        Text(
                          "Foto certificado inscripcion",
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        InkWell(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text("Elija una opcion"),
                                content: Text(
                                    "¿De que manera desea subir la imagen?"),
                                actions: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais5 = false;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Galeria"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.image),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais5 = true;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Camara"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.camera),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                            await openImagePicker5();

                            if (image_to_upload6 == null) {
                              setState(() {
                                image_to_upload6 = image5;
                              });
                            } else {
                              setState(() {
                                image_to_upload6 = image5;
                              });
                            }
                          },
                          child: SizedBox(
                            width: 35.w,
                            child: istrue5 == false
                                ? Image.asset(
                                    "assets/auditor/agregar.png",
                                    width: 35.w,
                                  )
                                : Image.file(
                                    image5!,
                                    width: 35.w,
                                  ),
                          ),
                        ),
                      ],
                    ),
                    Center(
                      child: Text(
                        "Permiso circulación",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    separador(),
                    inputFpCirculacion(
                        context, controllerFechaVencimientoPcirculacion),
                    separador(),
                    Column(
                      children: [
                        Text(
                          'Foto permiso circulación',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        InkWell(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text("Elija una opcion"),
                                content: Text(
                                    "¿De que manera desea subir la imagen?"),
                                actions: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais3 = false;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Galeria"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.image),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais3 = true;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Camara"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.camera),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                            await openImagePicker3();

                            if (image_to_upload3 == null) {
                              setState(() {
                                image_to_upload3 = image3;
                              });
                            } else {
                              setState(() {
                                image_to_upload3 = image3;
                              });
                            }
                          },
                          child: SizedBox(
                            width: 35.w,
                            child: istrue3 == false
                                ? Image.asset(
                                    "assets/auditor/agregar.png",
                                    width: 35.w,
                                  )
                                : Image.file(
                                    image3!,
                                    width: 35.w,
                                  ),
                          ),
                        ),
                      ],
                    ),
                    separador(),
                    Center(
                      child: Text(
                        "Taximetro",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    separador(),
                    inputMarcaT(controllerMarcaT, focusMarcaTaximetro,
                        focusModeloTaximetro, context),
                    separador(),
                    inputModeloT(controllerModeloT, focusModeloTaximetro,
                        focusNumeroSerieTaximetro, context),
                    separador(),
                    inputNumeroSerieT(controllerNumeroSerieT,
                        focusNumeroSerieTaximetro, context),
                    separador(),
                    inputFcontrolTaximetro(
                        context, controllerFechaControlTaximetroT),
                    separador(),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      child: SearchField<dynamic>(
                          focusNode: focusSearch,
                          searchStyle: TextStyle(fontWeight: FontWeight.bold),
                          controller: controllerCiudad,
                          onSearchTextChanged: (query) {
                            final filter = lasCiudades
                                .where((element) => element
                                    .toLowerCase()
                                    .contains(query.toLowerCase()))
                                .toList();
                            return filter
                                .map((e) => SearchFieldListItem<String>(e,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 4.0),
                                      child: Text(e,
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.deepPurple)),
                                    )))
                                .toList();
                          },
                          textInputAction: TextInputAction.done,
                          suggestionsDecoration: SuggestionDecoration(
                            padding: const EdgeInsets.all(4),
                            border: Border.all(color: Colors.amber.shade700),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          itemHeight: 50,
                          suggestionState: Suggestion.expand,
                          hint: 'Seleccione ciudad',
                          suggestions: lasCiudades
                              .map(
                                (e) => SearchFieldListItem<dynamic>(
                                  e,
                                  child: Center(child: Text(e)),
                                ),
                              )
                              .toList()),
                    ),
                    separador(),
                    Center(
                      child: Text(
                        "Foto certificado taximetro",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Column(
                      children: [
                        InkWell(
                          onTap: () async {
                            focusSearch.unfocus();
                            await showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text("Elija una opcion"),
                                content: Text(
                                    "¿De que manera desea subir la imagen?"),
                                actions: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais4 = false;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Galeria"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.image),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        camerais4 = true;
                                      });
                                      Navigator.pop(context);
                                    },
                                    icon: Row(
                                      children: [
                                        Text("Camara"),
                                        SizedBox(
                                          width: 3.w,
                                        ),
                                        Icon(Icons.camera),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                            await openImagePicker4();

                            if (image_to_upload4 == null) {
                              setState(() {
                                image_to_upload4 = image4;
                              });
                            } else {
                              setState(() {
                                image_to_upload4 = image4;
                              });
                            }
                          },
                          
                          child: SizedBox(
                            width: 35.w,
                            child: istrue4 == false
                                ? Image.asset(
                                    "assets/auditor/agregar.png",
                                    width: 35.w,
                                  )
                                : Image.file(
                                    image4!,
                                    width: 35.w,
                                  ),
                          ),
                        ),
                      ],
                    ),
                    separador(),
                    inputBajadaBanderaT(controllerBajadaBanderaT,
                        focusBajadaBandera, focusCaidasParcialesMetro, context),
                    separador(),
                    inputCaidasParciales200mT(
                        controllercaidasParciales200mT,
                        focusCaidasParcialesMetro,
                        focusCaidasParcialesMinuto,
                        context),
                    separador(),
                    inputCaidasParcialesMT(controllercaidasParcialesMT,
                        focusCaidasParcialesMinuto, context),
                    separador(),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 14.w),
                      child: ElevatedButton(
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.deepPurple)),
                        onPressed: () async {
                          focusSearch.unfocus();

                          var datosduenio = await getOneDatasDuenioVehicle(
                              dniProvider.getRut, dniProvider.getPassword);

                          String partnerDuenio = datosduenio["partner"];


                          if (image_to_upload1 != null ||
                              image_to_upload2 != null ||
                              image_to_upload3 != null ||
                              image_to_upload4 != null ||
                              image_to_upload6 != null) {
                            if (controllerMarca.text == '' ||
                                controllerModelo.text == '' ||
                                controllerColor.text == '' ||
                                controllerFechaVencimientoRT.text == '' ||
                                selectedValue == '' ||
                                controllerFechaVencimientoPcirculacion.text ==
                                    '' ||
                                controllerMarcaT.text == '' ||
                                controllerModeloT.text == '' ||
                                controllerNumeroSerieT.text == '' ||
                                controllerFechaControlTaximetroT.text == '' ||
                                controllerBajadaBanderaT.text == '' ||
                                controllercaidasParciales200mT.text == '' ||
                                controllercaidasParcialesMT.text == '') {
                              showTopSnackBar(
                                Overlay.of(context),
                                const CustomSnackBar.error(
                                  message: "Llene todos los campos",
                                ),
                              );
                            } else {
                              showTopSnackBar(
                                animationDuration: const Duration(seconds: 1),
                                displayDuration: const Duration(seconds: 7),
                                Overlay.of(context),
                                const CustomSnackBar.info(
                                  message: "Guardando datos....",
                                ),
                              );
                              setState(() {
                                verificarConductorProvider.setLoading = true;
                              });

                              var save1 = await uuploadImagevehiculo1(
                                  image_to_upload1, homeProvider.getLaPatente);
                              var save2 = await uuploadImagevehiculo2(
                                  image_to_upload2, homeProvider.getLaPatente);
                              var save3 = await uuploadImagevehiculo3(
                                  image_to_upload6, homeProvider.getLaPatente);
                              var save4 = await uuploadImagevehiculo4(
                                  image_to_upload3, homeProvider.getLaPatente);
                              var save5 = await uuploadImagevehiculo5(
                                  image_to_upload4, homeProvider.getLaPatente);

                              var lastIndex1 = save1.toString().length;

                              print("el $save1 lastindex1 es: $lastIndex1");

                              var lastIndex2 = save2.toString().length;

                              print("el $save2 lastindex2 es: $lastIndex2");

                              var lastIndex3 = save3.toString().length;

                              print("el $save3 lastindex3 es: $lastIndex3");

                              var lastIndex4 = save4.toString().length;

                              print("el $save4 lastindex4 es: $lastIndex4");

                              var lastIndex5 = save5.toString().length;

                              print("el $save5 lastindex5 es: $lastIndex5");

                              var datas = {
                                "id": null,
                                "patente": "${homeProvider.getLaPatente}",
                                "marca": "${controllerMarca.text}",
                                "modelo": "${controllerModelo.text}",
                                "revicionTecnica":
                                    "{\"foto\": \"$url3/${save1.toString().substring(29, lastIndex1)}\", \"fechavencimiento\": \"${controllerFechaVencimientoRT.text}\"}",
                                "color": "${controllerColor.text}",
                                "certificadoInscripcionRNSTP":
                                    "{\"patente\": \"${homeProvider.getLaPatente}\", \"estadovehicle\": \"${selectedValue.toString().toLowerCase()}\", \"fotoCertificadoRNSTP\": \"$url3/${save2.toString().substring(29, lastIndex2)}\"}",
                                'certificadoInscripcion':
                                    '$url3/${save3.toString().substring(29, lastIndex3)}',
                                "permisocirculacion":
                                    "{\"fotoPermisoCirculacion\": \"$url3/${save4.toString().substring(29, lastIndex4)}\", \"fechaVencimientoPermiso\": \"${controllerFechaVencimientoPcirculacion.text}\"}",
                                "taximetro":
                                    "{\"marca\": \"${controllerMarcaT.text}\", \"modelo\": \"${controllerModeloT.text}\", \"numeroSerie\": \"${controllerNumeroSerieT.text}\", \"bajadaBandera\": \"${controllerBajadaBanderaT.text}\", \"caidasparciales200m\": \"${controllercaidasParciales200mT.text}\", \"fechaControlTaximetro\": \"${controllerFechaControlTaximetroT.text}\", \"caidasParcialesporminuto\": \"${controllercaidasParcialesMT.text}\", \"fotoCertificadoTaximetro\": \"$url3/${save5.toString().substring(29, lastIndex5)}\"}",
                                "rutconsuctores": "[{}]",
                                "aprobacion": "rechazado",
                                "motivo": "Cuenta recien creada",
                                "dni": "${dniProvider.getdniduenio}",
                                "partner": partnerDuenio,
                                "ciudad": controllerCiudad.text
                              };

                              await saveVehicle(datas);


                              setState(() {
                                verificarConductorProvider.setLoading = false;
                              });
                              showTopSnackBar(
                                Overlay.of(context),
                                const CustomSnackBar.success(
                                  message: "Datos guardados correctamente",
                                ),
                              );
                              setState(() {
                                verificaDuenioProvider.setLoading = false;
                              });
                              Navigator.pop(context);
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: const Text(
                            "Registrar",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          separador(),
        ],
      ),
    );
  }
}

Widget inputPatente(TextEditingController controller, homeProvider, focus) {
  return Column(
    children: [
      const Text("Patente"),
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          maxLength: 20,
          controller: controller,
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '${homeProvider.getLaPatente}',
          ),
        ),
      ),
    ],
  );
}

Widget inputMarca(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      autofocus: true,
      focusNode: focus,
      maxLength: 255,
      controller: controller,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su marca',
          labelText: 'Ingrese su marca'),
    ),
  );
}

Widget inputModelo(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      focusNode: focus,
      maxLength: 20,
      controller: controller,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su modelo',
          labelText: 'Ingrese su modelo'),
    ),
  );
}

Widget inputColor(TextEditingController controller, focus, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      focusNode: focus,
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su color',
          labelText: 'Ingrese su color'),
    ),
  );
}

Widget inputFvencimiento(context, controller) {
  return Column(
    children: [
      const Text("Fecha vencimiento revision tecnica"),
      InkWell(
        onTap: () {
          getCalendary(context, controller, "", "", "", "", "", "", "", "", "");
        },
        child: SizedBox(
          width: 70.w,
          child: TextFormField(
            controller: controller,
            enabled: false,
            decoration: const InputDecoration(
                suffixIcon: Icon(Icons.calendar_month),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                hintText: 'EJ:2022-09-07'),
          ),
        ),
      ),
    ],
  );
}

Widget inputPatenteRNSTP(TextEditingController controller, homeProvider) {
  return Column(
    children: [
      const Text("Patente RNSTP"),
      SizedBox(
        width: 70.w,
        child: TextFormField(
          enabled: false,
          controller: controller,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '${homeProvider.getLaPatente}',
          ),
        ),
      ),
    ],
  );
}

Widget inputFpCirculacion(context, controller) {
  return Column(
    children: [
      const Text("fecha vencimiento\n permiso circulación"),
      InkWell(
        onTap: () {
          getCalendary(context, controller, "", "", "", "", "", "", "", "", "");
        },
        child: SizedBox(
          width: 70.w,
          child: TextFormField(
            controller: controller,
            enabled: false,
            decoration: const InputDecoration(
                suffixIcon: Icon(Icons.calendar_month),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                hintText: 'EJ:2023-08-05'),
          ),
        ),
      ),
    ],
  );
}

Widget inputMarcaT(TextEditingController controller, focus, focus2, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          maxLength: 20,
          controller: controller,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'Ingrese marca taximetro',
              labelText: 'Ingrese marca taximetro'),
        ),
      ),
    ],
  );
}

Widget inputModeloT(TextEditingController controller, focus, focus2, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          controller: controller,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'Ingrese modelo taximetro',
              labelText: 'Ingrese modelo taximetro'),
        ),
      ),
    ],
  );
}

Widget inputNumeroSerieT(TextEditingController controller, focus, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          maxLength: 20,
          controller: controller,
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'N° serie taximetro',
              labelText: 'N° serie taximetro'),
        ),
      ),
    ],
  );
}

Widget inputFcontrolTaximetro(context, controller) {
  return Column(
    children: [
      const Text("Fecha control taximetro"),
      InkWell(
        onTap: () {
          getCalendary(context, controller, "", "", "", "", "", "", "", "", "");
        },
        child: SizedBox(
          width: 70.w,
          child: TextFormField(
            controller: controller,
            enabled: false,
            decoration: const InputDecoration(
                suffixIcon: Icon(Icons.calendar_month),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                hintText: 'EJ:2023-08-09'),
          ),
        ),
      ),
    ],
  );
}

Widget inputBajadaBanderaT(
    TextEditingController controller, focus, focus2, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          keyboardType: TextInputType.number,
          maxLength: 20,
          controller: controller,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'Ingrese bajada bandera',
              labelText: 'Ingrese bajada bandera'),
        ),
      ),
    ],
  );
}

Widget inputCaidasParciales200mT(
    TextEditingController controller, focus, focus2, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          keyboardType: TextInputType.number,
          controller: controller,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(focus2);
          },
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'Caidas parciales 200m',
              labelText: 'Caidas parciales 200m'),
        ),
      ),
    ],
  );
}

Widget inputCaidasParcialesMT(
    TextEditingController controller, focus, context) {
  return Column(
    children: [
      SizedBox(
        width: 70.w,
        child: TextFormField(
          focusNode: focus,
          keyboardType: TextInputType.number,
          maxLength: 20,
          controller: controller,
          decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hintText: 'Caidas parciales minutos',
              labelText: 'Caidas parciales minuto'),
        ),
      ),
    ],
  );
}
