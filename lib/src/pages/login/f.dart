import 'package:to_csv/to_csv.dart' as exportCSV;

import '../../apis/api_auditor/getApis.dart';

Future<void> createDocumentAuditor() async {
  List<List<String>> listOfLists = [];
  List<String> header = [];
  List<String> lista_datos_auditor = [];

  var datasAuditor = await getAllAuditor();

  Map auditor = datasAuditor[0] as Map;
  //log("El auditor es: $auditor");

  auditor.remove("password");
  auditor.remove("photoPerfil");
  auditor.remove("estado");

  auditor.forEach((key, value) {
    lista_datos_auditor.add(value);
  });
  lista_datos_auditor.remove('');

  header.add('ID');
  header.add('NOMBRE');
  header.add('APELLIDO');
  header.add('RUT');
  header.add('CORREO');
  header.add('TELEFONO');

  listOfLists.add(lista_datos_auditor);

  exportCSV.myCSV(header, listOfLists);
}

Future<void> createDocumentDuenioVehicle() async {

  List<List<String>> listOfLists = [];
  List<String> header = [];
  List datasDuenio = await getAllDatasDuenio();
  List<Map> listamapoa = [];



  datasDuenio.forEach((element) {
    listamapoa.add(element);
  });


  listamapoa.forEach((element) {
    element.remove("password");
    element.remove("photoPerfil");
    element.remove("estado");
    element.remove("status");
    element.remove("aprobacion");
    element.remove("motivo");
  });


  header.add('ID');
  header.add('NOMBRE');
  header.add('APELLIDO');
  header.add('RUT');
  header.add('CORREO');
  header.add('TELEFONO');
  header.add("fotoCarnetA");
  header.add("fotoCarnetB");
  header.add("tipoLicencia");
  header.add("restricción");
  header.add("fechaVencimientoLicencia");

  List<String> listastring = [];
  List<String> listanombres1 = [];
  List<String> listanombres2 = [];
  List<String> listanombres3 = [];

  listamapoa.forEach((element) {
    listastring.add(element.toString());
  });


  Map elmapa = listamapoa[0];
  Map elmapa2 = listamapoa[1];
   Map elmapa3 = listamapoa[2];

  elmapa.forEach((key, value) {
    listanombres1.add(value.toString());
  });

  elmapa2.forEach((key, value) {
    listanombres2.add(value.toString());
  });

    elmapa3.forEach((key, value) {
    listanombres3.add(value.toString());
  });



  listastring.remove("{}");

  //listOfLists.add(listastring);
  listOfLists.add(listanombres1);
  listOfLists.add(listanombres2);
  listOfLists.add(listanombres3);

  exportCSV.myCSV(header, listOfLists);
}

Future<void> createDocumentConductor() async {
  var datasAuditor = await getAllDatasConductores();

  Map auditor = datasAuditor[0] as Map;
  //log("El auditor es: $auditor");

  List<String> lista_datos_auditor = [];
  List<String> header = [];

  header.add('ID');
  header.add('NOMBRE');
  header.add('APELLIDO');
  header.add('RUT');
  header.add('CORREO');
  header.add('TELEFONO');
  header.add('PATENTES');
  header.add('TIPO LICENCIA');
  header.add('RESTRICCION');
  header.add('FECHA VENCIMIENTO LICENCIA');
  //header.add("photoPerfil");

  auditor.remove("password");
  auditor.remove("photoPerfil");
  auditor.remove("estado");
  auditor.remove("status");
  auditor.remove("conectado");
  auditor.remove("fotoCarnetA");
  auditor.remove("fotoCarnetB");
  auditor.remove("fotoLicenciaConducirA");
  auditor.remove("fotoLicenciaConducirB");
  List<List<String>> listOfLists = [];

  auditor.forEach((key, value) {


    lista_datos_auditor.add(value);
  });

  listOfLists.add(lista_datos_auditor);

  exportCSV.myCSV(header, listOfLists);
}
