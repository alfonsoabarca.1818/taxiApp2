import 'package:admin_taxi_seguro/src/apis/api_auditor/getAllConductoresPartner.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:uuid/uuid.dart';

import '../../../apis/api_auditor/loginAuditor.dart';
import '../../../apis/api_auditor/update_estado.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/send_email.dart';
import '../../../tools/separador.dart';
import '../../home/home_auditor/home_auditor.dart';
import '../select_user.dart';
import 'widgets/widgets.dart';

class LoginAuditor extends StatefulWidget {
  const LoginAuditor({super.key});

  @override
  State<LoginAuditor> createState() => _LoginState();
}

class _LoginState extends State<LoginAuditor> {
  TextEditingController controllerRut = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  FocusNode focusRut = FocusNode();
  FocusNode focusPass = FocusNode();

  bool validateRutChile = false;
  bool validate2 = false;
  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {

      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  @override
  void initState() {
    super.initState();
  }

  void onChangedApplyFormat(String text) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var status;
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);

    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => SelectUser()),
                (route) => false, // Esto elimina todas las pantallas anteriores
          );
          return true;
        },
        child: Container(
          width: 100.w,
          height: 100.h,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.center,
                end: Alignment.centerRight,
                colors: [
                  Color(0xFFEE973A),
                  Color(0xFFA14BCC),
                ]),
          ),
          child: Column(
            children: [
              separador(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        gradient: LinearGradient(
                            begin: Alignment.center,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xffffffff),
                              Color(0XFFFFFFFF),
                            ]),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          "Auditor",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        separador(),
                        Text(
                          "LOGIN",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 24.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Times New Roman'),
                        ),
                        separador(),
                        Focus(
                          onFocusChange: (value) {
                            if (value == false) {
                              String dad = rutValidate(controllerRut.text);

                              controllerRut.text = dad.toString();
                            }
                          },
                          child: inputTextDni(
                              controllerRut,
                              verificaDuenioProvider,
                              focusPass,
                              context,
                              validate2,
                              rutValidate,
                              validateRutChile),
                        ),
                        separador(),
                        inputTextPassword(controllerPassword, focusPass),
                       // separador(),
                        TextButton(
                            onPressed: () async =>
                            await send(context),
                            child: const Text(
                              "¡¡Problema con mi cuenta!!", style: TextStyle(color: Colors.black),)),
                        separador(),
                        InkWell(
                          onTap: () async {
                            final SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            final String? idsession =
                                prefs.getString('idsession');

                            await prefs.setString('user_rut', controllerRut.text);

                            await prefs.setString(
                                'user_password', controllerPassword.text);

                            String idGenerate = '';
                            if (idsession == null) {

                              var uuid = Uuid();



                              setState(() {
                                idGenerate = uuid.v4().toString();
                              });


                            }


                            if (controllerPassword.text.toString().length <= 4) {
                              showTopSnackBar(
                                Overlay.of(context),
                                const CustomSnackBar.error(
                                  message:
                                      "La contraseña debe tener un minimo de 5 caracteres",
                                ),
                              );
                            } else {
                              var estado =
                                  await SelectEstadoAuditor(controllerRut.text);



                              var estado2 = estado["data"]["estado"];


                              if (estado2 == "desconectado" || estado2 == null) {


                                setState(() {
                                  verificaDuenioProvider.setRutAuditor =
                                      controllerRut.text;
                                });

                                status = await postLoginAuditor(
                                    controllerRut.text, controllerPassword.text);
                                var s = status;


                                if (s == true &&
                                    verificaDuenioProvider.getIsRutTrue == true) {
                                  var modificado = await updateEstadoAuditor(
                                      'conectado', controllerRut.text);

                                  Map auditor =
                                      await getOneAuditor(controllerRut.text);





                                  String nombreAuditor =
                                      auditor["names"].toString();

                                  verificaDuenioProvider.nameAuditor =
                                      nombreAuditor;


                                  showTopSnackBar(
                                    Overlay.of(context),
                                    const CustomSnackBar.success(
                                      message: "Datos ingresados son correctos",
                                    ),
                                  );
                                  await prefs.setString(
                                      'idsession', idGenerate);
                                  await prefs.setString("tipo", "auditor");

                                  final String? idsession =
                                  prefs.getString('idsession');


                                  controllerRut.text = "";
                                  controllerPassword.text = "";

                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomeAuditor()),
                                        (route) => false, // Esto elimina todas las pantallas anteriores
                                  );


                                } else {
                                  showTopSnackBar(
                                    Overlay.of(context),
                                    const CustomSnackBar.error(
                                      message:
                                          "Ha ocurrido un error. Por favor verifica las credenciales e intenta de nuevo",
                                    ),
                                  );
                                }
                              } else {
                                showTopSnackBar(
                                  Overlay.of(context),
                                  const CustomSnackBar.error(
                                    message:
                                        "Este usuario se encuentra conectado actualmente",
                                  ),
                                );

                              }
                            }
                            //});
                          },
                          child: Container(
                            width: Device.deviceType == DeviceType.android
                                ? 70.w
                                : 70.w,
                            height: Device.deviceType == DeviceType.android
                                ? 5.h
                                : 5.h,
                            decoration: const BoxDecoration(

                                color: Colors.deepPurple,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Center(
                                child: Text(
                              "Login",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18.sp),
                            )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
