import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../tools/separador.dart';

class RegisterDuenio extends StatefulWidget {
  const RegisterDuenio({super.key});

  @override
  State<RegisterDuenio> createState() => _RegisterDuenioState();
}

class _RegisterDuenioState extends State<RegisterDuenio> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerDni = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerPhotoPerfil = TextEditingController();

  File? image;
bool istrue = false;
  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    final XFile? pickedImage =
        await _picker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      setState(() {
        image = File(pickedImage.path);
        istrue = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: 100.w,
        height: 100.h,
        child: Column(
          children: [
            titlePage(),
            Expanded(
              child: ListView(
                children: [
                  Column(
                    children: [
                      separador(),
                      inputName(controllerName),
                      separador(),
                      inputLastName(controllerLastName),
                      separador(),
                      inputDni(controllerDni),
                      separador(),
                      inputEmail(controllerEmail),
                      separador(),
                      inputPhone(controllerPhone),
                      separador(),
                      inputPassword(controllerPassword),
                      separador(),
                      Column(
                        children: [
                          Text('Foto perfil'),
                          InkWell(
                            onTap: () {
                              openImagePicker();
                            },
                            child: Container(
                              width: 35.w,
                              child: istrue == false ? Image.asset(
                                "assets/auditor/agregar.png",
                                width: 35.w,
                              ):Image.file(
                                image!,
                                width: 35.w,
                              ),
                            ),
                          ),
                        ],
                      ),
                      separador(),
                      btnRegister(),
                      separador(),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget titlePage() {
  return Container(
    width: 100.w,
    height: 17.h,
    decoration: const BoxDecoration(
      color: Colors.black,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        bottomRight: Radius.circular(20),
      ),
    ),
    child: Center(
      child: Text(
        "Registro",
        style: TextStyle(
            fontSize: 18.sp, fontWeight: FontWeight.bold, color: Colors.white),
      ),
    ),
  );
}

Widget inputName(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su nombre',
          labelText: 'Ingrese su nombre'),
    ),
  );
}

Widget inputLastName(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su apellido',
          labelText: 'Ingrese su apellido'),
    ),
  );
}

Widget inputDni(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su rut',
          labelText: 'Ingrese su rut'),
    ),
  );
}

Widget inputEmail(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su correo',
          labelText: 'Ingrese su correo'),
    ),
  );
}

Widget inputPhone(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su telefono',
          labelText: 'Ingrese su telefono'),
    ),
  );
}

Widget inputPassword(TextEditingController controller) {
  return Container(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      obscureText: true,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su contraseña',
          labelText: 'Ingrese su contraseña'),
    ),
  );
}

Widget btnRegister() {
  return Container(
    width: 30.w,
    decoration: const BoxDecoration(
      color: Colors.black,
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
          child: Text(
        "Registrar",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.sp),
      )),
    ),
  );
}
