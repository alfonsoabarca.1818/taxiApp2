
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../apis/api_auditor/loginAuditor.dart';
import '../../../home/home_auditor/home_auditor.dart';

Widget inputTextDni(controller, verificaDuenioProvider, focus, context, validate2, rutValidate, validateRutChile) {
  return Container(
    width: 70.w,

    child: TextFormField(
      autofocus: true,

      controller: controller,
      autovalidateMode: AutovalidateMode.always,
      keyboardType: TextInputType.datetime,
       validator: (value) {
        validate2 = false;
        if (value!.isEmpty) {
          return 'Debes Ingresar RUT Valido';
        } else if (value.length < 7) {
          return 'Debes Ingresar RUT Valido';
        } else if ((value.length >= 8)) {
          value = rutValidate(value);
          bool isveri = isRutValid(value!);

          if (isveri) {
            validate2 = true;
            return null;
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        } else {
          return 'si termina en K reemplaza por un 0';
        }
      },
      maxLength: 12,
      onEditingComplete: () {
        if (validateRutChile == true) {
          String dad = rutValidate(controller.text);
          validate2 = true;
          FocusScope.of(context!).nextFocus();
        }
        FocusScope.of(context).requestFocus(focus);
      },
      onChanged: (value) {
        var formattedRut = formatRut(controller.text);
        verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);

      },
      onTap: () {
        validate2 = false;
        controller.text = deFormatRut(controller.text);
      },
      inputFormatters: [RutFormatter()],
      decoration: const InputDecoration(
        filled: true,
        fillColor: Colors.white,

        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Rut:',
      ),
    ),
  );
}

Widget inputTextPassword(controller, focus) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      focusNode: focus,
      controller: controller,
      maxLength: 10,
      obscureText: true,
      decoration: const InputDecoration(
        filled: true,
        fillColor: Colors.white,

        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Contraseña:',
      ),
    ),
  );
}

Widget buttonLogin(BuildContext context, controllerRut, controllerPassword) {
  Map status;
  return InkWell(
    onTap: () async {
      if (controllerPassword.toString().length <= 4) {
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.error(
            message: "La contraseña debe tener un minimo de 5 caracteres",
          ),
        );
        controllerRut.text = '';
        controllerPassword.text = '';
      } else {
        status = await postLoginAuditor(controllerRut, controllerPassword);
        var s = status;

        if (s == true) {
          controllerRut.text = '';
          controllerPassword.text = '';
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const HomeAuditor(),
            ),
          );
        } else {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message:
                  "Ha ocurrido un error. Por favor verifica las credenciales e intenta de nuevo",
            ),
          );
        }
      }
    },
    child: Container(
      width: Device.deviceType == DeviceType.android ? 70.w : 70.w,
      height: Device.deviceType == DeviceType.android ? 5.h : 5.h,
      decoration: const BoxDecoration(
          color: Colors.orange,
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: const Center(child: Text("Login")),
    ),
  );
}
