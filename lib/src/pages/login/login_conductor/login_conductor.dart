// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:developer';

import 'package:admin_taxi_seguro/src/apis/api_auditor/update_estado.dart';
import 'package:admin_taxi_seguro/src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:uuid/uuid.dart';

import '../../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/send_email.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';
import '../../home/home_conductor/home_conductor.dart';
import '../select_user.dart';
import 'widgets/widgets.dart';

class LoginConductor extends StatefulWidget {
  const LoginConductor({super.key});

  @override
  State<LoginConductor> createState() => _LoginConductorState();
}

class _LoginConductorState extends State<LoginConductor> {
  TextEditingController controllerRut = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  FocusNode focusRut = FocusNode();
  FocusNode focusPass = FocusNode();

  bool validateRutChile = false;
  bool validate2 = false;
  bool pantellaNext = false;
  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);
      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  Dio dio = Dio();

  /*getBynaryAllImageConductor(String rut) async {
    try {
      Response resp = await dio.post("$url/getImagesConductor/$rut",
          options: Options(
            receiveTimeout: Duration(seconds: 5),
          ));

      if (resp.statusCode == 200) {
        var image1 = resp.data;

        Map<dynamic, dynamic> mapita1 = image1 as Map<dynamic, dynamic>;

        List imagebynarydynamic1 = mapita1["image1"];
        List imagebynarydynamic2 = mapita1["image2"];
        List imagebynarydynamic3 = mapita1["image3"];
        List imagebynarydynamic4 = mapita1["image4"];
        List imagebynarydynamic5 = mapita1["image5"];
        List imagebynarydynamic6 = mapita1["image6"];

        List<int> imageByteInt1 = imagebynarydynamic1.cast<int>();
        List<int> imageByteInt2 = imagebynarydynamic2.cast<int>();
        List<int> imageByteInt3 = imagebynarydynamic3.cast<int>();
        List<int> imageByteInt4 = imagebynarydynamic4.cast<int>();
        List<int> imageByteInt5 = imagebynarydynamic5.cast<int>();
        List<int> imageByteInt6 = imagebynarydynamic6.cast<int>();

        List<List> mislistas = [
          imageByteInt1,
          imageByteInt2,
          imageByteInt3,
          imageByteInt4,
          imageByteInt5,
          imageByteInt6,
        ];

        return mislistas;
      } else {
        showTopSnackBar(
          Overlay.of(context),
          CustomSnackBar.error(
            message:
                "No se ha podido iniciar session porfavor intenta nuevamente",
          ),
        );
      }
    } catch (e) {
      print("Ha ocurrido un error al obtener el bynario: $e");
    }
  }*/

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var loginProvider = Provider.of<LoginProvider>(context);

    var verificarConductorProvider = Provider.of<AuditorProvider>(context);

    //getDatas(verificarConductorProvider);

    var status;
    return Scaffold(
      body: verificaDuenioProvider.getLoading == true
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                   Column(
                     children: [
                       Text("Iniciando sesion"),
                       Text("esto puede tardar un momento"),
                       CircularProgressIndicator.adaptive(
                         backgroundColor: Colors.amber,
                       ),
                     ],
                   )
                  ],
                ),
              ],
            )
          : WillPopScope(
              onWillPop: () async {
                final SharedPreferences prefs =
                    await SharedPreferences.getInstance();

                /* var modificado = await updateEstadoConductor(
              'desconectado', controllerRut.text);*/

                await prefs.remove('idsession');
                await prefs.remove('user_rut');
                await prefs.remove('user_password');
                await prefs.remove('tipo');
                await prefs.remove('id');

                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => SelectUser()),
                  (route) =>
                      false, // Esto elimina todas las pantallas anteriores
                );
                return true;
              },
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.center,
                      end: Alignment.centerRight,
                      colors: [
                        Color(0xFFEE973A),
                        Color(0xFFA14BCC),
                      ]),
                ),
                child: loginProvider.loginConductor == true
                    ? circulatorCustom()
                    : ListView(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              separador(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                        color: Colors.white,
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Text(
                                          "Conductor",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.sp),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              separador(),
                              Text(
                                "LOGIN",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 24.sp,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Times New Roman'),
                              ),
                              separador(),
                              Focus(
                                onFocusChange: (value) {
                                  if (value == false) {
                                    String dad =
                                        rutValidate(controllerRut.text);

                                    controllerRut.text = dad.toString();
                                  }
                                },
                                child: inputTextDni(
                                    controllerRut,
                                    verificaDuenioProvider,
                                    focusPass,
                                    context,
                                    validate2,
                                    rutValidate,
                                    validateRutChile,
                                    pantellaNext),
                              ),
                              separador(),
                              inputTextPassword(controllerPassword, focusPass),
                              //separador(),
                              TextButton(
                                  onPressed: () async => await send(context),
                                  child: const Text(
                                    "¡¡Problema con mi cuenta!!",
                                    style: TextStyle(color: Colors.black),
                                  )),
                              separador(),
                              InkWell(
                                onTap: () async {
                                  final SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  final String? idsession =
                                      prefs.getString('idsession');

                                  await prefs.setString(
                                      'user_rut', controllerRut.text);

                                  await prefs.setString(
                                      'user_password', controllerPassword.text);

                                  String idGenerate = '';
                                  if (idsession == null) {
                                    var uuid = Uuid();

                                    setState(() {
                                      idGenerate = uuid.v4().toString();
                                    });
                                  }

                                  if (controllerRut.text == '' ||
                                      controllerPassword.text == '') {
                                    showTopSnackBar(
                                      Overlay.of(context),
                                      const CustomSnackBar.error(
                                        message: "Contraseña o rut invalido",
                                      ),
                                    );
                                  } else {
                                    if (controllerRut.text.contains(".") &&
                                        controllerRut.text.contains("-")) {
                                      String dad =
                                          rutValidate(controllerRut.text);

                                      controllerRut.text = dad.toString();

                                      validate2 = false;

                                      bool isveri = isRutValid(dad.toString());
                                      if (isveri) {
                                        if (controllerPassword.text
                                                .toString()
                                                .length <=
                                            4) {
                                          showTopSnackBar(
                                            Overlay.of(context),
                                            const CustomSnackBar.error(
                                              message:
                                                  "La contraseña debe tener un minimo de 5 caracteres",
                                            ),
                                          );
                                        } else {
                                          setState(() {
                                            loginProvider.loginConductor = true;
                                          });

                                          var datasConductor =
                                              await getOneDatasConductor(
                                                  controllerRut.text);

                                          if (datasConductor == null) {
                                            showTopSnackBar(
                                              Overlay.of(context),
                                              const CustomSnackBar.error(
                                                message:
                                                    "¡¡¡Error!!!\nVerifique los datos o cree una cuenta.",
                                              ),
                                            );
                                            setState(() {
                                              loginProvider.loginConductor =
                                                  false;
                                            });
                                          } else {
                                            /* var data = await getOneDatasConductor(
                                                controllerRut.text);*/

                                            Map mimap = datasConductor as Map;

                                            // log("el mimap es: ${mimap}");

                                            if (mimap["estado"] ==
                                                    "desconectado" ||
                                                mimap["estado"] == null) {
                                              if (controllerPassword.text ==
                                                      mimap["password"]
                                                          .toString() &&
                                                  controllerRut.text ==
                                                      mimap["dni"].toString()) {
                                                verificarConductorProvider
                                                    .setDatasVerificarOneConductor(
                                                        datasConductor);
                                                verificarConductorProvider
                                                    .setLoading = false;

                                                if (verificarConductorProvider
                                                                .getDatasVerificarOneConductor()[
                                                            "aprobacion"] ==
                                                        "aprobado" ||
                                                    verificarConductorProvider
                                                                .getDatasVerificarOneConductor()[
                                                            "aprobacion"] ==
                                                        "rechazado" ||
                                                    verificarConductorProvider
                                                                .getDatasVerificarOneConductor()[
                                                            "aprobacion"] ==
                                                        "rechasado") {
                                                  //&&verificaDuenioProvider.getIsRutTrue == true

                                                  setState(() {
                                                    verificaDuenioProvider
                                                            .setRutAuditor =
                                                        controllerRut.text;
                                                  });

                                                  verificarConductorProvider
                                                          .setAprobadoProvider =
                                                      verificarConductorProvider
                                                              .getDatasVerificarOneConductor()[
                                                          "aprobacion"];

                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = true;
                                                  });

                                                  var modificado =
                                                      await updateEstadoConductor(
                                                          'conectado',
                                                          controllerRut.text);

                                                  await prefs.setString(
                                                      'idsession', idGenerate);
                                                  await prefs.setString(
                                                      "tipo", "conductor");

                                                  final String? idsession =
                                                      prefs.getString(
                                                          'idsession');

                                                /*  List<List> miListasImage =
                                                      await getBynaryAllImageConductor(
                                                          controllerRut.text);

                                                  List image1byte =
                                                      miListasImage[0];
                                                  List image2byte =
                                                      miListasImage[1];
                                                  List image3byte =
                                                      miListasImage[2];
                                                  List image4byte =
                                                      miListasImage[3];
                                                  List image5byte =
                                                      miListasImage[4];
                                                  List image6byte =
                                                      miListasImage[5];

                                                  loginProvider
                                                          .milistaFotoPerfilConductor =
                                                      image1byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoCarnetAconductor =
                                                      image2byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoCarnetBconductor =
                                                      image3byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoLicenciaAconductor =
                                                      image4byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoLicenciaBconductor =
                                                      image5byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoAntecedentesConductor =
                                                      image6byte
                                                          .map((e) => e as int)
                                                          .toList();*/

                                                  controllerRut.text = '';
                                                  controllerPassword.text = '';
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = false;
                                                  });
                                                  setState(() {
                                                    pantellaNext = true;
                                                  });
                                                  Navigator.pushAndRemoveUntil(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            HomeConductor()),
                                                    (route) =>
                                                        false, // Esto elimina todas las pantallas anteriores
                                                  );

                                                  setState(() {
                                                    loginProvider
                                                        .loginConductor = false;
                                                  });
                                                  loginProvider.loginConductor =
                                                      false;
                                                } else if (verificarConductorProvider
                                                            .getDatasVerificarOneConductor()[
                                                        "aprobacion"] ==
                                                    "revision") {
                                                  await prefs.setString(
                                                      'idsession', idGenerate);
                                                  await prefs.setString(
                                                      "tipo", "conductor");

                                                  final String? idsession =
                                                      prefs.getString(
                                                          'idsession');

                                                  setState(() {
                                                    verificaDuenioProvider
                                                            .setRutAuditor =
                                                        controllerRut.text;
                                                  });
                                                  verificarConductorProvider
                                                          .setAprobadoProvider =
                                                      verificarConductorProvider
                                                              .getDatasVerificarOneConductor()[
                                                          "aprobacion"];
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = true;
                                                  });

                                                  var modificado =
                                                      await updateEstadoConductor(
                                                          'conectado',
                                                          controllerRut.text);
                                                  /*List<List> miListasImage =
                                                      await getBynaryAllImageConductor(
                                                          controllerRut.text);

                                                  List image1byte =
                                                      miListasImage[0];
                                                  List image2byte =
                                                      miListasImage[1];
                                                  List image3byte =
                                                      miListasImage[2];
                                                  List image4byte =
                                                      miListasImage[3];
                                                  List image5byte =
                                                      miListasImage[4];
                                                  List image6byte =
                                                      miListasImage[5];

                                                  loginProvider
                                                          .milistaFotoPerfilConductor =
                                                      image1byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoCarnetAconductor =
                                                      image2byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoCarnetBconductor =
                                                      image3byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoLicenciaAconductor =
                                                      image4byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoLicenciaBconductor =
                                                      image5byte
                                                          .map((e) => e as int)
                                                          .toList();

                                                  loginProvider
                                                          .milistaFotoAntecedentesConductor =
                                                      image6byte
                                                          .map((e) => e as int)
                                                          .toList();*/
                                                  controllerRut.text = '';
                                                  controllerPassword.text = '';
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = false;
                                                  });
                                                  setState(() {
                                                    pantellaNext = true;
                                                  });
                                                  Navigator.pushAndRemoveUntil(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            HomeConductor()),
                                                    (route) =>
                                                        false, // Esto elimina todas las pantallas anteriores
                                                  );
                                                } else if (verificarConductorProvider
                                                            .getDatasVerificarOneConductor()[
                                                        "aprobacion"] ==
                                                    "desaprobado") {
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    const CustomSnackBar.error(
                                                      message:
                                                          "Su cuenta no se encuentra activa \n contactar al administrador",
                                                    ),
                                                  );
                                                  setState(() {
                                                    loginProvider
                                                        .loginConductor = false;
                                                  });
                                                } else {
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    const CustomSnackBar.error(
                                                      message:
                                                          "Ha ocurrido un error. Por favor verifica las credenciales e intenta de nuevo",
                                                    ),
                                                  );
                                                  setState(() {
                                                    loginProvider
                                                        .loginConductor = false;
                                                  });
                                                }
                                              } else {
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.error(
                                                    message:
                                                        "Ha ocurrido un error, verifica los datos o crea una cuenta si no lo has echo",
                                                  ),
                                                );
                                              }
                                            } else {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                const CustomSnackBar.error(
                                                  message:
                                                      "Este usuario se encuentra conectado actualmente",
                                                ),
                                              );
                                            }
                                          }
                                        }
                                      } else {
                                        showTopSnackBar(
                                          Overlay.of(context),
                                          const CustomSnackBar.error(
                                            message: "Rut no valido",
                                          ),
                                        );
                                      }
                                    } else {
                                      var pass, rutt;
                                      if (controllerRut.text
                                              .toString()
                                              .contains('.') &&
                                          controllerRut.text
                                              .toString()
                                              .contains('-')) {
                                        var data = await getOneDatasConductor(
                                            controllerRut.text);

                                        Map mimap = data as Map;
                                        setState(() {
                                          pass = mimap["password"].toString();
                                          rutt = mimap["dni"].toString();
                                        });
                                      } else {
                                        String dad =
                                            rutValidate(controllerRut.text);

                                        controllerRut.text = dad.toString();

                                        validate2 = false;
                                      }

                                      if (controllerPassword.text == pass &&
                                          controllerRut.text == rutt) {
                                        setState(() {
                                          loginProvider.loginConductor = true;
                                        });
                                        var formattedRut =
                                            formatRut(controllerRut.text);
                                        verificaDuenioProvider.setIsRutTrue =
                                            isRutValid(formattedRut);

                                        String dad = rutValidate(formattedRut);

                                        controllerRut.text = dad.toString();

                                        validate2 = false;

                                        bool isveri =
                                            isRutValid(dad.toString());
                                        if (isveri) {
                                          if (controllerPassword.text
                                                  .toString()
                                                  .length <=
                                              4) {
                                            showTopSnackBar(
                                              Overlay.of(context),
                                              const CustomSnackBar.error(
                                                message:
                                                    "La contraseña debe tener un minimo de 5 caracteres",
                                              ),
                                            );
                                            setState(() {
                                              loginProvider.loginConductor =
                                                  false;
                                            });
                                          } else {
                                            if (await getOneDatasConductor(
                                                    controllerRut.text) ==
                                                null) {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                const CustomSnackBar.error(
                                                  message:
                                                      "Ha ocurrido un error, verifica los datos o crea una cuenta si no lo has echo",
                                                ),
                                              );
                                              setState(() {
                                                loginProvider.loginConductor =
                                                    false;
                                              });
                                            } else {
                                              verificarConductorProvider
                                                  .setDatasVerificarOneConductor(
                                                      await getOneDatasConductor(
                                                          controllerRut.text));
                                              verificarConductorProvider
                                                  .setLoading = false;

                                              if (verificarConductorProvider
                                                              .getDatasVerificarOneConductor()[
                                                          "aprobacion"] ==
                                                      "aprobado" ||
                                                  verificarConductorProvider
                                                              .getDatasVerificarOneConductor()[
                                                          "aprobacion"] ==
                                                      "rechazado" ||
                                                  verificarConductorProvider
                                                              .getDatasVerificarOneConductor()[
                                                          "aprobacion"] ==
                                                      "rechasado") {
                                                //&&verificaDuenioProvider.getIsRutTrue == true

                                                await prefs.setString(
                                                    'idsession', idGenerate);

                                                await prefs.setString(
                                                    "tipo", "conductor");

                                                final String? idsession = prefs
                                                    .getString('idsession');

                                                setState(() {
                                                  verificaDuenioProvider
                                                          .setRutAuditor =
                                                      controllerRut.text;
                                                });

                                                verificarConductorProvider
                                                        .setAprobadoProvider =
                                                    verificarConductorProvider
                                                            .getDatasVerificarOneConductor()[
                                                        "aprobacion"];

                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = true;
                                                });

                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.success(
                                                    message:
                                                        "Datos ingresados son correctos",
                                                  ),
                                                );

                                                var modificado =
                                                    await updateEstadoConductor(
                                                        'conectado',
                                                        controllerRut.text);
                                               /* List<List> miListasImage =
                                                    await getBynaryAllImageConductor(
                                                        controllerRut.text);

                                                List image1byte =
                                                    miListasImage[0];
                                                List image2byte =
                                                    miListasImage[1];
                                                List image3byte =
                                                    miListasImage[2];
                                                List image4byte =
                                                    miListasImage[3];
                                                List image5byte =
                                                    miListasImage[4];
                                                List image6byte =
                                                    miListasImage[5];

                                                loginProvider
                                                        .milistaFotoPerfilConductor =
                                                    image1byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoCarnetAconductor =
                                                    image2byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoCarnetBconductor =
                                                    image3byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoLicenciaAconductor =
                                                    image4byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoLicenciaBconductor =
                                                    image5byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoAntecedentesConductor =
                                                    image6byte
                                                        .map((e) => e as int)
                                                        .toList();*/

                                                controllerRut.text = '';
                                                controllerPassword.text = '';
                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = false;
                                                });
                                                setState(() {
                                                  loginProvider.loginConductor =
                                                      false;
                                                });
                                                setState(() {
                                                  pantellaNext = true;
                                                });
                                                Navigator.pushAndRemoveUntil(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          HomeConductor()),
                                                  (route) =>
                                                      false, // Esto elimina todas las pantallas anteriores
                                                );
                                              } else if (verificarConductorProvider
                                                          .getDatasVerificarOneConductor()[
                                                      "aprobacion"] ==
                                                  "revision") {
                                                await prefs.setString(
                                                    'idsession', idGenerate);
                                                await prefs.setString(
                                                    "tipo", "conductor");

                                                final String? idsession = prefs
                                                    .getString('idsession');

                                                setState(() {
                                                  verificaDuenioProvider
                                                          .setRutAuditor =
                                                      controllerRut.text;
                                                });
                                                verificarConductorProvider
                                                        .setAprobadoProvider =
                                                    verificarConductorProvider
                                                            .getDatasVerificarOneConductor()[
                                                        "aprobacion"];
                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = true;
                                                });

                                                var modificado =
                                                    await updateEstadoConductor(
                                                        'conectado',
                                                        controllerRut.text);

                                               /* List<List> miListasImage =
                                                    await getBynaryAllImageConductor(
                                                        controllerRut.text);

                                                List image1byte =
                                                    miListasImage[0];
                                                List image2byte =
                                                    miListasImage[1];
                                                List image3byte =
                                                    miListasImage[2];
                                                List image4byte =
                                                    miListasImage[3];
                                                List image5byte =
                                                    miListasImage[4];
                                                List image6byte =
                                                    miListasImage[5];

                                                loginProvider
                                                        .milistaFotoPerfilConductor =
                                                    image1byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoCarnetAconductor =
                                                    image2byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoCarnetBconductor =
                                                    image3byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoLicenciaAconductor =
                                                    image4byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoLicenciaBconductor =
                                                    image5byte
                                                        .map((e) => e as int)
                                                        .toList();

                                                loginProvider
                                                        .milistaFotoAntecedentesConductor =
                                                    image6byte
                                                        .map((e) => e as int)
                                                        .toList();*/

                                                controllerRut.text = '';
                                                controllerPassword.text = '';
                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = false;
                                                });
                                                setState(() {
                                                  loginProvider.loginConductor =
                                                      false;
                                                });
                                                setState(() {
                                                  pantellaNext = true;
                                                });
                                                Navigator.pushAndRemoveUntil(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          HomeConductor()),
                                                  (route) =>
                                                      false, // Esto elimina todas las pantallas anteriores
                                                );
                                              } else if (verificarConductorProvider
                                                          .getDatasVerificarOneConductor()[
                                                      "aprobacion"] ==
                                                  "desaprobado") {
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.error(
                                                    message:
                                                        "Su cuenta no se encuentra activa \n contactar al administrador",
                                                  ),
                                                );
                                                setState(() {
                                                  loginProvider.loginConductor =
                                                      false;
                                                });
                                              } else {
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.error(
                                                    message:
                                                        "Ha ocurrido un error. Por favor verifica las credenciales e intenta de nuevo",
                                                  ),
                                                );
                                                setState(() {
                                                  loginProvider.loginConductor =
                                                      false;
                                                });
                                              }
                                            }
                                          }
                                        } else {
                                          showTopSnackBar(
                                            Overlay.of(context),
                                            const CustomSnackBar.error(
                                              message: "Rut no valido",
                                            ),
                                          );
                                          setState(() {
                                            loginProvider.loginConductor =
                                                false;
                                          });
                                        }
                                      } else {
                                        showTopSnackBar(
                                          Overlay.of(context),
                                          const CustomSnackBar.error(
                                            message:
                                                "Ha ocurrido un error, verifica los datos o crea una cuenta si no lo has echo",
                                          ),
                                        );
                                      }
                                    }
                                  }
                                  setState(() {
                                    loginProvider.loginConductor = false;
                                  });

                                  setState(() {});
                                },
                                child: Container(
                                  width: Device.deviceType == DeviceType.android
                                      ? 70.w
                                      : 70.w,
                                  height: 5.h,
                                  decoration: const BoxDecoration(
                                      color: Colors.deepPurple,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: Center(
                                      child: Text(
                                    "Login",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18.sp),
                                  )),
                                ),
                              ),
                              separador(),
                              resgisterButton(context),
                            ],
                          ),
                        ],
                      ),
              ),
            ),
    );
  }
}
