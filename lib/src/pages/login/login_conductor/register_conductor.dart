// ignore_for_file: prefer_const_constructors
import 'dart:io';
import 'dart:async';
import 'package:admin_taxi_seguro/src/pages/calendary_conductores.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:searchfield/searchfield.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../../apis/api_duenio_vehiculo/duenio_vehicle_register.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/email_validator.dart';
import '../../../tools/loading.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';
import '../login_duenio_vehiculo/register_duenio.dart';

class RegisterConductor extends StatefulWidget {
  const RegisterConductor({super.key});

  @override
  State<RegisterConductor> createState() => _RegisterConductorState();
}

class _RegisterConductorState extends State<RegisterConductor> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerDni = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerConfirmPassword = TextEditingController();
  TextEditingController controllerFechaVencimiento = TextEditingController();
  TextEditingController controllerRestriccion = TextEditingController();
  TextEditingController controllerTipoLIcencia = TextEditingController();
  TextEditingController controllerPartner = TextEditingController();

  FocusNode focusSearch = FocusNode();

  List<dynamic> losPartner = [];

  bool camerais1 = false;
  bool camerais2 = false;
  bool camerais3 = false;
  bool camerais4 = false;
  bool camerais5 = false;
  bool camerais6 = false;

  Dio dio = Dio();

  getAllPartners() async {
    Response resp = await dio.get("$url/getAllPartner");
    var respuesta = resp.data;

    List listaMapas = respuesta["Partners"];

    //List listaNombres = [];

    listaMapas.forEach((element) {
      losPartner.add(element["nombre"]);
    });

    setState(() {});

    setState(() {});
  }

  bool validateRutChile = false;
  bool validate2 = false;
  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  uuploadImageFotoPerfil(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorPerfil/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('EL RESPONSE ES: $response');
        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImageFotoCarnetA(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorCarnetLadoA/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageFotoCarnetB(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorCarnetLadoB/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageLicenciaConducirA(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorConducirA/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageLicenciaConducirB(imagepath, rut) async {
    Dio dio = Dio();
    final serverUrl = '$url/uploadImageConductorConducirB/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  uuploadImageAntecedentes(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageConductorAntecedentes/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'image': await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {
      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {
        print('Imagen subida con éxito 2 $response');
        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  File? image_to_upload1,
      image_to_upload2,
      image_to_upload3,
      image_to_upload4,
      image_to_upload5,
      image_to_upload6;
  var imageUrl;

  File? image1;
  bool istrue1 = false;
// This is the image picker
  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais1) {
      final XFile? pickedImage = await _picker.pickImage(

          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    } else {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image1 = File(pickedImage.path);
          istrue1 = true;
        });
      }
    }

    setState(() {});
  }

  File? image2;
  bool istrue2 = false;
// This is the image picker
  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais2) {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  File? image3;
  bool istrue3 = false;
// This is the image picker
  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais3) {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  File? image4;
  bool istrue4 = false;
// This is the image picker
  final _picker4 = ImagePicker();
  Future<void> openImagePicker4() async {
    if (camerais4) {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    } else {
      final XFile? pickedImage4 = await _picker4.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage4 != null) {
        setState(() {
          image4 = File(pickedImage4.path);
          istrue4 = true;
        });
      }
    }
  }

  File? image5;
  bool istrue5 = false;
// This is the image picker
  final _picker5 = ImagePicker();
  Future<void> openImagePicker5() async {
    if (camerais5) {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    } else {
      final XFile? pickedImage5 = await _picker5.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage5 != null) {
        setState(() {
          image5 = File(pickedImage5.path);
          istrue5 = true;
        });
      }
    }
  }

  File? image6;
  bool istrue6 = false;
// This is the image picker
  final _picker6 = ImagePicker();
  Future<void> openImagePicker6() async {
    if (camerais6) {
      final XFile? pickedImage6 = await _picker6.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage6 != null) {
        setState(() {
          image6 = File(pickedImage6.path);
          istrue6 = true;
        });
      }
    } else {
      final XFile? pickedImage6 = await _picker6.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage6 != null) {
        setState(() {
          image6 = File(pickedImage6.path);
          istrue6 = true;
        });
      }
    }
  }

  FocusNode focusApellido = FocusNode();
  FocusNode focusRut = FocusNode();
  FocusNode focusCorreo = FocusNode();
  FocusNode focusTelefono = FocusNode();
  FocusNode focusPassword = FocusNode();
  FocusNode focusConfirmPassword = FocusNode();
  FocusNode focusFvencimientoLicencia = FocusNode();
  FocusNode focusRestriccion = FocusNode();
  FocusNode focusTipoLicencia = FocusNode();

  @override
  void initState() {
    super.initState();
    getAllPartners();
  }

  @override
  Widget build(BuildContext context) {
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var verificarConductorProvider = Provider.of<AuditorProvider>(context);

    List<dynamic> listafinalquery2 = [];

    losPartner.forEach((element) {
      if (element.toString() == 'taxiseguro') {
        listafinalquery2.add('ningun partner');
      } else {
        listafinalquery2.add(element.toString());
      }
    });
    return WillPopScope(
      onWillPop: () {
        return alertShow(context);
      },
      child: Scaffold(
        body: SizedBox(
          width: 100.w,
          height: 100.h,
          child: Column(
            children: [
              titlePage(),
              verificarConductorProvider.getLoading == true
                  ? circulatorCustom()
                  : Expanded(
                      child: ListView(
                        children: [
                          Column(
                            children: [
                              inputName(controllerName, focusApellido, context),
                              separador(),
                              inputLastName(controllerLastName, focusApellido,
                                  focusRut, context),
                              separador(),
                              Focus(
                                onFocusChange: (value) {
                                  if (value == false) {
                                    String dad =
                                        rutValidate(controllerDni.text);

                                    controllerDni.text = dad.toString();
                                  }
                                },
                                child: inputDni(
                                    controllerDni,
                                    verificaDuenioProvider,
                                    focusRut,
                                    focusCorreo,
                                    context,
                                    validate2,
                                    rutValidate),
                              ),
                              separador(),
                              inputEmail(controllerEmail, focusCorreo,
                                  focusTelefono, context),
                              separador(),
                              inputPhone(controllerPhone, focusTelefono,
                                  focusPassword, context),
                              separador(),
                              SizedBox(
                                width: 70.w,
                                child: TextFormField(
                                  maxLength: 10,
                                  controller: controllerPassword,
                                  focusNode: focusPassword,
                                  obscureText:
                                      verificaDuenioProvider.getIsEyes == false
                                          ? true
                                          : false,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(focusConfirmPassword);
                                  },
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          if (verificaDuenioProvider
                                              .getIsEyes) {
                                            verificaDuenioProvider.setIsEyes =
                                                false;
                                          } else {
                                            verificaDuenioProvider.setIsEyes =
                                                true;
                                          }
                                          setState(() {});
                                        },
                                        icon: Icon(
                                            verificaDuenioProvider.getIsEyes ==
                                                    true
                                                ? Icons.remove_red_eye_rounded
                                                : Icons.emergency),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                      ),
                                      hintText: 'Ingrese su contraseña',
                                      labelText: 'Ingrese su contraseña'),
                                ),
                              ),
                              separador(),
                              SizedBox(
                                width: 70.w,
                                child: TextFormField(
                                  maxLength: 10,
                                  controller: controllerConfirmPassword,
                                  focusNode: focusConfirmPassword,
                                  obscureText:
                                      verificaDuenioProvider.getIsEyes2 == false
                                          ? true
                                          : false,
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          if (verificaDuenioProvider
                                              .getIsEyes2) {
                                            verificaDuenioProvider.setIsEyes2 =
                                                false;
                                          } else {
                                            verificaDuenioProvider.setIsEyes2 =
                                                true;
                                          }
                                          setState(() {});
                                        },
                                        icon: Icon(
                                            verificaDuenioProvider.getIsEyes2 ==
                                                    true
                                                ? Icons.remove_red_eye_rounded
                                                : Icons.emergency),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                      ),
                                      hintText: 'Confirme su contraseña',
                                      labelText: 'Confirme su contraseña'),
                                ),
                              ),
                              separador(),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: SearchField<dynamic>(
                                    focusNode: focusSearch,
                                    searchStyle:
                                        TextStyle(fontWeight: FontWeight.bold),
                                    controller: controllerPartner,
                                    onSearchTextChanged: (query) {
                                      if (query == 'Ningun partner') {}

                                      List<dynamic> listafinalquery = [];

                                      losPartner.forEach((element) {
                                        if (element.toString() ==
                                            'taxiseguro') {
                                          listafinalquery.add('Ningun partner');
                                        } else {
                                          listafinalquery
                                              .add(element.toString());
                                        }
                                      });

                                      final filter = listafinalquery
                                          .where((element) => element
                                              .toLowerCase()
                                              .contains(query.toLowerCase()))
                                          .toList();
                                      return filter
                                          .map((e) => SearchFieldListItem<
                                                  String>(e,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 4.0),
                                                child: Text(e,
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color:
                                                            Colors.deepPurple)),
                                              )))
                                          .toList();
                                    },
                                    textInputAction: TextInputAction.done,
                                    suggestionsDecoration: SuggestionDecoration(
                                      padding: const EdgeInsets.all(4),
                                      border: Border.all(
                                          color: Colors.amber.shade700),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(10),
                                      ),
                                    ),
                                    itemHeight: 50,
                                    suggestionState: Suggestion.expand,
                                    hint: 'Seleccione partner',
                                    //searchStyle: TextStyle(color: Colors.deepPurple),

                                    suggestions: listafinalquery2
                                        .map(
                                          (e) => SearchFieldListItem<dynamic>(
                                            e,
                                            child: Center(child: Text(e)),
                                          ),
                                        )
                                        .toList()),
                              ),
                              separador(),
                              fechaVencimientoLicenciaConducir(
                                  context,
                                  controllerFechaVencimiento,
                                  focusFvencimientoLicencia,
                                  focusRestriccion,
                                  '',
                                  '',
                                  focusSearch),
                              separador(),
                              inputRestriccion(controllerRestriccion,
                                  focusRestriccion, focusTipoLicencia, context),
                              separador(),
                              inputTipoLicencia(
                                  controllerTipoLIcencia, focusTipoLicencia),
                              separador(),
                              Column(
                                children: [
                                  Text(
                                    'Foto perfil',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      focusSearch.unfocus();
                                      await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text("Elija una opción"),
                                          content: Text(
                                              "¿Como desea subir la imagen?"),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais1 = false;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("Galeria"),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais1 = true;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("camara"),
                                            ),
                                          ],
                                        ),
                                      );

                                      await openImagePicker();

                                      if (image_to_upload1 == null) {
                                        setState(() {
                                          image_to_upload1 = image1;
                                        });
                                      } else {
                                        setState(() {
                                          image_to_upload1 = image1;
                                        });
                                      }
                                      setState(() {});
                                    },
                                    child: SizedBox(
                                      width: 35.w,
                                      child: istrue1 == false
                                          ? Image.asset(
                                              "assets/auditor/agregar.png",
                                              width: 35.w,
                                            )
                                          : Image.file(
                                              image1!,
                                              width: 35.w,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              separador(),
                              Text(
                                "Foto carnet por ambos lados",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              Column(
                                children: [
                                  Text('lado 1'),
                                  InkWell(
                                    onTap: () async {
                                      focusSearch.unfocus();
                                      await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text("Elija una opción"),
                                          content: Text(
                                              "¿Como desea subir la imagen?"),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais2 = false;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("Galeria"),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais2 = true;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("camara"),
                                            ),
                                          ],
                                        ),
                                      );

                                      await openImagePicker2();

                                      if (image_to_upload2 == null) {
                                        setState(() {
                                          image_to_upload2 = image2;
                                        });
                                      } else {
                                        setState(() {
                                          image_to_upload2 = image2;
                                        });
                                      }
                                    },
                                    child: SizedBox(
                                      width: 35.w,
                                      child: istrue2 == false
                                          ? Image.asset(
                                              "assets/auditor/agregar.png",
                                              width: 35.w,
                                            )
                                          : Image.file(
                                              image2!,
                                              width: 35.w,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Text('lado 2'),
                                  InkWell(
                                    onTap: () async {
                                      await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text("Elija una opción"),
                                          content: Text(
                                              "¿Como desea subir la imagen?"),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais3 = false;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("Galeria"),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais3 = true;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("camara"),
                                            ),
                                          ],
                                        ),
                                      );

                                      await openImagePicker3();

                                      if (image_to_upload3 == null) {
                                        setState(() {
                                          image_to_upload3 = image3;
                                        });
                                      } else {
                                        setState(() {
                                          image_to_upload3 = image3;
                                        });
                                      }
                                    },
                                    child: SizedBox(
                                      width: 35.w,
                                      child: istrue3 == false
                                          ? Image.asset(
                                              "assets/auditor/agregar.png",
                                              width: 35.w,
                                            )
                                          : Image.file(
                                              image3!,
                                              width: 35.w,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              separador(),
                              Center(
                                child: Text(
                                  "Foto licencia de conducir\n por ambos lados",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Column(
                                children: [
                                  Text('lado 1'),
                                  InkWell(
                                    onTap: () async {
                                      await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text("Elija una opción"),
                                          content: Text(
                                              "¿Como desea subir la imagen?"),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais4 = false;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("Galeria"),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais4 = true;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("camara"),
                                            ),
                                          ],
                                        ),
                                      );

                                      await openImagePicker4();

                                      if (image_to_upload4 == null) {
                                        setState(() {
                                          image_to_upload4 = image4;
                                        });
                                      } else {
                                        setState(() {
                                          image_to_upload4 = image4;
                                        });
                                      }
                                    },
                                    child: SizedBox(
                                      width: 35.w,
                                      child: istrue4 == false
                                          ? Image.asset(
                                              "assets/auditor/agregar.png",
                                              width: 35.w,
                                            )
                                          : Image.file(
                                              image4!,
                                              width: 35.w,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Text('lado 2'),
                                  InkWell(
                                    onTap: () async {
                                      await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text("Elija una opción"),
                                          content: Text(
                                              "¿Como desea subir la imagen?"),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais5 = false;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("Galeria"),
                                            ),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  camerais5 = true;
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Text("camara"),
                                            ),
                                          ],
                                        ),
                                      );

                                      await openImagePicker5();

                                      if (image_to_upload5 == null) {
                                        setState(() {
                                          image_to_upload5 = image5;
                                        });
                                      } else {
                                        setState(() {
                                          image_to_upload5 = image5;
                                        });
                                      }
                                    },
                                    child: SizedBox(
                                      width: 35.w,
                                      child: istrue5 == false
                                          ? Image.asset(
                                              "assets/auditor/agregar.png",
                                              width: 35.w,
                                            )
                                          : Image.file(
                                              image5!,
                                              width: 35.w,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                              separador(),
                              Text(
                                "Certificado antecedentes",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              InkWell(
                                onTap: () async {
                                  await showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text("Elija una opción"),
                                      content:
                                          Text("¿Como desea subir la imagen?"),
                                      actions: [
                                        ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              camerais6 = false;
                                            });
                                            Navigator.pop(context);
                                          },
                                          child: Text("Galeria"),
                                        ),
                                        ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              camerais6 = true;
                                            });
                                            Navigator.pop(context);
                                          },
                                          child: Text("camara"),
                                        ),
                                      ],
                                    ),
                                  );

                                  await openImagePicker6();

                                  if (image_to_upload6 == null) {
                                    setState(() {
                                      image_to_upload6 = image6;
                                    });
                                  } else {
                                    setState(() {
                                      image_to_upload6 = image6;
                                    });
                                  }
                                },
                                child: SizedBox(
                                  width: 35.w,
                                  child: istrue6 == false
                                      ? Image.asset(
                                          "assets/auditor/agregar.png",
                                          width: 35.w,
                                        )
                                      : Image.file(
                                          image6!,
                                          width: 35.w,
                                        ),
                                ),
                              ),
                              separador(),
                              InkWell(
                                onTap: () async {
                                  DateTime fechaInicio = DateTime.now();

                                  String fechainicio = fechaInicio.toString();

                                 if(controllerPartner.text == ''){
                                   showTopSnackBar(
                                     Overlay.of(context),
                                     CustomSnackBar.error(
                                       message: "No se ha asignado ningun partner",
                                     ),
                                   );
                                 }else{
                                   if (controllerDni.text.contains(".") &&
                                       controllerDni.text.contains("-")) {
                                     String dad =
                                     rutValidate(controllerDni.text);

                                     controllerDni.text = dad.toString();

                                     validate2 = false;

                                     bool isveri = isRutValid(dad.toString());

                                     final emailValid =
                                     validatesEmail(controllerEmail.text);


                                     if (emailValid) {
                                       if (controllerConfirmPassword.text ==
                                           '' ||
                                           controllerName.text == '' ||
                                           controllerLastName.text == '' ||
                                           controllerDni.text == '' ||
                                           controllerEmail.text == '' ||
                                           controllerPassword.text == '' ||
                                           controllerPhone.text == '' ||
                                           controllerTipoLIcencia.text == '' ||
                                           controllerRestriccion.text == '' ||
                                           controllerFechaVencimiento.text ==
                                               '') {
                                         showTopSnackBar(
                                           Overlay.of(context),
                                           CustomSnackBar.error(
                                             message:
                                             "Debe llenar todos los campos",
                                           ),
                                         );
                                       } else {
                                         var conductor_registrado =
                                         await getOneDatasConductor(
                                             controllerDni.text);

                                         if (conductor_registrado != null) {
                                           showTopSnackBar(
                                             Overlay.of(context),
                                             CustomSnackBar.error(
                                               message:
                                               "El conductor con el rut: ${controllerDni.text} ya se encuentra registrado",
                                             ),
                                           );
                                         } else {
                                           setState(() {
                                             verificarConductorProvider
                                                 .setLoading = true;
                                           });

                                           if (controllerConfirmPassword.text !=
                                               controllerPassword.text) {
                                             showTopSnackBar(
                                               Overlay.of(context),
                                               CustomSnackBar.error(
                                                 message:
                                                 "Las contraseñas no son iguales",
                                               ),
                                             );
                                           } else {
                                             var yaregistrado =
                                             await getOneDatasConductor(
                                                 controllerDni.text);

                                             if (yaregistrado != null) {
                                               showTopSnackBar(
                                                 Overlay.of(context),
                                                 CustomSnackBar.error(
                                                   message:
                                                   "ya existe un conductor con el rut ${controllerDni.text}",
                                                 ),
                                               );
                                             } else {
                                               if (image_to_upload1 != null &&
                                                   image_to_upload2 != null &&
                                                   image_to_upload3 != null &&
                                                   image_to_upload4 != null &&
                                                   image_to_upload5 != null &&
                                                   image_to_upload6 != null) {
                                                 showTopSnackBar(
                                                   animationDuration:
                                                   Duration(seconds: 1),
                                                   displayDuration:
                                                   Duration(seconds: 7),
                                                   Overlay.of(context),
                                                   CustomSnackBar.info(
                                                     message:
                                                     "Registrando datos....",
                                                   ),
                                                 );

                                                 var resAntecedentes =
                                                 await uuploadImageAntecedentes(
                                                     image_to_upload6,
                                                     controllerDni.text);
                                                 var resPerfil =
                                                 await uuploadImageFotoPerfil(
                                                     image_to_upload1,
                                                     controllerDni.text);
                                                 var resCarnetA =
                                                 await uuploadImageFotoCarnetA(
                                                     image_to_upload2,
                                                     controllerDni.text);
                                                 var resCarnetB =
                                                 await uuploadImageFotoCarnetB(
                                                     image_to_upload3,
                                                     controllerDni.text);
                                                 var resLicenciaA =
                                                 await uuploadImageLicenciaConducirA(
                                                     image_to_upload4,
                                                     controllerDni.text);
                                                 var resLicenciaB =
                                                 await uuploadImageLicenciaConducirB(
                                                     image_to_upload5,
                                                     controllerDni.text);

                                                 var lastIndexCarnetA =
                                                     resCarnetA
                                                         .toString()
                                                         .length;

                                                 var lastIndexCarnetB =
                                                     resCarnetB
                                                         .toString()
                                                         .length;

                                                 var lastIndexPerfil =
                                                     resPerfil.toString().length;

                                                 var lastIndexAntecedentes =
                                                     resAntecedentes
                                                         .toString()
                                                         .length;

                                                 var lastIndexLicenciaA =
                                                     resLicenciaA
                                                         .toString()
                                                         .length;

                                                 var lastIndexLicenciaB =
                                                     resLicenciaB
                                                         .toString()
                                                         .length;

                                                 var data = {
                                                   "id": null,
                                                   "name":
                                                   "${controllerName.text}",
                                                   "lastName":
                                                   "${controllerLastName.text}",
                                                   "dni":
                                                   "${controllerDni.text}",
                                                   "fotoCarnetA":
                                                   "$url3/${resCarnetA.toString().substring(29, lastIndexCarnetA)}", //.substring(57, lastIndexCarnetA)}
                                                   "fotoCarnetB":
                                                   "$url3/${resCarnetB.toString().substring(29, lastIndexCarnetB)}", //.substring(57, lastIndexCarnetB)
                                                   "email":
                                                   "${controllerEmail.text}",
                                                   "password":
                                                   "${controllerPassword.text}",
                                                   "phone":
                                                   "+569${controllerPhone.text}",
                                                   "fotoPerfil":
                                                   "$url3/${resPerfil.toString().substring(29, lastIndexPerfil)}", //.substring(57, lastIndexPerfil)
                                                   "tipoLicencia":
                                                   "${controllerTipoLIcencia.text}",
                                                   "restriccion":
                                                   "${controllerRestriccion.text}",
                                                   "fechaVencimientoLicencia":
                                                   "${controllerFechaVencimiento.text}",
                                                   "fotoLicenciaConducirA":
                                                   "$url3/${resLicenciaA.toString().substring(29, lastIndexLicenciaA)}", //.substring(57, lastIndexLicenciaA)
                                                   "fotoLicenciaConducirB":
                                                   "$url3/${resLicenciaB.toString().substring(29,lastIndexLicenciaB)}", //.substring(57, lastIndexLicenciaB)
                                                   "aprobacion": "rechazado",
                                                   "status": "revision",
                                                   "motivo":
                                                   "Cuenta recien creada",
                                                   "partner": controllerPartner
                                                       .text ==
                                                       'Ningun partner' ||
                                                       controllerPartner
                                                           .text ==
                                                           'ningun partner'
                                                       ? 'taxiseguro'
                                                       : controllerPartner.text,
                                                   "certificadoAntecedentes":
                                                   "$url3/${resAntecedentes.toString().substring(29, lastIndexAntecedentes)}", //.substring(57, lastIndexAntecedentes)
                                                   "fechainicio": fechainicio,
                                                 };

                                                 var stat =
                                                 await saveConductor(data);


                                                 showTopSnackBar(
                                                   Overlay.of(context),
                                                   CustomSnackBar.success(
                                                     message:
                                                     "Datos guardados correctamente",
                                                   ),
                                                 );
                                                 Future.delayed(
                                                   Duration(seconds: 1),
                                                       () {
                                                     Navigator.pop(context);
                                                   },
                                                 );
                                               } else {}
                                             }
                                           }
                                           setState(() {
                                             verificarConductorProvider
                                                 .setLoading = false;
                                           });
                                         }
                                       }
                                     } else {
                                       showTopSnackBar(
                                         Overlay.of(context),
                                         CustomSnackBar.error(
                                           message: "Email no valido",
                                         ),
                                       );
                                     }
                                   } else {
                                     var formattedRut =
                                     formatRut(controllerDni.text);
                                     verificaDuenioProvider.setIsRutTrue =
                                         isRutValid(formattedRut);

                                     String dad = rutValidate(formattedRut);

                                     controllerDni.text = dad.toString();

                                     validate2 = false;

                                     bool isveri = isRutValid(dad.toString());
                                     if (isveri) {
                                       final emailValid =
                                       validatesEmail(controllerEmail.text);


                                       if (emailValid) {
                                         if (controllerConfirmPassword.text ==
                                             '' ||
                                             controllerName.text == '' ||
                                             controllerLastName.text == '' ||
                                             controllerDni.text == '' ||
                                             controllerEmail.text == '' ||
                                             controllerPassword.text == '' ||
                                             controllerPhone.text == '' ||
                                             controllerTipoLIcencia.text == '' ||
                                             controllerRestriccion.text == '' ||
                                             controllerFechaVencimiento.text ==
                                                 '') {
                                           showTopSnackBar(
                                             Overlay.of(context),
                                             CustomSnackBar.error(
                                               message:
                                               "Debe llenar todos los campos",
                                             ),
                                           );
                                         } else {
                                           var conductor_registrado =
                                           await getOneDatasConductor(
                                               controllerDni.text);

                                           if (conductor_registrado != null) {
                                             showTopSnackBar(
                                               Overlay.of(context),
                                               CustomSnackBar.error(
                                                 message:
                                                 "El conductor con el rut: ${controllerDni.text} ya se encuentra registrado",
                                               ),
                                             );
                                           } else {
                                             setState(() {
                                               verificarConductorProvider
                                                   .setLoading = true;
                                             });

                                             if (controllerConfirmPassword
                                                 .text !=
                                                 controllerPassword.text) {
                                               showTopSnackBar(
                                                 Overlay.of(context),
                                                 CustomSnackBar.error(
                                                   message:
                                                   "Las contraseñas no son iguales",
                                                 ),
                                               );
                                             } else {
                                               var yaregistrado =
                                               await getOneDatasConductor(
                                                   controllerDni.text);

                                               if (yaregistrado != null) {
                                                 showTopSnackBar(
                                                   Overlay.of(context),
                                                   CustomSnackBar.error(
                                                     message:
                                                     "ya existe un conductor con el rut ${controllerDni.text}",
                                                   ),
                                                 );
                                               } else {
                                                 if (image_to_upload1 != null &&
                                                     image_to_upload2 != null &&
                                                     image_to_upload3 != null &&
                                                     image_to_upload4 != null &&
                                                     image_to_upload5 != null &&
                                                     image_to_upload6 != null) {
                                                   showTopSnackBar(
                                                     animationDuration:
                                                     Duration(seconds: 1),
                                                     displayDuration:
                                                     Duration(seconds: 7),
                                                     Overlay.of(context),
                                                     CustomSnackBar.info(
                                                       message:
                                                       "Registrando datos....",
                                                     ),
                                                   );

                                                   var resAntecedentes =
                                                   await uuploadImageAntecedentes(
                                                       image_to_upload6,
                                                       controllerDni.text);
                                                   var resPerfil =
                                                   await uuploadImageFotoPerfil(
                                                       image_to_upload1,
                                                       controllerDni.text);
                                                   var resCarnetA =
                                                   await uuploadImageFotoCarnetA(
                                                       image_to_upload2,
                                                       controllerDni.text);
                                                   var resCarnetB =
                                                   await uuploadImageFotoCarnetB(
                                                       image_to_upload3,
                                                       controllerDni.text);
                                                   var resLicenciaA =
                                                   await uuploadImageLicenciaConducirA(
                                                       image_to_upload4,
                                                       controllerDni.text);
                                                   var resLicenciaB =
                                                   await uuploadImageLicenciaConducirB(
                                                       image_to_upload5,
                                                       controllerDni.text);

                                                   var lastIndexCarnetA =
                                                       resCarnetA
                                                           .toString()
                                                           .length;

                                                   var lastIndexCarnetB =
                                                       resCarnetB
                                                           .toString()
                                                           .length;

                                                   var lastIndexPerfil =
                                                       resPerfil
                                                           .toString()
                                                           .length;

                                                   var lastIndexAntecedentes =
                                                       resAntecedentes
                                                           .toString()
                                                           .length;

                                                   var lastIndexLicenciaA =
                                                       resLicenciaA
                                                           .toString()
                                                           .length;

                                                   var lastIndexLicenciaB =
                                                       resLicenciaB
                                                           .toString()
                                                           .length;

                                                   print("##################################################### ${resCarnetA.toString().substring(28, lastIndexCarnetA)} ###########################################");

                                                   var data = {
                                                     "id": null,
                                                     "name":
                                                     "${controllerName.text}",
                                                     "lastName":
                                                     "${controllerLastName.text}",
                                                     "dni":
                                                     "${controllerDni.text}",
                                                     "fotoCarnetA":
                                                     "$url3/${resCarnetA.toString().substring(29, lastIndexCarnetA)}", //.substring(57, lastIndexCarnetA)
                                                     "fotoCarnetB":
                                                     "$url3/${resCarnetB.toString().substring(29, lastIndexCarnetB)}", //.substring(57, lastIndexCarnetB)
                                                     "email":
                                                     "${controllerEmail.text}",
                                                     "password":
                                                     "${controllerPassword.text}",
                                                     "phone":
                                                     "+569${controllerPhone.text}",
                                                     "fotoPerfil":
                                                     "$url3/${resPerfil.toString().substring(29, lastIndexPerfil)}", //.substring(57, lastIndexPerfil)
                                                     "tipoLicencia":
                                                     "${controllerTipoLIcencia.text}",
                                                     "restriccion":
                                                     "${controllerRestriccion.text}",
                                                     "fechaVencimientoLicencia":
                                                     "${controllerFechaVencimiento.text}",
                                                     "fotoLicenciaConducirA":
                                                     "$url3/${resLicenciaA.toString().substring(29, lastIndexLicenciaA)}", //.substring(57, lastIndexLicenciaA)
                                                     "fotoLicenciaConducirB":
                                                     "$url3/${resLicenciaB.toString().substring(29, lastIndexLicenciaB)}", //.substring(57, lastIndexLicenciaB)
                                                     "aprobacion": "rechazado",
                                                     "status": "revision",
                                                     "motivo":
                                                     "Cuenta recien creada",
                                                     "partner": controllerPartner
                                                         .text ==
                                                         'Ningun partner' ||
                                                         controllerPartner
                                                             .text ==
                                                             'ningun partner'
                                                         ? 'taxiseguro'
                                                         : controllerPartner
                                                         .text,
                                                     "certificadoAntecedentes":
                                                     "$url3/${resAntecedentes.toString().substring(29, lastIndexAntecedentes)}", //.substring(57, lastIndexAntecedentes)
                                                     "fechainicio": fechainicio,
                                                   };

                                                   var stat =
                                                   await saveConductor(data);


                                                   showTopSnackBar(
                                                     Overlay.of(context),
                                                     CustomSnackBar.success(
                                                       message:
                                                       "Datos guardados correctamente",
                                                     ),
                                                   );
                                                   Future.delayed(
                                                     Duration(seconds: 1),
                                                         () {
                                                       Navigator.pop(context);
                                                     },
                                                   );
                                                 } else {}
                                               }
                                             }
                                             setState(() {
                                               verificarConductorProvider
                                                   .setLoading = false;
                                             });
                                           }
                                         }
                                       } else {
                                         showTopSnackBar(
                                           Overlay.of(context),
                                           CustomSnackBar.error(
                                             message: "Email no valido",
                                           ),
                                         );
                                       }
                                     } else {
                                       showTopSnackBar(
                                         Overlay.of(context),
                                         const CustomSnackBar.error(
                                           message: "Rut no valido",
                                         ),
                                       );
                                     }
                                   }
                                 }
                                },
                                child: Container(
                                  width: 30.w,
                                  decoration: BoxDecoration(
                                    color: Colors.deepPurple,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Center(
                                        child: Text(
                                      "Registrar",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.sp),
                                    )),
                                  ),
                                ),
                              ),
                              separador(),
                            ],
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget inputDni(TextEditingController controller, verificaDuenioProvider,
      focus, focus2, context, validate2, rutValidate) {
    return SizedBox(
      width: 70.w,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.always,
        maxLength: 12,
        focusNode: focus,
        keyboardType: TextInputType.datetime,
        onEditingComplete: () {
          if (validateRutChile == true) {
            String dad = rutValidate(controller.text);

            validate2 = true;
            //cont.text = formatRut(dad);
            FocusScope.of(context!).nextFocus();
          }
          FocusScope.of(context).requestFocus(focus2);
        },
        onChanged: (value) {
          var formattedRut = formatRut(controller.text);
          verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);
        },
        inputFormatters: [RutFormatter()],
        onTap: () {
          validate2 = false;
          controller.text = deFormatRut(controller.text);
        },
        validator: (value) {
          validate2 = false;
          if (value!.isEmpty) {
            return 'Debes Ingresar RUT Valido';
          } else if (value.length < 7) {
            return 'Debes Ingresar RUT Valido';
          } else if ((value.length >= 8)) {
            value = rutValidate(value);
            bool isveri = isRutValid(value!);

            if (isveri) {
              validate2 = true;
              return null;
            } else {
              return 'si termina en K reemplaza por un 0';
            }
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        },
        controller: controller,
        decoration: const InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: 'Ingrese su rut',
            labelText: 'Ingrese su rut'),
      ),
    );
  }
}

Widget fechaVencimientoLicenciaConducir(
    context, controller, focus, focus2, fecha, homeProvider, focusSearch) {
  bool iboolean = false;
  return Column(
    children: [
      const Text("Fecha vencimiento licencia"),
      InkWell(
        onTap: () {
          focusSearch.unfocus();
          getCalendaryConductores(
              context, controller, fecha, homeProvider, iboolean);
        },
        child: SizedBox(
          width: 70.w,
          child: TextFormField(
            focusNode: focus,
            controller: controller,
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(focus2);
            },
            enabled: false,
            decoration: const InputDecoration(
                suffixIcon: Icon(Icons.calendar_month),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                hintText: 'EJ: 2023-09-07'),
          ),
        ),
      ),
    ],
  );
}

Widget titlePage() {
  return Container(
    width: 100.w,
    height: 13.h,
    decoration: BoxDecoration(
      color: Colors.amber.shade700,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        bottomRight: Radius.circular(20),
      ),
    ),
    child: Center(
      child: Text(
        "REGISTRO",
        style: TextStyle(
            fontSize: 20.sp,
            fontWeight: FontWeight.bold,
            color: Colors.deepPurple),
      ),
    ),
  );
}

Widget inputName(TextEditingController controller, focus, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      autofocus: true,
      maxLength: 20,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus);
      },
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su nombre',
          labelText: 'Ingrese su nombre'),
    ),
  );
}

Widget inputLastName(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 20,
      controller: controller,
      focusNode: focus,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su apellido',
          labelText: 'Ingrese su apellido'),
    ),
  );
}

Widget inputEmail(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 255,
      controller: controller,
      focusNode: focus,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su correo',
          labelText: 'Ingrese su correo'),
    ),
  );
}

Widget inputPhone(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 8,
      controller: controller,
      focusNode: focus,
      keyboardType: TextInputType.phone,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          prefixText: '+569',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su telefono',
          labelText: 'Ingrese su telefono'),
    ),
  );
}

Widget inputRestriccion(
    TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      focusNode: focus,
      maxLength: 20,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese restricción',
          labelText: 'Ingrese restricción'),
    ),
  );
}

Widget inputTipoLicencia(TextEditingController controller, focus) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      controller: controller,
      focusNode: focus,
      maxLength: 10,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese Tipo licencia',
          labelText: 'Ingrese Tipo licencia'),
    ),
  );
}
