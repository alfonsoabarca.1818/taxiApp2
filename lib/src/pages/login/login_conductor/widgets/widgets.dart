import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';

import '../register_conductor.dart';

Widget inputTextDni(controller, verificaDuenioProvider, focus, context,
    validate2, rutValidate, validateRutChile, pantella) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      keyboardType: TextInputType.datetime,
      autofocus: pantella == true ? false : true,
      controller: controller,
      autovalidateMode: AutovalidateMode.always,
      validator: (value) {
        validate2 = false;
        if (value!.isEmpty) {
          return 'Debes Ingresar RUT Valido';
        } else if (value.length < 7) {
          return 'Debes Ingresar RUT Valido';
        } else if ((value.length >= 8)) {
          value = rutValidate(value);
          bool isveri = isRutValid(value!);

          if (isveri) {
            validate2 = true;
            return null;
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        } else {
          return 'si termina en K reemplaza por un 0';
        }
      },
      maxLength: 12,
      onEditingComplete: () {
        if (validateRutChile == true) {
          String dad = rutValidate(controller.text);

          validate2 = true;

          FocusScope.of(context!).nextFocus();
        }
        FocusScope.of(context).requestFocus(focus);
      },
      onChanged: (value) {
        var formattedRut = formatRut(controller.text);
        verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);

      },
      onTap: () {
        validate2 = false;
        controller.text = deFormatRut(controller.text);
      },
      inputFormatters: [RutFormatter()],
      decoration: const InputDecoration(
        filled: true,
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Rut:',
      ),
    ),
  );
}

Widget inputTextPassword(controller, focus) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      focusNode: focus,
      controller: controller,
      maxLength: 10,
      obscureText: true,
      decoration: const InputDecoration(
        filled: true,
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Contraseña:',
      ),
    ),
  );
}

Widget resgisterButton(BuildContext context) {
  return InkWell(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const RegisterConductor(),
        ),
      );
    },
    child: Container(child: Text("Registrar", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.sp),)),
  );
}

Widget buttonRecoveryPassword() {
  return Container();
}
