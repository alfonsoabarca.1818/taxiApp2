import 'dart:convert';
import 'package:admin_taxi_seguro/src/pages/login/login_duenio_vehiculo/register_duenio.dart';
import 'package:admin_taxi_seguro/src/pages/login/login_duenio_vehiculo/widgets/widgets.dart';
import 'package:admin_taxi_seguro/src/pages/validaterutall.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:uuid/uuid.dart';

import '../../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../../apis/api_duenio_vehiculo/login_duenio_vehiculo.dart';
import '../../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../../provider/provider_preference.dart';
import '../../../tools/loading.dart';
import '../../../tools/send_email.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';
import '../../home/home_duenio_vehiculo/home_duenio_vehiculo.dart';
import '../select_user.dart';

class LoginDuenioVehiculo extends StatefulWidget {
  const LoginDuenioVehiculo({super.key});

  @override
  State<LoginDuenioVehiculo> createState() => _LoginDuenioVehiculoState();
}

class _LoginDuenioVehiculoState extends State<LoginDuenioVehiculo> {
  TextEditingController controllerRut = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  FocusNode focusRut = FocusNode();
  FocusNode focusPass = FocusNode();

  bool validateRutChile = false;
  bool validate2 = false;
  bool pantellaNext = false;
  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  Dio dio = Dio();

  /*getBynaryA(String rut) async {
    print("object $rut");
    try {
      Response resp = await dio.post("$url/bynary1/$rut",
          options: Options(
            receiveTimeout: Duration(seconds: 10),
          ));

      if (resp.statusCode == 200) {
        var r = resp.data;

        List<dynamic> mapita = jsonDecode(r["image_byte"]);

        List<int> mapita2 = mapita.map((e) => e as int).toList();

        return mapita2;
      } else {
        showTopSnackBar(
          Overlay.of(context),
          CustomSnackBar.error(
            message:
                "No se ha podido iniciar session porfavor intenta nuevamente",
          ),
        );
      }
    } catch (e) {
      print("Ha ocurrido un error al obtener el bynario: $e");
      showTopSnackBar(
        Overlay.of(context),
        CustomSnackBar.error(
          message:
              "No se ha podido iniciar session porfavor intenta nuevamente",
        ),
      );
      // Navigator.pop(context);
    }
  }

  getBynaryB(String rut) async {
    try {
      Response resp = await dio.post("$url/bynary2/$rut",
          options: Options(
            receiveTimeout: Duration(seconds: 10),
          ));
      if (resp.statusCode == 200) {
        var r = resp.data;

        List<dynamic> mapita = jsonDecode(r["image_byte"]);

        List<int> mapita2 = mapita.map((e) => e as int).toList();

        return mapita2;
      } else {
        showTopSnackBar(
          Overlay.of(context),
          CustomSnackBar.error(
            message:
                "No se ha podido iniciar session porfavor intenta nuevamente",
          ),
        );
      }
    } catch (e) {
      print("Ha ocurrido un error al obtener el bynario: $e");
    }
  }

  getBynaryAllImage(String rut) async {
    try {
      Response resp = await dio.post("$url/getImagesDuenio/$rut",
          options: Options(
            receiveTimeout: Duration(seconds: 5),
          ));

      if (resp.statusCode == 200) {
        var image1 = resp.data;

        Map<dynamic, dynamic> mapita1 = image1 as Map<dynamic, dynamic>;

        List imagebynarydynamic1 = mapita1["image1"];
        List imagebynarydynamic2 = mapita1["image2"];

        List<int> imageByteInt1 = imagebynarydynamic1.cast<int>();
        List<int> imageByteInt2 = imagebynarydynamic2.cast<int>();

        List<List> mislistas = [imageByteInt1, imageByteInt2];

        return mislistas;
      } else {
        showTopSnackBar(
          Overlay.of(context),
          CustomSnackBar.error(
            message:
                "No se ha podido iniciar session porfavor intenta nuevamente",
          ),
        );
      }
    } catch (e) {
      print("Ha ocurrido un error al obtener el bynario: $e");
    }
  }*/

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    var status;

    var dniProvider = Provider.of<LoginProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var preferenceProvider = Provider.of<ProviderPreference>(context);

    setState(() {
      verificaDuenioProvider.setLoading = false;
    });

    return Scaffold(
      body: verificaDuenioProvider.getLoading == true
          ? WillPopScope(
              onWillPop: () async {
                final SharedPreferences prefs =
                    await SharedPreferences.getInstance();
                var conectado = await updateEstadoDuenio(
                    controllerRut.text, "desconectado");

                await prefs.remove('idsession');
                await prefs.remove('user_rut');
                await prefs.remove('user_password');
                await prefs.remove('tipo');
                await prefs.remove('id');

                dniProvider.setAllDatasPatentes = {};
                dniProvider.setPatentesObtenidasList = [];
                dniProvider.patenteguardada = "";
                //homeProvider.setLista = [];
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => SelectUser()),
                  (route) =>
                      false, // Esto elimina todas las pantallas anteriores
                );
                return true;
              },
              child: circulatorCustom())
          : WillPopScope(
              onWillPop: () async {
                final SharedPreferences prefs =
                    await SharedPreferences.getInstance();

                var conectado = await updateEstadoDuenio(
                    controllerRut.text, "desconectado");

                await prefs.remove('idsession');
                await prefs.remove('user_rut');
                await prefs.remove('user_password');
                await prefs.remove('tipo');
                await prefs.remove('id');

                dniProvider.setAllDatasPatentes = {};
                dniProvider.setPatentesObtenidasList = [];
                dniProvider.patenteguardada = "";
                //homeProvider.setLista = [];
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => SelectUser()),
                  (route) =>
                      false, // Esto elimina todas las pantallas anteriores
                );
                return true;
              },
              child: Container(
                child: dniProvider.loginDuenio == true
                    ? circulatorCustom()
                    : Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.center,
                              end: Alignment.centerRight,
                              colors: [
                                Color(0xFFEE973A),
                                Color(0xFFA14BCC),
                              ]),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: ListView(children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    separador(),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Row(
                                        children: [
                                          Container(
                                            decoration: const BoxDecoration(
                                              gradient: LinearGradient(
                                                  begin: Alignment.center,
                                                  end: Alignment.centerRight,
                                                  colors: [
                                                    Color(0xFFFFFFFF),
                                                    Color(0xFFFFFFFF),
                                                  ]),
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(10.0),
                                              ),
                                            ),
                                            child: const Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 16.0),
                                              child: Text(
                                                "Dueño Vehiculo",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    separador(),
                                    Text(
                                      "LOGIN",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 24.sp,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Times New Roman'),
                                    ),
                                    separador(),
                                    Focus(
                                      onFocusChange: (value) {
                                        if (value == false) {
                                          String dad =
                                              rutValidate(controllerRut.text);

                                          controllerRut.text = dad.toString();
                                        }
                                      },
                                      child: inputTextDni(
                                          controllerRut,
                                          verificaDuenioProvider,
                                          focusPass,
                                          context,
                                          validate2,
                                          rutValidate,
                                          validateRutChile,
                                          pantellaNext),
                                    ),
                                    separador(),
                                    inputTextPassword(
                                        controllerPassword, focusPass),
                                    //separador(),
                                    TextButton(
                                        onPressed: () async =>
                                            await send(context),
                                        child: const Text(
                                          "¡¡Problema con mi cuenta!!",
                                          style: TextStyle(color: Colors.black),
                                        )),
                                    separador(),
                                    InkWell(
                                      onTap: () async {
                                        final SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        final String? idsession =
                                            prefs.getString('idsession');

                                        await prefs.setString(
                                            'user_rut', controllerRut.text);

                                        await prefs.setString('user_password',
                                            controllerPassword.text);

                                        String idGenerate = '';
                                        if (idsession == null) {
                                          var uuid = Uuid();

                                          setState(() {
                                            idGenerate = uuid.v4().toString();
                                          });
                                        }

                                        bool isveri = verifiRut(
                                            controllerRut,
                                            rutValidate,
                                            validate2,
                                            isRutValid,
                                            formatRut,
                                            verificaDuenioProvider);

                                        if (isveri) {
                                          status =
                                              await postLoginDuenioVehiculo(
                                                  controllerRut.text,
                                                  controllerPassword.text);
                                          var datasPatentes =
                                              await patentesDuenio(
                                                  controllerRut.text,
                                                  controllerPassword.text);

                                          dniProvider.setAllDatasProvider =
                                              datasPatentes;

                                          var s = status;

                                          if (status == null) {
                                            showTopSnackBar(
                                              Overlay.of(context),
                                              const CustomSnackBar.error(
                                                message: "El usuario no existe",
                                              ),
                                            );
                                          } else {
                                            List patentes = [];

                                            if (datasPatentes["dni"] == null ||
                                                datasPatentes["password"] ==
                                                    null) {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                CustomSnackBar.error(
                                                  message:
                                                      "Rut O Contraseña invalidos\nVerifica los datos",
                                                ),
                                              );
                                            } else {
                                              String rut_obtenido =
                                                  datasPatentes["dni"];
                                              String password_obtenido =
                                                  datasPatentes["password"];

                                              if (rut_obtenido ==
                                                      controllerRut.text &&
                                                  password_obtenido ==
                                                      controllerPassword.text) {
                                                String aprobacion =
                                                    datasPatentes["aprobacion"];
                                                String motivo =
                                                    datasPatentes["motivo"];

                                                var estadoduenio =
                                                    await getEstadoDuenio(
                                                        controllerRut.text);

                                                if (estadoduenio["estado"] ==
                                                        null ||
                                                    estadoduenio["estado"] ==
                                                        "desconectado") {
                                                  await prefs.setString(
                                                      'idsession', idGenerate);
                                                  await prefs.setString(
                                                      "tipo", "duenio");

                                                  final String? idsession =
                                                      prefs.getString(
                                                          'idsession');

                                                  if (aprobacion ==
                                                          "aprobado" ||
                                                      aprobacion ==
                                                          "rechazado" ||
                                                      aprobacion ==
                                                          "revision") {
                                                    setState(() {
                                                      verificaDuenioProvider
                                                          .setLoading = true;
                                                    });

                                                    setState(() {
                                                      dniProvider.loginDuenio =
                                                          true;
                                                    });
                                                    if (await datasPatentes[
                                                            "patentes"] ==
                                                        "[{}]") {
                                                      List patentes_obtenidas =
                                                          jsonDecode(
                                                              datasPatentes[
                                                                  "patentes"]);

                                                      dniProvider
                                                              .setPatentesObtenidasList =
                                                          patentes_obtenidas;

                                                      List lasPatentesAll = [];

                                                      lasPatentesAll.add(
                                                          await getAllPatentesAll());

                                                      dniProvider
                                                              .setAllPatentesAhora =
                                                          lasPatentesAll[0];

                                                      dniProvider.dni_duenio =
                                                          controllerRut.text;

                                                      showTopSnackBar(
                                                        Overlay.of(context),
                                                        CustomSnackBar.success(
                                                          message:
                                                              "Datos ingresados son correctos",
                                                        ),
                                                      );
                                                      dniProvider.setRut =
                                                          controllerRut.text;
                                                      dniProvider
                                                              .setAprobacion =
                                                          aprobacion;
                                                      dniProvider.setMotivo =
                                                          motivo;
                                                      dniProvider.setPassword =
                                                          controllerPassword
                                                              .text;

                                                      var conectado =
                                                          await updateEstadoDuenio(
                                                              controllerRut
                                                                  .text,
                                                              "conectado");

                                                 /*     List<List> miListasImage =
                                                          await getBynaryAllImage(
                                                              controllerRut
                                                                  .text);

                                                      List image1byte =
                                                          miListasImage[0];
                                                      List image2byte =
                                                          miListasImage[1];

                                                      dniProvider.mibyte1 =
                                                          image1byte
                                                              .map((e) =>
                                                                  e as int)
                                                              .toList();

                                                      dniProvider.mibyte2 =
                                                          image2byte
                                                              .map((e) =>
                                                                  e as int)
                                                              .toList();*/

                                                      controllerRut.text = '';
                                                      controllerPassword.text =
                                                          '';
                                                      setState(() {
                                                        pantellaNext = true;
                                                      });

                                                      Navigator
                                                          .pushAndRemoveUntil(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                HomeDuenioVehiculo()),
                                                        (route) =>
                                                            false, // Esto elimina todas las pantallas anteriores
                                                      );

                                                      setState(() {
                                                        dniProvider
                                                                .loginDuenio =
                                                            false;
                                                      });
                                                      setState(() {
                                                        verificaDuenioProvider
                                                            .setLoading = false;
                                                      });
                                                    } else {
                                                      dniProvider
                                                              .setAllDatasPatentes =
                                                          jsonDecode(
                                                              await datasPatentes[
                                                                  "patentes"]);

                                                      List lasPatentesAll = [];

                                                      lasPatentesAll.add(
                                                          await getAllPatentesAll());

                                                      dniProvider
                                                              .setAllPatentesAhora =
                                                          lasPatentesAll[0];

                                                      dniProvider.dni_duenio =
                                                          controllerRut.text;

                                                      showTopSnackBar(
                                                        Overlay.of(context),
                                                        CustomSnackBar.success(
                                                          message:
                                                              "Datos ingresados son correctos",
                                                        ),
                                                      );

                                                      var conectado =
                                                          await updateEstadoDuenio(
                                                              controllerRut
                                                                  .text,
                                                              "conectado");

                                                   /*   List<List> miListasImage =
                                                          await getBynaryAllImage(
                                                              controllerRut
                                                                  .text);

                                                      List image1byte =
                                                          miListasImage[0];
                                                      List image2byte =
                                                          miListasImage[1];

                                                      dniProvider.mibyte1 =
                                                          image1byte
                                                              .map((e) =>
                                                                  e as int)
                                                              .toList();

                                                      dniProvider.mibyte2 =
                                                          image2byte
                                                              .map((e) =>
                                                                  e as int)
                                                              .toList();*/

                                                      dniProvider
                                                              .setAprobacion =
                                                          aprobacion;
                                                      dniProvider.setMotivo =
                                                          motivo;
                                                      dniProvider.setRut =
                                                          controllerRut.text;
                                                      dniProvider.setPassword =
                                                          controllerPassword
                                                              .text;
                                                      controllerRut.text = '';
                                                      controllerPassword.text =
                                                          '';
                                                      setState(() {
                                                        pantellaNext = true;
                                                      });
                                                      Navigator
                                                          .pushAndRemoveUntil(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                HomeDuenioVehiculo()),
                                                        (route) =>
                                                            false, // Esto elimina todas las pantallas anteriores
                                                      );
                                                      /*Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              HomeDuenioVehiculo(),
                                                        ),
                                                      );*/
                                                      setState(() {
                                                        dniProvider
                                                                .loginDuenio =
                                                            false;
                                                      });
                                                      setState(() {
                                                        verificaDuenioProvider
                                                            .setLoading = false;
                                                      });
                                                    }
                                                  } else {
                                                    showTopSnackBar(
                                                      Overlay.of(context),
                                                      CustomSnackBar.error(
                                                        message:
                                                            "Su cuenta se encuentra en revision",
                                                      ),
                                                    );
                                                  }
                                                } else {
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    CustomSnackBar.error(
                                                      message:
                                                          "El usuario ya se encuentra conectado",
                                                    ),
                                                  );

                                                  setState(() {
                                                    verificaDuenioProvider
                                                        .setLoading = false;
                                                  });

                                                  setState(() {
                                                    verificaDuenioProvider
                                                        .setLoading = false;
                                                  });
                                                }
                                              } else {
                                                if (rut_obtenido == "no" ||
                                                    password_obtenido == "no") {
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    CustomSnackBar.error(
                                                      message:
                                                          "Rut O Contraseña incorrectos",
                                                    ),
                                                  );
                                                  setState(() {
                                                    verificaDuenioProvider
                                                        .setLoading = false;
                                                  });
                                                } else {
                                                  setState(() {
                                                    verificaDuenioProvider
                                                        .setLoading = false;
                                                  });
                                                }
                                              }
                                            }
                                          }
                                        }

                                        setState(() {});
                                      },
                                      child: Container(
                                        width: Device.deviceType ==
                                                DeviceType.android
                                            ? 70.w
                                            : 70.w,
                                        height: Device.deviceType ==
                                                DeviceType.android
                                            ? 5.h
                                            : 5.h,
                                        decoration: const BoxDecoration(
                                            color: Colors.deepPurple,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0))),
                                        child: Center(
                                          child: Text(
                                            "Login",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    separador(),
                                    InkWell(
                                      onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const RegisterDuenio(),
                                        ),
                                      ),
                                      child: Container(
                                        child: Text(
                                          "Registrar",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                            ),
                          ],
                        ),
                      ),
              ),
            ),
    );
  }

  Future<bool> alertShow(context, homeProvider, rut, dniProvider) async {
    bool action = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.black,
        title: const Text(
          "Alerta !!!",
          style: TextStyle(color: Colors.white),
        ),
        content: const Text(
          "Esta seguro que desea salir ???",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.black),
            ),
            onPressed: () => Navigator.pop(context, false),
            child: const Text(
              "No",
              style: TextStyle(color: Colors.white),
            ),
          ),
          ElevatedButton(
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.black),
              ),
              onPressed: () async {
                final SharedPreferences prefs =
                    await SharedPreferences.getInstance();

                var conectado = await updateEstadoDuenio(rut, "desconectado");

                await prefs.remove('idsession');
                await prefs.remove('user_rut');
                await prefs.remove('user_password');
                await prefs.remove('tipo');
                await prefs.remove('id');

                dniProvider.setAllDatasPatentes = {};
                dniProvider.setPatentesObtenidasList = [];
                dniProvider.patenteguardada = "";
                homeProvider.setLista = [];
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginDuenioVehiculo()),
                  (route) =>
                      false, // Esto elimina todas las pantallas anteriores
                );
              },
              child: const Text(
                "Si",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );

    return action;
  }
}
