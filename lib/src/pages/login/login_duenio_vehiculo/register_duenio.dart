import 'dart:io';
import 'dart:async';
import 'package:admin_taxi_seguro/src/apis/api_auditor/getApis.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';
import 'package:searchfield/searchfield.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../../apis/api_duenio_vehiculo/duenio_vehicle_register.dart';
import '../../../provider/auditor_providers/auditor_provider.dart';
import '../../../tools/email_validator.dart';
import '../../../tools/loading.dart';
import '../../../tools/separador.dart';
import '../../../tools/url_base.dart';

class RegisterDuenio extends StatefulWidget {
  const RegisterDuenio({super.key});

  @override
  State<RegisterDuenio> createState() => _RegisterDuenioState();
}

class _RegisterDuenioState extends State<RegisterDuenio> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerDni = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerConfirmPassword = TextEditingController();
  TextEditingController controllerPhotoPerfil = TextEditingController();
  TextEditingController controllerPartner = TextEditingController();

  List<dynamic> losPartner = [];

  FocusNode focusApellido = FocusNode();
  FocusNode focusRut = FocusNode();
  FocusNode focusCorreo = FocusNode();
  FocusNode focusTelefono = FocusNode();
  FocusNode focusPassword = FocusNode();
  FocusNode focusConfirmPassword = FocusNode();
  FocusNode focusSearch = FocusNode();

  bool validateRutChile = false;
  bool validate2 = false;

  Dio dio = Dio();

  uuploadImageDuenio1(imagepath, rut) async {
    Dio dio = Dio();

    final serverUrl = '$url/uploadImageDuenioLateral/$rut';
    var pathimages = imagepath.path;

    final formData = FormData.fromMap({
      'imagelateral':
          await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {

      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {

        return response;
      } else {
        print('Error al subir la imagen');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  uuploadImageDuenio2(imagepath, rut) async {

    Dio dio = Dio();


    final serverUrl = '$url/uploadImageDuenioreves/$rut';
    var pathimages = imagepath.path;


    final formData = FormData.fromMap({
      'imagereves':
          await MultipartFile.fromFile('$pathimages', filename: 'image'),
    });

    try {

      final response = await dio.post(
        serverUrl,
        data: formData,
      );

      if (response.statusCode == 200) {

        return response;
      } else {
        print('Error al subir la imagen 2');
      }
    } catch (e) {
      print('Error 2: $e');
    }
  }

  getAllPartners() async {
    Response resp = await dio.get("$url/getAllPartner");
    var respuesta = resp.data;

    List listaMapas = respuesta["Partners"];

    listaMapas.forEach((element) {
      losPartner.add(element["nombre"]);
    });

    setState(() {});

    setState(() {});
  }

  String rutValidate(String ruti) {
    ruti = ruti.toUpperCase();
    bool isveri = ruti.endsWith('0');
    if (isveri == true) {
      int vValue = ruti.lastIndexOf('0');
      String verValue = ruti.substring(0, vValue);
      verValue = '${verValue}K';
      isveri = isRutValid(verValue);

      if (isveri == true) {
        ruti = verValue;
      }
    }
    bool isver = isRutValid(ruti);
    validateRutChile = isver;
    return ruti;
  }

  File? image;
  File? image_to_upload;
  var imageUrl;
  bool camerais = false;
  bool camerais2 = false;
  bool istrue = false;
  final _picker = ImagePicker();
  Future<void> openImagePicker() async {
    if (camerais) {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image = File(pickedImage.path);
          istrue = true;
        });
      }
    } else {
      final XFile? pickedImage = await _picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage != null) {
        setState(() {
          image = File(pickedImage.path);
          istrue = true;
        });
      }
    }
  }

  File? image2;
  File? image_to_upload2;
  var imageUrl2;
  bool istrue2 = false;
// This is the image picker
  final _picker2 = ImagePicker();
  Future<void> openImagePicker2() async {
    if (camerais) {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    } else {
      final XFile? pickedImage2 = await _picker2.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage2 != null) {
        setState(() {
          image2 = File(pickedImage2.path);
          istrue2 = true;
        });
      }
    }
  }

  File? image3;
  File? image_to_upload3;
  var imageUrl3;
  bool istrue3 = false;
// This is the image picker
  final _picker3 = ImagePicker();
  Future<void> openImagePicker3() async {
    if (camerais2) {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.camera,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    } else {
      final XFile? pickedImage3 = await _picker3.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 700,
          maxWidth: 700);
      if (pickedImage3 != null) {
        setState(() {
          image3 = File(pickedImage3.path);
          istrue3 = true;
        });
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllPartners();
  }

  @override
  Widget build(BuildContext context) {

    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    var verificarConductorProvider = Provider.of<AuditorProvider>(context);

    List<dynamic> listafinalquery2 = [];

    losPartner.forEach((element) {
      if (element.toString() == 'taxiseguro') {
        listafinalquery2.add('ningun partner');
      } else {
        listafinalquery2.add(element.toString());
      }
    });

    return WillPopScope(
      onWillPop: () {
        return alertShow(context);
      },
      child: Scaffold(
        body: SizedBox(
          width: 100.w,
          height: 100.h,
          child: Column(
            children: [
              titlePage(),
              verificarConductorProvider.getLoading == true
                  ? circulatorCustom()
                  : Expanded(
                      child: ListView(
                        children: [
                          Column(
                            children: [

                              inputName(controllerName, focusApellido, context),
                              separador(),
                              inputLastName(controllerLastName, focusApellido,
                                  focusRut, context),
                              separador(),
                              inputDni(
                                  controllerDni,
                                  verificaDuenioProvider,
                                  focusRut,
                                  focusCorreo,
                                  context,
                                  validate2,
                                  rutValidate,
                                  validateRutChile),
                              separador(),
                              inputEmail(controllerEmail, focusCorreo,
                                  focusTelefono, context),
                              separador(),
                              inputPhone(controllerPhone, focusTelefono,
                                  focusPassword, context),
                              separador(),
                              SizedBox(
                                width: 70.w,
                                child: TextFormField(
                                  maxLength: 10,
                                  focusNode: focusPassword,
                                  controller: controllerPassword,
                                  obscureText:
                                      verificaDuenioProvider.getIsEyes == false
                                          ? true
                                          : false,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(focusConfirmPassword);
                                  },
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          if (verificaDuenioProvider
                                              .getIsEyes) {
                                            verificaDuenioProvider.setIsEyes =
                                                false;
                                          } else {
                                            verificaDuenioProvider.setIsEyes =
                                                true;
                                          }
                                          setState(() {});
                                        },
                                        icon: Icon(
                                            verificaDuenioProvider.getIsEyes ==
                                                    true
                                                ? Icons.remove_red_eye_rounded
                                                : Icons.emergency),
                                      ),
                                      border: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                      ),
                                      hintText: 'Ingrese su contraseña',
                                      labelText: 'Ingrese su contraseña'),
                                ),
                              ),
                              separador(),
                              SizedBox(
                                width: 70.w,
                                child: TextFormField(
                                  maxLength: 10,
                                  focusNode: focusConfirmPassword,
                                  controller: controllerConfirmPassword,
                                  obscureText:
                                      verificaDuenioProvider.getIsEyes2 == false
                                          ? true
                                          : false,
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          if (verificaDuenioProvider
                                              .getIsEyes2) {
                                            verificaDuenioProvider.setIsEyes2 =
                                                false;
                                          } else {
                                            verificaDuenioProvider.setIsEyes2 =
                                                true;
                                          }
                                          setState(() {});
                                        },
                                        icon: Icon(
                                            verificaDuenioProvider.getIsEyes2 ==
                                                    true
                                                ? Icons.remove_red_eye_rounded
                                                : Icons.emergency),
                                      ),
                                      border: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                      ),
                                      hintText: 'Confirme su contraseña',
                                      labelText: 'Confirme su contraseña'),
                                ),
                              ),
                              separador(),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: SearchField<dynamic>(

                                    focusNode: focusSearch,
                                    searchStyle:
                                        TextStyle(fontWeight: FontWeight.bold),

                                    controller: controllerPartner,
                                    onSearchTextChanged: (query) {
                                      if (query == 'Ningun partner') {}

                                      List<dynamic> listafinalquery = [];

                                      losPartner.forEach((element) {
                                        if (element.toString() ==
                                            'taxiseguro') {
                                          listafinalquery.add('Ningun partner');
                                        } else {
                                          listafinalquery
                                              .add(element.toString());
                                        }
                                      });

                                      final filter = listafinalquery
                                          .where((element) => element
                                              .toLowerCase()
                                              .contains(query.toLowerCase()))
                                          .toList();
                                      return filter
                                          .map((e) => SearchFieldListItem<
                                                  String>(e,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 4.0),
                                                child: Text(e,
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color:
                                                            Colors.deepPurple)),
                                              )))
                                          .toList();
                                    },
                                    textInputAction: TextInputAction.done,
                                    suggestionsDecoration: SuggestionDecoration(
                                      padding: const EdgeInsets.all(4),
                                      border: Border.all(
                                          color: Colors.amber.shade700),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(10),
                                      ),
                                    ),
                                    itemHeight: 50,
                                    suggestionState: Suggestion.expand,
                                    hint: 'Seleccione partner',


                                    suggestions: listafinalquery2
                                        .map(
                                          (e) => SearchFieldListItem<dynamic>(
                                            e,
                                            child: Center(child: Text(e)),
                                          ),
                                        )
                                        .toList()),
                              ),
                              separador(),
                              Column(
                                children: [
                                  Text(
                                    'Foto carnet',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Column(
                                    children: [
                                      const Text("Lado a"),
                                      InkWell(
                                        onTap: () async {
                                          focusSearch.unfocus();
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opcion"),
                                              content: Text(
                                                  "¿De que manera desea subir la imagen?"),
                                              actions: [
                                                IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  icon: Row(
                                                    children: [
                                                      Text("Galeria"),
                                                      SizedBox(
                                                        width: 3.w,
                                                      ),
                                                      Icon(Icons.image),
                                                    ],
                                                  ),
                                                ),
                                                IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  icon: Row(
                                                    children: [
                                                      Text("Camara"),
                                                      SizedBox(
                                                        width: 3.w,
                                                      ),
                                                      Icon(Icons.camera),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                          await openImagePicker2();
                                          if (image_to_upload2 == null) {
                                            setState(() {
                                              image_to_upload2 =
                                                  File(image2!.path);
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload2 =
                                                  File(image2!.path);
                                            });
                                          }
                                        },
                                        child: SizedBox(
                                          width: 35.w,
                                          child: istrue2 == false
                                              ? Image.asset(
                                                  "assets/auditor/agregar.png",
                                                  width: 35.w,
                                                )
                                              : Image.file(
                                                  image2!,
                                                  width: 35.w,
                                                ),
                                        ),
                                      ),
                                      const Text("Lado B"),
                                      InkWell(
                                        onTap: () async {
                                          focusSearch.unfocus();
                                          await showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              title: Text("Elija una opción"),
                                              content: Text(
                                                  "¿Como desea subir la imagen?"),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais2 = false;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Galeria"),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      camerais2 = true;
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("camara"),
                                                ),
                                              ],
                                            ),
                                          );
                                          await openImagePicker3();
                                          if (image_to_upload3 == null) {
                                            setState(() {
                                              image_to_upload3 =
                                                  File(image3!.path);
                                            });
                                          } else {
                                            setState(() {
                                              image_to_upload3 =
                                                  File(image3!.path);
                                            });
                                          }
                                        },
                                        child: SizedBox(
                                          width: 35.w,
                                          child: istrue3 == false
                                              ? Image.asset(
                                                  "assets/auditor/agregar.png",
                                                  width: 35.w,
                                                )
                                              : Image.file(
                                                  image3!,
                                                  width: 35.w,
                                                ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              separador(),


                              InkWell(
                                onTap: () async {
                                  focusSearch.unfocus();

                                  if(controllerPartner.text == ''){
                                    showTopSnackBar(
                                      Overlay.of(context),
                                      const CustomSnackBar.error(
                                        message: "No se ha asignado ningun partner",
                                      ),
                                    );
                                  }else{
                                    if (controllerDni.text.contains(".") &&
                                        controllerDni.text.contains("-")) {
                                      String dad =
                                      rutValidate(controllerDni.text);

                                      controllerDni.text = dad.toString();

                                      validate2 = false;

                                      bool isveri = isRutValid(dad.toString());

                                      final emailValid =
                                      validatesEmail(controllerEmail.text);

                                      if (emailValid) {
                                        verificaDuenioProvider.setIsEyes = false;
                                        verificaDuenioProvider.setIsEyes2 = false;
                                        if (controllerDni.text == '' ||
                                            controllerConfirmPassword.text ==
                                                '' ||
                                            controllerEmail.text == '' ||
                                            controllerLastName.text == '' ||
                                            controllerName.text == '' ||
                                            controllerPassword.text == '' ||
                                            controllerPhone.text == '') {
                                          showTopSnackBar(
                                            Overlay.of(context),
                                            const CustomSnackBar.error(
                                              message:
                                              "Debe completar todos los campos",
                                            ),
                                          );
                                        } else {
                                          if (controllerPassword.text !=
                                              controllerConfirmPassword.text) {
                                            showTopSnackBar(
                                              Overlay.of(context),
                                              const CustomSnackBar.error(
                                                message:
                                                "Las contrseñas no coinciden",
                                              ),
                                            );
                                          } else {

                                            List yaresgistrado =
                                            await getAllDatasDuenio();

                                            bool soniguales = false;

                                            yaresgistrado.forEach((element) {
                                              if (element["dni"].toString() ==
                                                  controllerDni.text.toString()) {
                                                setState(() {
                                                  soniguales = true;
                                                });
                                              } else {}
                                            });

                                            if (soniguales == true) {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                CustomSnackBar.error(
                                                  message:
                                                  "Ya existe un usuario con el rut ${controllerDni.text}",
                                                ),
                                              );
                                            } else {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                const CustomSnackBar.info(
                                                  message: "Registrando datos",
                                                ),
                                              );
                                              setState(() {
                                                verificarConductorProvider
                                                    .setLoading = true;
                                              });


                                              if (image_to_upload2 == null ||
                                                  image_to_upload3 == null) {
                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = false;
                                                });
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.error(
                                                    message:
                                                    "Debe completar todos los campos",
                                                  ),
                                                );
                                              } else {
                                                var resp1 =
                                                await uuploadImageDuenio1(
                                                    image_to_upload2,
                                                    controllerDni.text);
                                                var resp2 =
                                                await uuploadImageDuenio2(
                                                    image_to_upload3,
                                                    controllerDni.text);

                                                var lastIndex =
                                                    resp1.toString().length;



                                                var lastIndex2 =
                                                    resp2.toString().length;


                                                print("############## EL RESP ES: $resp1");



                                                var datas = {
                                                  "id": null,
                                                  "names": controllerName.text,
                                                  "lastNames":
                                                  controllerLastName.text,
                                                  "dni": controllerDni.text,
                                                  "email": controllerEmail.text,
                                                  "phone":
                                                  "+569${controllerPhone.text}",
                                                  "password":
                                                  controllerPassword.text,
                                                  "status": "rechazado",
                                                  "aprobacion": "revision",
                                                  "patentes": "[{}]",
                                                  "fotoCarnetA":
                                                  "$url3/${resp1.toString().substring(29, lastIndex)}",
                                                  // "${providerUrl.getUrlProvider()}",
                                                  "fotoCarnetB":
                                                  "$url3/${resp2.toString().substring(29, lastIndex2)}",
                                                  //"${providerUrl.getUrlProvider1()}",
                                                  "tipoLicencia": "fgf",
                                                  "restriccion": "fgf",
                                                  "fechaVencimientoLicencia":
                                                  "fgfg",
                                                  "fotoLicenciaConducir": "fggf",
                                                  "motivo":
                                                  "Cuenta recien creada",
                                                  "partner": controllerPartner
                                                      .text ==
                                                      'Ningun partner' ||
                                                      controllerPartner
                                                          .text ==
                                                          'ningun partner'
                                                      ? 'taxiseguro'
                                                      : controllerPartner.text,
                                                };
                                                var stat =
                                                await saveDuenioVehicle(
                                                    datas);

                                                if (stat["status"] == 200) {
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = false;
                                                  });
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    const CustomSnackBar.success(
                                                      message:
                                                      "Datos guardados con exito",
                                                    ),
                                                  );
                                                  Navigator.pop(context);
                                                } else {
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = false;
                                                  });
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    const CustomSnackBar.error(
                                                      message:
                                                      "Hay un error al guardar los datos",
                                                    ),
                                                  );
                                                }
                                              }
                                            }
                                          }
                                        }
                                      } else {
                                        setState(() {
                                          verificarConductorProvider.setLoading =
                                          false;
                                        });
                                        showTopSnackBar(
                                          Overlay.of(context),
                                          const CustomSnackBar.error(
                                            message: "Email no valido",
                                          ),
                                        );
                                      }
                                    } else {
                                      var formattedRut =
                                      formatRut(controllerDni.text);
                                      verificaDuenioProvider.setIsRutTrue =
                                          isRutValid(formattedRut);

                                      String dad = rutValidate(formattedRut);

                                      controllerDni.text = dad.toString();

                                      validate2 = false;

                                      bool isveri = isRutValid(dad.toString());
                                      if (isveri) {
                                        final emailValid =
                                        validatesEmail(controllerEmail.text);
                                        if (emailValid) {
                                          verificaDuenioProvider.setIsEyes =
                                          false;
                                          verificaDuenioProvider.setIsEyes2 =
                                          false;
                                          if (controllerDni.text == '' ||
                                              controllerConfirmPassword.text ==
                                                  '' ||
                                              controllerEmail.text == '' ||
                                              controllerLastName.text == '' ||
                                              controllerName.text == '' ||
                                              controllerPassword.text == '' ||
                                              controllerPhone.text == '') {
                                            showTopSnackBar(
                                              Overlay.of(context),
                                              const CustomSnackBar.error(
                                                message:
                                                "Debe completar todos los campos",
                                              ),
                                            );
                                          } else {
                                            if (controllerPassword.text !=
                                                controllerConfirmPassword.text) {
                                              showTopSnackBar(
                                                Overlay.of(context),
                                                const CustomSnackBar.error(
                                                  message:
                                                  "Las contrseñas no coinciden",
                                                ),
                                              );
                                            } else {

                                              List yaresgistrado =
                                              await getAllDatasDuenio();

                                              bool soniguales = false;

                                              yaresgistrado.forEach((element) {
                                                if (element["dni"].toString() ==
                                                    controllerDni.text
                                                        .toString()) {
                                                  setState(() {
                                                    soniguales = true;
                                                  });
                                                } else {}
                                              });

                                              if (soniguales == true) {
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  CustomSnackBar.error(
                                                    message:
                                                    "Ya existe un usuario con el rut ${controllerDni.text}",
                                                  ),
                                                );
                                              } else {
                                                showTopSnackBar(
                                                  Overlay.of(context),
                                                  const CustomSnackBar.info(
                                                    message: "Registrando datos",
                                                  ),
                                                );
                                                setState(() {
                                                  verificarConductorProvider
                                                      .setLoading = true;
                                                });

                                                if (image_to_upload2 == null ||
                                                    image_to_upload3 == null) {
                                                  setState(() {
                                                    verificarConductorProvider
                                                        .setLoading = false;
                                                  });
                                                  showTopSnackBar(
                                                    Overlay.of(context),
                                                    const CustomSnackBar.error(
                                                      message:
                                                      "Debe completar todos los campos",
                                                    ),
                                                  );
                                                } else {


                                                  var resp1 =
                                                  await uuploadImageDuenio1(
                                                      image_to_upload2,
                                                      controllerDni.text);
                                                  var resp2 =
                                                  await uuploadImageDuenio2(
                                                      image_to_upload3,
                                                      controllerDni.text);

                                                  var lastIndex =
                                                      resp1.toString().length;

                                                  var lastIndex2 =
                                                      resp2.toString().length;

                                                  var datas = {
                                                    "id": null,
                                                    "names": controllerName.text,
                                                    "lastNames":
                                                    controllerLastName.text,
                                                    "dni": controllerDni.text,
                                                    "email": controllerEmail.text,
                                                    "phone":
                                                    "+569${controllerPhone.text}",
                                                    "password":
                                                    controllerPassword.text,
                                                    "status": "rechazado",
                                                    "aprobacion": "revision",
                                                    "patentes": "[{}]",
                                                    "fotoCarnetA":
                                                    "$url3/${resp1.toString().substring(29, lastIndex)}",
                                                    // "${providerUrl.getUrlProvider()}",
                                                    "fotoCarnetB":
                                                    "$url3/${resp2.toString().substring(29, lastIndex2)}",
                                                    //"${providerUrl.getUrlProvider1()}",
                                                    "tipoLicencia": "fgf",
                                                    "restriccion": "fgf",
                                                    "fechaVencimientoLicencia":
                                                    "fgfg",
                                                    "fotoLicenciaConducir":
                                                    "fggf",
                                                    "motivo":
                                                    "Cuenta recien creada",
                                                    "partner": controllerPartner
                                                        .text ==
                                                        'Ningun partner'
                                                        ? 'taxiseguro'
                                                        : controllerPartner.text,
                                                  };
                                                  var stat =
                                                  await saveDuenioVehicle(
                                                      datas);

                                                  if (stat["status"] == 200) {
                                                    setState(() {
                                                      verificarConductorProvider
                                                          .setLoading = false;
                                                    });
                                                    showTopSnackBar(
                                                      Overlay.of(context),
                                                      const CustomSnackBar
                                                          .success(
                                                        message:
                                                        "Datos guardados con exito",
                                                      ),
                                                    );
                                                    Navigator.pop(context);
                                                  } else {
                                                    setState(() {
                                                      verificarConductorProvider
                                                          .setLoading = false;
                                                    });
                                                    showTopSnackBar(
                                                      Overlay.of(context),
                                                      const CustomSnackBar.error(
                                                        message:
                                                        "Hay un error al guardar los datos",
                                                      ),
                                                    );
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        } else {
                                          setState(() {
                                            verificarConductorProvider
                                                .setLoading = false;
                                          });
                                          showTopSnackBar(
                                            Overlay.of(context),
                                            const CustomSnackBar.error(
                                              message: "Email no valido",
                                            ),
                                          );
                                        }
                                      } else {
                                        showTopSnackBar(
                                          Overlay.of(context),
                                          const CustomSnackBar.error(
                                            message: "Rut no valido",
                                          ),
                                        );
                                      }
                                    }
                                  }
                                },
                                child: Container(
                                  width: 30.w,
                                  decoration: BoxDecoration(
                                    color: Colors.deepPurple,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Center(
                                      child: Text(
                                        "Registrar",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.sp),
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              separador(),
                            ],
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget titlePage() {
  return Container(
    width: 100.w,
    height: 13.h,
    decoration: BoxDecoration(
      color: Colors.amber.shade700,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        bottomRight: Radius.circular(20),
      ),
    ),
    child: Center(
      child: Text(
        "¡¡¡REGISTRO!!!",
        style: TextStyle(
            fontSize: 20.sp,
            fontWeight: FontWeight.bold,
            color: Colors.deepPurple),
      ),
    ),
  );
}

Future<bool> alertShow(context) async {
  bool action = await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.black,
      title: Center(
        child: const Text(
          "¡Alerta!",
          style: TextStyle(color: Colors.white),
        ),
      ),
      content: const Text(
        "¿Esta seguro que desea salir del registro?",
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () => Navigator.pop(context, false),
          child: const Text(
            "No",
            style: TextStyle(color: Colors.white),
          ),
        ),
        ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.black),
          ),
          onPressed: () async {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          },
          child: const Text(
            "Si",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    ),
  );

  return action;
}

Widget inputName(TextEditingController controller, focus, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      autofocus: true,
      maxLength: 20,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus);
      },
      controller: controller,
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su nombre',
          labelText: 'Ingrese su nombre'),
    ),
  );
}

Widget inputLastName(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 20,
      controller: controller,
      focusNode: focus,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su apellido',
          labelText: 'Ingrese su apellido'),
    ),
  );
}

Widget inputDni(TextEditingController controller, verificaDuenioProvider, focus,
    focus2, context, validate2, rutValidate, validateRutChile) {
  return SizedBox(
    width: 70.w,
    child: Focus(
      onFocusChange: (value) {
        if (value == false) {
          String dad = rutValidate(controller.text);

          controller.text = dad.toString();
        }
      },
      child: TextFormField(
        keyboardType: TextInputType.datetime,
        controller: controller,
        focusNode: focus,
        autovalidateMode: AutovalidateMode.always,
        validator: (value) {
          validate2 = false;
          if (value!.isEmpty) {
            return 'Debes Ingresar RUT Valido';
          } else if (value.length < 7) {
            return 'Debes Ingresar RUT Valido';
          } else if ((value.length >= 8)) {
            value = rutValidate(value);
            bool isveri = isRutValid(value!);

            if (isveri) {
              validate2 = true;
              return null;
            } else {
              return 'si termina en K reemplaza por un 0';
            }
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        },
        maxLength: 12,
        onEditingComplete: () {
          if (validateRutChile == true) {
            String dad = rutValidate(controller.text);

            validate2 = true;

            FocusScope.of(context!).nextFocus();

            controller.text = dad.toString();
          }
          FocusScope.of(context).requestFocus(focus2);
        },
        onChanged: (value) {
          var formattedRut = formatRut(controller.text);
          verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);
        },
        onTap: () {
          validate2 = false;
          controller.text = deFormatRut(controller.text);
        },
        inputFormatters: [RutFormatter()],
        decoration: const InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: 'Ingrese su rut',
            labelText: 'Ingrese su rut'),
      ),
    ),
  );
}

Widget inputEmail(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 255,
      controller: controller,
      focusNode: focus,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su correo',
          labelText: 'Ingrese su correo'),
    ),
  );
}

Widget inputPhone(TextEditingController controller, focus, focus2, context) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      maxLength: 8,
      controller: controller,
      focusNode: focus,
      keyboardType: TextInputType.phone,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(focus2);
      },
      decoration: const InputDecoration(
          prefixText: '+569',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          hintText: 'Ingrese su telefono',
          labelText: 'Ingrese su telefono'),
    ),
  );
}
