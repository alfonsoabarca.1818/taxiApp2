import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rut_utils/rut_utils.dart';

Widget inputTextDni(TextEditingController controllerRut, verificaDuenioProvider,
    focus, context, validate2, rutValidate, validateRutChile, pantalla) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      autofocus: pantalla ==false ? true:false,
      autovalidateMode: AutovalidateMode.always,
      keyboardType: TextInputType.datetime,
      validator: (value) {
        validate2 = false;
        if (value!.isEmpty) {
          return 'Debes Ingresar RUT Valido';
        } else if (value.length < 7) {
          return 'Debes Ingresar RUT Valido';
        } else if ((value.length >= 8)) {
          value = rutValidate(value);
          bool isveri = isRutValid(value!);

          if (isveri) {
            validate2 = true;
            return null;
          } else {
            return 'si termina en K reemplaza por un 0';
          }
        } else {
          return 'si termina en K reemplaza por un 0';
        }
      },
      maxLength: 12,
      onEditingComplete: () {
        if (validateRutChile == true) {
          String dad = rutValidate(controllerRut.text);

          validate2 = true;

          FocusScope.of(context!).nextFocus();
        }
        FocusScope.of(context).requestFocus(focus);
      },
      onChanged: (value) {
        var formattedRut = formatRut(controllerRut.text);
        verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);

      },
      onTap: () {
        validate2 = false;
        controllerRut.text = deFormatRut(controllerRut.text);
      },
      inputFormatters: [RutFormatter()],
      controller: controllerRut,
      decoration: const InputDecoration(
        fillColor: Colors.white,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Rut:',

      ),
    ),
  );
}

Widget inputTextPassword(TextEditingController controllerPassword, focus) {
  return SizedBox(
    width: 70.w,
    child: TextFormField(
      focusNode: focus,
      controller: controllerPassword,
      maxLength: 10,
      obscureText: true,
      decoration: const InputDecoration(
        fillColor: Colors.white,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Contraseña:',
      //  labelText: 'Ingrese contraseña 5 caracteres min.',
      ),
    ),
  );
}


Widget buttonRecoveryPassword() {
  return Container();
}
