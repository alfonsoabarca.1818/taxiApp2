import 'dart:convert';
import 'package:admin_taxi_seguro/src/pages/home/home_auditor/home_auditor.dart';
import 'package:admin_taxi_seguro/src/pages/home/home_conductor/home_conductor.dart';
import 'package:admin_taxi_seguro/src/pages/home/home_duenio_vehiculo/home_duenio_vehiculo.dart';
import 'package:admin_taxi_seguro/src/pages/pdf/view_condiciones.dart';
import 'package:admin_taxi_seguro/src/pages/pdf/view_privacidad.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../apis/api_auditor/getAllConductoresPartner.dart';
import '../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../apis/api_auditor/update_estado.dart';
import '../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../apis/api_duenio_vehiculo/login_duenio_vehiculo.dart';
import '../../apis/api_duenio_vehiculo/updateVehicle.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../tools/loading.dart';
import '../../tools/separador.dart';
import 'login_auditor/login_auditor.dart';
import 'login_conductor/login_conductor.dart';
import 'login_duenio_vehiculo/login_duenio_vehiculo.dart';

class SelectUser extends StatefulWidget {
  const SelectUser({super.key});

  @override
  State<SelectUser> createState() => _SelectUserState();
}

class _SelectUserState extends State<SelectUser> {
  bool pantellaNext = false;

  Dio dio = Dio();

  final Uri _urlPrivacidad = Uri.parse('https://www.taxiseguro.cl/TaxiSeguro_Politicas_de_Privacidad.pdf');
  final Uri _urlCondiciones = Uri.parse('https://www.taxiseguro.cl/TaxiSeguro_Terminos_y_Condiciones.pdf');

  Future<void> _launchUrlPrivacidad() async {
    if (!await launchUrl(_urlPrivacidad)) {
      throw Exception('Could not launch $_urlPrivacidad');
    }
  }

  Future<void> _launchUrlCondiciones() async {
    if (!await launchUrl(_urlCondiciones)) {
      throw Exception('Could not launch $_urlCondiciones');
    }
  }

  loginAutomatico(dniProvider, verificaDuenioProvider,
      verificarConductorProvider, loginProvider) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final String? id = prefs.getString('idsession');
    final String? tipo = prefs.getString('tipo');

    if (tipo == 'auditor') {
      setState(() {
        dniProvider.isloadingbool = true;
      });
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      final String? idsession = prefs.getString('idsession');

      final String? rutPref = prefs.getString('user_rut');
      final String? passwordPref = prefs.getString('user_password');

      if (idsession != null) {
        var modificado = await updateEstadoAuditor('conectado', rutPref);

        Map auditor = await getOneAuditor(rutPref);

        String nombreAuditor = auditor["names"].toString();

        verificaDuenioProvider.nameAuditor = nombreAuditor;

        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.success(
            message: "Datos ingresados son correctos",
          ),
        );

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeAuditor()),
          (route) => false, // Esto elimina todas las pantallas anteriores
        );

        setState(() {
          dniProvider.isloadingbool = false;
        });
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const LoginAuditor(),
          ),
        );
        setState(() {
          dniProvider.isloadingbool = false;
        });
      }
    } else if (tipo == 'duenio') {
      setState(() {
        dniProvider.isloadingbool = true;
      });

      final SharedPreferences prefs = await SharedPreferences.getInstance();

      final String? idsession = prefs.getString('idsession');

      final String? rutPref = prefs.getString('user_rut');
      final String? passwordPref = prefs.getString('user_password');

      if (idsession != null) {
        var status = await postLoginDuenioVehiculo(rutPref, passwordPref);
        var datasPatentes = await patentesDuenio(rutPref, passwordPref);

        if (datasPatentes["status"] == false) {
          var s = status;

          var estadoduenio = await getEstadoDuenio(rutPref);

          List patentes_obtenidas = jsonDecode(datasPatentes["patentes"]);

          dniProvider.setPatentesObtenidasList = patentes_obtenidas;

          List lasPatentesAll = [];

          lasPatentesAll.add(await getAllPatentesAll());

          dniProvider.setAllPatentesAhora = lasPatentesAll[0];

          dniProvider.dni_duenio = rutPref!;

          String aprobacion = datasPatentes["aprobacion"];
          String motivo = datasPatentes["motivo"];

          showTopSnackBar(
            Overlay.of(context),
            CustomSnackBar.success(
              message: "Datos ingresados son correctos",
            ),
          );

          dniProvider.setRut = rutPref!;
          dniProvider.setAprobacion = aprobacion;
          dniProvider.setMotivo = motivo;
          dniProvider.setPassword = passwordPref;

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeDuenioVehiculo()),
            (route) => false, // Esto elimina todas las pantallas anteriores
          );

          setState(() {
            dniProvider.isloadingbool = false;
          });
        } else {
          dniProvider.setAllDatasProvider = datasPatentes;

          if (await datasPatentes["patentes"] == "[{}]") {
            var s = status;

            var estadoduenio = await getEstadoDuenio(rutPref);

            List patentes_obtenidas = jsonDecode(datasPatentes["patentes"]);

            dniProvider.setPatentesObtenidasList = patentes_obtenidas;

            List lasPatentesAll = [];

            lasPatentesAll.add(await getAllPatentesAll());

            dniProvider.setAllPatentesAhora = lasPatentesAll[0];

            dniProvider.dni_duenio = rutPref!;

            String aprobacion = datasPatentes["aprobacion"];
            String motivo = datasPatentes["motivo"];

            showTopSnackBar(
              Overlay.of(context),
              CustomSnackBar.success(
                message: "Datos ingresados son correctos",
              ),
            );

            dniProvider.setRut = rutPref!;
            dniProvider.setAprobacion = aprobacion;
            dniProvider.setMotivo = motivo;
            dniProvider.setPassword = passwordPref;

            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeDuenioVehiculo()),
              (route) => false, // Esto elimina todas las pantallas anteriores
            );

            setState(() {
              dniProvider.isloadingbool = false;
            });
          } else {
            dniProvider.setAllDatasPatentes =
                jsonDecode(await datasPatentes["patentes"]);

            List lasPatentesAll = [];

            lasPatentesAll.add(await getAllPatentesAll());

            dniProvider.setAllPatentesAhora = lasPatentesAll[0];

            dniProvider.dni_duenio = rutPref!;

            showTopSnackBar(
              Overlay.of(context),
              CustomSnackBar.success(
                message: "Datos ingresados son correctos",
              ),
            );

            var conectado = await updateEstadoDuenio(rutPref!, "conectado");

            String aprobacion = datasPatentes["aprobacion"];
            String motivo = datasPatentes["motivo"];

            dniProvider.setAprobacion = aprobacion;
            dniProvider.setMotivo = motivo;
            dniProvider.setRut = rutPref!;
            dniProvider.setPassword = passwordPref;

            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeDuenioVehiculo()),
              (route) => false, // Esto elimina todas las pantallas anteriores
            );

            setState(() {
              dniProvider.isloadingbool = false;
            });
          }
        }
      } else {
        setState(() {
          dniProvider.isloadingbool = false;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const LoginDuenioVehiculo(),
          ),
        );
      }
    } else if (tipo == 'conductor') {
      setState(() {
        dniProvider.isloadingbool = true;
      });
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      final String? idsession = prefs.getString('idsession');

      final String? rutPref = prefs.getString('user_rut');
      final String? passwordPref = prefs.getString('user_password');

      if (idsession != null) {
        verificarConductorProvider
            .setDatasVerificarOneConductor(await getOneDatasConductor(rutPref));
        verificarConductorProvider.setLoading = false;

        if (verificarConductorProvider
                    .getDatasVerificarOneConductor()["aprobacion"] ==
                "aprobado" ||
            verificarConductorProvider
                    .getDatasVerificarOneConductor()["aprobacion"] ==
                "rechazado" ||
            verificarConductorProvider
                    .getDatasVerificarOneConductor()["aprobacion"] ==
                "rechasado") {
          setState(() {
            verificaDuenioProvider.setRutAuditor = rutPref!;
          });

          verificarConductorProvider.setAprobadoProvider =
              verificarConductorProvider
                  .getDatasVerificarOneConductor()["aprobacion"];

          setState(() {
            verificarConductorProvider.setLoading = true;
          });

          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.success(
              message: "Datos ingresados son correctos",
            ),
          );

          var modificado = await updateEstadoConductor('conectado', rutPref);

          final String? idsession = prefs.getString('idsession');


          setState(() {
            verificarConductorProvider.setLoading = false;
          });
          setState(() {
            pantellaNext = true;
          });
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeConductor()),
            (route) => false, // Esto elimina todas las pantallas anteriores
          );

          setState(() {
            dniProvider.isloadingbool = false;
          });
          setState(() {
            loginProvider.loginConductor = false;
          });
          loginProvider.loginConductor = false;
        } else if (verificarConductorProvider
                .getDatasVerificarOneConductor()["aprobacion"] ==
            "revision") {
          final String? idsession = prefs.getString('idsession');

          setState(() {
            verificaDuenioProvider.setRutAuditor = rutPref!;
          });
          verificarConductorProvider.setAprobadoProvider =
              verificarConductorProvider
                  .getDatasVerificarOneConductor()["aprobacion"];
          setState(() {
            verificarConductorProvider.setLoading = true;
          });

          var modificado = await updateEstadoConductor('conectado', rutPref);



          setState(() {
            verificarConductorProvider.setLoading = false;
          });
          setState(() {
            pantellaNext = true;
          });
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeConductor()),
            (route) => false, // Esto elimina todas las pantallas anteriores
          );

          setState(() {
            dniProvider.isloadingbool = false;
          });
          setState(() {
            loginProvider.loginConductor = false;
          });
        } else if (verificarConductorProvider
                .getDatasVerificarOneConductor()["aprobacion"] ==
            "desaprobado") {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message:
                  "Su cuenta no se encuentra activa \n contactar al administrador",
            ),
          );
          setState(() {
            loginProvider.loginConductor = false;
          });
        } else {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message:
                  "Ha ocurrido un error. Por favor verifica las credenciales e intenta de nuevo",
            ),
          );
          setState(() {
            loginProvider.loginConductor = false;
          });
        }

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeConductor()),
          (route) => false, // Esto elimina todas las pantallas anteriores
        );

        setState(() {
          dniProvider.isloadingbool = false;
        });
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const LoginConductor(),
          ),
        );
        setState(() {
          dniProvider.isloadingbool = false;
        });
      }
    }
  }
  String _appVersion = "Cargando...";

  @override
  void initState() {
    // TODO: implement initState

    super.initState();

    _getAppVersion();

    loginAutomatico(
        context.read<LoginProvider>(),
        context.read<AuditorProvider>(),
        context.read<AuditorProvider>(),
        context.read<LoginProvider>());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    final dniProvider = Provider.of<LoginProvider>(context);
    final verificarConductorProvider = Provider.of<AuditorProvider>(context);
    final verificaDuenioProvider = Provider.of<AuditorProvider>(context);
    final loginProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
      body: dniProvider.isloadingbool == true
          ? circulatorCustom()
          : Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.center,
                    end: Alignment.centerRight,
                    colors: [
                      Color(0xFFEE973A),
                      Color(0xfffA14BCC),
                    ]),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.center,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xFFEE973A),
                              Color(0xfffA14BCC),
                            ]),
                      ),
                      child: Column(
                        children: [
                          Container(
                            width: 100.w,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.center,
                                  end: Alignment.centerRight,
                                  colors: [
                                    Color(0xFFEE973A),
                                    Color(0xFFA14BCC),
                                  ]),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Center(
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 5.h,
                                  ),
                                  Center(
                                    child: Text(
                                      "Administrador\n   TaxiSeguro",
                                      style: TextStyle(
                                          fontSize: 22.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 3.h,
                                  ),
                                  Text(
                                    "Selecciona Perfil",
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                        onTap: () async {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    LoginAuditor(),
                                              ));
                                        },
                                        child: imageUser(
                                            "assets/login/auditor.png",
                                            "Auditor")),
                                    InkWell(
                                        onTap: () async {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    LoginDuenioVehiculo(),
                                              ));
                                        },
                                        child: imageUser(
                                            "assets/login/carro.png",
                                            "Dueño vehiculo")),
                                    InkWell(
                                      onTap: () async {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  LoginConductor(),
                                            ));
                                      },
                                      child: imageUser(
                                          "assets/login/conductor.png",
                                          "Conductor"),
                                    ),
                                    separador(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ViewCondiciones(),)),
                      child: Text(
                        "Terminos y condiciones",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15.sp),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPrivacidad(),)),
                      child: Text(
                        "Terminos de privacidad",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15.sp),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Versión: $_appVersion'),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
  Future<void> _getAppVersion() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      _appVersion = packageInfo.version;
    });
  }
}



Widget imageUser(img, text) {
  return Column(
    children: [
      Image.asset(
        img,
        width: 16.w,
        height: 16.h,
      ),
      Text(
        text,
        style: TextStyle(
            fontSize: 17.sp, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    ],
  );
}
