import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:googleapis/drive/v2.dart';

class ViewCondiciones extends StatefulWidget {
  const ViewCondiciones({super.key});

  @override
  State<ViewCondiciones> createState() => _ViewCondicionesState();
}

class _ViewCondicionesState extends State<ViewCondiciones> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: PDF(
          swipeHorizontal: true,
        
        ).fromUrl('https://www.taxiseguro.cl/TaxiSeguro_Terminos_y_Condiciones.pdf'),
      ),

    );
  }
}
