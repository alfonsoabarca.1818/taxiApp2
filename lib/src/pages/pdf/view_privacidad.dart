import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:googleapis/drive/v2.dart';

class ViewPrivacidad extends StatefulWidget {
  const ViewPrivacidad({super.key});

  @override
  State<ViewPrivacidad> createState() => _ViewPrivacidadState();
}

class _ViewPrivacidadState extends State<ViewPrivacidad> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: PDF(
          swipeHorizontal: true,
        
        ).fromUrl('https://www.taxiseguro.cl/TaxiSeguro_Politicas_de_Privacidad.pdf'),
      ),

    );
  }
}
