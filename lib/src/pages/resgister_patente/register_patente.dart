import 'dart:convert';
import 'package:admin_taxi_seguro/src/apis/api_auditor/update_aprobacion/update_aprobacion_conductor.dart';
import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../apis/api_duenio_vehiculo/getAllPatentes.dart';
import '../../apis/api_duenio_vehiculo/login_duenio_vehiculo.dart';
import '../../provider/duenio_vehiculo_providers/login_provider.dart';
import '../../tools/separador.dart';
import '../../tools/upper_case.dart';
import '../home/home_duenio_vehiculo/home_duenio_vehiculo.dart';

class RegisterPatente extends StatefulWidget {
  const RegisterPatente({super.key});

  @override
  State<RegisterPatente> createState() => _RegisterPatenteState();
}

class _RegisterPatenteState extends State<RegisterPatente> {
  TextEditingController controllerPatente = TextEditingController();

  List listaDates = [];

  @override
  Widget build(BuildContext context) {
    var dniProvider = Provider.of<LoginProvider>(context);

    var dni_duenio = dniProvider.getdniduenio;

    Map patentess = dniProvider.getAllDatasPatentes;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        foregroundColor: Colors.black,
        title: Center(
            child: Text(
          "AÑADIR PATENTES",
          style: TextStyle(
            color: Colors.deepPurple,
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
          ),
        )),
      ),
      body: dniProvider.savepatentebool == true
          ? circulatorCustom()
          : dniProvider.guardarPatente == true
              ? circulatorCustom()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        maxLength: 6,
                        controller: controllerPatente,
                        inputFormatters: [
                          UpperCaseTextFormatter(),
                        ],
                        textCapitalization: TextCapitalization.characters,
                        style: const TextStyle(color: Colors.black),
                        decoration: const InputDecoration(
                          labelStyle: TextStyle(
                            color: Colors.black,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.black,
                          ),
                          hintText: 'Ingrese nueva patente',
                          labelText: 'Ingrese nueva patente',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    separador(),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          dniProvider.guardarPatente = true;
                        });
                        if (controllerPatente.text == '') {
                          showTopSnackBar(
                            Overlay.of(context),
                            const CustomSnackBar.error(
                                message: "Debes ingresar una patente"),
                          );
                          setState(() {
                            dniProvider.guardarPatente = false;
                          });
                        } else {
                          if (controllerPatente.text.contains("Ñ") ||
                              controllerPatente.text.contains("ñ")) {
                            showTopSnackBar(
                              Overlay.of(context),
                              const CustomSnackBar.error(
                                maxLines: 3,
                                message:
                                    "¡¡¡Se ha detectado un caracter invalido!!!",
                              ),
                            );
                            setState(() {
                              dniProvider.guardarPatente = false;
                            });
                          } else {
                            var todaslaspatentes =
                                dniProvider.getAllPatentesAhora;

                            List listaahora = [];

                            todaslaspatentes.forEach((element) {
                              listaahora.add(element["patentes"]);
                            });

                            List sinnull = [];
                            for (var i = 0; i < listaahora.length; i++) {
                              if (listaahora[i] != null) {
                                sinnull.add(jsonDecode(listaahora[i]));
                              }
                            }
                            bool yes = false;

                            sinnull.forEach((element) {
                              if (element.contains(controllerPatente.text)) {
                                setState(() {
                                  yes = true;
                                });
                              }
                            });
                            if (yes == true ||
                                dniProvider.patenteguardada ==
                                    controllerPatente.text) {
                              showTopSnackBar(
                                Overlay.of(context),
                                const CustomSnackBar.error(
                                  message: "La patente ya existe",
                                ),
                              );
                              setState(() {
                                dniProvider.guardarPatente = false;
                              });
                            } else {
                              bool siesta = false;

                              setState(() {
                                dniProvider.savepatentebool = true;
                              });

                              if (patentess.isEmpty) {
                                Map data = {};

                                List milista = [controllerPatente.text];

                                for (var i = 0; i < milista.length; i++) {
                                  data.addAll({"patente$i": "${milista[i]}"});
                                }
                                dniProvider.setAllDatasPatentes = data;

                                var date =
                                    await deletePatentes(data, dni_duenio);

                                var datasPatentes = await patentesDuenio(
                                    dniProvider.getRut,
                                    dniProvider.getPassword);

                                var updateDni = await updateDniVehicle(
                                    dniProvider.getRut, controllerPatente.text);

                                setState(() {
                                  dniProvider.savepatentebool = false;
                                });
                                setState(() {
                                  dniProvider.guardarPatente = false;
                                  dniProvider.patenteguardada =
                                      controllerPatente.text;
                                });

                                await showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      backgroundColor: Colors.amber.shade700,
                                      title: Center(
                                        child: Text(
                                          '¡¡¡AVISO!!!',
                                          style: TextStyle(
                                              color: Colors.deepPurple,
                                              fontSize: 20.sp),
                                        ),
                                      ),
                                      content: Text(
                                        "Patente registrada con éxito\n ¡¡¡IMPORTANTE!!!\n *Sigue los siguientes pasos:\n\n*SELECCIONA TU PATENTE E INGRESA LOS DATOS CORRESPONDIENTES",
                                        style: TextStyle(
                                            color: Colors.deepPurple,
                                            fontSize: 18.sp),
                                      ),
                                      actions: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            ElevatedButton(
                                              style: const ButtonStyle(
                                                backgroundColor:
                                                    MaterialStatePropertyAll(
                                                        Colors.red),
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              child: const Text(
                                                "Continuar",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    );
                                  },
                                );

                                Navigator.pop(context);
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const HomeDuenioVehiculo(),
                                    ));
                              } else {
                                Map data = {};

                                List milista = [controllerPatente.text];

                                patentess.forEach((key, value) {
                                  milista.add(value);
                                });

                                for (var i = 0; i < milista.length; i++) {
                                  data.addAll({"patente$i": "${milista[i]}"});
                                }

                                dniProvider.setAllDatasPatentes = data;

                                var date =
                                    await deletePatentes(data, dni_duenio);

                                var datasPatentes = await patentesDuenio(
                                    dniProvider.getRut,
                                    dniProvider.getPassword);

                                var updateDni = await updateDniVehicle(
                                    dniProvider.getRut, controllerPatente.text);

                                Map varias =
                                    jsonDecode(datasPatentes["patentes"]);

                                await showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      backgroundColor: Colors.amber.shade700,
                                      title: Center(
                                        child: Text(
                                          '¡¡¡AVISO!!!',
                                          style: TextStyle(
                                              color: Colors.deepPurple,
                                              fontSize: 18.sp),
                                        ),
                                      ),
                                      content: Text(
                                        "Patente registrada con éxito\n ¡¡¡IMPORTANTE!!!\n\n*Selecciona tu patente e ingresa los datos correspondientes",
                                        style: TextStyle(
                                            color: Colors.deepPurple,
                                            fontSize: 15.sp),
                                      ),
                                      actions: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            ElevatedButton(
                                              style: const ButtonStyle(
                                                backgroundColor:
                                                    MaterialStatePropertyAll(
                                                        Colors.red),
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              child: const Text(
                                                "Continuar",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    );
                                  },
                                );

                                setState(() {
                                  dniProvider.savepatentebool = false;
                                });
                                setState(() {
                                  dniProvider.guardarPatente = false;
                                  dniProvider.patenteguardada =
                                      controllerPatente.text;
                                });
                                Navigator.pop(context);
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const HomeDuenioVehiculo(),
                                    ));
                              }
                            }
                          }
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Añadir',
                          style: TextStyle(
                              color: Colors.deepPurple,
                              fontSize: 17.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStatePropertyAll(Colors.amber.shade700),
                      ),
                    ),
                  ],
                ),
    );
  }
}
