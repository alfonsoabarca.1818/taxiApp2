import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../apis/api_auditor/update_aprobacion/update_aprobacion_conductor.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../../tools/loading.dart';
import '../../tools/separador.dart';

class MostrarDatosConductorVerificar extends StatefulWidget {
  const MostrarDatosConductorVerificar({super.key});

  @override
  State<MostrarDatosConductorVerificar> createState() =>
      _MostrarDatosConductorVerificarState();
}

class _MostrarDatosConductorVerificarState
    extends State<MostrarDatosConductorVerificar> {

  TextEditingController controllerAprobacion = TextEditingController();
  TextEditingController controllerMotivo = TextEditingController();

  String? selectedValue;
  final List<String> items = ["Aprobado", "Rechazado"];

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<AuditorProvider>(context);

    var name = datas.getDatasVerificarOneConductor()["name"];

    var lastName = datas.getDatasVerificarOneConductor()["lastName"];
    var dni = datas.getDatasVerificarOneConductor()["dni"];

    var fotoCarnetA = datas.getDatasVerificarOneConductor()["fotoCarnetA"];
    var fotoCarnetB = datas.getDatasVerificarOneConductor()["fotoCarnetB"];

    var email = datas.getDatasVerificarOneConductor()["email"];
    var phone = datas.getDatasVerificarOneConductor()["phone"];
    var fotoPerfil = datas.getDatasVerificarOneConductor()["fotoPerfil"];
    var tipoLicencia = datas.getDatasVerificarOneConductor()["tipoLicencia"];
    var restriccion = datas.getDatasVerificarOneConductor()["restriccion"];

    var fechaVencimientoLicencia =
        datas.getDatasVerificarOneConductor()["fechaVencimientoLicencia"];
    var fotoLicenciaConducirA =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirA"];
    var fotoLicenciaConducirB =
        datas.getDatasVerificarOneConductor()["fotoLicenciaConducirB"];

    var certificadoantecedentes =
        datas.getDatasVerificarOneConductor()["certificadoAntecedentes"];
    return Scaffold(
      body: SizedBox(
        width: 100.w,
        height: 100.h,
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 17.h,
              decoration: BoxDecoration(
                color: Colors.amber.shade700,
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  "Verificación datos \n        conductor",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp,
                      color: Colors.deepPurple),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: ListView(
                  children: [
                    showNames(name),
                    separador(),
                    showLastNames(lastName),
                    separador(),
                    showDni(dni),
                    separador(),
                    const Text("foto carnet lado A"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoCarnetA",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    const Text("foto carnet lado B"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoCarnetB",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    showEmail(email),
                    separador(),
                    showPhone(phone),
                    separador(),
                    const Text("Foto perfil"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoPerfil",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    showTipoLicencia(tipoLicencia),
                    separador(),
                    showRestriccion(restriccion),
                    separador(),
                    showFechaVencimientoLicencia(fechaVencimientoLicencia),
                    separador(),
                    const Text("Licencia conducir lado A"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoLicenciaConducirA",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    const Text("Licencia conducir lado B"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoLicenciaConducirB",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    const Text("Certificado antecedentes"),
                    SizedBox(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$certificadoantecedentes",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    DropdownButtonHideUnderline(
                      child: DropdownButton2<String>(
                        isExpanded: true,
                        hint: const Row(
                          children: [
                            Icon(
                              Icons.list,
                              size: 16,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Expanded(
                              child: Text(
                                'Selecciona un estado',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                        items: items
                            .map((String item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(
                                    item,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ))
                            .toList(),
                        value: selectedValue,
                        onChanged: (String? value) {
                          setState(() {
                            selectedValue = value;
                          });
                        },
                        buttonStyleData: ButtonStyleData(
                          height: 50,
                          width: 160,
                          padding: const EdgeInsets.only(left: 14, right: 14),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            border: Border.all(
                              color: Colors.black26,
                            ),
                            color: Colors.deepPurple,
                          ),
                          elevation: 2,
                        ),
                        iconStyleData: const IconStyleData(
                          icon: Icon(
                            Icons.arrow_forward_ios_outlined,
                          ),
                          iconSize: 14,
                          iconEnabledColor: Colors.white,
                          iconDisabledColor: Colors.grey,
                        ),
                        dropdownStyleData: DropdownStyleData(
                          maxHeight: 200,
                          width: 200,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            color: Colors.deepPurple,
                          ),
                          offset: const Offset(-20, 0),
                          scrollbarTheme: ScrollbarThemeData(
                            radius: const Radius.circular(40),
                            thickness: MaterialStateProperty.all<double>(6),
                            thumbVisibility:
                                MaterialStateProperty.all<bool>(true),
                          ),
                        ),
                        menuItemStyleData: const MenuItemStyleData(
                          height: 40,
                          padding: EdgeInsets.only(left: 14, right: 14),
                        ),
                      ),
                    ),
                    separador(),
                    selectedValue == "Rechazado"
                        ? TextFormField(
                            controller: controllerMotivo,
                            enabled: true,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              hintText: 'Ingrese motivo de rechazo',
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          )
                        : Container(),
                    separador(),
                    aprobarVerificacion(
                        selectedValue.toString().toLowerCase(),
                        dni,
                        context,
                        controllerMotivo,
                        fotoPerfil,
                        fotoCarnetA,
                        fotoCarnetB,
                        fotoLicenciaConducirA,
                        fotoLicenciaConducirB,
                        certificadoantecedentes),
                    separador(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget showNames(names) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Nombre"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$names',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showLastNames(lastNames) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Apellido"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$lastNames',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showDni(dni) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Rut"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$dni',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showEmail(email) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Email"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$email',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPhone(phone) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Telefono"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$phone',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPasswod(password) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Contraseña"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$password',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPhotoPerfil(perfil) {
  return Column(
    children: [
      const Text("Foto perfil"),
      SizedBox(
        width: 20.w,
        child: Image.network(perfil, width: 20.w),
      ),
    ],
  );
}

Widget showAprobacion(aprobacion, controllerAprobacion) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      controller: controllerAprobacion,
      enabled: true,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Estado del usuario: $aprobacion',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showFotoCarnet(fotoCarnetA, fotoCarnetB) {
  return Column(
    children: [
      const Text("foto carnet lado A"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoCarnetA, width: 20.w),
      ),
      separador(),
      const Text("foto carnet lado B"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoCarnetB, width: 20.w),
      ),
    ],
  );
}

Widget showTipoLicencia(tipoLicenciaConducir) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Tipo licencia"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$tipoLicenciaConducir',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showRestriccion(restriccion) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Restricción"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$restriccion',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showFechaVencimientoLicencia(vencimientoLicencia) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Fecha vencimiento licencia"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$vencimientoLicencia',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showFotoLicenciaConducir(fotoLicenciaA, fotoLicenciaB) {
  return Column(
    children: [
      const Text("Licencia conducir lado A"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoLicenciaA, width: 20.w),
      ),
      separador(),
      const Text("Licencia conducir lado B"),
      SizedBox(
        width: 20.w,
        child: Image.network(fotoLicenciaB, width: 20.w),
      ),
    ],
  );
}

Widget aprobarVerificacion(
    controllerAprobacion,
    dni,
    context,
    motivo,
    fotoPerfil,
    fotoCarnetA,
    fotoCarnetB,
    fotoLicenciaConducirA,
    fotoLicenciaConducirB,
    certificadoAntecedentes) {
  return ElevatedButton(
    style: const ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
    ),
    onPressed: () async {
      if (controllerAprobacion == "aprobado" ||
          controllerAprobacion == "rechazado") {
        CachedNetworkImage.evictFromCache("$fotoPerfil");
        CachedNetworkImage.evictFromCache("$fotoCarnetA");
        CachedNetworkImage.evictFromCache("$fotoCarnetB");
        CachedNetworkImage.evictFromCache("$fotoLicenciaConducirA");
        CachedNetworkImage.evictFromCache("$fotoLicenciaConducirB");
        CachedNetworkImage.evictFromCache("$certificadoAntecedentes");

        var response = await updateAprobacionConductor(controllerAprobacion,
            dni, motivo.text == '' ? 'Usted esta aprobado' : motivo.text);

        if (response["status_code"] == true) {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.success(
              message: "Datos actualizados correctamente",
            ),
          );
          Navigator.pop(context);
          Navigator.pop(context);
        } else {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message: "Usted ha ingresado una aprobación no valida !!!!!",
            ),
          );
        }
      } else {
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.error(
            message:
                "Datos ingresados incorrectos (EJ: 'aprobado' O 'desaprobado')",
          ),
        );
      }
    },
    child: const Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        "Verificar",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}
