import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../apis/api_auditor/update_aprobacion/update_aprobacion.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../../tools/loading.dart';
import '../../tools/separador.dart';

class MostrarDatosDuenioVerificar extends StatefulWidget {
  const MostrarDatosDuenioVerificar({super.key});

  @override
  State<MostrarDatosDuenioVerificar> createState() =>
      _MostrarDatosDuenioVerificarState();
}

class _MostrarDatosDuenioVerificarState
    extends State<MostrarDatosDuenioVerificar> {

  TextEditingController controllerAprobacion = TextEditingController();
  TextEditingController controllerMotivo = TextEditingController();
  String? selectedValue;
  final List<String> items = ["Aprobado", "Rechazado"];

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<AuditorProvider>(context);
    var names = datas.getDatasVerificarOneDuenio()["names"];
    var lastNames = datas.getDatasVerificarOneDuenio()["lastNames"];
    var dni = datas.getDatasVerificarOneDuenio()["dni"];
    var email = datas.getDatasVerificarOneDuenio()["email"];
    var phone = datas.getDatasVerificarOneDuenio()["phone"];
    var photoCarnetA = datas.getDatasVerificarOneDuenio()["fotoCarnetA"];
    var photoCarnetB = datas.getDatasVerificarOneDuenio()["fotoCarnetB"];

    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 100.w,
            height: 17.h,
            decoration: BoxDecoration(
              color: Colors.amber.shade700,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
            child: Center(
              child: Text(
                "Verificación datos \n   dueño vehiculo",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.sp,
                    color: Colors.deepPurple),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListView(
                children: [
                  Row(
                    children: [
                      Expanded(child: showNames(names)),
                    ],
                  ),
                  separador(),
                  Row(
                    children: [
                      Expanded(child: showLastNames(lastNames)),
                    ],
                  ),
                  separador(),
                  Row(
                    children: [
                      Expanded(child: showDni(dni)),
                    ],
                  ),
                  separador(),
                  Row(
                    children: [
                      Expanded(child: showEmail(email)),
                    ],
                  ),
                  separador(),
                  Row(
                    children: [
                      Expanded(child: showPhone(phone)),
                    ],
                  ),
                  separador(),
                  Column(
                    children: [
                      Text("Foto carnet \npor ambos lados",
                          style: TextStyle(
                              fontSize: 18.sp, fontWeight: FontWeight.bold)),
                      separador(),
                      Text("Lado a",
                          style: TextStyle(
                              fontSize: 18.sp, fontWeight: FontWeight.bold)),
                      Row(
                        children: [
                          Expanded(child: showPhotoCarnet(photoCarnetA)),
                        ],
                      ),
                      separador(),
                      Text("Lado b",
                          style: TextStyle(
                              fontSize: 18.sp, fontWeight: FontWeight.bold)),
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              width: 20.w,
                              child: CachedNetworkImage(
                                imageUrl: "$photoCarnetB",
                                placeholder: (context, url) => circulatorCustom(),
                                errorWidget: (context, url, error) => const Icon(Icons.error),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  separador(),
                  DropdownButtonHideUnderline(
                    child: DropdownButton2<String>(
                      isExpanded: true,
                      hint: const Row(
                        children: [
                          Icon(
                            Icons.list,
                            size: 16,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: Text(
                              'Selecciona un estado',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      items: items
                          .map((String item) => DropdownMenuItem<String>(
                                value: item,
                                child: Text(
                                  item,
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ))
                          .toList(),
                      value: selectedValue,
                      onChanged: (String? value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },


                      buttonStyleData: ButtonStyleData(
                        height: 50,
                        width: 160,
                        padding: const EdgeInsets.only(left: 14, right: 14),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          border: Border.all(
                            color: Colors.black26,
                          ),
                          color: Colors.deepPurple,
                        ),
                        elevation: 2,
                      ),
                      iconStyleData: const IconStyleData(
                        icon: Icon(
                          Icons.arrow_forward_ios_outlined,
                        ),
                        iconSize: 14,
                        iconEnabledColor: Colors.white,
                        iconDisabledColor: Colors.grey,
                      ),
                      dropdownStyleData: DropdownStyleData(
                        maxHeight: 200,
                        width: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          color: Colors.deepPurple,
                        ),
                        offset: const Offset(-20, 0),
                        scrollbarTheme: ScrollbarThemeData(
                          radius: const Radius.circular(40),
                          thickness: MaterialStateProperty.all<double>(6),
                          thumbVisibility:
                              MaterialStateProperty.all<bool>(true),
                        ),
                      ),
                      menuItemStyleData: const MenuItemStyleData(
                        height: 40,
                        padding: EdgeInsets.only(left: 14, right: 14),
                      ),
                    ),
                  ),
                  separador(),
                  selectedValue == "Rechazado"
                      ? TextFormField(
                          controller: controllerMotivo,
                          enabled: true,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                            ),
                            hintText: 'Ingrese motivo de rechazo',
                            hintStyle: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        )
                      : Container(),
                  separador(),
                  aprobarVerificacion(selectedValue.toString().toLowerCase(),
                      dni, context, controllerMotivo, photoCarnetA, photoCarnetB),
                  separador(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget showId(id) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'ID: $id',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showNames(names) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Nombre"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$names',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showLastNames(lastNames) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Apellido"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$lastNames',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showDni(dni) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Rut"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$dni',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showEmail(email) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Email"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$email',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPhone(phone) {
  return SizedBox(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Telefono"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$phone',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPhotoPerfil(perfil) {
  return SizedBox(
    width: 20.w,
    child: Image.network(perfil, width: 20.w),
  );
}

Widget showPhotoCarnet(photoCarnet) {
  return SizedBox(
    width: 20.w,
    child: CachedNetworkImage(
      imageUrl: "$photoCarnet",
      placeholder: (context, url) => circulatorCustom(),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    ),
  );
}

Widget showStatus(status) {
  return SizedBox(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'Aprobado para conducir: $status',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget aprobarVerificacion(controllerAprobacion, dni, context, motivo, photoCarnetA, photoCarnetB) {
  return ElevatedButton(
    style: const ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(Colors.deepPurple),
    ),
    onPressed: () async {
      if (controllerAprobacion == "aprobado" ||
          controllerAprobacion == "rechazado") {
        CachedNetworkImage.evictFromCache("$photoCarnetA");
        CachedNetworkImage.evictFromCache("$photoCarnetB");
        var response = await updateAprobacionDuenio(
            controllerAprobacion,
            dni,
            controllerAprobacion == "rechazado"
                ? motivo.text
                : 'cuaquier cosa');

        if (response["status_query"] == true) {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.success(
              message: "Datos actualizados correctamente",
            ),
          );
          Navigator.pop(context);
          Navigator.pop(context);
        } else {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message: "Usted ha ingresado una aprobación no valida !!!!!",
            ),
          );
        }
      } else {
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.error(
            message:
                "Datos ingresados incorrectos (EJ: 'aprobado' O 'desaprobado')",
          ),
        );
      }
    },
    child: const Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        "Verificar",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}
