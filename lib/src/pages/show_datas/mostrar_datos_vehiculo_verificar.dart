import 'dart:convert';
import 'package:admin_taxi_seguro/src/provider/duenio_vehiculo_providers/login_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../apis/api_auditor/update_aprobacion/update_aprobacion_vehiculo.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../../tools/loading.dart';
import '../../tools/separador.dart';

class MostrarDatosVehiculoVerificar extends StatefulWidget {
  const MostrarDatosVehiculoVerificar({super.key});

  @override
  State<MostrarDatosVehiculoVerificar> createState() =>
      _MostrarDatosVehiculoVerificarState();
}

class _MostrarDatosVehiculoVerificarState
    extends State<MostrarDatosVehiculoVerificar> {

  TextEditingController controllerAprobacion = TextEditingController();
  String? selectedValue;
  final List<String> items = ["Aprobado", "Rechazado"];

  TextEditingController controllerMotivo = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<AuditorProvider>(context);
    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);

    var patente = datas.getDatasVerificarOneVehiculo()["patente"];
    var marca = datas.getDatasVerificarOneVehiculo()["marca"];
    var modelo = datas.getDatasVerificarOneVehiculo()["modelo"];

    var revicionTecnica =
        jsonDecode(datas.getDatasVerificarOneVehiculo()["revicionTecnica"]);
    var fotoRevicionTecnica = revicionTecnica["foto"];
    var fechaVencimientoTecnica = revicionTecnica["fechavencimiento"];

    var color = datas.getDatasVerificarOneVehiculo()["color"];

    var certificadoInscripcionRNSTP = jsonDecode(
        datas.getDatasVerificarOneVehiculo()["certificadoInscripcionRNSTP"]);
    var patenteRNSTP = certificadoInscripcionRNSTP["patente"];
    var estadoVehiculoRNSTP = certificadoInscripcionRNSTP["estadovehicle"];

    var fotoCertificadoRNSTP =
        certificadoInscripcionRNSTP["fotoCertificadoRNSTP"];

    var fotoCertificadoInscripcion =
        datas.getDatasVerificarOneVehiculo()["certificadoInscripcion"];

    var permisocirculacion =
        jsonDecode(datas.getDatasVerificarOneVehiculo()["permisocirculacion"]);
    var fechaVencimientoPermisoCirculacion =
        permisocirculacion["fechaVencimientoPermiso"];
    var fotoPermisoCirculacion = permisocirculacion["fotoPermisoCirculacion"];

    var taximetro =
        jsonDecode(datas.getDatasVerificarOneVehiculo()["taximetro"]);
    var fotoTaximetro = taximetro["fotoCertificadoTaximetro"];

    var datasIndex = verificaDuenioProvider.getDatasVehiculoIndex["aprobacion"];

    return Scaffold(
      body: Container(
        width: 100.w,
        height: 100.h,
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 17.h,
              decoration: BoxDecoration(
                color: Colors.amber.shade700,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  "Verificación datos \n          vehiculo",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp,
                      color: Colors.deepPurple),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: ListView(
                  children: [
                    showPatente(patente),
                    separador(),
                    showMarca(marca),
                    separador(),
                    showModelo(modelo),
                    separador(),
                    showColor(color),
                    separador(),
                    Text(
                      "Rut dueño vehiculo",
                      style: TextStyle(
                          fontSize: 20.sp, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "${verificaDuenioProvider.getRutDuenio}",
                      style: TextStyle(
                          fontSize: 16.sp, fontWeight: FontWeight.bold),
                    ),
                    separador(),
                    Text(
                      "Foto revision tecnica",
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    Container(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoRevicionTecnica",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    showFechaVencimienntoRtecnica(fechaVencimientoTecnica),
                    separador(),
                    showPatenteRNSTP(patenteRNSTP),
                    separador(),
                    showEstadoVehiculoRNSTP(estadoVehiculoRNSTP),
                    separador(),
                    Text(
                      "Foto certificado RNSTP",
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    Container(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoCertificadoRNSTP",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Foto certificado inscripcion",
                          style: TextStyle(fontSize: 18.sp),
                        ),
                      ],
                    ),
                    Container(
                      width: 20.w,
                      child: fotoCertificadoInscripcion == null
                          ? Container(
                              height: 4.h,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color.fromARGB(255, 79, 180, 93),
                                  Color.fromARGB(255, 71, 146, 184),
                                ]),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              child: const Center(
                                  child: Text("Foto no encontrada")))
                          : CachedNetworkImage(
                              imageUrl: "$fotoCertificadoInscripcion",
                              placeholder: (context, url) => circulatorCustom(),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                    ),
                    separador(),
                    showFechaVencimientoPermisoCirculacion(
                        fechaVencimientoPermisoCirculacion),
                    separador(),
                    Text(
                      "Foto permiso circulación",
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    Container(
                      width: 20.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoPermisoCirculacion",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    const Text("Foto certificado taximetro"),
                    SizedBox(
                      width: 35.w,
                      child: CachedNetworkImage(
                        imageUrl: "$fotoTaximetro",
                        placeholder: (context, url) => circulatorCustom(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    separador(),
                    datasIndex == 'rechazado'
                        ? Container()
                        : DropdownButtonHideUnderline(
                            child: DropdownButton2<String>(
                              isExpanded: true,
                              hint: const Row(
                                children: [
                                  Icon(
                                    Icons.list,
                                    size: 16,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Selecciona un estado',
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                              items: items
                                  .map(
                                      (String item) => DropdownMenuItem<String>(
                                            value: item,
                                            child: Text(
                                              item,
                                              style: const TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ))
                                  .toList(),
                              value: selectedValue,
                              onChanged: (String? value) {
                                setState(() {
                                  selectedValue = value;
                                });
                              },
                              buttonStyleData: ButtonStyleData(
                                height: 50,
                                width: 160,
                                padding:
                                    const EdgeInsets.only(left: 14, right: 14),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(14),
                                  border: Border.all(),
                                  color: Colors.deepPurple,
                                ),
                                elevation: 2,
                              ),
                              iconStyleData: const IconStyleData(
                                icon: Icon(
                                  Icons.arrow_forward_ios_outlined,
                                ),
                                iconSize: 14,
                                iconEnabledColor: Colors.white,
                                iconDisabledColor: Colors.grey,
                              ),
                              dropdownStyleData: DropdownStyleData(
                                maxHeight: 200,
                                width: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(14),
                                  color: Colors.deepPurple,
                                ),
                                offset: const Offset(-20, 0),
                                scrollbarTheme: ScrollbarThemeData(
                                  radius: const Radius.circular(40),
                                  thickness:
                                      MaterialStateProperty.all<double>(6),
                                  thumbVisibility:
                                      MaterialStateProperty.all<bool>(true),
                                ),
                              ),
                              menuItemStyleData: const MenuItemStyleData(
                                height: 40,
                                padding: EdgeInsets.only(left: 14, right: 14),
                              ),
                            ),
                          ),
                    separador(),
                    selectedValue == "Rechazado"
                        ? TextFormField(
                            controller: controllerMotivo,
                            enabled: true,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              hintText: 'Ingrese motivo de rechazo',
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          )
                        : Container(),
                    separador(),
                    aprobarVerificacion(
                        controllerAprobacion,
                        patente,
                        context,
                        selectedValue.toString().toLowerCase(),
                        controllerMotivo,
                        fotoRevicionTecnica,
                        fotoCertificadoRNSTP,
                        fotoCertificadoInscripcion,
                        fotoPermisoCirculacion,
                        fotoTaximetro),
                    separador(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget showId(id) {
  return Container(
    width: 20.w,
    child: TextFormField(
      enabled: false,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        hintText: 'ID: $id',
        hintStyle: const TextStyle(
          color: Colors.black,
        ),
      ),
    ),
  );
}

Widget showPatente(patente) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Patente",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp),
            ),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$patente',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showMarca(marca) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Marca"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$marca',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showModelo(modelo) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Modelo"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$modelo',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showFotoRtecnica(fotoRtecnica) {
  return const Column(
    children: [],
  );
}

Widget showFechaVencimienntoRtecnica(fechaVencimientoRtecnica) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Fecha vencimiento revición tecnica"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$fechaVencimientoRtecnica',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showColor(color) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [Text("Color")],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$color',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showPatenteRNSTP(patenteRNSTP) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [Text("Patente RNSTP")],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$patenteRNSTP',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showEstadoVehiculoRNSTP(showEstadoVehiculoRNSTP) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Estado vehiculo RNSTP"),
          ],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$showEstadoVehiculoRNSTP',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showFotoCertificadoRNSTP(fotoCertificadoRNSTP) {
  return const Column(
    children: [],
  );
}

Widget showFechaVencimientoPermisoCirculacion(
    fechaVencimientoPermisoCirculacion) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [Text("Fecha vencimiento permiso circulación")],
        ),
        TextFormField(
          enabled: false,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$fechaVencimientoPermisoCirculacion',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget showFotoPermisoCirculacion(fotoPermisoCirculacion) {
  return const Column(
    children: [],
  );
}

Widget showAprobacion(aprobacion, controllerAprobacion) {
  return Container(
    width: 20.w,
    child: Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Estado aprobación vehiculo"),
          ],
        ),
        TextFormField(
          controller: controllerAprobacion,
          enabled: true,
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            hintText: '$aprobacion',
            hintStyle: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget aprobarVerificacion(
    controllerAprobacion,
    patente,
    context,
    selectedValue,
    motivo,
    fotoRtecnica,
    fotoRNSTP,
    fotoCertificadoInscripcion,
    fotoPermisoCirculacion,
    fotoTaximetro) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      color: Colors.deepPurple,
    ),
    child: ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.transparent),
        shadowColor: MaterialStateProperty.all(Colors.transparent),
      ),
      onPressed: () async {
        if (selectedValue == "aprobado" || selectedValue == "rechazado") {
          CachedNetworkImage.evictFromCache("$fotoRtecnica");
          CachedNetworkImage.evictFromCache("$fotoRNSTP");
          CachedNetworkImage.evictFromCache("$fotoCertificadoInscripcion");
          CachedNetworkImage.evictFromCache("$fotoPermisoCirculacion");
          CachedNetworkImage.evictFromCache("$fotoTaximetro");
          var response;
          if (selectedValue == "rechazado") {
            response = await updateAprobacionVehiculo(
                selectedValue, patente, motivo.text);
          } else {
            response = await updateAprobacionVehiculo(
                selectedValue, patente, 'motivo');
          }

          if (response["status_code"] == true) {
            showTopSnackBar(
              Overlay.of(context),
              const CustomSnackBar.success(
                message: "Datos actualizados correctamente",
              ),
            );
            Navigator.pop(context);
            Navigator.pop(context);
          } else {
            showTopSnackBar(
              Overlay.of(context),
              const CustomSnackBar.error(
                message: "Usted ha ingresado una aprobación no valida !!!!!",
              ),
            );
          }
        } else {
          showTopSnackBar(
            Overlay.of(context),
            const CustomSnackBar.error(
              message:
                  "Datos ingresados incorrectos (EJ: 'aprobado' O 'desaprobado')",
            ),
          );
        }
      },
      child: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          "Verificar",
          style: TextStyle(color: Colors.white),
        ),
      ),
    ),
  );
}
