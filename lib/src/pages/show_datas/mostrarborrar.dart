import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../provider/provi.dart';

class MostarBorrar extends StatelessWidget {
  const MostarBorrar({super.key});

  @override
  Widget build(BuildContext context) {
    var datas = Provider.of<BorrarProvider>(context);
    datas.setIsRutTrue = false;
    return Scaffold(
      body: CheckboxListTile(
        title: Text("jajaj"),
        controlAffinity: ListTileControlAffinity.platform,
        value: datas.getIsRutTrue,
        onChanged: (value) {
          datas.setIsRutTrue = value!;
        },
        activeColor: Colors.green,
        checkColor: Colors.black,
      ),
    );
  }
}
