verifiRut(controllerDni, rutValidate, validate2, isRutValid, formatRut,
    verificaDuenioProvider) {
  if (controllerDni.text.contains(".") && controllerDni.text.contains("-")) {
    String dad = rutValidate(controllerDni.text);

    controllerDni.text = dad.toString();

    validate2 = false;

    bool isveri = isRutValid(dad.toString());


    return isveri;
  } else {
    var formattedRut = formatRut(controllerDni.text);
    verificaDuenioProvider.setIsRutTrue = isRutValid(formattedRut);

    String dad = rutValidate(formattedRut);

    controllerDni.text = dad.toString();

    validate2 = false;

    bool isveri = isRutValid(dad.toString());
    if (isveri) {

      return isveri;
    }
  }
}
