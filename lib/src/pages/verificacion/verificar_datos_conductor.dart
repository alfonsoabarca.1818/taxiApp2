import 'package:admin_taxi_seguro/src/pages/show_datas/mostrar_datos_conductor_cambiar.dart';
import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../show_datas/mostrar_datos_conductor_verificar.dart';

class VerificarDatosConductor extends StatefulWidget {
  const VerificarDatosConductor({super.key});

  @override
  State<VerificarDatosConductor> createState() =>
      _VerificarDatosConductorState();
}

class _VerificarDatosConductorState extends State<VerificarDatosConductor> {

  Dio dio = Dio();

  @override
  void initState() {
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool ya = false;

  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificarConductorProvider = Provider.of<AuditorProvider>(context);

    List datas = verificarConductorProvider.getDatasVerificarConductor();


    List listaaprobacion = [];

    datas.forEach((element) {


      listaaprobacion.add(element["aprobacion"]);

    });



    return Scaffold(
      key: _scaffoldKey,
      body: isloading == true ? circulatorCustom() : Container(
        width: 100.w,
        height: 100.h,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 17.h,
              decoration: BoxDecoration(
                color: Colors.amber.shade700,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  "Verificar Datos \n     Conductor",
                  style: TextStyle(
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                      color: Colors.deepPurple),
                ),
              ),
            ),

            datas.isEmpty || listaaprobacion.contains("revision") != true
                ? Padding(
              padding: EdgeInsets.symmetric(vertical: 4.h),
              child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.deepPurple,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "No se han encontrado datos para revision",
                      style: TextStyle(color: Colors.white),
                    ),
                  )),
            ):Expanded(
              child: ListView.builder(
                itemCount: ya == true ? 1 : datas.length,
                itemBuilder: (context, index) {

                  if(datas[index]["aprobacion"] == 'aprobado' ||
                      datas[index]["aprobacion"] == 'rechasado' ||
                      datas[index]["aprobacion"] == 'rechazado'){

                      ya = true;

                  }

                  return datas[index]["aprobacion"] == 'aprobado' ||
                      datas[index]["aprobacion"] == 'rechasado' ||
                      datas[index]["aprobacion"] == 'rechazado' ? Container(): Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration:
                                BoxDecoration(color: Colors.amber.shade700),
                            child: ListTile(
                              onTap: () async {
                                setState(() {
                                  isloading = true;
                                });
                                verificarConductorProvider.setindex(index);
                                verificarConductorProvider
                                    .setDatasVerificarOneConductor(
                                        await getOneDatasConductor(
                                            "${datas[index]["dni"]}"));



                                Map<String, dynamic> datass = verificarConductorProvider.getDatasVerificarOneConductor() as Map<String, dynamic>;





                                setState(() {
                                  isloading = false;
                                });

                                if(datass["motivo"] == 'true'){

                                  Navigator.of(_scaffoldKey.currentContext!).push( MaterialPageRoute(
                                    builder: (context) =>
                                    const MostrarDatosConductorCambiar(),
                                  ),);
                                }else{

                                  Navigator.of(_scaffoldKey.currentContext!).push( MaterialPageRoute(
                                    builder: (context) =>
                                    const MostrarDatosConductorVerificar(),
                                  ),);
                                }





                              },
                              leading: Text(
                                "N°${index + 1}",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.deepPurple),
                              ),
                              title: Text(
                                "${datas[index]["name"]}",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.deepPurple),
                              ),
                              trailing: Text(
                                "${datas[index]["dni"]}",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.deepPurple),
                              ),
                            ),
                          ),
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
