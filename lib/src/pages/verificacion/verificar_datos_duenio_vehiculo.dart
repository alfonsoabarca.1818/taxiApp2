import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../../tools/url_base.dart';
import '../show_datas/mostrar_datos_duenio_verificar.dart';

class VerificarDatosDuenioVehiculo extends StatefulWidget {
  const VerificarDatosDuenioVehiculo({super.key});

  @override
  State<VerificarDatosDuenioVehiculo> createState() =>
      _VerificarDatosDuenioVehiculoState();
}

class _VerificarDatosDuenioVehiculoState
    extends State<VerificarDatosDuenioVehiculo> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool isbooleano = false;
  bool isnextloading = false;

  Dio dio = Dio();
  getBynaryAllImage(String rut) async {
    try {
      Response resp = await dio.post("$url/getImagesDuenio/$rut",
          options: Options(
            receiveTimeout: Duration(seconds: 5),
          ));

      if (resp.statusCode == 200) {
        var image1 = resp.data;

        Map<dynamic, dynamic> mapita1 = image1 as Map<dynamic, dynamic>;

        List imagebynarydynamic1 = mapita1["image1"];
        List imagebynarydynamic2 = mapita1["image2"];

        List<int> imageByteInt1 = imagebynarydynamic1.cast<int>();
        List<int> imageByteInt2 = imagebynarydynamic2.cast<int>();

        List<List> mislistas = [imageByteInt1, imageByteInt2];

        return mislistas;
      } else {
        showTopSnackBar(
          Overlay.of(context),
          CustomSnackBar.error(
            message:
                "No se ha podido iniciar session porfavor intenta nuevamente",
          ),
        );
      }
    } catch (e) {
      print("Ha ocurrido un error al obtener el bynario: $e");
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool ya = false;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);

    List datas = verificaDuenioProvider.getDatasVerificarDuenio();

    List listaaprobacion = [];

    datas.forEach((element) {
      listaaprobacion.add(element["aprobacion"]);
    });

    return Scaffold(
      key: _scaffoldKey,
      body: isnextloading == true
          ? Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text("Cargando.. "),
                      Text("esto puede tardar un momento"),
                      CircularProgressIndicator(),
                    ],
                  )
                ],
              ),
            )
          : Container(
              width: 100.w,
              height: 100.h,
              decoration: BoxDecoration(),
              child: Column(
                children: [
                  Container(
                    width: 100.w,
                    height: 17.h,
                    decoration: BoxDecoration(
                      color: Colors.amber.shade700,
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Verificación Datos\n   Dueño Vehiculo",
                        style: TextStyle(
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.deepPurple),
                      ),
                    ),
                  ),
                  //separador(),
                  datas.isEmpty || listaaprobacion.contains("revision") != true
                      ? Padding(
                          padding: EdgeInsets.symmetric(vertical: 4.h),
                          child: Container(
                              decoration: const BoxDecoration(
                                color: Colors.deepPurple,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  "No se han encontrado datos para revision",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        )
                      : Expanded(
                          child: ListView.builder(
                            itemCount: ya == true ? 1 : datas.length,
                            itemBuilder: (context, index) {
                              if (datas[index]["aprobacion"] == 'aprobado' ||
                                  datas[index]["aprobacion"] == 'rechasado' ||
                                  datas[index]["aprobacion"] == 'rechazado') {
                                ya = true;
                              }
                              return datas[index]["aprobacion"] == 'aprobado' ||
                                      datas[index]["aprobacion"] ==
                                          'rechasado' ||
                                      datas[index]["aprobacion"] == 'rechazado'
                                  ? Container()
                                  : Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)),
                                            color: Colors.amber.shade700),
                                        child: ListTile(
                                          onTap: () async {
                                            setState(() {
                                              isnextloading = true;
                                            });
                                            verificaDuenioProvider
                                                .setindex(index);
                                            verificaDuenioProvider
                                                .setDatasVerificarOneDuenio(
                                                    await getOneDatasDuenioVehicle(
                                                        "${datas[index]["dni"]}",
                                                        "${datas[index]["password"]}"));

                                            var dni = "${datas[index]["dni"]}";
                                            var pass =
                                                "${datas[index]["password"]}";



                                            setState(() {
                                              isnextloading = false;
                                            });
                                            Navigator.of(_scaffoldKey
                                                    .currentContext!)
                                                .push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    const MostrarDatosDuenioVerificar(),
                                              ),
                                            );

                                          },
                                          leading: Text(
                                            "N°${index + 1}",
                                            style: TextStyle(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepPurple),
                                          ),
                                          title: Text(
                                            "${datas[index]["names"]}",
                                            style: TextStyle(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepPurple),
                                          ),
                                          trailing: Text(
                                            "${datas[index]["dni"]}",
                                            style: TextStyle(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepPurple),
                                          ),
                                        ),
                                      ),
                                    );
                            },
                          ),
                        ),
                ],
              ),
            ),
    );
  }
}
