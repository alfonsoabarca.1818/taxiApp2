import 'package:admin_taxi_seguro/src/tools/loading.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../../apis/api_auditor/getApis.dart';
import '../../apis/api_auditor/getOneDataDuenioVehicle.dart';
import '../../apis/api_auditor/verificar/verificar_datos_vehiculo.dart';
import '../../provider/auditor_providers/auditor_provider.dart';
import '../show_datas/mostrar_datos_vehiculo_verificar.dart';

class VerificarDatosVehiculo extends StatefulWidget {
  const VerificarDatosVehiculo({super.key});

  @override
  State<VerificarDatosVehiculo> createState() => _VerificarDatosVehiculoState();
}

class _VerificarDatosVehiculoState extends State<VerificarDatosVehiculo> {
  @override
  void initState() {
    super.initState();
  }

  bool ya = false;
  bool isloading = false;

  bool isloa = false;

  Dio dio = Dio();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var verificaDuenioProvider = Provider.of<AuditorProvider>(context);

    var datas = verificaDuenioProvider.getDatasVerificarVehivulo();

    return Scaffold(
      key: _scaffoldKey,
      body: isloading == true
          ? circulatorCustom()
          : Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              width: 100.w,
              height: 100.h,
              child: Column(
                children: [
                  Container(
                    width: 100.w,
                    height: 17.h,
                    decoration: BoxDecoration(
                      color: Colors.amber.shade700,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Verificar Datos \n      Vehiculo",
                        style: TextStyle(
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.deepPurple),
                      ),
                    ),
                  ),

                  datas.isEmpty
                      ? Padding(
                          padding: EdgeInsets.symmetric(vertical: 4.h),
                          child: Container(
                              decoration: const BoxDecoration(
                                color: Colors.deepPurple,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  "No se han encontrado datos para revision",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        )
                      : Expanded(
                          child: ListView.builder(
                            itemCount: datas.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      color: Colors.amber.shade700),
                                  child: ListTile(
                                    onTap: () async {
                                      int ind = index - 1;

                                      setState(() {
                                        isloading = true;
                                      });

                                      verificaDuenioProvider
                                          .setDatasVerificarVehiculo(
                                              await getDatasAprobacionVehiculo());

                                      verificaDuenioProvider
                                              .setDatasVehiculoIndex =
                                          verificaDuenioProvider
                                                  .getDatasVerificarVehivulo()[
                                              index];

                                      verificaDuenioProvider.setindex(index);
                                      verificaDuenioProvider
                                          .setDatasVerificarOneVehiculo(
                                              await getOneDatasVehicle(
                                                  "${datas[index]["patente"]}"));

                                      var eldni = await getDniDuenio(
                                          datas[index]["patente"]);

                                      verificaDuenioProvider.setRutDuenio =
                                          eldni;

                                      setState(() {
                                        isloading = false;
                                      });

                                      Navigator.of(_scaffoldKey.currentContext!).push( MaterialPageRoute(
                                        builder: (context) =>
                                        const MostrarDatosVehiculoVerificar(),
                                      ),);


                                    },
                                    leading: Text(
                                      "N°${index}",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepPurple),
                                    ),
                                    title: Text(
                                      "${datas[index]["marca"]}",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepPurple),
                                    ),
                                    trailing: Text(
                                      "${datas[index]["patente"]}",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.deepPurple),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                ],
              ),
            ),
    );
  }
}
