import 'package:flutter/cupertino.dart';

class AuditorProvider with ChangeNotifier {
  String _nameAuditor = '';

  String get nameAuditor => _nameAuditor;

  set nameAuditor(String value) {
    _nameAuditor = value;
  }

  List DatasVeficarDuenio = [];
  List DatasVeficarVehiculo = [];
  List DatasVeficarConductor = [];
  int index = 0;
  int num = 0;
  Map datsOneDuenio = {};
  Map datsOneVehiculo = {};
  Map datsOneConductor = {};
  Map datasVehiculoIndex = {};

  bool isRutTrue = false;
  bool isEyes = false;
  bool isEyes2 = false;
  bool loading = false;

  String rutDuenio = "";
  String get getRutDuenio => this.rutDuenio;

  set setRutDuenio(String rutDuenio) => this.rutDuenio = rutDuenio;

  String rutAuditor = "";
  String get getRutAuditor => this.rutAuditor;

  set setRutAuditor(String rutAuditor) => this.rutAuditor = rutAuditor;

  bool get getLoading => this.loading;

  set setLoading(bool loading) => this.loading = loading;

  get getDatasVehiculoIndex => this.datasVehiculoIndex;

  set setDatasVehiculoIndex(datasVehiculoIndex) =>
      this.datasVehiculoIndex = datasVehiculoIndex;

  get getIsEyes2 => this.isEyes2;

  set setIsEyes2(isEyes2) => this.isEyes2 = isEyes2;

  bool get getIsEyes => this.isEyes;

  set setIsEyes(bool isEyes) => this.isEyes = isEyes;

  String aprobadoProvider = '';
  get getAprobadoProvider => this.aprobadoProvider;

  set setAprobadoProvider(aprobadoProvider) =>
      this.aprobadoProvider = aprobadoProvider;

  get getIsRutTrue => this.isRutTrue;
  set setIsRutTrue(bool isRutTrue) => this.isRutTrue = isRutTrue;

  setDatasVerificarDuenio(datas) {
    this.DatasVeficarDuenio = datas == null ? [] : datas;
  }

  getDatasVerificarDuenio() {
    return DatasVeficarDuenio;
  }

  setindex(index) {
    this.index = index;
  }

  getindex() {
    return index;
  }

  setNum(num) {
    this.num = num;
  }

  getNum() {
    return num;
  }

  setDatasVerificarOneDuenio(datas) {
    this.datsOneDuenio = datas;
  }

  getDatasVerificarOneDuenio() {
    return datsOneDuenio;
  }

  setDatasVerificarOneVehiculo(datas) {
    this.datsOneVehiculo = datas;
  }

  getDatasVerificarOneVehiculo() {
    return datsOneVehiculo;
  }

  setDatasVerificarOneConductor(datas) {
    this.datsOneConductor = datas;
  }

  getDatasVerificarOneConductor() {
    return datsOneConductor;
  }

  setDatasVerificarVehiculo(datas) {
    this.DatasVeficarDuenio = datas == null ? [] : datas;
  }

  getDatasVerificarVehivulo() {
    return DatasVeficarDuenio;
  }

  setDatasVerificarConductor(datas) {
    this.DatasVeficarConductor = datas == null ? [] : datas;
  }

  getDatasVerificarConductor() {
    return DatasVeficarConductor;
  }
}
