import 'package:flutter/material.dart';

class HomeProvider with ChangeNotifier {

  bool _iboolean2 = false;

  bool get iboolean2 => _iboolean2;

  set iboolean2(bool value) {
    _iboolean2 = value;
    notifyListeners();
  }

  bool _modificarvehiculo = false;

  bool get modificarvehiculo => _modificarvehiculo;

  set modificarvehiculo(bool value) {
    _modificarvehiculo = value;
    notifyListeners();
  }

  var patentes;
  bool _isbutton = false;

  bool get isbutton => _isbutton;

  set isbutton(bool value) {
    _isbutton = value;
    notifyListeners();
  }

  List lista = [];
  List<String> lista2 = [];
  Map dates = {};
  Map get getDates => this.dates;
  bool isTRue = false;
  bool get getIsTRue => this.isTRue;

  set setIsTRue(bool isTRue) => this.isTRue = isTRue;

  set setDates(Map dates) => this.dates = dates;
  List<String> get getLista2 => this.lista2;

  set setLista2(List<String> lista2) => this.lista2 = lista2;
  List datasNew = [];
  bool isTrue = false;
  int num = 0;
  get getNumm => this.num;

  set setNumm(num) => this.num = num;
  var laPatente = "";
  get getLaPatente => this.laPatente;

  set setLaPatente(laPatente) => this.laPatente = laPatente;

  get getIsTrue => this.isTrue;

  set setIsTrue(bool isTrue) => this.isTrue = isTrue;

  get getDatasNew => this.datasNew;

  set setDatasNew(List datasNew) => this.datasNew = datasNew;

  get getLista => this.lista;

  set setLista(List lista) => this.lista = lista;

  setPatentes(patentes) {
    this.patentes = patentes;
  }

  getPatentes() {
    return patentes;
  }
}
