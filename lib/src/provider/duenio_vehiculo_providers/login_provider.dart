import 'package:flutter/cupertino.dart';

class LoginProvider with ChangeNotifier {

  List<int> _milistaFotoRtecnica = [];

  List<int> get milistaFotoRtecnica => _milistaFotoRtecnica;

  set milistaFotoRtecnica(List<int> value) {
    _milistaFotoRtecnica = value;
  }

  List<int> _milistaFotoRnstp = [];
  List<int> _milistaFotoInscripcion = [];
  List<int> _milistaFotoCirculacion = [];
  List<int> _milistaFotoTaximetro = [];

  bool _isloadingverificarduenio = false;

  bool get isloadingverificarduenio => _isloadingverificarduenio;

  set isloadingverificarduenio(bool value) {
    _isloadingverificarduenio = value;
  }

  List<int> _milistaFotoPerfilConductor = [];
  List<int> _milistaFotoCarnetAconductor = [];

  List<int> get milistaFotoPerfilConductor => _milistaFotoPerfilConductor;

  set milistaFotoPerfilConductor(List<int> value) {
    _milistaFotoPerfilConductor = value;
  }

  List<int> _milistaFotoCarnetBconductor = [];
  List<int> _milistaFotoLicenciaAconductor = [];
  List<int> _milistaFotoLicenciaBconductor = [];
  List<int> _milistaFotoAntecedentesConductor = [];

  List<int> _milistalateralimagenduenio = [];

  List<int> get milistalateralimagenduenio => _milistalateralimagenduenio;

  set milistalateralimagenduenio(List<int> value) {
    _milistalateralimagenduenio = value;
  }

  List<int> _mibyte1 = [];

  List<int> get mibyte1 => _mibyte1;

  set mibyte1(List<int> value) {
    _mibyte1 = value;
  }

  List<int> _mibyte2 = [];

  List<int> get mibyte2 => _mibyte2;

  set mibyte2(List<int> value) {
    _mibyte2 = value;
  }

  bool _isloadingboolConductor = false;

  bool get isloadingboolConductor => _isloadingboolConductor;

  set isloadingboolConductor(bool value) {
    _isloadingboolConductor = value;
  }

  bool _isloadingbool = false;

  bool get isloadingbool => _isloadingbool;

  set isloadingbool(bool value) {
    _isloadingbool = value;
  }

  String _estadoVehiculoLogin = '';

  String get estadoVehiculoLogin => _estadoVehiculoLogin;

  set estadoVehiculoLogin(String value) {
    _estadoVehiculoLogin = value;
  }

  String _motivoVehiculoLogin = '';

  bool _eliminarPatente = false;

  bool get eliminarPatente => _eliminarPatente;

  set eliminarPatente(bool value) {
    _eliminarPatente = value;
  }

  String _patenteguardada = "";

  String get patenteguardada => _patenteguardada;

  set patenteguardada(String value) {
    _patenteguardada = value;
  }

  bool _guardarPatente = false;

  bool get guardarPatente => _guardarPatente;

  set guardarPatente(bool value) {
    _guardarPatente = value;
  }

  bool _loginConductor = false;

  bool get loginConductor => _loginConductor;

  set loginConductor(bool value) {
    _loginConductor = value;
  }

  bool _loginDuenio = false;

  bool get loginDuenio => _loginDuenio;

  set loginDuenio(bool value) {
    _loginDuenio = value;
  }

  List _listaPatentesprovider = [];

  List get listaPatentesprovider => _listaPatentesprovider;

  set listaPatentesprovider(List value) {
    _listaPatentesprovider = value;
  }

  bool _saveconductoresvehicle = false;

  bool get saveconductoresvehicle => _saveconductoresvehicle;

  set saveconductoresvehicle(bool value) {
    _saveconductoresvehicle = value;
  }

  bool _savepatentebool = false;

  bool get savepatentebool => _savepatentebool;

  set savepatentebool(bool value) {
    _savepatentebool = value;
  }

  var dni = {};
  int count = 0;
  int get getCount => this.count;

  set setCount(int count) => this.count = count;
  Map datasOneVehicle = {};
  Map datasOneDuenio = {};
  Map allDatasProvider = {};
  get getAllDatasProvider => this.allDatasProvider;

  set setAllDatasProvider(allDatasProvider) =>
      this.allDatasProvider = allDatasProvider;
  Map allDatasPatentes = {};
  List patentesObtenidasList = [];
  List newList = [];
  bool isloading = false;
  String rut = "";
  String password = "";
  String aprobacion = "";
  String motivo = "";
  String get getMotivo => this.motivo;

  set setMotivo(String motivo) => this.motivo = motivo;
  String get getAprobacion => this.aprobacion;

  set setAprobacion(String aprobacion) => this.aprobacion = aprobacion;
  get getRut => this.rut;

  set setRut(rut) => this.rut = rut;

  get getPassword => this.password;

  set setPassword(password) => this.password = password;
  bool get getIsloading => this.isloading;

  set setIsloading(bool isloading) => this.isloading = isloading;
  List get getNewList => this.newList;

  set setNewList(List newList) => this.newList = newList;
  get getPatentesObtenidasList => this.patentesObtenidasList;

  set setPatentesObtenidasList(patentesObtenidasList) =>
      this.patentesObtenidasList = patentesObtenidasList;
  List allPatentesAhora = [];
  List get getAllPatentesAhora => this.allPatentesAhora;

  set setAllPatentesAhora(List allPatentesAhora) =>
      this.allPatentesAhora = allPatentesAhora;
  Map get getAllDatasPatentes => this.allDatasPatentes;

  set setAllDatasPatentes(Map allDatasPatentes) =>
      this.allDatasPatentes = allDatasPatentes;
  String dni_duenio = "";
  List<dynamic> allRut = [];
  List<dynamic> get getAllRut => this.allRut;

  set setAllRut(List<dynamic> allRut) => this.allRut = allRut;

  set dniduenio(String value) => this.dni_duenio = value;
  get getdniduenio => this.dni_duenio;

  setDni(dni) {
    this.dni = dni;
  }

  setDatasVehicle(datasOneVehicle) {
    this.datasOneVehicle = datasOneVehicle;
  }

  getDni() {
    return dni;
  }

  getDatasVehicle() {
    return datasOneVehicle;
  }

  setDatasDuenio(datasOneDuenio) {
    this.datasOneDuenio = datasOneDuenio;
  }

  getDatasDuenio() {
    return datasOneDuenio;
  }

  String get motivoVehiculoLogin => _motivoVehiculoLogin;

  set motivoVehiculoLogin(String value) {
    _motivoVehiculoLogin = value;
  }

  List<int> get milistaFotoCarnetAconductor => _milistaFotoCarnetAconductor;

  set milistaFotoCarnetAconductor(List<int> value) {
    _milistaFotoCarnetAconductor = value;
  }

  List<int> get milistaFotoCarnetBconductor => _milistaFotoCarnetBconductor;

  set milistaFotoCarnetBconductor(List<int> value) {
    _milistaFotoCarnetBconductor = value;
  }

  List<int> get milistaFotoLicenciaAconductor => _milistaFotoLicenciaAconductor;

  set milistaFotoLicenciaAconductor(List<int> value) {
    _milistaFotoLicenciaAconductor = value;
  }

  List<int> get milistaFotoLicenciaBconductor => _milistaFotoLicenciaBconductor;

  set milistaFotoLicenciaBconductor(List<int> value) {
    _milistaFotoLicenciaBconductor = value;
  }

  List<int> get milistaFotoAntecedentesConductor =>
      _milistaFotoAntecedentesConductor;

  set milistaFotoAntecedentesConductor(List<int> value) {
    _milistaFotoAntecedentesConductor = value;
  }

  List<int> get milistaFotoRnstp => _milistaFotoRnstp;

  set milistaFotoRnstp(List<int> value) {
    _milistaFotoRnstp = value;
  }

  List<int> get milistaFotoInscripcion => _milistaFotoInscripcion;

  set milistaFotoInscripcion(List<int> value) {
    _milistaFotoInscripcion = value;
  }

  List<int> get milistaFotoCirculacion => _milistaFotoCirculacion;

  set milistaFotoCirculacion(List<int> value) {
    _milistaFotoCirculacion = value;
  }

  List<int> get milistaFotoTaximetro => _milistaFotoTaximetro;

  set milistaFotoTaximetro(List<int> value) {
    _milistaFotoTaximetro = value;
  }
}
