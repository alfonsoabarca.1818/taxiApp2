import 'package:flutter/material.dart';

class BorrarProvider with ChangeNotifier {
  bool isRutTrue = false;
  get getIsRutTrue => this.isRutTrue;

  set setIsRutTrue(bool isRutTrue) => this.isRutTrue = isRutTrue;
}
