import 'package:flutter/material.dart';

class ProviderPreference with ChangeNotifier {
  var rut = "";
  var pass = "";
  var sessions = 0;
  get getRut => this.rut;

  set setRut(rut) => this.rut = rut;

  get getPass => this.pass;

  set setPass(pass) => this.pass = pass;

  get getSessions => this.sessions;

  set setSessions(sessions) => this.sessions = sessions;
}
