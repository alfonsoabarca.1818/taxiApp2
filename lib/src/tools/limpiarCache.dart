import 'package:cached_network_image/cached_network_image.dart';

void clearCachedImages(archivo1, archivo2) {
  CachedNetworkImage.evictFromCache(archivo1);
  CachedNetworkImage.evictFromCache(archivo2);

}