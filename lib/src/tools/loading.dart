import 'package:flutter/material.dart';

Widget circulatorCustom() {
  return const Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Column(
                children: [
                  Text("Cargando "),

                  CircularProgressIndicator.adaptive(
                    backgroundColor: Colors.amber,
                    semanticsLabel: 'Cargando',
                  ),
                ],
              )

            ],
          ),
        ],
      ),
    ],
  );
}
