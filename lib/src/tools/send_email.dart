import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';

List<String> attachments = [];
bool isHTML = false;

final _recipientController = TextEditingController(
  text: 'taxiseguro407@gmail.com',
);

final _subjectController = TextEditingController(text: 'Ingrese Asunto');

final _bodyController = TextEditingController(
  text: 'Ingrese su problema',
);

Future<void> send(context) async {
  final Email email = Email(
    body: _bodyController.text,
    subject: _subjectController.text,
    recipients: [_recipientController.text],
    attachmentPaths: attachments,
    isHTML: isHTML,
  );

  String platformResponse;

  try {
    await FlutterEmailSender.send(email);
    platformResponse = 'success';
  } catch (error) {

    platformResponse = error.toString();
  }

  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(platformResponse),
    ),
  );
}
